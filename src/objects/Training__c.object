<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Training object to capture user trainings;</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Account_Email__c</fullName>
        <externalId>false</externalId>
        <label>Email</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account__c</fullName>
        <description>Account</description>
        <externalId>false</externalId>
        <inlineHelpText>Account</inlineHelpText>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Trainings</relationshipLabel>
        <relationshipName>Trainings</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Be_Accredited_Assessor__c</fullName>
        <description>Show Training Provider Register want to be Accreditor Accessor</description>
        <externalId>false</externalId>
        <formula>Account__r.PersonContact.Be_Accredited_Assessor__c</formula>
        <label>Be Accredited Assessor</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Email__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.PersonContact.Email</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Email</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Mobile__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.PersonContact.MobilePhone</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Mobile</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Person_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.FirstName + &apos; &apos;+  Account__r.LastName</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Person Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Phone__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.PersonContact.Phone</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Phone</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Request_Training__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Request Training provider to contact this user for training.</description>
        <externalId>false</externalId>
        <inlineHelpText>Request Training provider to contact this user for training.</inlineHelpText>
        <label>Request Training</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Training_Completed_Date__c</fullName>
        <description>Date on which the training has been completed.</description>
        <externalId>false</externalId>
        <inlineHelpText>Date on which the training has been completed.</inlineHelpText>
        <label>Accreditation Training Completed Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Training_Completed_Number__c</fullName>
        <externalId>false</externalId>
        <label>Training Completed_Number</label>
        <summaryFilterItems>
            <field>Assessor_Training_Module__c.Completed_Date__c</field>
            <operation>notEqual</operation>
            <value></value>
        </summaryFilterItems>
        <summaryForeignKey>Assessor_Training_Module__c.Training__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Training_Completed__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Tick if the training has been completed by the user.</description>
        <externalId>false</externalId>
        <inlineHelpText>Tick if the training has been completed by the user.</inlineHelpText>
        <label>Accreditation Training Completed?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Training_Courses__c</fullName>
        <description>Training courses for Accredited Assessor Certifications.</description>
        <externalId>false</externalId>
        <inlineHelpText>Training courses for Accredited Assessor Certifications.</inlineHelpText>
        <label>Training Courses</label>
        <picklist>
            <picklistValues>
                <fullName>Introduction to BOS</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>BAM</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Field skills</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Biodiversity Certification Method</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Accredited Assessors Code of Conduct</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Brokers Code of Conduct</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>BOS Administration</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Other</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Training_Modules_Number__c</fullName>
        <externalId>false</externalId>
        <label>Required Training Modules Number</label>
        <summaryForeignKey>Assessor_Training_Module__c.Training__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Training_Other__c</fullName>
        <description>If the Accreditor Accessor Select Other and Input the sentence, store these sentence.</description>
        <externalId>false</externalId>
        <label>Training-Other</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Willing_To_Travel_To_Training__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Willing To Travel To Training</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Training</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Person_Name__c</columns>
        <columns>Email__c</columns>
        <columns>Mobile__c</columns>
        <columns>Be_Accredited_Assessor__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>Request_Training</fullName>
        <columns>NAME</columns>
        <columns>Person_Name__c</columns>
        <columns>Mobile__c</columns>
        <columns>Phone__c</columns>
        <columns>Account_Email__c</columns>
        <columns>Training_Completed__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Training_Completed__c</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <label>Request Training</label>
    </listViews>
    <nameField>
        <displayFormat>TN-{0000}</displayFormat>
        <label>Training Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Trainings</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Person_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Account_Email__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Mobile__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Be_Accredited_Assessor__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Training_Completed__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Training_Completed_Date__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Person_Name__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Account_Email__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Mobile__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Be_Accredited_Assessor__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Training_Completed__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Training_Completed_Date__c</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Person_Name__c</searchFilterFields>
        <searchFilterFields>Account_Email__c</searchFilterFields>
        <searchFilterFields>Mobile__c</searchFilterFields>
        <searchFilterFields>Be_Accredited_Assessor__c</searchFilterFields>
        <searchFilterFields>Training_Completed__c</searchFilterFields>
        <searchFilterFields>Training_Completed_Date__c</searchFilterFields>
        <searchResultsAdditionalFields>Person_Name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Email__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Mobile__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Be_Accredited_Assessor__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Training_Completed__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CREATED_DATE</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Check_Completed_Date_is_not_empty</fullName>
        <active>true</active>
        <description>Make sure the Completed date is not empty when the Training Completed is ticked</description>
        <errorConditionFormula>(Training_Completed__c &amp;&amp; ISNULL(Training_Completed_Date__c)) || (!Training_Completed__c &amp;&amp; !ISNULL(Training_Completed_Date__c))</errorConditionFormula>
        <errorDisplayField>Training_Completed_Date__c</errorDisplayField>
        <errorMessage>Make sure the Accreditation Training Completed is ticked and Accreditation Training Completed Date is filled</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Training_Completed_Date</fullName>
        <active>true</active>
        <description>Training Completed Date</description>
        <errorConditionFormula>Training_Completed_Date__c &gt; Today()</errorConditionFormula>
        <errorDisplayField>Training_Completed_Date__c</errorDisplayField>
        <errorMessage>Accreditation Training Completed Date should be earlier or equal Today</errorMessage>
    </validationRules>
</CustomObject>

<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Credit Transaction</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Agreement_Number__c</fullName>
        <externalId>false</externalId>
        <formula>Source_Credit_Holding__r.Agreement__r.Case_Number__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Agreement Number</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Agreement__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Agreement</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>Only Agreement can be attached to Credit Transaction</errorMessage>
            <filterItems>
                <field>Case.RecordType.Name</field>
                <operation>equals</operation>
                <value>Agreement</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>Credit Transactions (Agreement)</relationshipLabel>
        <relationshipName>Agreemnet_Case</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Amount_deposited_into_the_Trust_Fund__c</fullName>
        <externalId>false</externalId>
        <label>Amount deposited into the Trust Fund</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Amount_paid_direct_to_the_seller__c</fullName>
        <description>Note: GST must also be paid to the seller both for the Total Fund deposit and any amount paid to the seller&quot;</description>
        <externalId>false</externalId>
        <label>Amount paid direct to the seller</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Available_Credits__c</fullName>
        <externalId>false</externalId>
        <label>Available Credits</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Case</description>
        <externalId>false</externalId>
        <inlineHelpText>Case</inlineHelpText>
        <label>Case</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>Credit Transactions</relationshipLabel>
        <relationshipName>Credit_Transactions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Credit_Id__c</fullName>
        <externalId>false</externalId>
        <label>Credit Id</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Credit_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Credits__r.Credit_Name__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Credit Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Credit_Status__c</fullName>
        <externalId>false</externalId>
        <label>Credit Status</label>
        <picklist>
            <picklistValues>
                <fullName>Pending</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Issued</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Canceled</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Credit_Type__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT( Credits__r.Credit_Type__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Credit Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Credits__c</fullName>
        <externalId>false</externalId>
        <label>Credits</label>
        <referenceTo>Assessed_Credit__c</referenceTo>
        <relationshipLabel>Credit Transactions (Sold)</relationshipLabel>
        <relationshipName>Credit_Transactions</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Number_To_Transfer_Retire__c</fullName>
        <externalId>false</externalId>
        <label>Number To Transfer/Retire</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Number_To_Transfer__c</fullName>
        <externalId>false</externalId>
        <label>Number To Transfer</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Percentage_of_the_Total_Fund_Deposit__c</fullName>
        <externalId>false</externalId>
        <label>Percentage of the Total Fund Deposit</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Price_per_credit__c</fullName>
        <externalId>false</externalId>
        <label>Price per credit ($)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Relevant_portion_of_Total_Fund_Deposit__c</fullName>
        <externalId>false</externalId>
        <label>Relevant portion of Total Fund Deposit</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Source_Credit_Holding__c</fullName>
        <externalId>false</externalId>
        <label>Source Credit Holding</label>
        <referenceTo>Credit_Holding__c</referenceTo>
        <relationshipLabel>Credit Transactions(Sold)</relationshipLabel>
        <relationshipName>Credit_Transactions</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Target_Credit_Holding__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Target Credit Holding - buyer details</description>
        <externalId>false</externalId>
        <label>Target Credit Holding</label>
        <referenceTo>Credit_Holding__c</referenceTo>
        <relationshipLabel>Credit Transactions (Bought)</relationshipLabel>
        <relationshipName>Target_Credit_Transactions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Target_Credit__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Target Credit</label>
        <referenceTo>Assessed_Credit__c</referenceTo>
        <relationshipLabel>Credit Transactions (Bought)</relationshipLabel>
        <relationshipName>Source_Credit_Transactions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Total_Fund_Deposit_in_Agreement__c</fullName>
        <externalId>false</externalId>
        <label>Total Fund Deposit in Agreement</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Total_Fund_Deposit_payment__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Please fill in below details if TFD payment is required.</inlineHelpText>
        <label>Total Fund Deposit payment</label>
        <picklist>
            <picklistValues>
                <fullName>A Total Fund Deposit payment is required.</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>The Total Fund Deposit has already been satisfied for agreement ID.</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>This is a re-sale of credits and therefore, no deposit is required.</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>There are no credits being retired without first having been sold.</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>There are credits being retired without first having been sold but the Total Fund Deposit has already been satisfied for BioBanking agreement ID</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>There are credits being retired without first having been transferred (sold) and a cheque for the relevant portion of the Total Fund Deposit is attached. Payment can only be made by cheque payable to ‘BioBanking Trust Fund’. </fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Total_number_of_credits_created_at_site__c</fullName>
        <externalId>false</externalId>
        <label>Total number of credits created at site</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Total_proceeds_from_the_sale__c</fullName>
        <externalId>false</externalId>
        <formula>Number_To_Transfer_Retire__c * Price_per_credit__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total proceeds from the sale</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Transaction_Status__c</fullName>
        <externalId>false</externalId>
        <label>Transaction Status</label>
        <picklist>
            <picklistValues>
                <fullName>Pending</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Submitted</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Completed</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Cancelled</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Transfer_Retire__c</fullName>
        <externalId>false</externalId>
        <label>Transfer/Retire</label>
        <picklist>
            <picklistValues>
                <fullName>Transfer</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Retire</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <label>Credit Transaction</label>
    <nameField>
        <displayFormat>CT-{000}</displayFormat>
        <label>Transaction ID</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Credit Transactions</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>

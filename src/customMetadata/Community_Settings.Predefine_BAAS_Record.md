<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Predefine BAAS Record</label>
    <protected>false</protected>
    <values>
        <field>BAAS_Admin_User_ID__c</field>
        <value xsi:type="xsd:string">0050k000000MxOW</value>
    </values>
    <values>
        <field>BAAS_Community_Profile__c</field>
        <value xsi:type="xsd:string">~ Accredited Assessor Community User</value>
    </values>
    <values>
        <field>BOAM_Community_Profile__c</field>
        <value xsi:type="xsd:string">~ BOAM Proponent Community User</value>
    </values>
    <values>
        <field>Bank_Account__c</field>
        <value xsi:type="xsd:string">203420</value>
    </values>
    <values>
        <field>Bank_BSB__c</field>
        <value xsi:type="xsd:string">032001</value>
    </values>
    <values>
        <field>Bank_Name__c</field>
        <value xsi:type="xsd:string">OEH Operating Account</value>
    </values>
    <values>
        <field>Beverage_Registration_Fee__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Cancel_URL__c</field>
        <value xsi:type="xsd:string">https://uat-bio-uat.cs57.force.com/baas/s/</value>
    </values>
    <values>
        <field>Certification_Fee__c</field>
        <value xsi:type="xsd:double">300.0</value>
    </values>
    <values>
        <field>Collection_Point_Application_Fee__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Collection_Point_Registration_Fee__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Community_Code__c</field>
        <value xsi:type="xsd:string">OEHBIO</value>
    </values>
    <values>
        <field>Container_Notification_Email_Template__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Course_Fee__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Default_Account_OwnerId__c</field>
        <value xsi:type="xsd:string">0050k000000MxOW</value>
    </values>
    <values>
        <field>Default_Start_Dt__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Expiry_Term_Maximum__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
    <values>
        <field>Expiry_Term_Minimum__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Home_URL__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NVRM_Community_Profile__c</field>
        <value xsi:type="xsd:string">~ NVRM Proponent Community User</value>
    </values>
    <values>
        <field>Notification_Email_Address__c</field>
        <value xsi:type="xsd:string">santhosh.lakshmanan@environment.nsw.gov.au</value>
    </values>
    <values>
        <field>Org_Wide_Email_Display_Name__c</field>
        <value xsi:type="xsd:string">DoNotReply</value>
    </values>
    <values>
        <field>Password__c</field>
        <value xsi:type="xsd:string">OEHBIO</value>
    </values>
    <values>
        <field>Payment_URL__c</field>
        <value xsi:type="xsd:string">https://quickweb.support.qvalent.com/OnlinePaymentServlet3</value>
    </values>
    <values>
        <field>Return_URL__c</field>
        <value xsi:type="xsd:string">https://uat-bio-uat.cs57.force.com/baas/BAASQWTransactionReturn</value>
    </values>
    <values>
        <field>Service_Exception_Email__c</field>
        <value xsi:type="xsd:string">santhosh.lakshmanan@environment.nsw.gov.au</value>
    </values>
    <values>
        <field>Service_Return_URL__c</field>
        <value xsi:type="xsd:string">https://uat-biouat.cs57.force.com/QuickWebPaymentNotification/services/apexrest/QWebTransactions</value>
    </values>
    <values>
        <field>Submission_Open_Day_End_Date__c</field>
        <value xsi:type="xsd:date">2017-08-28</value>
    </values>
    <values>
        <field>Submission_Open_Day_Start_Date__c</field>
        <value xsi:type="xsd:date">2017-08-06</value>
    </values>
    <values>
        <field>Subscription_Fees__c</field>
        <value xsi:type="xsd:double">200.0</value>
    </values>
    <values>
        <field>Supplier_Business_Code__c</field>
        <value xsi:type="xsd:string">BAAS</value>
    </values>
    <values>
        <field>Text_Value__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Token_TimeOut__c</field>
        <value xsi:type="xsd:double">30.0</value>
    </values>
    <values>
        <field>Token_URL__c</field>
        <value xsi:type="xsd:string">https://ws.support.qvalent.com/services/quickweb/CommunityTokenRequestServlet</value>
    </values>
    <values>
        <field>User_Name__c</field>
        <value xsi:type="xsd:string">OEHBIO</value>
    </values>
</CustomMetadata>

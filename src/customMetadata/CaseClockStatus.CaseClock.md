<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CaseClock</label>
    <protected>false</protected>
    <values>
        <field>ClockRunningStatus__c</field>
        <value xsi:type="xsd:string">Application Accepted;Category Explanation Report In Progress;Under Mapping Expert Review;Determination In Progress;Determination Sent For Approval;Determination Approved;Determination on Hold;Request Further Information;Peer review mapping team</value>
    </values>
    <values>
        <field>ClockStopStatus__c</field>
        <value xsi:type="xsd:string">New;Submitted;Application Rejected;Determination Sent To Applicant;Category Explanation Report Sent To Applicant;Closed</value>
    </values>
    <values>
        <field>DaysCountThresholdForClockRestart__c</field>
        <value xsi:type="xsd:double">25.0</value>
    </values>
</CustomMetadata>

<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>BAM_JWT_Key</label>
    <protected>false</protected>
    <values>
        <field>BAAS_Admin_User_ID__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>BAAS_Community_Profile__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>BOAM_Community_Profile__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Bank_Account__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Bank_BSB__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Bank_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Beverage_Registration_Fee__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Cancel_URL__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Certification_Fee__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Collection_Point_Application_Fee__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Collection_Point_Registration_Fee__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Community_Code__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Container_Notification_Email_Template__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Course_Fee__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Default_Account_OwnerId__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Default_Start_Dt__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Expiry_Term_Maximum__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Expiry_Term_Minimum__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Home_URL__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NVRM_Community_Profile__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Notification_Email_Address__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Org_Wide_Email_Display_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Password__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Payment_URL__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Return_URL__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Exception_Email__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Return_URL__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Submission_Open_Day_End_Date__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Submission_Open_Day_Start_Date__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Subscription_Fees__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Supplier_Business_Code__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Text_Value__c</field>
        <value xsi:type="xsd:string">sample key</value>
    </values>
    <values>
        <field>Token_TimeOut__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Token_URL__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>User_Name__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>

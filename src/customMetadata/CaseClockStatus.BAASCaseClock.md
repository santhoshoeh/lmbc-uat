<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>BAASCaseClock</label>
    <protected>false</protected>
    <values>
        <field>ClockRunningStatus__c</field>
        <value xsi:type="xsd:string">App Complete/Ready for Panel;Applicant Adds Additional Information;OEH Internal Decision Process</value>
    </values>
    <values>
        <field>ClockStopStatus__c</field>
        <value xsi:type="xsd:string">Panel Requests Additional Info;App Approved;App Approved and Fee Paid;Closed;App Not Approved</value>
    </values>
    <values>
        <field>DaysCountThresholdForClockRestart__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>

@isTest


private class TestCreditCardServiceREST {

    static testMethod void testCreditCardService() 
    {
        TestDataGenerator.createRecordSet(new TestDataGenerator.DefaultDummyRecordSet());
	   Account acct = [Select Id from Account where IsPersonAccount = true limit 1];
        Case cs = [Select Id from Case limit 1];
        //create transaction'
        Transaction__c trans = TestDataGenerator.createTransaction(acct.Id, 1000.00);
        trans.Reference_Number__c = 'BAAS999999';
        trans.Purchase_Item__c = 'Subscription Fee';
        trans.Case__c = cs.Id;
        insert trans;
		

	   list<CreditCardServiceREST.creditcardtransaction> ccTransfers = new list<CreditCardServiceREST.creditcardtransaction>();
	    CreditCardServiceREST.creditcardtransaction ccTransfer = new CreditCardServiceREST.creditcardtransaction();
	    ccTransfer.referencenumber = 'BAAS999999';
         ccTransfer.paymentamount = '1000.00';
		ccTransfer.receiptnumber = 'Test';
		ccTransfer.responsecode = '0';
		ccTransfer.responsedescription = '123444';
		ccTransfer.paymentdate = '01/01/2017';
		ccTransfer.summarycode = '0';
		ccTransfer.surchargeamount = '9.00';
		ccTransfer.bankcustomername = 'Test';
		ccTransfer.cardholdername = 'Test';
		ccTransfer.cardscheme = 'visa';
		ccTransfer.cardlast4digits = '1111';
		ccTransfer.sourceproduct = '111';
		ccTransfer.supplierbusinesscode = 'ABC';
		ccTransfers.add(ccTransfer);
		String jsonPaymentDetails = JSON.serialize(ccTransfers);
 	    Test.startTest();
	    RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();
		
	    req.requestURI = '/services/apexrest/creditcardtransactions';  
	    req.httpMethod = 'POST';
	    req.requestBody = Blob.valueof(jsonPaymentDetails);
	    RestContext.request = req;
	    RestContext.response = res;
	    CreditCardServiceREST.processCreditCard(ccTransfers);
	    system.debug(RestContext.response);
	    system.debug(res);


    }

}
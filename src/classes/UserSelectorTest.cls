@isTest
private class UserSelectorTest
{
	@isTest
	static void test_UserSelector() {
		User trainingProvider = TestDataGenerator.trainingProvider;
		
		System.runAs(TestDataGenerator.Admin) {
			UserSelector uSelector = new UserSelector();
			List<User> trainingProviderUsers = uSelector.getTrainingProviderUsers();
			system.debug('trainingProviderUsers ='+ trainingProviderUsers);
			system.assertNotEquals(trainingProviderUsers[0].Id, Null);
		}
	}
}
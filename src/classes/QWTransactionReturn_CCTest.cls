@isTest
private class QWTransactionReturn_CCTest
{
	static TestDataGenerator.DefaultDummyRecordSet rs = TestDataGenerator.createDefaultDummyRecordSet();
	
	@testSetup
	static void createTransactionRecord() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser) {
			Transaction__c trans = new Transaction__c();
			trans.Case__c = rs.cases[0].Id;
			trans.Purchase_Item__c = 'Subscription Fee';
			trans.Payment_Status__c = 'Pending Payment';
			trans.Reference_Number__c = 'baas098787';
			insert trans;
		}
		
	}

	@isTest
	static void testQWTransactionReturn_CC() {
		Test.setCurrentPage(Page.BAASQWTransactionReturn);
		ApexPages.currentPage().getParameters().put('customerFullName', rs.clients[0].FirstName+' '+rs.clients[0].LastName);
		ApexPages.currentPage().getParameters().put('surchargeAmount', '0.80');
		ApexPages.currentPage().getParameters().put('cardScheme', 'VISA');
		ApexPages.currentPage().getParameters().put('paymentReference','baas098787');
		ApexPages.currentPage().getParameters().put('createdDateTime', '02 Aug 2017 17:16:47');	
		ApexPages.currentPage().getParameters().put('settlementDate', '02 Aug 2017 17:16:47');
		ApexPages.currentPage().getParameters().put('expiryDateYear', '2019');
		ApexPages.currentPage().getParameters().put('paymentAmount', '200.80');
		ApexPages.currentPage().getParameters().put('summaryCode', '0');
		ApexPages.currentPage().getParameters().put('maskedCardNumber', '424242...242');
		ApexPages.currentPage().getParameters().put('responseCode', '08');
		ApexPages.currentPage().getParameters().put('responseDescription', 'Honour with identification');
		ApexPages.currentPage().getParameters().put('cardholderName', '4242424242424242');
		ApexPages.currentPage().getParameters().put('hmac', '857a315e22cc95edb6896eaf9cbbd5c4c208d57b670a42b64cb52b4251b63823');
		ApexPages.currentPage().getParameters().put('supplierBusinessCode', 'BAAS');
		ApexPages.currentPage().getParameters().put('expiryDateMonth', '01');
		ApexPages.currentPage().getParameters().put('receiptNumber', '1021629496');
		ApexPages.currentPage().getParameters().put('communityCode', 'OEHBIO');

		QWTransactionReturn_CC qwTran = new QWTransactionReturn_CC();

		system.assertNotEquals(qwTran.redirectUrl, null);
	}
}
/**
 * Created by junzhou on 14/6/17.
 */

@isTest
private class CommunitySettingsSelectorTest {

    @isTest
    static void test_getCommunitySettings(){
        //Community_Settings__mdt.QualifiedApiName = 'Predefine_BAAS_Record';
        //Community_Settings__mdt.BAAS_Admin_User_ID__c = Userinfo.getUserId();

        Test.startTest();
        Community_Settings__mdt cSettingmdt = CommunitySettingsSelector.getCommunitySettings();
        system.assertEquals(cSettingmdt.QualifiedApiName, 'Predefine_BAAS_Record');
        cSettingmdt = CommunitySettingsSelector.getPaymentSettings();
        system.assertEquals(cSettingmdt.QualifiedApiName, 'Predefine_BAAS_Record');
        cSettingmdt = CommunitySettingsSelector.getPaymentCalloutSettings();
        system.assertEquals(cSettingmdt.QualifiedApiName, 'Predefine_BAAS_Record');
        CaseClockStatus__mdt cClockSettingmdt = CommunitySettingsSelector.getBASSClockRunningSettings();
        system.assertEquals(cClockSettingmdt.QualifiedApiName, 'BAASCaseClock');
        Test.stopTest();
    }
}
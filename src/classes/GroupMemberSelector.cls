public with sharing class GroupMemberSelector {
	private static String GROUP_MEMBER_QUERY_STRING = 'SELECT Id, group.id, group.name, group.type FROM GroupMember';
	private static String GROUP_QUERY_STRING = 'Select Id, name from group';
	public GroupMemberSelector() {
		
	}

	public static List<GroupMember> getGroupMemberByGroupName(String groupName) {
		List<GroupMember> groups = new List<GroupMember>();
		String queryString =  GROUP_MEMBER_QUERY_STRING + ' WHERE Group.Name =: groupName';

		groups = Database.query(queryString);
		return groups;
	}

	public static Group getGroupByGroupName(String groupName) {
		Group existingGroup = new Group();
		String queryString = GROUP_QUERY_STRING+ ' WHERE Name =: groupName';
		existingGroup = Database.query(queryString);

		return existingGroup;
	}
}
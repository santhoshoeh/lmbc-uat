@isTest
public class AccountTriggerHandlerTest {
     
    @isTest
    static void testUpdateUser(){
        test.startTest();
        //create test account list
        Set<Id> accntList = new Set<Id>();
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        accntList.add(newAccount.Id);
        
        //get profile id
        //profile assessorProfile = [SELECT Id FROM Profile WHERE Name='Read Only'];//00e0l000000DgZaAAK
		Profile assessorProfile = [select id from profile where name='System Administrator'];

        User u = new User(alias = 'test123', email='test123@noemail.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = assessorProfile.Id, country='United States',
            timezonesidkey='America/Los_Angeles', username='test123@noemail.com');
        insert u;
        System.runas(u){
			//test the method
        	AccountTriggerHandler.updateUser(accntList, assessorProfile.Id);    
        }

        test.stopTest();
    }
    
    @isTest
    static void testupdateUserAsAssessor(){
        
        //create test account list
        List<Account> accntList = new List<Account>();
        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        newAccount.Accredited_Assessor__pc = true;
        newAccount.Assessor_Certification_Number__pc = 'Test12345';
        date myDate = system.today();
        newAccount.Valid_From__pc = myDate;
        newAccount.Valid_To__pc = myDate.addYears(1);//give a fture date
        accntList.add(newAccount);
        
        //test the method
        AccountTriggerHandler.updateUserAsAssessor(accntList, null);
        
    }
    
}
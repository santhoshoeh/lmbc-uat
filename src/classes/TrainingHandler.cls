/**
 * Created by junzhou on 5/6/17.
 */

public without sharing class TrainingHandler implements ITrigger {
    private static List<AccountShare> accountShares;
    private Group trainingProviderGroup;

    public TrainingHandler() {
    }

    public void bulkBefore() {}

    public void bulkAfter() {
        accountShares = new List<AccountShare>();
        trainingProviderGroup = GroupMemberSelector.getGroupByGroupName('BAAS Training Provider Group');
        system.debug('trainingProviderGroup ='+ trainingProviderGroup);
    }

    public void beforeInsert(SObject so) {
    }

    public void beforeUpdate(SObject oldSo, SObject so) {
    }

    public void beforeDelete(SObject so) {
    }

    public void afterInsert(SObject so) {
        Training__c training = (Training__c)so;
        AccountShare accountShare = new AccountShare();
        if(training.Request_Training__c && trainingProviderGroup!=null) {
            accountShare.accountid = training.Account__c;
            accountShare.UserOrGroupId = trainingProviderGroup.Id;
            accountShare.AccountAccessLevel = 'Edit';
            accountshare.OpportunityAccessLevel = 'None';
            accountShare.CaseAccessLevel = 'None';
            accountShares.add(accountShare);
        }
    }

    public void afterUpdate(SObject oldSo, SObject so) {
        //Training__c training = (Training__c)so;
        //AccountShare accountShare = new AccountShare();
        //if(training.Request_Training__c && trainingProUsers!=null && !trainingProUsers.isEmpty()) {
        //    accountShare.accountid = training.Account__c;
        //    accountShare.UserOrGroupId = trainingProUsers[0].Id;
        //    accountShare.AccountAccessLevel = 'Edit';
        //    accountshare.OpportunityAccessLevel = 'None';
        //    accountShare.CaseAccessLevel = 'None';
        //    accountShares.add(accountShare);
        //}
    }

    public void afterDelete(SObject so) {
    }

    public void afterUndelete(SObject so) {
    }

    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally() {
        if(accountShares != null && !accountShares.isEmpty()) {
            Savepoint sp = Database.setSavepoint();
            try{
                insert(accountShares);
            }catch(Exception e) {
                Database.rollback( sp );
            }
        }
    }
}
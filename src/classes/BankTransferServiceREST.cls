@RestResource(urlMapping='/banktransfertransactions/*')
global class BankTransferServiceREST 
{
    /*------------------------------------------------------------------------
    Author:        Santhosh
    Company:       System Partners
    Description:   This REST service allows OEH MW (Biztalk) to post bank transfer responses to Salesforce; 
    Test Class:    
    History
    <Date>      <Authors Name>     <Brief Description of Change>
    10-Jul-2017    Santhosh         Initial Build
    --------------------------------------------------------------------------*/
    
    //====================================================================================
    // *** REST POST *** : Method to create a new customer;
    //====================================================================================
    
    list<Application_Log__c> exceptionLogs = new list<Application_Log__c>();
    
    @HttpPost
    global static void processbanktransfer(banktransfertransaction[] banktransfertransactions)
    {    

       RestResponse res = RestContext.response;
       RestRequest req = RestContext.request;
       res = new RestResponse();
       ServiceResponse result = new ServiceResponse();
       result.transactionresponses = new List<transactionresponse>(); 
        
       list<Transaction__c> btTransactionsToUpdate = new list<Transaction__c>();
        
       map<String, banktransfertransaction> transactions = new map<String, banktransfertransaction>();
        
       try
       {
           res.statusCode = 200;

           for (banktransfertransaction bt : banktransfertransactions)
           {
               //assess if there is a reference number; if not log the message to the exception log;
               if (String.isnotBlank(bt.referencenumber) && bt.referencenumber.contains(SystemConstants.CDS_PROJECT))
               {
                   //assess if this transaction is in the system;
                   transactions.put(bt.referencenumber, bt);
                              
               }
               //send this back as an error to BT and also log this exception;
               else
               {
                   //a transaction reference number was not present or is invalid transaction reference number;
                   result.transactionresponses.add(new transactionresponse('FAILED','BAAS001','Not a valid CDS Transaction reference number',bt.referencenumber,'',''));
               }
           }
           
           //get the transactions;
           if (!transactions.isempty())
           {
               map<String, Transaction__c> transactionObjs = new map<String, Transaction__c>(queryTransactions(transactions.keySet()));
               
               for (banktransfertransaction bt : transactions.values())
               {
                   if (transactionObjs.containskey(bt.referencenumber))
                   {
                       //transaction record is available;Assess if the payment amount matches and if its not paid already;
                       Transaction__c tr = transactionObjs.get(bt.referencenumber);
                       if(tr.Bank_Payment_Status__c != 'Paid')
                       {
                           if (tr.Payment_Amount__c == Decimal.valueOf(bt.paymentamount))
                           {
                               //now set all values;
                               tr.Bank_Branch_Details__c = bt.bankbranchdetails;
                               tr.Account_Number__c = bt.accountnumber;
                               tr.Statement_Number__c = bt.statementnumber;
                               tr.Payment_Date__c = UtilityClass.convertStringToDate(bt.paymentdate);
                               tr.Bank_Description_1__c = bt.bankdescription1;
                               tr.Serial_Number__c = bt.serialnumber;
                               tr.Booking_Date__c = UtilityClass.convertStringToDate(bt.bookingdate);
                               tr.Bank_Description_2__c = bt.bankdescription2;
                               tr.Bank_Description_3__c = bt.bankdescription3;
                               tr.Bank_Description_4__c = bt.bankdescription4;
                               tr.Summary_Code__c = bt.transactioncode;
                               tr.Payment_Method__c = SystemConstants.CDS_BT_PAYMENT_METHOD;
                               tr.Bank_Payment_Status__c = SystemConstants.CDS_PAID_STATUS;
                               tr.Payment_Status__c = SystemConstants.CDS_PAID_STATUS;
                               tr.Bank_Payment_Status_Updated_Date__c = system.today();
                               tr.Receipt_Number__c = bt.serialnumber;
                               btTransactionsToUpdate.add(tr);
                               
                           }
                           else
                           {
                               //amount does not match; reject this;
                               result.transactionresponses.add(new transactionresponse('FAILED','BAAS002','Payment Amount in Salesforce does not match with the amount sent',bt.referencenumber,String.valueOf(tr.Payment_Amount__c),bt.paymentAmount));

                           }
                       }
                       else //already paid transaction. error;
                       {
                          result.transactionresponses.add(new transactionresponse('FAILED','BAAS003','Cannot update a paid transaction.',bt.referencenumber,'',''));

                       }
                       
                   }
                   else{
                       
                       //not a valid transaction, respond to the message;
                       result.transactionresponses.add(new transactionresponse('FAILED','BAAS004','A record matching the reference number was not found.',bt.referencenumber,'',''));
                   }
               }
           }
           
           if (!btTransactionsToUpdate.isEmpty())
           {
               //list<DatabaseSaveResult> updResult = new list<DatabaseSaveResult>
               list<Database.SaveResult> updResults = Database.update(btTransactionsToUpdate, false);
               
               for (integer i=0;i<updResults.size();i++)
               {
                   if (updResults[i].isSuccess())
                   {
                       result.transactionresponses.add(new transactionresponse('SUCCESS',null,null,btTransactionsToUpdate[i].Reference_Number__c,'',''));
                   }
                   else
                   {
                      result.transactionresponses.add(new transactionresponse('FAILED','BAAS005','Update Error: An error occurred when updating Salesforce. '+updResults[i].getErrors()[0].getMessage(),btTransactionsToUpdate[i].Reference_Number__c,'',''));                   
                   }
               }
                   
                   
           }
           
  
       }
       catch (exception ex)
       {
           Application_Log__c exLog = new Application_Log__c();

       }
       RestContext.response.addHeader('Content-Type', 'application/json');
       RestContext.response.responseBody = Blob.valueOf(UtilityClass.stripJsonNulls(JSON.serialize(result)));        
       //res.addHeader('Content-Type', 'application/json');
       //res.responseBody = Blob.valueOf(UtilityClass.stripJsonNulls(JSON.serialize(result)));     
    }
    
    
 



    //====================================================================================
    // Method to retrieve the transaction records;
    //====================================================================================     
    
    public static map<string, Transaction__c> queryTransactions (set<String> referenceIds)
    {
         map<string, Transaction__c> transactionObjs = new map<String, Transaction__c>(); 
         string transactionQuery = UtilityClass.getQueryString(UtilityClass.getFieldMap('Transaction__c'), null) + 'Transaction__c where Reference_Number__c IN :referenceIds';
         system.debug('CDS Project: '+ transactionquery);
                   
         //get the list of transactions based on the transaction reference number;
         for (Transaction__c tr : Database.query(transactionquery))
         {
             transactionObjs.put(tr.Reference_Number__c, tr);
         }
      return transactionObjs;
    }
    //====================================================================================
    // Method declaration for customer details;
    //====================================================================================

    global class banktransfertransaction
    {
        public String referencenumber;
        public String paymentamount;
        public String bankbranchdetails;
        public String accountnumber;
        public String statementnumber;
        public String paymentdate;
        public String bankdescription1;
        public String bankdescription2;
        public String bankdescription3;
        public String bankdescription4;
        public String bookingdate;
        public String serialnumber;
        public String transactioncode;
    }
    
    global class transactionresponse
    {
        public String status;
        public String errorcode;
        public String errordescription;
        public String referencenumber;
        public String sfamount;
        public String wpamount;
        
        public transactionresponse(String tstatus, String terrorcode, String terrordescription, String treferencenumber, String tsfamount, String twpamount)
        {
            this.status = tstatus;
            this.errorcode = terrorcode;
            this.errordescription = terrordescription;
            this.referencenumber = treferencenumber;
            this.sfamount = tsfamount;
            this.wpamount = twpamount;
        }
        
    }
    
    public class ServiceResponse
    {
        public list<transactionresponse> transactionresponses;
    }
}
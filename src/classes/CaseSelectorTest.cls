@isTest
private class CaseSelectorTest
{
	static TestDataGenerator.DefaultDummyRecordSet rs = TestDataGenerator.createDefaultDummyRecordSet();

	@isTest
	static void testCaseSelector() {
		Case caseRecord = CaseSelector.getCaseDetails(rs.cases[0].Id);
		system.assertNotEquals(caseRecord, null);

		caseRecord = CaseSelector.getCasePaymentDetails(rs.cases[0].Id);
		system.assertEquals(caseRecord.Subscription_fee_payment_status__c, 'Pending Payment');

		List<Case> cases = CaseSelector.getCasesPaymentDetails(new List<Id>{rs.cases[0].Id});
		system.assertEquals(cases.size(),1);
	}
}
public without sharing class CasePartyUtil{
    public static final String landholderRoleName;
    public static final String assessorRoleName;
    public static final String creditOwnerRoleName;
    public static final String creditBuyerRoleName;

    public static final String assessorRTDevName;
    public static final String landholderRTDevName;
    public static final String creditOwnerRTDevName;
    public static final String creditBuyerRTDevName;
    public static final String interestHolderRTDevName;

    public static ID assessorRTID;
    public static ID landholderRTID;
    public static ID creditOwnerRTID;
    public static ID creditBuyerRTID;
    public static ID interestHolderRTID;
    
    static{
        landholderRoleName = 'Landholder';
        assessorRoleName = 'Assessor';
        creditOwnerRoleName = 'Credit Owner';
        creditBuyerRoleName = 'Credit Buyer';

        assessorRTDevName = 'Assessor';
        landholderRTDevName = 'Individual_Landholder';
        creditOwnerRTDevName = 'Credit_Owner';
        creditBuyerRTDevName = 'Credit_Buyer';
        interestHolderRTDevName = 'Interest_Holders';
        
        map<String, ID> casePartyRecTypeIDMap = new map<String, ID>();
        for(RecordType rt : [SELECT ID, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Case_Party__c']){
            casePartyRecTypeIDMap.put(rt.DeveloperName, rt.ID);
        }

        assessorRTID = casePartyRecTypeIDMap.get(assessorRTDevName);
        landholderRTID = casePartyRecTypeIDMap.get(landholderRTDevName);
        creditOwnerRTID = casePartyRecTypeIDMap.get(creditOwnerRTDevName);
        creditBuyerRTID = casePartyRecTypeIDMap.get(creditBuyerRTDevName);
        interestHolderRTID = casePartyRecTypeIDMap.get(interestHolderRTDevName);
    }
}
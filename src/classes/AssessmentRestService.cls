@RestResource(urlMapping='/Assessment/*')
global with sharing class AssessmentRestService{
    global class requestElem{
        global String caseID;
        global String BAMStatus;
        global String version;
        global Assessed_Credit__c[] credits;
    }
    global class respElem{
        global String Status;
        global String Message;
    }
    global static final String caseNotFoundError = 'The caseID provided does not exist in the system';
    global static final String caseBAMStatusEmptyError = 'The BAM status cannot be empty';
    global static final String caseNotAssessmentError = 'Only assessment cases can be updated.';
    global static final String caseFinalizedError = 'This assessment has already been finalized and cannot be updated.';
    global static final String creditTypeNull = 'The credit type must be provided for all the credit records';
    global static final String speciesIDNullError = 'The species ID is mandatory for species credit';
    global static final String speciesScientificNameNullError = 'The scientific name is mandatory for species credit';
    global static final String ecosystemPCTRowIDNullError = 'The PCT Row ID must be provided for all the ecosystem records';
    global static final String invalidCreditType = 'The credit type value provided is invalid';
    global static final String creditCaseNull = 'The case ID must be provided for all the credit records';
    global static final String creditNumberNull = 'The number of assessed credits must be provided for all the credit records';
    global static final String UnknownError = 'An unkown error occurred. Please contact the system administrator';
    global static final String SuccessStatus = 'Success';
    global static final String ErrorStatus = 'Error';
    global static final String caseUpdateSuccessMessage = 'The case was updated successfully';
    global static final String SpeciesCreditType = 'Species';
    global static final String EcosystemCreditType = 'Ecosystem';

    @HttpPost
    global static respElem updateAssessmentStatus(requestElem req){
        RestResponse res = RestContext.response;
        res.statuscode = 400;
        respElem resp = new respElem();
        resp.Status = ErrorStatus;
        resp.Message = UnknownError;
        if(req != null){
            System.debug('request = '+req);
            System.debug('caseIDString = '+req.caseID);
            System.debug('caseStatus = '+req.BAMStatus);
            
            ID caseID = null;
            try{
                caseID = ID.ValueOf(req.caseID);
            }catch(Exception ex){
                system.debug('Exception ex = '+ex);
                resp.Message = ex.getMessage();
                return resp;
            }
            if(String.IsEmpty(req.BAMStatus)){
                resp.Message = caseBAMStatusEmptyError;
                return resp;
            }

            List<Case> caseList = [Select ID, Status, RecordTypeID, RecordType.Name, Related_Cases__c from Case where ID = :caseID];
            if(caseList.IsEmpty()){
                resp.Message = caseNotFoundError;
                return resp;
            }
            Case cs = caseList[0];
            cs.BAM_Assessment_Status__c = req.BAMStatus;
            
            // if not assessment - throw an error
            if(!CaseUtil.boamAssessmentRTIDs.contains(cs.RecordTypeID)){
                // if(cs.RecordType.Name == 'Assessment' || cs.RecordType.Name == 'Development Assessment'){
                resp.Message = caseNotAssessmentError;
                return resp;
            }
            // if status == finalized, throw error.
            if(cs.Related_Cases__c == null){
                resp.Message = caseNotAssessmentError;
                return resp;
            }
            list<Case> parents = [SELECT ID, (select ID FROM Child_Cases__r 
                                              WHERE RecordTypeID IN :CaseUtil.boamAppsAndAgreementRTIDs 
                                              AND Status != 'In-Progress' LIMIT 1) 
                                              FROM Case WHERE ID = :cs.Related_Cases__c];
            if(parents.IsEmpty()){
                resp.Message = caseNotAssessmentError;
                return resp;
            }
            Case parent = parents[0];
            if(!parent.Child_Cases__r.IsEmpty()){
                resp.Message = caseFinalizedError;
                return resp;
            }

            // parsing and preparing credits
            if(req.credits != null){
                set<String> creditRefExtIDs = new set<String>();
                map<String, Assessed_Credit__c> creditRefMap = new map<String, Assessed_Credit__c>();
                for(Assessed_Credit__c ac : req.credits){
                    if(ac.Credit_Type__c == null){
                        resp.Message = creditTypeNull;
                        return resp;
                    }else if(ac.Credit_Type__c == SpeciesCreditType){
                        if(ac.Species_ID__c == null){
                            resp.Message = speciesIDNullError;
                            return resp;
                        }else if(ac.Scientific_Name__c == null){
                            resp.Message = speciesScientificNameNullError;
                            return resp;
                        }
                    }else if(ac.Credit_Type__c == EcosystemCreditType && ac.PCT_Row_ID__c == null){
                        resp.Message = ecosystemPCTRowIDNullError;
                        return resp;
                    }

                    if(ac.Case__c == null){
                        resp.Message = creditCaseNull;
                        return resp;
                    }
                    if(ac.Number_of_Credits__c == null){
                        resp.Message = creditNumberNull;
                        return resp;
                    }else{
                        ac.Approved_Credits__c = ac.Number_of_Credits__c;
                        ac.Status__c = 'Assessed';
                    }
                }

                map<String, ID> creditRecTypeMap = new map<String, ID>();
                ID defaultRTID;
                for(Schema.RecordTypeInfo rtInfo : Schema.SObjectType.Assessed_Credit__c.getRecordTypeInfosByName().Values()){
                    creditRecTypeMap.put(rtInfo.getName(), rtInfo.getRecordTypeId());
                    if(rtInfo.isDefaultRecordTypeMapping()){
                        defaultRTID = rtInfo.getRecordTypeId();
                    }
                }
            }
            
            // Perform all DML
            Savepoint sp;
            try{
                sp = Database.setSavepoint();
                list<Assessed_Credit__c> existingCreds = [select id from Assessed_Credit__c where Case__c = :req.caseID];
                if(!existingCreds.IsEmpty())
                    delete existingCreds;
                if(req.credits != null)
                    insert req.credits; 
                update cs;
            }catch(Exception ex){
                if(sp != null)
                    Database.rollback(sp);
                system.debug('Exception ex = '+ex);
                resp.Message = ex.getMessage();
                return resp;
            }

            res.statuscode = 200;
            resp.Status = SuccessStatus;
            resp.Message = caseUpdateSuccessMessage;
        }
        return resp;
    }
}
public class CasePropertyTriggerHandler {

            
    /* This method is used to copy the Case party from parent case to child case whenver case is created
     * Case Party will be added to parent case only
     * Query the child cases and add case party record
     */ 
    public static void syncCasePropertyDetails(List<Properties__c> trgNew){
        system.debug(' entry point syncCasePropertyDetails');
        
        //get the parent case from case party
        Map<Id, Properties__c> caseIdMap = new Map<Id, Properties__c>();
        for(Properties__c thisElem : trgNew){
            caseIdMap.put(thisElem.Case__c, thisElem);
        }
        
        List<Properties__c> casePropertyList = new List<Properties__c>();
        
        //loop through the parent cases and get child details
        for(Case thisCase : [Select Id, Related_Cases__c 
                        		FROM Case 
                        		WHERE Related_Cases__c IN: caseIdMap.keySet()]){
                                    
            Properties__c newProperty = caseIdMap.get(thisCase.Related_Cases__c).clone();
            newProperty.Parent_Property__c = caseIdMap.get(thisCase.Related_Cases__c).Id;                        
			newProperty.Case__c = thisCase.Id;
			casePropertyList.add(newProperty);	            
        }
        
        //insert case parties
        if(casePropertyList != null){
            insert casePropertyList;
        }
    }
 
}
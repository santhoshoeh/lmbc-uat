@IsTest
public class AssessmentRestServiceTest{
    static Case cs;
    static Case parentCs;
    static Case_Party__c cp;
    static void loadData(){
        ID personAccRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

        Account acc1 = new Account(
            RecordTypeID=personAccRecTypeId,
            FirstName='FirstName ',
            LastName='LastName from Test Class',
            PersonMailingStreet='Mail Street',
            PersonMailingPostalCode='PostCode',
            PersonMailingCity='MailCity',
            PersonEmail='mailEmailFromClass@test.com',
            Email_Verified__pc = true,
            Business_Unit__pc = 'BOAM'
        );
        insert acc1;

        Contact cont = [select id, AccountID from Contact where AccountID = :acc1.ID];

        parentCs = new Case(
            Subject = 'Case from Test Class',
            RecordTypeID = CaseUtil.parentCaseRTID,
            ContactID = cont.ID
        );
        insert parentCs;
        
        cp = new Case_Party__c(
                        Case__c = parentCs.ID,
                        First_Name__c = acc1.FirstName,
                        Middle_Names__c = 'test',
                        Last_Name__c = acc1.LastName,
                        Person_Email__c = acc1.PersonEmail,
                        Assessor_accreditation_number__c = '1234',
                        RecordTypeID = CasePartyUtil.assessorRTID,
                        Role__c = CasePartyUtil.assessorRoleName
            );
        insert cp;

        cs = new Case(
            Subject = 'Case from Test Class',
            RecordTypeID = CaseUtil.parentCaseRTID,
            ContactID = cont.ID
        );
        insert cs; 
    }
    static testmethod void testService(){
        loadData();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
    
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
            AssessmentRestService.updateAssessmentStatus(null);
            
            AssessmentRestService.requestElem reqElem = new AssessmentRestService.requestElem();
            AssessmentRestService.updateAssessmentStatus(reqElem);
            
            reqElem.caseID = 'test String';
            AssessmentRestService.updateAssessmentStatus(reqElem);
            
            reqElem.caseID = cp.ID;
            AssessmentRestService.updateAssessmentStatus(reqElem);
            
            reqElem.BAMStatus = 'Some value';
            AssessmentRestService.updateAssessmentStatus(reqElem);
            
            reqElem.caseID = cs.ID;
            AssessmentRestService.updateAssessmentStatus(reqElem);
            
            cs.RecordTypeID = CaseUtil.assesmentRTID;
            update cs;
            AssessmentRestService.updateAssessmentStatus(reqElem);
            
            cs.Related_cases__c = parentCs.ID;
            update cs;
            AssessmentRestService.updateAssessmentStatus(reqElem);
            
            Assessed_Credit__c ac = new Assessed_Credit__c();
            reqElem.credits = new list<Assessed_Credit__c>();
            reqElem.credits.add(ac);
            AssessmentRestService.updateAssessmentStatus(reqElem);
            
            ac.Credit_Type__c = AssessmentRestService.SpeciesCreditType;
            AssessmentRestService.updateAssessmentStatus(reqElem);
            
            ac.Species_ID__c = 123;
            AssessmentRestService.updateAssessmentStatus(reqElem);
            
            ac.Credit_Type__c = AssessmentRestService.EcosystemCreditType;
            AssessmentRestService.updateAssessmentStatus(reqElem);
            
            ac.PCT_Row_ID__c = 123;
            AssessmentRestService.updateAssessmentStatus(reqElem);
            
            ac.Case__c = cs.ID;
            AssessmentRestService.updateAssessmentStatus(reqElem);
            
            ac.Number_of_Credits__c = 10;
            AssessmentRestService.updateAssessmentStatus(reqElem);

        Test.stopTest();
    }
}
/**
 * Created by junzhou on 20/6/17.
 */
@isTest
private class RecordTypeSelectorTest {
    @isTest static void recordTypeSelectorTest() {
        List<String> rtNames=new List<String>();
        rtNames.add('PersonAccount');
        rtNames.add('Business_Account');
        RecordTypeSelector rTSelector=new RecordTypeSelector();
        List<RecordType> recordTypes=rTSelector.fetchRecordTypesByName(rtNames);
        System.assertNotEquals(recordTypes,null);
        String rTName='Assessment';
        List<RecordType> recordTypes2=rTSelector.fetchRecordTypesByName(rTName);
        System.assertNotEquals(recordTypes2,null);
        List<RecordType> recordTypes3 = rTSelector.fetchRecordTypeList();
        System.assertNotEquals(recordTypes3,null);
        List<RecordType> recordTypes4 = rTSelector.fetchRecordTypesByObjectName('Account');
        System.assertNotEquals(recordTypes4,null);
    }
}
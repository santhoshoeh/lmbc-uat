/*------------------------------------------------------------
    Author:         Sakshi
    Company:        System Partners
    Description:    Class to hold all NVRM system constants and Utility Functions
    Inputs:         n/a
    Test Class:     
    History
    <Date>      <Authors Name>      <Brief Description of Change>
    20-Jul-17    Sakshi           Created
    ------------------------------------------------------------*/  
    
public with sharing class NVRM_Utility {
    // static variable to store request for further info status
    public static final String caseStatus_RFI = 'Request Further Information';
    public static final Integer CLOCK_PAUSE_WITHIN_DAY = 25;
    
    // static variable to store IndividualLandholder record type id
    public static final String CASEPARTY_LANDHOLDER_RECTYPE = [ SELECT ID,DeveloperName,IsActive,Name,SobjectType FROM RecordType WHERE DeveloperName = 'Individual_Landholder' AND IsActive=true AND SobjectType ='Case_Party__c'][0].Id;
    public static final String PERSON_ACC_RECORDTYPE = [  SELECT ID,DeveloperName,IsActive,Name,SobjectType FROM RecordType WHERE DeveloperName = 'PersonAccount' AND IsActive=true AND SobjectType ='Account'][0].Id;
    public static final String CER_RECORDTYPE = [  SELECT ID,DeveloperName,IsActive,Name,SobjectType FROM RecordType WHERE DeveloperName = 'CER' AND IsActive=true AND SobjectType ='Case'][0].Id;
    public static final String LIMR_RECORDTYPE = [  SELECT ID,DeveloperName,IsActive,Name,SobjectType FROM RecordType WHERE DeveloperName = 'LIMR' AND IsActive=true AND SobjectType ='Case'][0].Id;
    public static final String NVRM_ADMIN_PROFILE = [SELECT ID FROM Profile Where Name='NVRM_Admin'][0].Id;
    
    // Error Messages for NVRM_SubmitApplication
    public static final String APPLICANT_ERRORMESS = 'Please fill Appicant Type and Applicant Name( Contact )';
    public static final String LOT_DP_EMPTY = 'Please enter Lot/DP details manually from related list on right';
    public static final String FIRSTNAME_ERRORMESS = 'First name of Applicant is not filled';
    public static final String IDENTITY_ERRORMESS = 'Provide identity information for applicant';
    public static final String POSTAL_ADD_ERRORMESS = 'Enter all postal address details for applicant';
    public static final String ADD_ERRORMESS = 'Enter all postal address details or email address for applicant';
    public static final String PHONE_ERRORMESS = 'Provide atleast one contact number for the applicant';
    public static final String LANDHOLDER_ERRORMESS = 'Please enter landholder details for the case';
    public static final String LANDHOLDER_IDENTITY_ERRORMESS = 'Provide identity information for landholder';
    public static final String LANDHOLDER_POSTAL_ADD_ERRORMESS = 'Enter all postal address details for landholder';
    public static final String LANDHOLDER_PHONE_ERRORMESS = 'Provide atleast one contact number for landholder';
    public static final String LOTMETHOD = 'Select the method by which Lots/DPs will be provided i.e Manually entering or Attaching a Map';
    
    /*************************************************************      *********************************
    * @description Returns all case status for which clock days are supposed to be counted
    * @param 
    * @return the data from custom metadata
    * @example
    * @ Updated By : Sakshi Chauhan
    */
    public static Set<String> getCaseClockRunningStatus(){
        List<String> statusList = new List<String>();
        CaseClockStatus__mdt clockStatus = [ SELECT DeveloperName,MasterLabel,ClockRunningStatus__c,DaysCountThresholdForClockRestart__c FROM CaseClockStatus__mdt WHERE Label='CaseClock'][0];
        statusList = clockStatus.ClockRunningStatus__c.split(';');
        system.debug(statusList);
        Set<String> statusSet = new Set<String>(statusList);
        return statusSet;

    }
    
    /*************************************************************      *********************************
    * @description Returns all case NVRM Record Type Ids
    * @param 
    * @return set of all case NVRM Record Type Ids
    * @example
    * @ Updated By : Sakshi Chauhan
    */
     public static Set<Id> getNVRMCaseRecordType(){
        Map<Id,RecordType> recList = new Map<Id,RecordType>([SELECT Id FROM RecordType WHERE DeveloperName ='CER' OR DeveloperName ='LIMR']);
        Set<ID> recordTypeIDs = new Set<Id>();
        recordTypeIDs.addAll(recList.keyset());
        return recordTypeIDs;
     }
     
     /*************************************************************      *********************************
    * @description Returns record Type Id of Case object
    * @param 
    * @return record Type Id of Case object
    * @example
    * @ Updated By : Sakshi Chauhan
    */
     public static Id getCaseRecordTypeID( String recTypeName ){
        Id recTypeId = [ SELECT Id,name FROM RecordType WHERE DeveloperName = :recTypeName  AND SobjectType='Case' AND IsActive=true LIMIT 1].Id;
        return recTypeId;
     }
     
      /*************************************************************      *********************************
    * @description Returns Id of Profile
    * @param 
    * @return Id of System Admin Profile
    * @example
    * @ Updated By : Sakshi Chauhan
    */
     public static Id getProfileId( String profileName ){
        Id profileId = [ SELECT Id,Name FROM Profile WHERE Name= :profileName].Id;
        return profileId;
     }
    
    
}
public without sharing class CaseUtil{
    public static User currUser;
    public static AssignmentRule caseAssignRule;
    public static ID parentCaseRTID;
    public static ID devAppParentCaseRTID;
    public static ID assesmentRTID;
    public static ID devAssesmentRTID;
    public static ID stwAppRTID;
    public static ID bioCertAppRTID;
    public static ID activityAppRTID;
    public static ID approvalDetailsRTID;
    public static ID agreementRTID;
    public static ID transferCaseRTID;
    public static ID retireCaseRTID;
    public static ID purchaseCaseRTID;
    public static set<ID> boamAssessmentRTIDs;
    public static set<ID> boamApplicationRTIDs;
    public static set<ID> boamAppsAndAgreementRTIDs;
    public static set<ID> boamChildCasesRTIDs;
    public static set<ID> boamParentRTIDs;
    public static set<ID> allBoamRTIDs;

    public static ID accAssessorAppCaseRTID;
    
    public static ID mapReviewCaseRTID;
    
    public static final String parentCaseRTDevName;
    public static final String assesmentRTDevName;
    public static final String deveAssesmentRTDevName;
    public static final String stwAppRTDevName;
    public static final String bioCertAppRTDevName;
    public static final String activityAppRTDevName;
    public static final String approvalDetailsRTDevName;
    public static final String agreementRTDevName;
    public static final String transferCaseRTDevName;
    public static final String retireCaseRTDevName;
    public static final String purchaseCaseRTDevName;
    public static final String devAppParentCaseRTDevName;

    public static final String accAssessorAppCaseRTDevName;
    public static set<ID> boamAndBaasRTIDs;

    public static final String mapReviewCaseRTDevName;
    
    static{
        currUser = [Select id,ProfileID, Business_Unit__c from User where id =: UserInfo.getUserId() LIMIT 1];
        caseAssignRule = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];

        parentCaseRTDevName = 'Parent_Case';
        devAppParentCaseRTDevName = 'Application_Development_Parent';
        assesmentRTDevName = 'Assessment';
        deveAssesmentRTDevName = 'Development_Assessment';
        stwAppRTDevName = 'Stewardship_Application';
        bioCertAppRTDevName = 'Biocertification_Application';
        activityAppRTDevName = 'Activity_Application';
        approvalDetailsRTDevName = 'Development_Details';
        agreementRTDevName = 'Agreement';
        transferCaseRTDevName = 'Transfer_Credit';
        retireCaseRTDevName = 'Retire_Credits';
        purchaseCaseRTDevName = 'Purchased_Credit';
        
        accAssessorAppCaseRTDevName = 'Accreditor_Assessor_Application';
        
        mapReviewCaseRTDevName = 'LIMR';

        map<String, ID> caseRecTypeIDMap = new map<String, ID>();
        for(RecordType rt : [SELECT ID, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Case']){
            caseRecTypeIDMap.put(rt.DeveloperName, rt.ID);
        }

        devAppParentCaseRTID  = caseRecTypeIDMap.get(devAppParentCaseRTDevName);
        parentCaseRTID  = caseRecTypeIDMap.get(parentCaseRTDevName);
        assesmentRTID  = caseRecTypeIDMap.get(assesmentRTDevName);
        devAssesmentRTID  = caseRecTypeIDMap.get(deveAssesmentRTDevName);
        stwAppRTID  = caseRecTypeIDMap.get(stwAppRTDevName);
        bioCertAppRTID  = caseRecTypeIDMap.get(bioCertAppRTDevName);
        activityAppRTID  = caseRecTypeIDMap.get(activityAppRTDevName);
        approvalDetailsRTID  = caseRecTypeIDMap.get(approvalDetailsRTDevName);
        agreementRTID  = caseRecTypeIDMap.get(agreementRTDevName);
        transferCaseRTID  = caseRecTypeIDMap.get(transferCaseRTDevName);
        retireCaseRTID  = caseRecTypeIDMap.get(retireCaseRTDevName);
        purchaseCaseRTID  = caseRecTypeIDMap.get(purchaseCaseRTDevName);

        accAssessorAppCaseRTID  = caseRecTypeIDMap.get(accAssessorAppCaseRTDevName);
        
        mapReviewCaseRTID  = caseRecTypeIDMap.get(mapReviewCaseRTDevName);

        boamAssessmentRTIDs = new set<ID>();
        boamAssessmentRTIDs.add(assesmentRTID);
        boamAssessmentRTIDs.add(devAssesmentRTID);
        
        boamApplicationRTIDs = new set<ID>();
        if(stwAppRTID != null)
            boamApplicationRTIDs.add(stwAppRTID);
        if(bioCertAppRTID != null)
            boamApplicationRTIDs.add(bioCertAppRTID);
        if(activityAppRTID != null)
            boamApplicationRTIDs.add(activityAppRTID);
        if(approvalDetailsRTID != null)
            boamApplicationRTIDs.add(approvalDetailsRTID);
    
        boamAppsAndAgreementRTIDs = new set<ID>();
        if(boamApplicationRTIDs != null)
            boamAppsAndAgreementRTIDs.addAll(boamApplicationRTIDs);
        if(agreementRTID != null)
            boamAppsAndAgreementRTIDs.add(agreementRTID);

        boamChildCasesRTIDs = new set<ID>();
        boamChildCasesRTIDs.addAll(boamAssessmentRTIDs);
        boamChildCasesRTIDs.addAll(boamAppsAndAgreementRTIDs);
        
        boamParentRTIDs = new set<Id>();
        boamParentRTIDs.add(parentCaseRTID);
        boamParentRTIDs.add(devAppParentCaseRTID);
        
        allBoamRTIDs = new set<Id>();
        allBoamRTIDs.addAll(boamParentRTIDs);
        allBoamRTIDs.addAll(boamChildCasesRTIDs);
        allBoamRTIDs.add(transferCaseRTID);
        allBoamRTIDs.add(retireCaseRTID);
        allBoamRTIDs.add(purchaseCaseRTID);

        boamAndBaasRTIDs = new set<ID>();
        boamAndBaasRTIDs.addAll(allBoamRTIDs);
        boamAndBaasRTIDs.add(accAssessorAppCaseRTID);
    }
}
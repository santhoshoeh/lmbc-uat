@isTest
private class TransactionTriggerTest
{
	static TestDataGenerator.DefaultDummyRecordSet rs = TestDataGenerator.createDefaultDummyRecordSet();
	static Transaction__c trans;
	@testSetup
	static void createRequestParameters() {
		trans = new Transaction__c();
		trans.Case__c = rs.cases[0].Id;
		trans.Purchase_Item__c = 'Subscription Fee';
		trans.Payment_Status__c = 'Pending Payment';
		trans.Reference_Number__c = 'baas098787';
		insert trans;
	}

	@isTest
	static void testTransactionTrigger() {
		Transaction__c trans = TransactionSelector.getTransactionByReferenceNumber('baas098787');
		
		Test.startTest();
		trans.Payment_Status__c = 'Paid';
		trans.Purchase_Item__c = 'Subscription Fee';
		update trans;

		Case caseRecord = CaseSelector.getCasePaymentDetails(trans.Case__c);
		system.assertEquals(caseRecord.Subscription_fee_payment_status__c, 'Paid');

		
		Test.stopTest();
	}

	@isTest
	static void testTransactionBankPaymentTrigger() {
		Transaction__c trans = TransactionSelector.getTransactionByReferenceNumber('baas098787');
		
		Test.startTest();
		trans.Bank_Payment_Status__c = 'Paid';
		trans.Purchase_Item__c = 'Certification Fee';
		update trans;

		Case caseRecord = CaseSelector.getCasePaymentDetails(trans.Case__c);
		system.assertEquals(caseRecord.Certification_fee_payment_status__c, 'Paid');
		
		Test.stopTest();
	}

	@isTest
	static void testTransactionPaymentInitiatedTrigger() {
		Transaction__c trans = TransactionSelector.getTransactionByReferenceNumber('baas098787');
		
		Test.startTest();
		trans.Payment_Status__c = 'Payment Initiated';
		trans.Purchase_Item__c = 'Certification Fee';
		update trans;

		Case caseRecord = CaseSelector.getCasePaymentDetails(trans.Case__c);
		system.assertEquals(caseRecord.Certification_fee_payment_status__c, 'Payment Initiated');
		
		Test.stopTest();
	}

	@isTest
	static void testTransactionAppFeePaymentInitiatedTrigger() {
		Transaction__c trans = TransactionSelector.getTransactionByReferenceNumber('baas098787');
		
		Test.startTest();
		trans.Payment_Status__c = 'Payment Initiated';
		trans.Purchase_Item__c = 'Subscription Fee';
		update trans;

		Case caseRecord = CaseSelector.getCasePaymentDetails(trans.Case__c);
		system.assertEquals(caseRecord.Subscription_fee_payment_status__c, 'Payment Initiated');
		
		Test.stopTest();
	}

	//@isTest
	//static void testTransactionRenewalPaymentInitiatedTrigger() {
	//	Transaction__c trans = TransactionSelector.getTransactionByReferenceNumber('baas098787');
		
	//	Test.startTest();
	//	trans.Payment_Status__c = 'Renewal - Payment Initiated';
	//	trans.Purchase_Item__c = 'Certification Fee';
	//	update trans;

	//	Case caseRecord = CaseSelector.getCasePaymentDetails(trans.Case__c);
	//	system.assertEquals(caseRecord.Certification_fee_payment_status__c, 'Renewal - Payment Initiated');
		
	//	Test.stopTest();
	//}

	//@isTest
	//static void testTransactionAppFeeRenewalPaymentInitiatedTrigger() {
	//	Transaction__c trans = TransactionSelector.getTransactionByReferenceNumber('baas098787');
		
	//	Test.startTest();
	//	trans.Payment_Status__c = 'Renewal - Payment Initiated';
	//	trans.Purchase_Item__c = 'Subscription Fee';
	//	update trans;

	//	Case caseRecord = CaseSelector.getCasePaymentDetails(trans.Case__c);
	//	system.assertEquals(caseRecord.Subscription_fee_payment_status__c, 'Renewal - Payment Initiated');
		
	//	Test.stopTest();
	//}

	@isTest
	static void testTransactionCancelledTrigger() {
		Transaction__c trans = TransactionSelector.getTransactionByReferenceNumber('baas098787');
		
		Test.startTest();
		trans.Payment_Status__c = 'Cancelled';
		trans.Purchase_Item__c = 'Certification Fee';
		update trans;

		Case caseRecord = CaseSelector.getCasePaymentDetails(trans.Case__c);
		system.assertEquals(caseRecord.Certification_fee_payment_status__c, 'Pending Payment');
		
		Test.stopTest();
	}

	@isTest
	static void testTransactionAppFeeCancelledTrigger() {
		Transaction__c trans = TransactionSelector.getTransactionByReferenceNumber('baas098787');
		
		Test.startTest();
		trans.Payment_Status__c = 'Cancelled';
		trans.Purchase_Item__c = 'Subscription Fee';
		update trans;

		Case caseRecord = CaseSelector.getCasePaymentDetails(trans.Case__c);
		system.assertEquals(caseRecord.Subscription_fee_payment_status__c, 'Pending Payment');
		
		Test.stopTest();
	}
}
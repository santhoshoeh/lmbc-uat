/**
 * Created by junzhou on 6/7/17.
 */

public without sharing class PaymentProcesses {

    public class requestParameters {
        public Decimal finalAmount;
        public String customerName;
        public String customerId;
        public String referenceNumber;
        public String totalbevregs;
        public String paymentstatus;
        public Decimal totalApplfees;
        public Decimal totalApprovalfees;
        public String caseId;
        public String purchaseItem;
    }

    public String processQuickWebRequest(requestParameters reqs) {
        String redirectUrl = '';
        try {
            Community_Settings__mdt cMetaDataSettings = CommunitySettingsSelector.getPaymentCalloutSettings();
            if (cMetaDataSettings != null) {
                String tokenUrl         =    String.valueOf(cMetaDataSettings.Token_URL__c);
                String communityCode    =    String.valueOf(cMetaDataSettings.Community_Code__c);
                String paymentURL       =    String.valueOf(cMetaDataSettings.Payment_URL__c);
                Integer tokenTimeOut    =    Integer.valueOf(cMetaDataSettings.Token_TimeOut__c);
                String returnURL        =    String.valueOf(cMetaDataSettings.Return_URL__c);
                String cancelURL        =    String.valueOf(cMetaDataSettings.Cancel_URL__c);
                String serviceReturnURL =    String.valueOf(cMetaDataSettings.service_Return_URL__c);
                String userName         =    String.valueOf(cMetaDataSettings.User_Name__c);
                String password         =    String.valueOf(cMetaDataSettings.Password__c);
                String serviceReturnEmail =  String.valueOf(cMetaDataSettings.Service_Exception_Email__c);
                String supplierBusinessCode = String.valueOf(cMetaDataSettings.Supplier_Business_Code__c);
                String customerReferenceNumber = reqs.referenceNumber;
                HttpRequest req = new HttpRequest();
                HttpResponse res = new HttpResponse();
                Http http = new Http();
                //set the endpoint;
                req.setEndpoint('callout:QuickWeb_TokenURL');
                req.setMethod('POST');

                //construct the body for token;
                string postBody = 'username=' + userName +
                        '&password=' + password +
                        '&principalAmount=' + reqs.finalAmount +
                        '&supplierBusinessCode=' + supplierBusinessCode +
                        '&paymentReference=' + customerReferenceNumber +
                        '&customerFullName=' + reqs.customerName +
                        '&returnUrl=' + returnURL + 
                        '&cancelUrl=' + cancelURL +'case/' +reqs.caseId + 
                        '&serverReturnUrl=' + serviceReturnURL +
                        '&errorEmailToAddress=' + serviceReturnEmail;
                system.debug('postBody='+ postBody);
                req.setBody(postBody);
                req.setTimeout(tokenTimeOut*1000);
                res = http.send(req);

                //validate the status code;
                if(res.getStatusCode() == 200 && res.getBody() != null ) {
                    String token = res.getBody();
                    if (token.contains('token=')) {
                        token = token.remove('token=');
                    }
                    else {
                        redirectUrl = 'Error: Payment gateway did not respond with a valid token. Please try again later.';
                    }

                    redirectUrl = paymentURL + '?communityCode=' + communityCode + '&token=' + token;
                }
            }
            else {
                redirectUrl = 'Error: An internal configurable parameter has not been set. Please contact OEH Team.';
            }
            system.debug('redirectUrl ='+ redirectUrl);
            return redirectUrl;
        }

        catch(Exception e) {
            redirectUrl = 'Error: Payment gateway did not respond for a credit card payment. Please try again later.';
            return redirectUrl;
        }
    }

    //Method to set the beverage registration containers payment status and registration status ;

    public void updateCasePaymentStatus(list<Transaction__c> transactions) {
        if (!UtilityClass.isTransactionUpdateRecursive) {
            //get the related transactions;
            Map<Id, Case> caseIdMap = new Map<Id, Case>();
            List<Id> caseIds = new List<Id>();

            for (Transaction__c trans :transactions) {
                caseIds.add(trans.Case__c);
                system.debug('caseId ='+ trans.Case__c);
            }
            system.debug('caseIds ='+ caseIds);
            caseIdMap = new Map<id, Case>(CaseSelector.getCasesPaymentDetails(caseIds));

            for (Transaction__c trans :transactions) {
                Case caseRecord = caseIdMap.get(trans.Case__c);
                System.debug('Bank_Payment_Status__c = '+ trans.Bank_Payment_Status__c);
                System.debug('Purchase_Item__c = '+ trans.Purchase_Item__c);
                if(trans.Payment_Status__c == 'Paid' || trans.Bank_Payment_Status__c == 'Paid') {
                    if(trans.Purchase_Item__c == 'Subscription Fee') {
                        caseRecord.Subscription_fee_payment_status__c = 'Paid';
                    }else {
                        caseRecord.Certification_fee_payment_status__c = 'Paid';
                    }
                } else if(trans.Payment_Status__c == 'Payment Initiated' ) {
                    if(trans.Purchase_Item__c == 'Subscription Fee') {
                        caseRecord.Subscription_fee_payment_status__c = 'Payment Initiated';
                    }else {
                        caseRecord.Certification_fee_payment_status__c = 'Payment Initiated';
                    }
                } else if(trans.Payment_Status__c == 'Renewal - Payment Initiated') {
                    if(trans.Purchase_Item__c == 'Subscription Fee') {
                        caseRecord.Subscription_fee_payment_status__c = 'Renewal - Payment Initiated';
                    }else {
                        caseRecord.Certification_fee_payment_status__c = 'Renewal - Payment Initiated';
                    }
                } else if(trans.Payment_Status__c == 'Cancelled' || trans.Payment_Status__c == 'Not Processed') {
                    if(trans.Purchase_Item__c == 'Subscription Fee') {
                        caseRecord.Subscription_fee_payment_status__c = 'Pending Payment';
                    }else {
                        caseRecord.Certification_fee_payment_status__c = 'Pending Payment';
                    }
                }
            }
            UtilityClass.isTransactionUpdateRecursive = true;
            update caseIdMap.values();
        }
    }
    
    /*************************************************************      *********************************
    * @description process paid transactions , 
    * @param transaction records
    * @return 
    * @ Created By : Sakshi Chauhan ( 14th aug 2017 )
    */
    public void processInvoices(list<Transaction__c> paidTransactions){
        Set<ID> paidTransactionsID = new Set<Id>();
        for( Transaction__c tranVar: paidTransactions){
            paidTransactionsID.add( tranVar.Id );
        }
        PaymentProcesses.processInvoicesFuture( paidTransactionsID );
    }
    
    /*************************************************************      *********************************
    * @description process paid transactions , send email to customers along with Invoice PDF attachment
    * add attachment to transaction object
    * @param transaction records
    * @return 
    * @ Created By : Sakshi Chauhan ( 14th aug 2017 )
    */
    @future(CallOut=true)
    public static void processInvoicesFuture(Set<Id> paidTransactionsId ){
        Id customerInvoiceEmailTemplateId = UtilityClass.getEmailTemplateId('BAAS_Send_Invoice_To_Customer');
        //Id cdsOrdEmailAddressId = UtilityClass.getOrgWideEmailAddress('DoNotReplyCDS');   
        if( customerInvoiceEmailTemplateId != null){
            List<Messaging.SingleEmailMessage> emails = new list<Messaging.SingleEmailMessage>();
            List<Attachment> attachmentsList = new list<Attachment>();
              //query for the transactions;
              for (Transaction__c trans : [Select Id, Is_Container_Transaction__c, Reference_Number__c, Created_By_User_Contact_Id__c, Bank_Payment_Status__c, CreatedBy.Email from 
                                            Transaction__c where Id in :paidTransactionsId and Bank_Payment_Status__c = 'Paid']) {
                 Messaging.SingleEmailMessage mail = EmailNotificationsHelper.sendSingleEmail( customerInvoiceEmailTemplateId, trans.CreatedBy.Email,null, null, trans.Created_By_User_Contact_Id__c, null, true);

                 PageReference pdf = new PageReference('/apex/BAAS_InvoicePDF');
                 pdf.getParameters().put('id',trans.Id);     
                 // Get the PDF Content
                  Blob Body;
                  if (!test.isRunningTest())
                      body = pdf.getContent();
                  else
                      body = blob.valueOf('TEST CONTENT');
                  // Attach the output to the Parent Record
                  Attachment attachmentRec = new Attachment();
                  attachmentRec.ParentId = trans.Id;
                  attachmentRec.ContentType = 'application/pdf';
                  attachmentRec.Body = body;
                  attachmentRec.Name = trans.Reference_Number__c + ' ' + system.today() + '.pdf';
                  attachmentsList.add(attachmentRec);

                  Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
                  attach.body = body;
                  attach.setFileName(trans.Reference_Number__c + 'Invoice.pdf');
                  attach.setInline(false);
                  List<Messaging.EmailFileAttachment> emailAttachments = new List<Messaging.EmailFileAttachment>();
                  emailAttachments.add(attach);
                  mail.setFileAttachments(emailAttachments);
                  emails.add(mail);                         
                                                
              }
              List<Messaging.Sendemailresult> emailResult = new List<Messaging.Sendemailresult>();

              system.debug('***** emails is *****'+emails);
              system.debug('***** attachmentsList is *****'+attachmentsList);
              if(!test.isRunningTest()) {
                if (!emails.isEmpty()) emailResult = Messaging.sendEmail(emails);
                if (!attachmentsList.isEmpty()) insert attachmentsList;
              }
        }
        
    }
}
public without sharing class SystemConstants {
  
    /*------------------------------------------------------------
    Author:         Santhosh
    Company:        System Partners
    Description:    Class to hold all system constants
    Inputs:         n/a
    Test Class:     
    History
    <Date>      <Authors Name>      <Brief Description of Change>
    20-Jan-17    Santhosh           Created
    ------------------------------------------------------------*/      
     
     

     public static final String CDS_PROJECT                        = 'BAAS';
     public static final String CDS_CC_PAYMENT_METHOD              = 'Credit Card';
     public static final String CDS_BT_PAYMENT_METHOD              = 'Bank Transfer';
     public static final String CDS_PAID_STATUS                    = 'Paid';
     public static final String CDS_REFERENCE_NUMBER_ERROR         = 'Not a valid Transaction reference number';
     public static final String CDS_REFERENCE_NUMBER_ERROR_CODE    = 'BAAS001';
     public static final String CDS_AMOUNT_MISMATCH                = 'Payment Amount in Salesforce does not match the amount sent';
     public static final String CDS_PAYMENT_NOT_PROCESSED          = 'Not Processed';

     public static final Id DEFAULT_ACCOUNT_OWNERID                = '005N0000003ILFu';

     //Object names;
     public static final String COMMUNITY_METADATA_TYPE          = 'CustomerCommunitySettings__mdt';
    
     

}
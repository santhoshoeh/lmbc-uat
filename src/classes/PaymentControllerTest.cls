@isTest
private class PaymentControllerTest
{
	static TestDataGenerator.DefaultDummyRecordSet rs = TestDataGenerator.createDefaultDummyRecordSet();
	@isTest
	static void testGetPaymentOption() {
		PaymentController.PaymentOption pOption = PaymentController.getPaymentOption((rs.cases)[0].Id);
		system.assertNotEquals(pOption, null);
	}

	@isTest
	static void testProcessPayment() {
		String paymentJSONString = '{\"caseId\":\"'+(rs.cases)[0].Id+'\",\"approvalFee\":\"200.00\",\"businessUnit\":\"BAAS\",\"paymentMethod\":\"Credit Card\",\"paymentItem\":\"Subscription Fee\"}';
		String BankTransferPaymentJSONString = '{\"caseId\":\"'+(rs.cases)[0].Id+'\",\"approvalFee\":\"200.00\",\"businessUnit\":\"BAAS\",\"paymentMethod\":\"Bank Transfer\",\"paymentItem\":\"Subscription Fee\"}';
		
		System.runAs(TestDataGenerator.accreditedAssessor) {
			Test.startTest();
	        String retURL = PaymentController.processPayment(paymentJSONString);
	        System.assertNotEquals(retURL, null);
		
	        String BanksTransferretURL = PaymentController.processPayment(BankTransferPaymentJSONString);
	        System.assertNotEquals(BanksTransferretURL, null);
			Test.stopTest();
		}
	}
}
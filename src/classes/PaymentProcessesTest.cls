@isTest
private class PaymentProcessesTest
{
	static TestDataGenerator.DefaultDummyRecordSet rs = TestDataGenerator.createDefaultDummyRecordSet();
	static Transaction__c trans;
	@testSetup
	static void createRequestParameters() {
		trans = new Transaction__c();
		trans.Case__c = rs.cases[0].Id;
		trans.Purchase_Item__c = 'Subscription Fee';
		trans.Payment_Status__c = 'Pending Payment';
		trans.Reference_Number__c = 'baas098787';
		insert trans;

		
	}
	@isTest
	static void testProcessQuickWebRequest() {
		PaymentProcesses.requestParameters pProcess = new PaymentProcesses.requestParameters();
		pProcess.referenceNumber = 'baas098787';
		pProcess.finalAmount = 200.00;
		pProcess.customerName = rs.clients[0].FirstName + ' '+ rs.clients[0].LastName;
		pProcess.caseId  = rs.cases[0].Id;

		PaymentProcesses paymentProcess = new PaymentProcesses();
		String retURL = paymentProcess.processQuickWebRequest(pProcess);

		system.assertNotEquals(retURL, null);
	}

	@isTest
	static void testProcessInvoicesFuture() {
		trans = new Transaction__c();
		trans.Case__c = rs.cases[0].Id;
		trans.Purchase_Item__c = 'Subscription Fee';
		trans.Payment_Status__c = 'Paid';
		trans.Bank_Payment_Status__c = 'Paid';
		trans.Reference_Number__c = 'baas098788';
		insert trans;

		Set<Id> tranIds = new Set<Id>{trans.Id};
		PaymentProcesses.processInvoicesFuture(tranIds);
	}
}
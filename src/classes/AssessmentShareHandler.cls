/*------------------------------------------------------------
    Author:         Suraj
    Description:    Controller for Sharing Case records with Accredited Assessor
    Inputs:         n/a
    Test Class:     AssessmentShareHandlerTest
    History
    <Date>          <Authors Name>      <Brief Description of Change>   
    14-Jun-17       Suraj               Created
    ------------------------------------------------------------*/  

public without sharing class AssessmentShareHandler  {

    //local vars and collections
    private set<Id> caseToDelSet = new set<Id>();
    private List<caseShareWrapper> casesToShare = new List<caseShareWrapper>();

    
    //this method manupulates the case sharing based on Accredit Assessor
    public void shareCaseRecords(Map <Id, Case> oldCaseMap, List <Case> caseList) {
        
        for(Case thisCase : caseList){
            
            //if update event and Accredit Accessor has changed/deleted
            //1. Delete the old share
            //2. Create share for new Accredit Assessor
            if(Trigger.isUpdate && 
               oldCaseMap.get(thisCase.Id).Accredited_Assessor__c  != thisCase.Accredited_Assessor__c ){
               
               //add case for deleting existing share
               caseToDelSet.add(thisCase.Id);    
               
               //add case for creating New share if Accreditor is changed
               if(thisCase.Accredited_Assessor__c != null){
                casesToShare.add(new caseShareWrapper(thisCase.Accredited_Assessor__c, thisCase));                      
               }
                
            }
        
            //if Insert event and Accredit Accessor is provided, create share for Accredit Assessor    
            if(Trigger.isInsert && thisCase.Accredited_Assessor__c != null){
                //add case for creating New share if Accreditor is provided
                    casesToShare.add(new caseShareWrapper(thisCase.Accredited_Assessor__c, thisCase));                      
            }
        }
        
        //at this stage we will have list of shares to delete or create
        
        // DML to delete shares
        if (!caseToDelSet.isEmpty()){
            system.debug('deleting shares ' + caseToDelSet);
            delete [SELECT id FROM CaseShare 
                    WHERE caseId IN :caseToDelSet 
                    AND RowCause = 'Manual'];
        }
        
        //insert shares
        insertCaseShare();
          
    }
    
    
    // this method is used to insert the share records
    private void insertCaseShare(){
        List<CaseShare> caseShareList = new  List<CaseShare>();
        
        if(!casesToShare.isEmpty()){
            
            //loop through the list and create shares       
            for(caseShareWrapper thisElem : casesToShare){
                CaseShare newCaseShare = new CaseShare(); 
                newCaseShare.CaseId  = thisElem.caseRec.Id;
                newCaseShare.UserOrGroupId = thisElem.userId;
                newCaseShare.CaseAccessLevel = 'Edit';
                
                caseShareList.add(newCaseShare);
            }
            
            
            system.debug('caseShareList  to insert ' + caseShareList);
            //insert shares
            insert caseShareList;
        }
        
    }
    
    
    //case share wrapper
    private class caseShareWrapper{
        private Id userId{get; set;}  
        private Case caseRec{get; set;} 
        
        private caseShareWrapper(Id userId, Case caseRec){
            this.userId = userId;
            this.caseRec = caseRec;
        }
    }
}
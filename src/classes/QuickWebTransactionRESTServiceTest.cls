@isTest
private class QuickWebTransactionRESTServiceTest
{	static TestDataGenerator.DefaultDummyRecordSet rs = TestDataGenerator.createDefaultDummyRecordSet();
	
	static String bodyString  = '<PaymentResponse><receiptNumber>178858302</receiptNumber><communityCode>ACOMPANY</communityCode><supplierBusinessCode>ACOMPANY</supplierBusinessCode><paymentReference>baas098787</paymentReference>'+
			'<paymentAmount>200.00</paymentAmount><surchargeAmount>2.80</surchargeAmount><cardScheme>VISA</cardScheme><settlementDate>20120620</settlementDate><createdDateTime>20 Jun 2012 15:04</createdDateTime>'+
			'<responseCode>00</responseCode><responseDescription>Lost card</responseDescription><summaryCode>0</summaryCode><successFlag>true</successFlag></PaymentResponse>';
//   
	@testSetup
	static void createTransactionRecord() {
		System.runAs(TestDataGenerator.Admin) {
			Transaction__c trans = new Transaction__c();
			trans.Case__c = rs.cases[0].Id;
			trans.Purchase_Item__c = 'Subscription Fee';
			trans.Payment_Status__c = 'Pending Payment';
			trans.Reference_Number__c = 'baas098787';
			insert trans;
		}
	}

	@isTest
	static void testCreateTransaction() {
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = '/services/apexrest/QWebTransactions';  
	    req.httpMethod = 'POST';
	    req.requestBody = Blob.valueof(bodyString);
	    RestContext.request = req;
	    RestContext.response = res;
	    QuickWebTransactionRESTService.createTransaction();
	    system.debug(RestContext.response);
	    system.debug(res);
	}

	@isTest
	static void testcreateTransactionFromXML() {
		Transaction__c trans = new Transaction__c();

		trans = QuickWebTransactionRESTService.createTransactionFromXML(bodyString, trans);
		system.assertNotEquals(trans, null);
	}

	@isTest 
	static void testCreateTransactionFromParameters() {
		RestRequest rRequest = new RestRequest();
		rRequest.params.put('receiptNumber', '178858302');
		rRequest.params.put('customerReferenceNumber', 'baas098787');
		rRequest.params.put('paymentAmount', '200.00');
		rRequest.params.put('surchargeAmount', '2.80');
		rRequest.params.put('settlementDate', '20170620');
		rRequest.params.put('cardScheme', 'VISA');
		rRequest.params.put('responseCode', '00');
		rRequest.params.put('responseDescription', 'Lost card');
		rRequest.params.put('summaryCode', '0');

		Transaction__c trans = new Transaction__c();

		trans = QuickWebTransactionRESTService.createTransactionFromParameters(rRequest, trans);
		system.assertNotEquals(trans, null);
	}
}
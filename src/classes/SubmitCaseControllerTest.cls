@isTest
public class SubmitCaseControllerTest {

    static TestDataGenerator.DefaultDummyRecordSet rs = TestDataGenerator.createDefaultDummyRecordSet();

    public static id caseId;
    //insert test data
    static{
       Case testCase = new Case();
       testCase.Status = 'New'; 
       testCase.RecordTypeId = CaseUtil.parentCaseRTID; 
       insert testCase; 
        
       caseId = testCase.id; 
    }    
   
   @isTest
    static void testValidateTransfer() {
       
       Case testCase = new Case();
       testCase.Status = 'New'; 
       testCase.RecordTypeId = CaseUtil.transferCaseRTID; 
       insert testCase;  
        
       //add property
       Properties__c newProp = new Properties__c(); 
       newProp.Name = 'Test Prop';
       newProp.Case__c = testCase.Id; 
       newProp.City__c = 'Test'; 
       newProp.PostCode__c = 'Test';
       newProp.State__c = 'Test';
       newProp.Street__c = '123';
       insert newProp; 
        
       String testRetCase = SubmitCaseController.validateAssessment(testCase.Id);
       system.assert(testRetCase.equals('Please add Credit Owner and Credit Buyer in Case Parties.'));
        
    }         
    
    @isTest
    static void testValidateTransferNull() {
       
       Case testCase = new Case();
       testCase.Status = 'New'; 
       testCase.RecordTypeId = CaseUtil.transferCaseRTID; 
       insert testCase;  
        
       //add property
       Properties__c newProp = new Properties__c(); 
       newProp.Name = 'Test Prop';
       newProp.Case__c = testCase.Id; 
       newProp.City__c = 'Test'; 
       newProp.PostCode__c = 'Test';
       newProp.State__c = 'Test';
       newProp.Street__c = '123';
       insert newProp; 
        
       //add party 
       Case_Party__c newParty = new Case_Party__c(); 
       newParty.RecordTypeId  = CasePartyUtil.creditOwnerRTID;
       newParty.First_Name__c = 'Test';
       newParty.Person_Email__c = 'Test@test.com';
       newParty.Case__c = testCase.Id; 
       insert newParty; 
        
       String testRetCase = SubmitCaseController.validateAssessment(testCase.Id);
       system.assert(testRetCase.equals('Please add a Credit Buyer in Case Parties.'));
        
    }         
        
    @isTest
    static void testGetCaseDet() {
       Case testCase = new Case();
       testCase.Status = 'New'; 
       testCase.RecordTypeId = CaseUtil.retireCaseRTID; 
       insert testCase;   
        
       Case testRetCase = SubmitCaseController.getCaseDetails(testCase.Id);
       system.assert(testCase != null);
        
    }    
        
    @isTest
    static void testSubmitCase() {
        test.startTest();
        String status = SubmitCaseController.submitCaseMethod(caseId);
        List<Case> testCase = [SELECT id, Status FROM Case WHERE ID =: caseId LIMIT 1];
        system.assert(status != null);
        system.assert(testCase.size() == 1);
        system.assert(status.equalsIgnoreCase('Success'));//success response from method
        system.debug('testCase.get(0).Status ' + testCase.get(0).Status);
        system.assert(testCase.get(0).Status == 'Submitted');// case status should change to Submitted
        test.stopTest();
    }
    
    @isTest
    static void testDoInit() {
        test.startTest();
        system.debug('caseId ='+ caseId);
        Case testCase = SubmitCaseController.getCaseDetails(caseId);
        system.assert(testCase != null);
        system.assert(testCase.status.equalsIgnoreCase('New'));
        test.stopTest();
    }
    
        
    
     @isTest
    static void testSubmitCaseNullCase() {
        test.startTest();
        String status = SubmitCaseController.submitCaseMethod(null);
        system.assert(status != null);
        system.assert(status.equalsIgnoreCase('Case Id is Null'));//success response from method
        test.stopTest();
    }

    @isTest
    static void testGetSubmissionPeriod() {
        Map<String, Date> BAASSubmitCaseMap = SubmitCaseController.getSubmissionPeriod();
        system.assertNotEquals(BAASSubmitCaseMap, null);
    }
    
    @isTest
    static void testGetBAASCaseDetails() {
        Case BAASCase = SubmitCaseController.getBAASCaseDetails(rs.cases[0].Id);
        system.assertNotEquals(BAASCase, null);
    }

    @isTest
    static void testSubmitBAASCaseMethod() {
        String responseMessage = SubmitCaseController.submitBAASCaseMethod(rs.cases[0].Id);
        system.assertEquals(responseMessage, 'Success');
        
        Case responseMessage2 = SubmitCaseController.getApplicationCaseDet(rs.cases[0].Id);
        system.assert(responseMessage2 != null);
        
        Case responseMessage3 = SubmitCaseController.getApplicationCaseDetails(rs.cases[0].Id);
        system.assert(responseMessage2 != null);
        
        String responseMessage4 = SubmitCaseController.approveCaseMethod(rs.cases[0].Id);
        system.assertEquals(responseMessage4, 'Success');
        
        String responseMessage5 = SubmitCaseController.isssueCaseMethod(rs.cases[0].Id);
        system.assertEquals(responseMessage5, 'Success');
        
        String responseMessage6 = SubmitCaseController.validateAssessment(rs.cases[0].Id);
        system.assert(responseMessage5 != null);
        
        Case resCase = SubmitCaseController.validateApproveCase(rs.cases[0].Id);
        system.assert(resCase != null);
        
    }
}
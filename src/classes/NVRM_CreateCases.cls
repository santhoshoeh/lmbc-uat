/**
* @author Sakshi Chauhan
* @date 19 June 2017

* @description Class used for global action for community to create case
*/
public class NVRM_CreateCases{
    
    /*************************************************************   
    * @description Lightning method that return contact id of the community user
    * @param User Id
    * @param 
    * @return String (contact id )
    
    @AuraEnabled
    public static String getUserContactId( Id userId ){
        User userObj = [ SELECT Id,ContactId FROM User WHERE Id =: userId ];
        return userObj.ContactId;
    }*/
    
    /*************************************************************   
    * @description Lightning method that return case record
    * @param case Id
    * @param 
    * @return Case Object
    */
    @AuraEnabled
    public static Case getCaseDetails( Id caseId ){
        Case caseObj = [ SELECT Status,ContactId,Business_Unit__c,Applicant_Type__c,Origin,Subject,Description  FROM Case WHERE Id =: caseId ];
        system.debug(caseObj);
        return caseObj;
    }
    
    /*************************************************************   
    * @description Lightning method that return case record with clock details
    * @param case Id
    * @param 
    * @return Case Object
    */
    @AuraEnabled
    public static String getCaseClockDetails( Id caseId ){
        if( caseId == null)
            return null;
        try{
            Case caseObj = [ SELECT Status,ContactId ,Total_clock_days__c, Days_Left__c  FROM Case WHERE Id =: caseId ];
            Set<String> clockRunningStatus = NVRM_Utility.getCaseClockRunningStatus();
            List<LIMR_Clock_Stop_History__c> clockStopHistory = new List<LIMR_Clock_Stop_History__c>();
            clockStopHistory = [ SELECT New_Status__c,Old_Status__c,Clock_Day__c,CaseId__c,EndDate__c,Number_of_Days__c FROM LIMR_Clock_Stop_History__c WHERE CaseId__c = :caseObj.Id];
            system.debug('caseObj ' + caseObj + ' ' + caseObj.Status);
           
            
            List<NVRM_CaseWaitingForInfo> waitingforinfoList = new List<NVRM_CaseWaitingForInfo>();
            if( clockStopHistory.size() > 0 ){
                for( LIMR_Clock_Stop_History__c clockStopHistoryVar : clockStopHistory ){
                    system.debug('#### clockStopHistory' + clockStopHistoryVar);
                    waitingforinfoList.add( new NVRM_CaseWaitingForInfo( Integer.valueOf(clockStopHistoryVar.Clock_Day__c),Integer.valueOf(clockStopHistoryVar.Number_of_Days__c)) );
                }
            }
            else{
                waitingforinfoList.add(new NVRM_CaseWaitingForInfo( 0,0) );
            }
            system.debug('waitingforinfoList ' + waitingforinfoList);
            ClockStatus clockStatusVar = new ClockStatus(Integer.valueOf(caseObj.Total_clock_days__c),caseObj.Status,waitingforinfoList);
            
            if(   !clockRunningStatus.contains(caseObj.Status) ){
                clockStatusVar.clockStopped = true;
            }
            else if(clockStatusVar.currentStatus == NVRM_Utility.caseStatus_RFI && clockStatusVar.clockDay < NVRM_Utility.CLOCK_PAUSE_WITHIN_DAY){
                clockStatusVar.clockStopped = true;
            }
            
            
            system.debug(clockStatusVar);
            String jsonStr = JSON.serialize(clockStatusVar);
            system.debug('jsonStr ' + jsonStr);
            return jsonStr;
        }
       catch(Exception e){
            system.debug( 'Exception ' + e.getMessage() );
            return null;
       }
        
        
    }
    
    /*************************************************************   
    * @description Lightning method that return case record Type value
    * @param Record Type Developer Name
    * @param 
    * @return Case Object
    */
    @AuraEnabled
    public static Id getCaseRecordType( String devName ){
        ID recordTypeID = [ SELECT ID, SobjectType FROM RecordType WHERE SobjectType='Case' AND DeveloperName= :devName AND IsActive=true LIMIT 1][0].Id;
        system.debug(recordTypeID);
        return recordTypeID;
    }
    
    public class ClockStatus{
        public Boolean clockStopped;
        public Integer clockDay;
        public String currentStatus;
        public List<NVRM_CaseWaitingForInfo> waitingForInfo;
        public ClockStatus(Integer clockDayVar,String caseStatus, List<NVRM_CaseWaitingForInfo> waitingForInfoVar){
            clockStopped = false;
            clockDay = clockDayVar != null?clockDayVar:0;
            currentstatus = caseStatus;
            waitingForInfo = waitingForInfoVar;
        }
    }

  

}
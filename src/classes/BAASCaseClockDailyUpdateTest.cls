@isTest
private class BAASCaseClockDailyUpdateTest
{
	static List<Case> cases;
	@testSetup 
	static void createCaseRecords() {
		cases = new List<Case>();
		//TestDataGenerator.DefaultDummyRecordSet rs = TestDataGenerator.createDefaultDummyRecordSet();
	
		RecordTypeSelector rTypeSelector = new RecordTypeSelector();
        List<RecordType> AAapplicationRecType = rTypeSelector.fetchRecordTypesByName('Accreditor_Assessor_Application');
        for(Integer i = 0; i < 10; i++) {
            Case personAccountCase = new Case();
            personAccountCase.subject = 'PersonAccount_Case_'+i;
            personAccountCase.RecordTypeId = AAapplicationRecType[0].Id;
            personAccountCase.Origin = 'Web';
            personAccountCase.Business_Unit__c = 'BAAS';
            
            if(i<=1) {
            	personAccountCase.BAAS_Clock_Start_Date__c = system.today();
            	personAccountCase.status= 'App Complete/Ready for Panel';
            }
            else if(i<=3) {
            	personAccountCase.BAAS_Clock_Start_Date__c = system.today();
            	personAccountCase.status= 'Applicant adds additional information';
            }else if(i<=5) {
            	personAccountCase.BAAS_Clock_Start_Date__c = system.today();
            	personAccountCase.status= 'OEH Internal Decision Process';
            }else if(i==6) {
            	personAccountCase.BAAS_Clock_Start_Date__c = system.today();
            	personAccountCase.status= 'Panel Requests Additional Info';
            }else if(i==7) {
            	personAccountCase.BAAS_Clock_Start_Date__c = system.today();
            	personAccountCase.status= 'App Approved';
            }else if(i==8) {
            	personAccountCase.BAAS_Clock_Start_Date__c = system.today();
            	personAccountCase.status= 'App Approved and Fee Paid';
            }else if(i==9) {
            	personAccountCase.BAAS_Clock_Start_Date__c = system.today();
            	personAccountCase.status= 'App Not Approved';
            }
            system.debug('personAccountCase ='+ personAccountCase);
            cases.add(personAccountCase);
        }
        insert cases;
	}

	@isTest static void testBAAS_ScheduleClockUpdate() {
        Test.StartTest();
            BAASScheduleClockUpdate clockUpdate1 = new BAASScheduleClockUpdate();
            String sch = '0 0 23 * * ?'; 
            system.schedule('Test Territory Check', sch, clockUpdate1); 
            clockUpdate1.execute(null);
        Test.stopTest();
    }

    @isTest static void testGetCaseClockDetails() {
    	Case caseRecord = [Select Id, Status from Case where subject = 'PersonAccount_Case_0'];
    	String clockJSONString = BAASCaseDayPassedController.getCaseClockDetails(caseRecord.Id);
    	system.assertNotEquals(clockJSONString,null);

    	LIMR_Clock_Stop_History__c clockStopRecord = new LIMR_Clock_Stop_History__c();
    	clockStopRecord.CaseId__c = caseRecord.Id;
    	clockStopRecord.Clock_Day__c = 1;
    	insert clockStopRecord;

    	clockJSONString = BAASCaseDayPassedController.getCaseClockDetails(caseRecord.Id);
    	system.assertNotEquals(clockJSONString,null);
    }
    //@isTest static void testBAASCaseTrigger() {
    //	system.runAs(TestDataGenerator.BAASAdmin) {
    //		Case caseRecord = [Select Id, Status from Case where subject = 'PersonAccount_Case_0'];
	   // 	caseRecord.Status = 'Panel Requests Additional Info';
	   // 	Test.startTest();
	   // 	update caseRecord;

	   // 	List<LIMR_Clock_Stop_History__c> BAASClockRecord = [Select Id from LIMR_Clock_Stop_History__c];
	   // 	system.assertEquals(BAASClockRecord.size(),1);
	   // 	Test.stopTest();
    //	}
    //}
}
public with sharing class BAASCaseDayPassedController {
	public BAASCaseDayPassedController() {}

	/*************************************************************   
    * @description Lightning method that return case record with clock details
    * @param case Id
    * @param 
    * @return Case Object
    */
    @AuraEnabled
    public static String getCaseClockDetails( Id caseId ){
        if( caseId == null)
            return null;
        try{
            Case caseObj = [ SELECT Status,ContactId ,Total_clock_days__c, Days_Left__c  FROM Case WHERE Id =: caseId ];
            
            CaseClockStatus__mdt cClockStatus = CommunitySettingsSelector.getBASSClockRunningSettings();
        	Set<String> clockRunningStatus = new Set<String>(cClockStatus.ClockRunningStatus__c.split(';'));
            List<LIMR_Clock_Stop_History__c> clockStopHistory = new List<LIMR_Clock_Stop_History__c>();
            clockStopHistory = [ SELECT New_Status__c,Old_Status__c,Clock_Day__c,CaseId__c,EndDate__c,Number_of_Days__c FROM LIMR_Clock_Stop_History__c WHERE CaseId__c = :caseObj.Id];
            system.debug('caseObj ' + caseObj + ' ' + caseObj.Status);
            
            List<NVRM_CaseWaitingForInfo> waitingforinfoList = new List<NVRM_CaseWaitingForInfo>();
            if( clockStopHistory.size() > 0 ){
                for( LIMR_Clock_Stop_History__c clockStopHistoryVar : clockStopHistory ){
                    system.debug('#### clockStopHistory' + clockStopHistoryVar);
                    waitingforinfoList.add( new NVRM_CaseWaitingForInfo( Integer.valueOf(clockStopHistoryVar.Clock_Day__c),Integer.valueOf(clockStopHistoryVar.Number_of_Days__c)) );
                }
            }
            else{
                waitingforinfoList.add(new NVRM_CaseWaitingForInfo( 0,0) );
            }
            system.debug('waitingforinfoList ' + waitingforinfoList);
            NVRM_CreateCases.ClockStatus clockStatusVar = new NVRM_CreateCases.ClockStatus(Integer.valueOf(caseObj.Total_clock_days__c),caseObj.Status,waitingforinfoList);
            system.debug('clockStatusVar ='+ clockStatusVar);
            if(   !clockRunningStatus.contains(caseObj.Status) ){
                System.debug('not equlas');
                clockStatusVar.clockStopped = true;
            }
            //else if(clockStatusVar.currentStatus == UtilityClass.BAAS_STOP_CLOCK_CASE_STATUS && clockStatusVar.clockDay < NVRM_Utility.CLOCK_PAUSE_WITHIN_DAY){
            //    clockStatusVar.clockStopped = true;
            //}
            
            
            system.debug(clockStatusVar);
            String jsonStr = JSON.serialize(clockStatusVar);
            system.debug('jsonStr ' + jsonStr);
            return jsonStr;
        }
       catch(Exception e){
            system.debug( 'Exception ' + e.getMessage() );
            return null;
       }
        
        
    }
}
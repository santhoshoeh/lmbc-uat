@isTest
private class CasePartyTriggerHandlerTest{
    public static list<Account> accList;
    public static list<User> userList;
    public static list<Case> caseList;
    public static User usr;
    public static Integer recordCount;

    public static void setupTestData(){
        String personAccRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

        accList = new list<Account>();
        for(Integer i = 0; i < recordCount; i++){
            Account acc1 = new Account(
                RecordTypeID=personAccRecTypeId,
                FirstName='FirstName ',
                LastName='LastName from Test Class'+String.ValueOf(i),
                PersonMailingStreet='Mail Street',
                PersonMailingPostalCode='PostCode',
                PersonMailingCity='MailCity',
                PersonEmail='mailEmailFromClass'+String.ValueOf(i)+'@test.com',
                Email_Verified__pc = true,
                Business_Unit__pc = 'BOAM'
            );
            accList.add(acc1);
        }
        insert accList;

        Id profileId = [SELECT id, name FROM profile where UserType = 'PowerCustomerSuccess' LIMIT 1].get(0).Id;
        userList = new list<User>();
        
        map<ID, ID> accContMap = new map<ID, ID>();
        for(Contact cont : [select id, AccountID from Contact where AccountID IN :accList]){
            accContMap.put(cont.AccountID, cont.ID);
        }

        for(Integer i = 0; i < recordCount; i++){
            Account acc1 = accList[i];
            User userRec1 =  new User(
                Username = acc1.PersonEmail + '.testClassUser',
                FirstName = acc1.firstname,
                LastName = acc1.lastname,
                Email = acc1.PersonEmail,
                alias = 'test1',
                ContactId = accContMap.get(acc1.ID),
                TimeZoneSidKey = 'Australia/Sydney',
                LocaleSidKey='en_AU', 
                EmailEncodingKey='ISO-8859-1', 
                LanguageLocaleKey='en_US',
                profileId = profileId,
                Business_Unit__c = 'BOAM'
            );
            userList.add(userRec1);
        }

        usr = [select id from User where ID = :UserInfo.getUserID()];
        system.runAs(usr){
            insert userList;
        }
        
        caseList = new list<Case>();
        for(Integer i = 0; i < recordCount; i++){
            Case cs1 = new Case(
                Subject = 'Case from Test Class'+String.ValueOf(i),
                RecordTypeID = CaseUtil.parentCaseRTID,
                Business_Unit__c = 'BOAM'
            );
            caseList.add(cs1);
        }
        insert caseList;  
    }

    static testmethod void casePartyTest(){
        recordCount = 15;
        setupTestData();
        Test.StartTest();
            list<Case_Party__c> cpList = new list<Case_Party__c>();
            // add party records.
            for(Integer i = 0; i < recordCount; i++){
                Case_Party__c cp = new Case_Party__c();
                cp.First_Name__c = 'test';
                cp.Last_Name__c = 'name';
                cp.Person_Email__c = accList[i].PersonEmail;
                cp.RecordTypeID = CasePartyUtil.assessorRTID;
                cp.Role__c = 'Assessor';
                cp.Assessor_accreditation_number__c = '1234';
                cp.Case__c = caseList[i].ID;
                cpList.add(cp);
            }
            insert cpList;

            // Update to new party
            Integer shares = 5;
            Integer increment = cpList.size()/shares;
            Integer startCount = 0;
            Integer endCount = startCount + increment;
            
            list<Case> assessCaseList = new list<Case>();
            for(Integer i = startCount; i < endCount; i++){
                Case cs1 = new Case(
                    Subject = 'Case from Test Class'+String.ValueOf(i),
                    RecordTypeID = CaseUtil.assesmentRTID,
                    Business_Unit__c = 'BOAM',
                    Related_Cases__c = caseList[i].ID
                );
                assessCaseList.add(cs1);
            }
            insert assessCaseList;

            list<Case_Party__c> newCPList = new list<Case_Party__c>();
            for(Integer i = startCount; i < endCount; i++){
                Case_Party__c cp = new Case_Party__c();
                cp.First_Name__c = 'test';
                cp.Last_Name__c = 'name';
                cp.Person_Email__c = accList[i].PersonEmail;
                cp.RecordTypeID = CasePartyUtil.assessorRTID;
                cp.Role__c = 'Assessor';
                cp.Assessor_accreditation_number__c = '1234';
                cp.Case__c = caseList[i].ID;
                newCPList.add(cp);
            }
            insert newCPList;

            for(Integer i = startCount; i < endCount; i++){
                Case_Party__c cp = cpList[i];
                cp.Person_Email__c = accList[i+1].PersonEmail;
            }

            startCount = endCount;
            endCount = startCount + increment;
            for(Integer i = startCount; i < endCount; i++){
                Case_Party__c cp = cpList[i];
                cp.RecordTypeID = CasePartyUtil.interestHolderRTID;
            }

            startCount = endCount;
            endCount = startCount + increment;
            for(Integer i = startCount; i < endCount; i++){
                Case_Party__c cp = cpList[i];
                cp.Person_Email__c += '.new';
            }

            startCount = endCount;
            endCount = startCount + increment;
            for(Integer i = startCount; i < endCount; i++){
                Case_Party__c cp = cpList[i];
                cp.Person_Email__c = null;
            }

            update cpList;

            delete cpList;
            
            undelete cpList;

            CasePartyTriggerHandler cpt = new CasePartyTriggerHandler(false, 0, null, null, null, null);
            cpt.resetTriggerState();
        Test.StopTest();
    }
}
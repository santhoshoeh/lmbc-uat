/**
 * Created by junzhou on 6/7/17.
 */

public without sharing class TransactionService {
    public Transaction__c createTransaction(PaymentProcesses.requestParameters reqs, String transactionRTType) {
        Transaction__c transactionRec = new Transaction__c();

        transactionRec.Reference_Number__c = reqs.referenceNumber;
        transactionRec.Account__c = reqs.customerId;
        transactionRec.Payment_Amount__c = reqs.finalAmount;
        transactionRec.Payment_Status__C = reqs.paymentstatus;
        transactionRec.Case__c = reqs.caseId;
        transactionRec.Purchase_Item__c = reqs.purchaseItem;
        RecordTypeSelector rTypeSelector = new RecordTypeSelector();
        transactionRec.RecordTypeId = rTypeSelector.fetchRecordTypesByName(transactionRTType)[0].Id;
        system.debug('transactionRec ='+ transactionRec);
        insert transactionRec;
        return transactionRec;
    }
}
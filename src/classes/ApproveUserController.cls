public without sharing class ApproveUserController {
    
    /*------------------------------------------------------------
    Author:         Suraj
    Description:    Controller for User Managment functionality
    Inputs:         n/a
    Test Class:     ApproveUserControllerTest
    History
    <Date>          <Authors Name>      <Brief Description of Change>   
    06-Jun-17       Suraj               Created
    ------------------------------------------------------------*/  

    
    /* Public variables; */
    private id recordId;
    public map<Id, AccountWrapper> mapAccWrapper {get;set;} 
    public list<AccountWrapper> selectedAccounts{set;get;}
    public list<AccountWrapper> allAccounts{set;get;}
	public String selectedAccountsjson {get;set;} 
    public String userBuisUnit {get;set;}
    
    private transient ApexPages.Message currentMsg;

    // Constructor
    public ApproveUserController(ApexPages.StandardSetController stdController){
        User thisUser = [SELECT Id, Business_Unit__c FROM user WHERE Id =: UserInfo.getUserId()]; 
        userBuisUnit = thisUser.Business_Unit__c;
        
		allAccounts = getAccountsToApproveOrReject();
    }

    /* This method is used to query all the person accounts that are pending for approval, we use below filters
     * Email Verify flag : False
     * Approved Flag: False
     * Buisness Unit: Use business unit for the loged in user
     * Account Type: Person Accounts
	 */ 
    public List<AccountWrapper> getAccountsToApproveOrReject(){
		system.debug('userBuisUnit   ' + userBuisUnit);
        //If buisness unit is not provided return error message
        if(userBuisUnit == null){
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                        'Business unit not assigned to the logged in User, you can only approve Accounts related to your Business Unit')); 
                return null;  
        }
        
        mapAccWrapper = new map<Id, AccountWrapper>();

        for(Account acct: [SELECT Name, Id, PersonMobilePhone, Business_Unit__pc,
                           (SELECT Id, Email,FirstName,LastName FROM Contacts) FROM Account 
                            WHERE Approved__pc = false
                            AND Email_Verified__pc = true
                            AND Business_Unit__pc =: userBuisUnit
                            AND RecordTypeId =: Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId()])
        {
            if (acct.Contacts != null)
            {
                AccountWrapper personAccount = new AccountWrapper();
                personAccount.acctid = acct.Id;
                personAccount.acctname = acct.Name;
                personAccount.contid = acct.Contacts[0].id;
                personAccount.email = acct.Contacts[0].email;
                personAccount.firstname = acct.Contacts[0].FirstName;
                personAccount.mobile = acct.PersonMobilePhone;
                personAccount.lastname = acct.Contacts[0].LastName;
                personAccount.buisUnit = acct.Business_Unit__pc;
                personAccount.isselected = false;
                mapAccWrapper.put(acct.Id, personAccount);                        
            }
        }

        // if there are no accounts to approve shoe relevat error messae to user
        if(mapAccWrapper.isEmpty()){
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,
                        'There are no verified Accounts pending for Approval related to your Business Unit')); 
                return null;      
        }
        
        return mapAccWrapper.values();
    }


    public pageReference ApproveUsers() 
    {
        
        List<Account> accounts = new list<Account>();
        List<User> users = new List<User>();

        Savepoint sp = Database.setSavepoint();
        try
        {
            selectedAccounts = (list<AccountWrapper>) JSON.deserialize(selectedAccountsjson, list<AccountWrapper>.class);
            
            //Assigne profile based on the Account Business Unit
            Community_Settings__mdt customeSetting  = CommunitySettingsSelector.getCommunitySettings();
            String profileName;
            String usernamePrefix;
            
            if(userBuisUnit.equalsIgnoreCase('BAAS')){
           		profileName = customeSetting.BAAS_Community_Profile__c;
                usernamePrefix = '';
            } else if(userBuisUnit.equalsIgnoreCase('BOAM')){
           		profileName = customeSetting.BOAM_Community_Profile__c;
                usernamePrefix = '.boam';
            } else if(userBuisUnit.equalsIgnoreCase('NVRM')){
           		profileName = customeSetting.NVRM_Community_Profile__c;
                usernamePrefix = '.nvrm';
            }
            
            system.debug('profileName for this acount ' + profileName);
            
            //get profile id for Community User based on business unit
            Id profileId = [SELECT id, name FROM profile WHERE name =: profileName LIMIT 1].get(0).Id;

            if (profileId == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                        'A valid Salesforce profile was not found for Supplier or Network Operators. Please contact your Salesforce administrator.')); 
                return null;                
            }

            if (!selectedAccounts.isEmpty()){
                for (AccountWrapper acwselected : selectedAccounts)
                {
                    AccountWrapper acw = mapAccWrapper.get(acwselected.acctid);

                    if (acw != null)
                    {
                        String alias = acw.contid;
                        String nickFirstName = (acw.firstname).length()>1 ? acw.firstname.substring(0,2) : acw.firstname;
                        String nickLastName = (acw.lastname).length()>1? acw.lastname.substring(0,2) : acw.lastname;
                        system.debug('acw.contid ='+ acw.contid);                                
                        User userRec =  new User(
                            Username = acw.email + usernamePrefix,
                            FirstName = acw.firstname,
                            LastName = acw.lastname,
                            CommunityNickname =  acw.contid + String.valueOf(Datetime.Now()),
                            Email = acw.email,
                            ContactId = acw.contid,
                            MobilePhone = acw.mobile,
                            Alias = alias.right(8), 
                            TimeZoneSidKey = 'Australia/Sydney',
                                LocaleSidKey='en_AU', 
                                EmailEncodingKey='ISO-8859-1', 
                                LanguageLocaleKey='en_US',
                            profileId = profileId,
                            Business_Unit__c = acw.buisUnit
                        );
                        users.add(userRec);
                        accounts.add(new Account(Id = acw.acctid, Approved__pc = true));                    
                    }
                }

                system.debug('users to insert ' + users);
                system.debug('accounts to update ' + accounts);
                if (!users.isEmpty()) insert users;

                if (!accounts.isEmpty()) update accounts;                        
            }
            else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                        'No Accounts  were selected for Approval')); 
                return null;                 
            }
			
			String baseURL = '/one/one.app#/sObject/Account/list?filterName=';
			String redirecURL = baseURL+findListViewID();
			
            PageReference page = new PageReference(redirecURL);

            page.setRedirect(false);
            return page;  

        }
        catch (exception ex)  {
			Database.Rollback(sp);
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
				'An error occurred when approving the customers. Error is: '+ex.getMessage()));
            system.debug(ex);
            system.debug(ex.getTypeName());
            system.debug(ex.getMessage());
            system.debug(ex.getStackTraceString());
			return null;
        }
    }
	
	public String findListViewID(){
		list<ListView> lvList = [select id from listview where SobjectType = 'Account' and Name = 'Approved'];
		String listID = 'Recent';
		if(!lvList.IsEmpty())
			listID = lvList[0].ID;
		return listID;
	}
   
    //This method updates the Accounts selected for Approving 
    public pageReference updateSelectedAccounts (){
        
        try{
          selectedAccounts = (list<AccountWrapper>) JSON.deserialize(selectedAccountsjson, list<AccountWrapper>.class);
          system.debug('selected accounts ' + selectedAccounts);
        }catch(Exception ex){
          // Return Error on the page in case of exception
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error on the page: ' + ex.getMessage()));
            return null;          
        }

        return Page.ApproveAccounts;
    }

    //Wrapper Class to retrieve Customer Details to Approve/Reject
    public class AccountWrapper {
        public Id acctid {get;set;}
        public Id contid {get;set;}
        public String acctname{get;set;}
        public String email {get;set;}
        public String firstname {get;set;}
        public String lastname {get;set;}
        public String mobile {get;set;}
        public String buisUnit {get;set;}
        public boolean isselected{get;set;}   
    }
}
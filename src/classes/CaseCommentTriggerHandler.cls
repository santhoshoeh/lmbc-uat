/*--- Trigger Handler class for the Case_Comment__c Object ---*/
/* Description:     This class will contain methods which will contain all the logic for trigger events.
*                   This is recommended to control the order of execution of logic for the same event.
*                   This trigger also uses trigger flags logic to allow easy deactivation of trigger modules using custom settings.
*/
public class CaseCommentTriggerHandler{

    private boolean isExecuting = false;
    private integer BatchSize = 0;
    private list<Case_Comment__c> trgOld;
    private list<Case_Comment__c> trgNew;
    private map<ID, Case_Comment__c> trgOldMap;
    private map<ID, Case_Comment__c> trgNewMap;

    private static set<ID> triggeredSetBeforeUpdate = new set<ID>();
    private static set<ID> triggeredSetAfterUpdate = new set<ID>();

    public static String CaseCommObjName;
    public static set<ID> CaseCommQueueIDs;
    public static Schema.SObjectType groupType;

    // Static block to populate variables for usage by trigger methods
    // Executing as part of static block so that repeated executions of the trigger do not rerun this code
    static{
    }

    // Method to reset the processed records cache FROM the trigger variables.
    // This will especially be useful in test classes to perform repeated executions.
    public void resetTriggerState(){
        triggeredSetBeforeUpdate.clear();
        triggeredSetAfterUpdate.clear();
    }

    // Method to log the processed records. This will be used to prevent recursion
    private void computeTriggeredSet(list<Case_Comment__c> recList, set<ID> triggeredSet){
        for(Case_Comment__c rec: recList){
            triggeredSet.add(rec.id);
        }
    }

    // Constructor that will be called FROM trigger
    public CaseCommentTriggerHandler(boolean isExecuting, integer size, list<Case_Comment__c> trgOld, list<Case_Comment__c> trgNew, map<ID, Case_Comment__c> trgOldMap, map<ID, Case_Comment__c> trgNewMap){
        this.isExecuting = isExecuting;
        this.BatchSize = size;
        this.trgOld = trgOld;
        this.trgNew = trgNew;
        this.trgOldMap = trgOldMap;
        this.trgNewMap = trgNewMap;
    }

    public void OnBeforeInsert(){
        setBusinessUnit(trgNew, trgOldMap, true);
    }

    public void OnAfterInsert(){
        recalculateSharing(trgNew, trgOldMap, false);
    }

    public void OnBeforeUpdate(){
        setBusinessUnit(trgNew, trgOldMap, false);

        computeTriggeredSet(trgNew, triggeredSetBeforeUpdate);
    }

    public void OnAfterUpdate(){
        recalculateSharing(trgNew, trgOldMap, true);

        computeTriggeredSet(trgNew, triggeredSetAfterUpdate);
    }

    public void OnBeforeDelete(){
    }

    public void OnAfterDelete(){
    }

    public void OnAfterUndelete(){
        recalculateSharing(trgNew, trgOldMap, false);
    }

    private void recalculateSharing(list<Case_Comment__c> cCommList, map<ID, Case_Comment__c> trgOldMap, Boolean IsUpdate){
		set<ID> caseIDs = new set<ID>();
		for(Case_Comment__c cCom : trgNew){
            if(IsUpdate){
                Case_Comment__c oldCCom = trgOldMap.get(cCom.Id);
                if(cCom.Case__c != oldCCom.Case__c || cCom.Private__c != oldCCom.Private__c){
                    if(cCom.Case__c != null)
                        caseIDs.add(cCom.Case__c);
                    if(oldCCom.Case__c != null)
                        caseIDs.add(oldCCom.Case__c);
                }
			}else{
				if(cCom.Case__c != null && !cCom.Private__c)
					caseIDs.add(cCom.Case__c);
			}
		}
        PortalUserSharingUtil.recalculateSharingByCaseID(caseIDs);
    }
	
	private void setBusinessUnit(list<Case_Comment__c> cCommList, map<ID, Case_Comment__c> trgOldMap, Boolean isInsert){
        set<ID> caseIDs = new set<ID>();
        if(isInsert){
            for(Case_Comment__c cComm : cCommList){
                caseIDs.add(cComm.Case__c);
            }
        }else{
            for(Case_Comment__c cComm : cCommList){
                Case_Comment__c oldComm = trgOldMap.get(cComm.ID);
                if(cComm.Case__c != oldComm.Case__c)
                    caseIDs.add(cComm.Case__c);
            }
        }
        if(!caseIDs.IsEmpty()){
            map<ID, Case> caseMap = new map<ID, Case>([select id, Business_Unit__c from Case where id in :caseIDs]);
            for(Case_Comment__c cComm : cCommList){
                if(caseMap.containsKey(cComm.Case__c)){
                    cComm.Business_Unit__c = caseMap.get(cComm.Case__c).Business_Unit__c;
                }
            }
        }
    }
}
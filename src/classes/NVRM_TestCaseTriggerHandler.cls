/**
* @author Sakshi Chauhan
* @date 2 August 2017
*
*
* @description TTest class to test below methods of Case Trigger Handler
* copyCaseLots
* checkCaseStatusFlow
* triggerAssignmentRule
* stampClockStatusData
*/

@isTest
public class NVRM_TestCaseTriggerHandler{
	public static Integer SIZE = 30;
	public static String testEmail = 'daveAdm@gmail.com';
	
	testMethod static void testcopyCaseLots(){
		
        Profile pf = [Select id,name from Profile where Name='NVRM_Admin' limit 1];
        
        User userObj = new User(username=testEmail, firstname='Smith',
        lastname='Dave', email=testEmail,communityNickname = 'Dave' + '_fn' ,
        alias = 'adm', profileid = pf.Id, emailencodingkey='UTF-8',
        languagelocalekey='en_US', localesidkey='en_US', timezonesidkey='America/Los_Angeles',Business_Unit__c='NVRM');
        
        insert userObj;
        
        Case newLIMRCase = new Case();
		Test.startTest();
		system.runAs(userObj){
			// inserting new child LIMR Case
			// Creating a closed case which will be parent to a case child record
			Case newCERCase = new Case();
			newCERCase.recordTypeId = NVRM_Utility.getCaseRecordTypeID('CER');
			newCERCase.Status = 'New';
			newCERCase.Business_Unit__c = 'NVRM';
			insert newCERCase;
			List<Lot_DP_Details__c> lotDPListToinsert = new List<Lot_DP_Details__c>();
			for(Integer i =0;i<SIZE; i++){
				Lot_DP_Details__c lotDPObj = new Lot_DP_Details__c();
				lotDPObj.Application__c = newCERCase.Id;
				lotDPObj.Lot__c = 'LT' + i;
				lotDPObj.Deposited_Plan__c = 'DP'+i;
				lotDPListToinsert.add(lotDPObj);
				
			}
			insert lotDPListToinsert;
			newLIMRCase.recordTypeId = NVRM_Utility.getCaseRecordTypeID('LIMR');
			newLIMRCase.Status = 'New';
			newLIMRCase.ParentId = newCERCase.Id;
			newLIMRCase.Business_Unit__c = 'NVRM';
			insert newLIMRCase;
		}
			
		Test.stopTest();
		List<Lot_DP_Details__c> lotDPLIMRList = [ SELECT Deposited_Plan__c,Lot__c ,Application__c FROM Lot_DP_Details__c WHERE Application__c = :newLIMRCase.Id ];
		
		system.assertEquals(lotDPLIMRList.size(),SIZE);
		
	}
	testMethod static void testTriggerAssignmentRule(){
		
		Case newCERCase = new Case();
		newCERCase.recordTypeId = NVRM_Utility.getCaseRecordTypeID('CER');
		newCERCase.Status = 'New';
		newCERCase.Business_Unit__c = 'NVRM';
		insert newCERCase;
		newCERCase = [ SELECT Status,Business_Unit__c FROM Case WHERE Id = :newCERCase.Id];
		newCERCase.Status = 'Submitted';
		Test.startTest();
			update newCERCase;
		Test.stopTest();
		newCERCase = [ SELECT OwnerID,Status,Business_Unit__c FROM Case WHERE Id = :newCERCase.ID];
		system.debug(newCERCase.OwnerID);
		
		
	}
	
	testMethod static void testCheckCaseStatusFlow(){
		Profile pf = [Select id,name from Profile where Name='NVRM_Admin' limit 1];
        
        User userObj = new User(username=testEmail, firstname='Smith',
        lastname='Dave', email=testEmail,communityNickname = 'Dave' + '_fn' ,
        alias = 'adm', profileid = pf.Id, emailencodingkey='UTF-8',
        languagelocalekey='en_US', localesidkey='en_US', timezonesidkey='America/Los_Angeles',Business_Unit__c='NVRM');
        
        insert userObj;
        
        Test.startTest();
        try{
	        system.runAs(userObj){
				Case newCERCase = new Case();
				newCERCase.recordTypeId = NVRM_Utility.getCaseRecordTypeID('CER');
				newCERCase.Status = 'New';
				newCERCase.Business_Unit__c = 'NVRM';
				insert newCERCase;
				newCERCase = [ SELECT OwnerID,Status,Business_Unit__c FROM Case WHERE Id = :newCERCase.ID ];
				newCERCase.Status = 'Application Accepted';	
				
				update newCERCase;
	        }
        }
        catch(Exception e){
        	system.debug('Inside Exception');
        	system.debug('Message ##' + e.getMessage());
        	String errMEss = e.getMessage();
        	system.assert(true,errMEss.contains('You cannot progress to this State'));
        }
		Test.stopTest();
		
	}
	
	testMethod static void testStampClockStatusData(){
		Profile pf = [Select id,name from Profile where Name='NVRM_Admin' limit 1];
        Id roleId = [ SELECT  Name FROM UserRole WHERE DeveloperName='NVRM_Admin' LIMIT 1].ID;
        User userObj = new User(username=testEmail, firstname='Smith',
        lastname='Dave', email=testEmail,communityNickname = 'Dave' + '_fn' ,
        alias = 'adm', profileid = pf.Id, emailencodingkey='UTF-8',
        languagelocalekey='en_US', localesidkey='en_US', timezonesidkey='America/Los_Angeles',Business_Unit__c='NVRM',UserRoleId=roleId);
        
        insert userObj;	
        Test.startTest();
			system.runAs(userObj){
				// insert Account 
				Account acc = new Account( lastname='Dave',firstname='Smith', personemail=testEmail);
				acc.Identification_provided__c = 'Medicare card';
				acc.Medicare_card_number__c ='MD1212';
				acc.Position_on_Medicare_card__c = 'Valid';
				acc.ShippingCity = 'Sydney';
				acc.ShippingCountry = 'AU';
				acc.ShippingPostalCode = '2000';
				acc.ShippingState = 'NSW';
				acc.ShippingStreet = '34 Ramsay street';
				acc.Phone = '0423334546';
        		insert acc;
        		Contact contactAcc = [ SELECT Name FROM Contact where AccountID =: acc.Id];
				// create new case
				Case newCERCase = new Case();
				newCERCase.recordTypeId = NVRM_Utility.getCaseRecordTypeID('CER');
				newCERCase.Status = 'New';
				newCERCase.Business_Unit__c = 'NVRM';
				newCERCase.Applicant_Type__c = 'Landholder';
				newCERCase.ContactId = contactAcc.Id;
				newCERCase.Attach_a_map_with_area__c = true;
				insert newCERCase;
				Id caseId = newCERCase.Id;
				// Application submitted
				newCERCase = [ SELECT OwnerID,Status,Business_Unit__c FROM Case WHERE Id = :caseId ];
				newCERCase.Status = 'Submitted';
				newCERCase.Submitted_Date__c = system.today();
				update newCERCase;
				system.debug('***Case new' + newCERCase);
				
				Case caseVar = [ SELECT OwnerID,Status,Business_Unit__c FROM Case WHERE Id =: newCERCase.ID ];
				system.debug('***caseVar ' + caseVar);
				// Application accepted to start the clock
				Case newCERCase2 = [ SELECT OwnerID,Status,Business_Unit__c FROM Case WHERE Id = :newCERCase.ID ];
				system.debug('After query ### '+ newCERCase);
				newCERCase2.Status = 'Application Accepted';
				update newCERCase2;
				// run the clock
				Database.executeBatch(new NVRM_CaseClockDailyUpdate(), 200);
			
				// Application marked as 'Request Further Information'
				newCERCase2 = [ SELECT OwnerID,Status,Business_Unit__c FROM Case WHERE Id = :caseId ];
				newCERCase2.Status = 'Request Further Information';	
				update newCERCase2;
				List<LIMR_Clock_Stop_History__c> clockHisRec = [ SELECT EndDate__c,CaseId__c,Old_Status__c,New_Status__c,Clock_Day__c FROM LIMR_Clock_Stop_History__c WHERE CaseId__c = :caseId ];
				system.assertEquals(clockHisRec.size(),1);
				system.assertEquals(clockHisRec[0].Clock_Day__c,0);
				system.assertEquals(clockHisRec[0].EndDate__c,null);
	        }
        Test.stopTest();
        
		
		
	}

}
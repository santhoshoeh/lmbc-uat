public class LoggedInUserController{
    static ListView defListViewID;
    static map<String, Community_Settings__mdt> commAttrMap;

	static{
        commAttrMap = CommunityUtil.queryCommAttributes();
		defListViewID = null;
		list<ListView> listviews = [SELECT Id, Name, Sobjecttype, DeveloperName 
            FROM ListView 
            WHERE Sobjecttype = 'Case' ORDER BY Name LIMIT 1];
        // Perform isAccessible() check here
        if(!listviews.IsEmpty())
            defListViewID = listviews[0];
	}
    
    public static String findListViewName(String settingName){
		Community_Settings__mdt viewName = commAttrMap.get(settingName);
        if(viewName == null)
            return null;
		return viewName.Text_Value__c;
    }

	public static ListView queryListView(String ObjectName, String viewName){
		if(String.IsEmpty(ObjectName) || String.IsEmpty(viewName))
			return defListViewID;
        list<ListView> listviews = [SELECT Id, Name, Sobjecttype, DeveloperName 
            FROM ListView 
            WHERE DeveloperName = :viewName
                              AND Sobjecttype = :ObjectName];

        // Perform isAccessible() check here
        if(listviews.IsEmpty())
            return defListViewID;
        return listviews[0];
    }
	
	@AuraEnabled
    public static ListView getListViewStewAssess(){
		//String listViewName = findListViewName('BOAM_Case_List_Stew_Assessment_Name');
		String listViewName = 'Stewardship_Cases';
        return queryListView('Case', listViewName);
    }
        
    @AuraEnabled
    public static ListView getListCreditHoldingViews() {
		String listViewName = findListViewName('BOAM_CreditHolding_List_All_Name');
        return queryListView('Credit_Holding__c', 'All_Credit_Holdings');
    }
       
    @AuraEnabled
    public static ListView getListMyDevApplications() {
		//String listViewName = findListViewName('BOAM_Case_List_Dev_Assessment_Name');
		String listViewName = 'Development_Cases';
        
        return queryListView('Case', listViewName);
    } 
    
    @AuraEnabled
    public static ListView getListApprovalViews() {
		String listViewName = findListViewName('BOAM_Case_List_Dev_Assessment_Name');
        return queryListView('Case', listViewName);
    }
    
	@AuraEnabled
    public static ListView getListMyApplications() {
		String listViewName = findListViewName('Stew_Applications_View__c');
        return queryListView('Case', listViewName);
    }
    
    @AuraEnabled
    public static String getLoggedInUserUrl() {
        User userRec=[SELECT Id, Name,FirstName, LastName, AccountId, 
                      Contact.Account.Id, LastLoginDate, IsAssessor__c 
                      FROM User 
                      WHERE Id = :UserInfo.getUserId() LIMIT 1];
        
        if(userRec.IsAssessor__c){
        return Label.BaasCommunityURL + userRec.AccountId;
        }else{
        return Label.BoamCommunityURL + userRec.AccountId;
        }
    }
    
    @AuraEnabled
    public string userMsg {get; set;}

    @AuraEnabled
    public string userName{get;set;}
    
    @AuraEnabled
    public dateTime userLoginDate{get;set;}
    
    @AuraEnabled
    public string userAccountId{get;set;}
    
    @AuraEnabled
    public string userId{get;set;}

    @AuraEnabled
    public static LoggedInUserController getLoggedInUser() {

        LoggedInUserController result=new LoggedInUserController();
        User userRec = new User();
        userRec=[SELECT Id, Name,FirstName, LastName, AccountId, Contact.Account.Id, LastLoginDate 
                 FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        result.userMsg='N';
        result.userName=userRec.Name;
        result.userLoginDate = userRec.LastLoginDate;
        result.userAccountId = userRec.AccountId;
        result.userId = userRec.Id;
        
        if(userRec.AccountId !=null){
           result.userMsg = checkAccountProfileDetails(userRec.AccountId);
        }
        return result;
    }
    
    public static String checkAccountProfileDetails(Id accountId){
        String userMsg ='N';
        Account accRec = new Account();
        accRec = [Select Id, Business_Unit__pc ,FirstName,LastName, Preferred_contact_method__c ,Identification_provided__c,
                  Phone,PersonHomePhone ,PersonMobilePhone ,Medicare_card_number__c ,Position_on_Medicare_card__c ,Passport_Expiry_Date__c ,
                  Passport_number__c ,ShippingCity , ShippingState , ShippingPostalCode ,Website from Account Where Id=:accountId];
        if(accRec.Phone==null ||accRec.Website==null){
            userMsg = 'Y';
        }
        return userMsg;
    }

}
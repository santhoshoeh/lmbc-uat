/**
 * Created by junzhou on 20/6/17.
 */

public without sharing class AccountController {
    public List<Account> accreditedAssessor {get; set;}

    public AccountController(){
        getAccreditedAssessorList();
    }

    private List<Account> getAccreditedAssessorList() {
        AccountSelector aSelector = new AccountSelector();
        accreditedAssessor = aSelector.getAccreditedAssessorsAccounts();
        return accreditedAssessor;
    }
}

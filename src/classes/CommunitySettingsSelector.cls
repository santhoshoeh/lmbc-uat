/**
 * Created by junzhou on 13/6/17.
 */

public with sharing class CommunitySettingsSelector {
    public CommunitySettingsSelector(){}

    public static Community_Settings__mdt getCommunitySettings() {
        Community_Settings__mdt cSettingmdt = null;
        cSettingmdt = [
                SELECT id, QualifiedApiName, BAAS_Admin_User_ID__c, BAAS_Community_Profile__c,
                       BOAM_Community_Profile__c, NVRM_Community_Profile__c, Submission_Open_Day_End_Date__c,
                       Submission_Open_Day_Start_Date__c
                FROM Community_Settings__mdt
                WHERE QualifiedApiName='Predefine_BAAS_Record'
        ];
        return cSettingmdt;
    }

    public static Community_Settings__mdt getPaymentSettings() {
        Community_Settings__mdt cSettingmdt = null;
        cSettingmdt = [
                SELECT id, Certification_Fee__c, Subscription_Fees__c, QualifiedApiName
                FROM Community_Settings__mdt
                WHERE QualifiedApiName='Predefine_BAAS_Record'
        ];
        return cSettingmdt;
    }

    public static Community_Settings__mdt getPaymentCalloutSettings() {
        Community_Settings__mdt cSettingmdt = null;
        cSettingmdt = [
                SELECT  id, Token_URL__c, Community_Code__c, Payment_URL__c, Token_TimeOut__c, Cancel_URL__c,
                        Service_Return_URL__c, User_Name__c, Return_URL__c, Password__c, Service_Exception_Email__c,
                        Supplier_Business_Code__c, QualifiedApiName
                FROM Community_Settings__mdt
                WHERE QualifiedApiName='Predefine_BAAS_Record'
        ];
        return cSettingmdt;
    }

    public static CaseClockStatus__mdt getBASSClockRunningSettings() {
        CaseClockStatus__mdt cClockStatusmdt = null;
        cClockStatusmdt = [
                SELECT DeveloperName,MasterLabel,ClockRunningStatus__c,DaysCountThresholdForClockRestart__c, QualifiedApiName
                FROM CaseClockStatus__mdt
                WHERE QualifiedApiName='BAASCaseClock'
        ];
        return cClockStatusmdt;
    }
}
/**
 * Created by junzhou on 17/7/17.
 */

public with sharing class BAASBankTransferPaymentController {
    public String referenceNumber{get; set;}
    public Decimal totalAmount {get; set;}
    public String paymentItem {get; set;}
    private Id caseId;

    public BAASBankTransferPaymentController() {
        String transactionId = Apexpages.currentPage().getParameters().get('Id');

        Transaction__c tRecord = TransactionSelector.getTransactionById(transactionId);

        referenceNumber = tRecord.Reference_Number__c;
        totalAmount = tRecord.Payment_Amount__c;
        paymentItem = tRecord.Purchase_Item__c;
        caseId = tRecord.Case__c;
        system.debug('tRecord ='+ tRecord);
    }

    public PageReference finish() {
        String commUrl = Network.getLoginUrl(UtilityClass.getCommunityNetworkId('BAAS'));
        commUrl = commUrl.substring(0, commUrl.lastIndexOf('/')+1) ;
        String URLString = commUrl+'s/case/'+caseId;

        PageReference pReference = new PageReference(URLString);
        pReference.setRedirect(true);
        return pReference;
    }
}
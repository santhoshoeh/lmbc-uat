@isTest
private class TrainingModuleSelectorTest
{
	@testSetup 
	static void createTrainingModules() {
		Training_Modules__c tModule = new Training_Modules__c();
		tModule.Name = 'BAAS Training Module';
		tModule.Compulsory__c = false;
		tModule.Training_Module_Number__c = 'BAASTM';
		insert tModule;
	}
	@isTest
	static void testGetTrainingModules() {
		List<Training_Modules__c> tModules = TrainingModuleSelector.getTrainingModules();
		system.assertEquals(tModules.size(), 1);
	}
}
global class BAASScheduleClockUpdate implements Schedulable {
	global void execute(SchedulableContext sc) {
		BAASCaseClockDailyUpdate clockUpdateBatch = new BAASCaseClockDailyUpdate();
		database.executebatch( clockUpdateBatch );
	}
}
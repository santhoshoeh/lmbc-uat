@isTest
private class CaseCommentTriggerHandlerTest{
    static list<Case> caseList;
    static void loadData(){
        caseList = new list<Case>();
        for(Integer i = 0; i < 100; i++){
            Case cs = new Case(
                Subject = 'Case from Test Class'+String.ValueOf(i),
                RecordTypeID = CaseUtil.accAssessorAppCaseRTID
            );
            caseList.add(cs);
        }
        for(Integer i = 100; i < 200; i++){
            Case cs = new Case(
                Subject = 'Case from Test Class'+String.ValueOf(i),
                RecordTypeID = CaseUtil.parentCaseRTID
            );
            caseList.add(cs);
        }
        for(Integer i = 200; i < 300; i++){
            Case cs = new Case(
                Subject = 'Case from Test Class'+String.ValueOf(i),
                RecordTypeID = CaseUtil.mapReviewCaseRTID
            );
            caseList.add(cs);
        }
        insert caseList;
    }
    static testmethod void test_Trigger() {
        loadData();
        caseList = [select id, Business_Unit__c from Case where ID in :caseList];
        Test.StartTest();
            list<Case_Comment__c> ccList = new list<Case_Comment__c>();
            map<ID, String> caseBUMap = new map<ID, String>();
            Integer listSize = caseList.size();
            // Create case comments for each case.
            for(Integer i = 0; i < listSize; i++){
                Case_Comment__c cc = new Case_Comment__c();
                cc.Comment__c = 'Test comment for '+String.ValueOf(i);
                cc.Case__c = caseList[i].ID;
                ccList.add(cc);
                caseBUMap.put(caseList[i].ID, caseList[i].Business_Unit__c);
            }
            insert ccList;
            // Query the BU field to assert for equality
            list<Case_Comment__c> ccListForTest = [select id, Business_Unit__c, Case__c from Case_Comment__c where id in:ccList];
            for(Integer i = 0; i < listSize; i++){
                Case_Comment__c cc = ccListForTest[i];
                String BUExpected = caseBUMap.get(cc.Case__c);
                // BU on case comment should match BU on case
                system.assertEquals(BUExpected, cc.Business_Unit__c);
                
                // While we are at it, prepare the data for next test case which is to update the case comments to a different case.
                ID newCaseID = caseList[listSize - i - 1].ID;
                cc.Case__c = newCaseID;
            }
            update ccListForTest;
            // Query again to test the new mapping of case has update the BU value on CC.
            ccListForTest = [select id, Business_Unit__c, Case__c from Case_Comment__c where id in:ccList];
            for(Integer i = 0; i < listSize; i++){
                Case_Comment__c cc = ccListForTest[i];
                String BUExpected = caseBUMap.get(cc.Case__c);
                system.assertEquals(BUExpected, cc.Business_Unit__c);
            }
            // Calling trigger handler for delete and undelete operations.
            delete ccListForTest;
            undelete ccListForTest;
            // also calling the trigger handlers unused method
            CaseCommentTriggerHandler cct = new CaseCommentTriggerHandler(false, 0, null, null, null, null);
            cct.resetTriggerState();
        Test.StopTest();
    }
}
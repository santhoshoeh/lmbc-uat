public with sharing class RedirectToAssessmentCalcController{
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    private Case cs;
    public Boolean IsAssessment{get;set;}
    public Boolean IsParentAvailable{get;set;}
    public Boolean IsNoApplicationCreated{get;set;}
    public String accessToken{get;set;}
    
    public RedirectToAssessmentCalcController(ApexPages.StandardController stdController) {
        cs = (Case)stdController.getRecord();
        checkIfClosed();
        generateToken();
    }
    
    public void checkIfClosed(){
        IsAssessment = false;
        IsParentAvailable = false;
        IsNoApplicationCreated = false;
        list<Case> thisCase = [SELECT ID, Related_Cases__c, RecordTypeID, RecordType.Name, Case_Number__c FROM Case WHERE ID = :cs.ID];
        if(!thisCase.IsEmpty()){
            cs = thisCase[0];
            if(CaseUtil.boamAssessmentRTIDs.contains(cs.RecordTypeID)){
                IsAssessment = true;
                if(cs.Related_Cases__c != null){
                    list<Case> parents = [SELECT ID, (select ID FROM Child_Cases__r WHERE RecordTypeID IN :CaseUtil.boamApplicationRTIDs AND Status != 'In-progress' LIMIT 1) FROM Case WHERE ID = :cs.Related_Cases__c];
                    if(!parents.IsEmpty()){
                        Case parent = parents[0];
                        IsParentAvailable = true;
                        if(parent.Child_Cases__r.IsEmpty()){
                            IsNoApplicationCreated = true;
                        }
                    }
                }
            }
        }
    }

    public void generateToken(){
        JWT jwt = new JWT('HS256');
        
        String keyValue = [SELECT DeveloperName, Text_Value__c FROM Community_Settings__mdt WHERE DeveloperName = 'BAM_JWT_Key' LIMIT 1].Text_Value__c;
        jwt.privateKey = EncodingUtil.base64Encode(Blob.valueOf(keyValue));
        
        jwt.sendIssueAt = true;
        jwt.sendExpireAt = true;
        jwt.claims = new map<String, String>();
        
        // set claims 
        jwt.claims.put('asseId',String.ValueOf(cs.ID));
        if(cs.Case_Number__c != null)
            jwt.claims.put('asseNo',cs.Case_Number__c);

        // Identify the logged in user role
        String defaultInternalRoleName = 'Case_Manager';
        String defaultExternalRoleName = 'Proponent';
        User usr = [select id, BAM_Role__c from User where ID = :UserInfo.getUserId()];
        String userRole = usr.BAM_Role__c;
        if(String.IsBlank(userRole)){
            if(UserInfo.getUserType() == 'Standard'){
                userRole = defaultInternalRoleName;
            }else{
                userRole = defaultExternalRoleName;
            }
        }
        jwt.claims.put('isAssessmentLocked',String.ValueOf(!IsNoApplicationCreated));
        // assessment is closed, send proponent for non admins.
        if(!IsNoApplicationCreated && userRole != 'BAM_Admin')
            userRole = defaultExternalRoleName;
        jwt.claims.put('role',userRole);

        // Find assessor details
        list<Case_Party__c> cpList = [select First_Name__c, Middle_Names__c, Last_Name__c, Assessor_accreditation_number__c from Case_Party__c where Case__c = :cs.ID AND Role__c = 'Assessor' ORDER BY LastModifiedDate DESC];
        if(!cpList.IsEmpty()){
            Case_Party__c assessor = cpList[0];
            if(assessor.First_Name__c != null)
                jwt.claims.put('assesorFirstName',assessor.First_Name__c);
            if(assessor.Middle_Names__c != null)
                jwt.claims.put('assesorMiddleName',assessor.Middle_Names__c);
            if(assessor.Last_Name__c != null)
                jwt.claims.put('assesorLastName',assessor.Last_Name__c);
            if(assessor.Assessor_accreditation_number__c != null)
                jwt.claims.put('assesorNumber',assessor.Assessor_accreditation_number__c);
        }
        accessToken = jwt.issue();
        system.debug('@@@@@@ '+accessToken);
    }
}
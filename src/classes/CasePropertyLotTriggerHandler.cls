public class CasePropertyLotTriggerHandler {

            
    /* This method is used to copy the Property lot from parent case to child case whenver case is created
     * Case Party will be added to parent case only
     * Query the child cases and add case party record
     */ 
    public static void syncCasePropertyLotDetails(List<Property_Lots__c > trgNew){
        system.debug(' entry point syncCasePropertyDetails');
        
        //get the parent case from case party
        Map<Id, Property_Lots__c > caseIdMap = new Map<Id, Property_Lots__c >();
        for(Property_Lots__c  thisElem : trgNew){
            caseIdMap.put(thisElem.Case__c, thisElem);
        }
        
        List<Property_Lots__c > casePropertyLotList = new List<Property_Lots__c >();
        
        //loop through the parent cases and get child details
        for(Case thisCase : [Select Id, Related_Cases__c 
                        		FROM Case 
                        		WHERE Related_Cases__c IN: caseIdMap.keySet()]){
                                    
            Property_Lots__c  newPropertyLot = caseIdMap.get(thisCase.Related_Cases__c).clone(); 
			newPropertyLot.Case__c = thisCase.Id;
            newPropertyLot.Parent_Property_Lot__c = caseIdMap.get(thisCase.Related_Cases__c).Id;
			casePropertyLotList.add(newPropertyLot);	            
        }
        
        //insert case parties
        if(casePropertyLotList != null){
            insert casePropertyLotList;
        }
    }
}
public class CommunityUtil{
    public static map<String, Community_Settings__mdt> queryCommAttributes(){
        map<String, Community_Settings__mdt> retMap = new map<String, Community_Settings__mdt>();
        for(Community_Settings__mdt ca : [select DeveloperName, Text_Value__c FROM Community_Settings__mdt WHERE Text_Value__c != null]){
            retMap.put(ca.DeveloperName, ca);
        }
        return retMap;
    }
}
public without sharing class VerifyEmailController {
    
    public static String message{get; set;}
    public static Boolean messageStatus{get; set;}
    public static String commUrl{get; set;}
    //constructor
    public VerifyEmailController()
    {
		messageStatus = false;
    }
    public static void updateEmailVerifiedFlag(){
        
        try
        {
            Id accountId = ApexPages.currentPage().getParameters().get('customerid');
            
            if(accountId == null){
                            
                message = 'Account Id is null ' + accountId;
				return;
            }
 				
            Account acc = [Select Id, business_unit__pc FROM Account WHERE Id =: accountId];
            //get the community id based on the name pased in login method
            List<Network> commIdList = [SELECT Id FROM Network WHERE name =: acc.business_unit__pc];
			if(commIdList.size() == 0){
            	message = 'Issue in getting community details, please connect support.';
			}
                
            //construct the url for directing to community landing page
            commUrl = Network.getLoginUrl(commIdList.get(0).Id);
            commUrl = commUrl.substring(0, commUrl.lastIndexOf('/')+1) + 's/' ;
			system.debug('commUrl ' + commUrl);
           
            acc.Email_Verified__pc = true;
            update acc;
            messageStatus = true;
            //set a parameter and display a success message to the user that email is verified;
            message = 'Thank you for verifying your email address !!!';
            return;
        }
        catch (exception ex)
        {
            //use a set parameter and display not a valid record error to the user;
            message = 'Sorry there was an issue in email verification, please contact support.';
 
        }
        
    } 
    
    public static void finish(){}

}
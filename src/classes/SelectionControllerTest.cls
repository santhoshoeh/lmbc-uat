/**
 * Created by junzhou on 16/6/17.
 */

@isTest
private class SelectionControllerTest {
	@testSetup 
	static void createTrainingModules() {
		Training_Modules__c tModule = new Training_Modules__c();
		tModule.Name = 'BAAS Training Module';
		tModule.Compulsory__c = false;
		tModule.Training_Module_Number__c = 'BAASTM';
		insert tModule;
	}

    @isTest
    static void test_getOrganisationType(){
        List<SelectionController.SelectItem> organisationTypeItems = SelectionController.getOrganisationType();
        system.assert(organisationTypeItems.size()>0);
        List<SelectionController.SelectItem> trainingCourseItems = SelectionController.getTrainingCourses();
        system.assert(trainingCourseItems.size()>0);
        List<SelectionController.SelectItem> workLocalityItems = SelectionController.getWorklocality();
        system.assert(workLocalityItems.size()>0);
    }
}
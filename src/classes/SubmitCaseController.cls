/* @author: Suraj
 * This class is used by Lightning component to submit a case
 * Contains Aura enabled methods
 * Test Class: SubmitCaseControllerTest
 */ 

public class  SubmitCaseController {
     
    //public SubmitCaseController(){}//constructor
    
        
    //this method is used to vlidate case
    @AuraEnabled
    public static Case validateApproveCase(Id caseId){
        
        //get plot and lot details
        return [SELECT id, Status, RecordType.Name FROM Case WHERE id =: caseId];     
        
    }
    
    //this method returns the case details for te case id passed to the method
	@AuraEnabled
    public static Case getApplicationCaseDet(Id caseId){
        
             return  [SELECT id, Status, RecordType.Name
                             FROM Case WHERE id =: caseId];
    }    
    
    // this method is used to get case details for Validations before submit 
    // Used for BOAM
    // - Suraj
    @AuraEnabled
    public static Case getCaseDetails(Id caseId){
        system.debug('caseId in aura ' + caseId);
        if(caseId == null){
            return null;
        }
        try{           
            
            //get parent case
             Case thisParentCase = [Select Id, RecordTypeId, Related_Cases__c 
                         FROM Case 
                         WHERE ID =: caseId];
            
                        
            //for retirement case, just return status
            if(CaseUtil.transferCaseRTID == thisParentCase.RecordTypeId ||
              	CaseUtil.retireCaseRTID == thisParentCase.RecordTypeId)
            {
                return  [SELECT id, Status, RecordType.Name
                             FROM Case WHERE id =: caseId];
            }

            
        
            Id thisCaseId = (!CaseUtil.boamParentRTIDs.contains(thisParentCase.RecordTypeId)
                         ? thisParentCase.Related_Cases__c : caseId);
            
            //thisCaseId = caseId;//need to return parent record to check if assessment is aatached
        	system.debug('thisCaseId in aura ' + caseId);
            //Case thisCase = CaseSelector.getCaseDetails(caseId);
            Case thisCase = [SELECT id, Status, Related_Assessment__c, Business_Unit__c, RecordType.Name,
                             BAAS_All_Doc_list_check__c, Subscription_fee_payment_status__c,Related_Cases__c,
                             (SELECT id FROM Child_Cases__r WHERE RecordTypeId IN: CaseUtil.boamAssessmentRTIDs AND Status = 'Finalised')
                             FROM Case WHERE id =: thisCaseId];
          system.debug('returning case ' + thisCase);
            return thisCase;
        }catch(Exception exp){
        	system.debug('thisCaseId in exp ' + exp.getMessage());
           return null;
        }
    }


    
    //This method is used in creae agreement component to check Application status before approving    
    @AuraEnabled
    public static Case getApplicationCaseDetails(Id caseId){
        system.debug('caseId in aura ' + caseId);
        if(caseId == null){
            return null;
        }
        //update the case status as submitted
        try{
                       
                        
            //Case thisCase = CaseSelector.getCaseDetails(caseId);
            Case thisCase = [SELECT id, Status, RecordType.Name
                             FROM Case WHERE id =: caseId];
          
            return thisCase;
        }catch(Exception exp){
           return null;
        }
    }

    @AuraEnabled
    public static Map<String, Date> getSubmissionPeriod() {
        Map<String, Date> submissionPeriod = new Map<String, Date>();
        Community_Settings__mdt customeSetting  = CommunitySettingsSelector.getCommunitySettings();
        submissionPeriod.put('submissionStartDate', customeSetting.Submission_Open_Day_Start_Date__c);
        submissionPeriod.put('submissionEndDate', customeSetting.Submission_Open_Day_End_Date__c);
        system.debug('submissionPeriod ='+ submissionPeriod);
        return submissionPeriod;
    }

 //used for BOAM
    @AuraEnabled
    public static String submitCaseMethod(Id caseId){
       system.debug('caseId   in aura ' + caseId);
        if(caseId == null){
            return 'Case Id is Null';
        }
        //update the case status as submitted
        try{
            Case thisCase = new Case(Id=caseId);
            thisCase.Status = 'Submitted';
            thisCase.Submitted_Date__c = system.today();
            update thisCase;
            return 'Success';
        }catch(Exception exp){
           return exp.getMessage();
        }
    }

    @AuraEnabled
    public static Case getBAASCaseDetails(Id caseId){
        system.debug('caseId in aura ' + caseId);
        if(caseId == null){
            return null;
        }
        //update the case status as submitted
        try{
            //Case thisCase = CaseSelector.getCaseDetails(caseId);
            Case thisCase = [SELECT id, Status, Related_Assessment__c, Business_Unit__c, RecordType.Name,
                             BAAS_All_Doc_list_check__c, Subscription_fee_payment_status__c,Related_Cases__c 
                             FROM Case WHERE id =: caseId];
          
            return thisCase;
        }catch(Exception exp){
           return null;
        }
    }

    @AuraEnabled
    public static String submitBAASCaseMethod(Id caseId){
        //RecordTypeSelector rTypeSelector = new RecordTypeSelector();
        //List<RecordType> SubmittedAccreditorAssessorApplicationRecordType = rTypeSelector.fetchRecordTypesByName('Submitted_Accreditor_Assessor_Application');
        if(caseId == null){
            return 'Case Id is Null';
        }

        try{
            Case thisCase = CaseSelector.getCaseDetails(caseId);
            if(thisCase.Business_Unit__c == 'BAAS') {
                thisCase.Status = 'Submitted';
                thisCase.Submitted_Date__c = system.today();
                //thisCase.RecordTypeId = SubmittedAccreditorAssessorApplicationRecordType[0].Id;
                update thisCase;
                return 'Success';
            }else {
                return null;
            }
        }catch(Exception exp){
           return exp.getMessage();
        }
    }
     
	@AuraEnabled
    public static String approveCaseMethod(Id caseId){
        system.debug('caseId in aura ' + caseId);
        if(caseId == null){
            return 'Case Id is Null';
        }
        //update the case status as submitted
        try{
            Case thisCase = new Case(Id=caseId);
            thisCase.Status = 'Approved';
            update thisCase;
        	system.debug('caseId in aura after update' + caseId);
            return 'Success';
        }catch(Exception exp){
                    
            system.debug('In catch block ' + exp.getMessage());

           return exp.getMessage();
        }
    }
    
    @AuraEnabled
    public static String isssueCaseMethod(Id caseId){
        system.debug('caseId in aura ' + caseId);
        if(caseId == null){
            return 'Case Id is Null';
        }
        //update the case status as submitted
        try{
            Case thisCase = new Case(Id=caseId);
            thisCase.Status = 'Issued';
            update thisCase;
            return 'Success';
        }catch(Exception exp){
           return exp.getMessage();
        }
    }
    
     
    //this method is used to vlidate properties, lots before creating assessment
    @AuraEnabled
    public static String validateAssessment(Id caseId){
        
        //get parties, properties and lot details
		Case parentCase = [SELECT id, Status, RecordtypeId, 
                           	(SELECT id FROM Properties__r),
                           	(SELECT id FROM Property_Lots__r),
                           	(SELECT id, recordtypeId, recordtype.DeveloperName FROM Case_Parties__r)
                           	FROM Case 
                           	WHERE id =: caseId];
        
        //check if no property details attached show error
        system.debug('parentCase.Properties__r.isEmpty() ' + parentCase.Properties__r.isEmpty());
        if((parentCase.Properties__r == null || parentCase.Properties__r.isEmpty())
           
           && parentCase.RecordtypeId != CaseUtil.transferCaseRTID && parentCase.RecordtypeId != CaseUtil.retireCaseRTID){
            
               return 'Please add Property and Lots to the parent case before submitting applcation';
        }        
        
        //check if credit buyer was added to transfer credit case
        if(parentCase.RecordtypeId == CaseUtil.transferCaseRTID ){
            if(parentCase.Case_Parties__r == null || parentCase.Case_Parties__r.size() == 0)
            {
            	return 'Please add Credit Owner and Credit Buyer in Case Parties.';
            }
            
            //loop through case parties and get parties added
            Set<Id> partySet = new Set<Id> ();
            for(Case_Party__c  thisParty : parentCase.Case_Parties__r){
                partySet.add(thisParty.recordtypeId);
            }

            //check if buyer is added
            if(!partySet.contains(CasePartyUtil.creditBuyerRTID)){
            	return 'Please add a Credit Buyer in Case Parties.';
            }
            
        }
        
        return null;
    }
    
}
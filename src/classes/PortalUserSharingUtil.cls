public class PortalUserSharingUtil{
    public static Boolean executeSharingSetup;
    public static final String personAccountRecTypeName;
    public static final ID personAccountRecTypeID;
    public static final ID DefaultCaseTeamRoleID;
    public static final ID DefaultAccountTeamRole;
    public static final map<String, ID> portalCaseTeamRoleIDMap;
    public static final map<String, ID> accessLevelToPortalCaseTeamRoleIDMap;

    // Static block to populate variables for usage by trigger methods
    // Executing as part of static block so that repeated executions of the trigger do not rerun this code
    static{
        executeSharingSetup = true;
        String DefaultCaseTeamRole = 'Other';
        personAccountRecTypeName = 'Person Account';
        personAccountRecTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(personAccountRecTypeName).getRecordTypeId();
        portalCaseTeamRoleIDMap = new map<String, ID>();
        set<String> caseTeamRoleNames = new set<String>();
        caseTeamRoleNames.add(DefaultCaseTeamRole);
        caseTeamRoleNames.add('Portal User');
        caseTeamRoleNames.add('Read Only Portal User');
        accessLevelToPortalCaseTeamRoleIDMap = new map<String, ID>();
        for(CaseTeamRole ctr : [SELECT id, Name, AccessLevel FROM CaseTeamRole WHERE Name IN: caseTeamRoleNames]){
            if(ctr.Name == DefaultCaseTeamRole)
                DefaultCaseTeamRoleID = ctr.ID;
            else
                accessLevelToPortalCaseTeamRoleIDMap.put(ctr.AccessLevel, ctr.ID);
        }
        
        String DefaultAccountTeamRole = 'Portal User';
    }

    public static void recalculateSharingByAccountID(set<ID> accountIDs){
        if(!accountIDs.IsEmpty()){
            map<ID, User> userMap = new map<ID, User>([SELECT id FROM User WHERE AccountID IN :accountIDs AND AccountID != null]);
            if(!userMap.IsEmpty()){
                recalculateSharingByUserID(userMap.KeySet());
            }
        }
    }

    public static void recalculateSharingByUserID(set<ID> userIDs){
        if(executeSharingSetup){
            recalculateAccountSharingByUserID(userIDs);
            recalculateCaseSharingByUserID(userIDs);
            recalculateCreditHoldingSharingByUserID(userIDs);
        }
    }

    public static void recalculateSharingByCaseID(set<ID> caseIDs){
        if(!caseIDs.IsEmpty()){
            set<ID> accountIDs = new set<ID>();
            for(Case_Party__c cp : [SELECT PersonAccount__c FROM Case_Party__c WHERE Case__c IN :caseIDs AND PersonAccount__c != null]){
                accountIDs.add(cp.PersonAccount__c);
            }
            recalculateSharingByAccountID(accountIDs);
        }
    }

    public static ID findPortalRoleID(String accessLevel){
        ID roleID = accessLevelToPortalCaseTeamRoleIDMap.get(accessLevel);
        if(roleID == null)
            return DefaultCaseTeamRoleID;
        return roleID;
    }

    public static String findHighestsAccess(String currentAccess, String newAccess){
        if(currentAccess == null){
            if(newAccess == null)
                return null;
            else
                return newAccess;
        }else{
            // Current is Read or Edit
            if(newAccess == null)
                return currentAccess;
            else if(newAccess == 'Edit')
                return newAccess;
            else
                return currentAccess;
        }
    }

    public static void recalculateCreditHoldingSharingByUserID(set<ID> portalUserIDs){
        if(!portalUserIDs.IsEmpty()){
            try{
                // find the account ID for the User record being activated.
                map<ID, ID> AccountToUserMap = new map<ID, ID>();
                for(User usr : [SELECT id, AccountID FROM User WHERE ID IN :portalUserIDs AND IsActive = TRUE]){
                    AccountToUserMap.put(usr.AccountID, usr.ID);
                }
                // find the case parties that have been associated with this account.
                // this will be used to identify the list of credit holdings that this portal user should have access to
                map<ID, set<ID>> userToChIDsMap = new map<ID, set<ID>>();
                for(Case_Party__c cp : [SELECT ID, Credit_Holding__c, Role__c, PersonAccount__c FROM Case_Party__c WHERE PersonAccount__c IN :AccountToUserMap.KeySet() AND Credit_Holding__c != null]){
                    ID userID = AccountToUserMap.get(cp.PersonAccount__c);
                    if(userID != null){
                        set<ID> chIDs = userToChIDsMap.get(userID);
                        if(chIDs == null){
                            chIDs = new set<ID>();
                            userToChIDsMap.put(userID, chIDs);
                        }
                        chIDs.add(cp.Credit_Holding__c);
                    }
                }

                // query and find existing share records.
                list<Credit_Holding__Share> chShareList = [SELECT ParentID, UserOrGroupID, RowCause FROM Credit_Holding__Share WHERE UserOrGroupID IN :portalUserIDs AND RowCause IN ('CasePartySharing__c', 'Owner')];
                map<ID, map<ID, Credit_Holding__Share>> existingCHShares = new map<ID, map<ID, Credit_Holding__Share>>();
                for(Credit_Holding__Share chs : chShareList){
                    map<ID, Credit_Holding__Share> exstChIds = existingCHShares.get(chs.UserOrGroupID);
                    if(exstChIds == null){
                        exstChIds = new map<ID, Credit_Holding__Share>();
                        existingCHShares.put(chs.UserOrGroupID, exstChIds);
                    }
                    exstChIds.put(chs.ParentID, chs);
                }
                
                // for each user id map<accountID, set<CHIDs>>

                list<Credit_Holding__Share> ChShareInsert = new list<Credit_Holding__Share>();
                for(ID userID : userToChIDsMap.KeySet()){
                    map<ID, Credit_Holding__Share> exstChIds = existingCHShares.get(userID);
                    set<ID> chIDs = userToChIDsMap.get(userID);
                    for(ID chID : chIDs){
                        if(exstChIds == null || !exstChIds.containsKey(chID)){
                            Credit_Holding__Share chs = new Credit_Holding__Share();
                            chs.ParentID = chID;
                            chs.UserOrGroupID = userID;
                            chs.RowCause = 'CasePartySharing__c';
                            chs.AccessLevel = 'Read';
                            ChShareInsert.add(chs);
                        }
                    }
                }

                // If the user was removed FROM the case party list, time to check and remove them FROM case team.
                list<Credit_Holding__Share> ChShareDelete = new list<Credit_Holding__Share>();
                // iterate through the cases
                for(ID userID : existingCHShares.KeySet()){
                    map<ID, Credit_Holding__Share> exstChIds = existingCHShares.get(userID);
                    set<ID> chIDs = userToChIDsMap.get(userID);
                    for(ID chID : exstChIds.KeySet()){
                        if(chIDs == null || !chIDs.contains(chID)){
                            Credit_Holding__Share chs = exstChIds.get(chID);
                            if(chs.RowCause != 'Owner'){
                                ChShareDelete.add(chs);
                            }
                        }
                    }
                }

                if(!ChShareInsert.IsEmpty()){
                    list<Database.UpsertResult> srList = Database.upsert(ChShareInsert,false);
                    logErrors(srList, ChShareInsert, 'CreditHoldingSharing');
                }
                if(!ChShareDelete.IsEmpty())
                    delete ChShareDelete;
            }
            catch(Exception ex){
                String sourceClass = PortalUserSharingUtil.Class.getName();
                String sourceFunction = 'recalculateCreditHoldingSharing';
                String logMessage = 'Exception = '+ex.getMessage()+'; Details = '+String.ValueOf(ex);
                if(ex.getTypeName() == 'DMLException')
                    logMessage += ' and DML Exception';
                String referenceID;
                String referenceInfo;
                String payLoad = '';
                for( ID userID : portalUserIDs ){
                    payLoad += 'ID = '+String.ValueOf(userID) + '; ';
                }
                String logCode;
                Long timeTaken;

                ApplicationLogUtility.logError(sourceClass, sourceFunction, ex, logMessage, referenceID, referenceInfo, payLoad, logCode, timeTaken);
                ApplicationLogUtility.commitLog();
                system.debug('Please contact you administrator. Exception occurred '+ ex.getMessage());
            }
        }
    }

    public static void recalculateCaseSharingByUserID(set<ID> portalUserIDs){
        if(!portalUserIDs.IsEmpty()){
            try{
                map<String, String> accessLevelsMap = new map<String, String>();

                // list<Case_Access_Level__c> caList = [SELECT ID, Record_Type_Label__c, Role__c, CaseOwnership__c, AccessLevel__c FROM Case_Access_Level__c];
                // for(Case_Access_Level__c ca : caList){
                    // accessLevelsMap.put('RecordType = '+ca.Record_Type_Label__c+'; Role = '+ca.Role__c+'; Ownership = '+ca.CaseOwnership__c, ca.AccessLevel__c);
                // }
                
                String PortalOwnership = 'Portal User';
                String InternalOwnership = 'Internal User';
                String ReadOnlyAccessLevel = 'Read';
                String ReadWriteAccessLevel = 'Edit';
                accessLevelsMap.put('RecordType = '+CaseUtil.accAssessorAppCaseRTDevName+ '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.assessorRoleName,    ReadWriteAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.accAssessorAppCaseRTDevName+ '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.landholderRoleName,  ReadWriteAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.accAssessorAppCaseRTDevName+ '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.assessorRoleName,    ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.accAssessorAppCaseRTDevName+ '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.landholderRoleName,  ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.devAppParentCaseRTDevName+   '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.assessorRoleName,    ReadWriteAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.devAppParentCaseRTDevName+   '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.landholderRoleName,  ReadWriteAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.devAppParentCaseRTDevName+   '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.assessorRoleName,    ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.devAppParentCaseRTDevName+   '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.landholderRoleName,  ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.deveAssesmentRTDevName+          '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.assessorRoleName,    ReadWriteAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.deveAssesmentRTDevName+          '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.landholderRoleName,  ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.deveAssesmentRTDevName+          '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.assessorRoleName,    ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.deveAssesmentRTDevName+          '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.landholderRoleName,  ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.agreementRTDevName+          '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.assessorRoleName,    ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.agreementRTDevName+          '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.landholderRoleName,  ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.agreementRTDevName+          '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.assessorRoleName,    ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.agreementRTDevName+          '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.landholderRoleName,  ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.approvalDetailsRTDevName+    '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.assessorRoleName,    ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.approvalDetailsRTDevName+    '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.landholderRoleName,  ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.approvalDetailsRTDevName+    '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.assessorRoleName,    ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.approvalDetailsRTDevName+    '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.landholderRoleName,  ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.assesmentRTDevName+          '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.assessorRoleName,    ReadWriteAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.assesmentRTDevName+          '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.landholderRoleName,  ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.assesmentRTDevName+          '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.assessorRoleName,    ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.assesmentRTDevName+          '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.landholderRoleName,  ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.bioCertAppRTDevName+         '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.assessorRoleName,    ReadWriteAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.bioCertAppRTDevName+         '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.landholderRoleName,  ReadWriteAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.bioCertAppRTDevName+         '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.assessorRoleName,    ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.bioCertAppRTDevName+         '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.landholderRoleName,  ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.parentCaseRTDevName+         '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.assessorRoleName,    ReadWriteAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.parentCaseRTDevName+         '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.landholderRoleName,  ReadWriteAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.parentCaseRTDevName+         '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.assessorRoleName,    ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.parentCaseRTDevName+         '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.landholderRoleName,  ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.purchaseCaseRTDevName+       '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.creditOwnerRoleName, ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.purchaseCaseRTDevName+       '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.creditOwnerRoleName, ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.retireCaseRTDevName+         '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.creditOwnerRoleName, ReadWriteAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.retireCaseRTDevName+         '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.creditBuyerRoleName, ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.retireCaseRTDevName+         '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.creditOwnerRoleName, ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.retireCaseRTDevName+         '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.creditBuyerRoleName, ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.stwAppRTDevName+             '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.assessorRoleName,    ReadWriteAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.stwAppRTDevName+             '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.landholderRoleName,  ReadWriteAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.stwAppRTDevName+             '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.assessorRoleName,    ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.stwAppRTDevName+             '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.landholderRoleName,  ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.transferCaseRTDevName+       '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.creditOwnerRoleName, ReadWriteAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.transferCaseRTDevName+       '; Ownership = '+PortalOwnership+'; Role = '    +CasePartyUtil.creditBuyerRoleName, ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.transferCaseRTDevName+       '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.creditOwnerRoleName, ReadOnlyAccessLevel);
                accessLevelsMap.put('RecordType = '+CaseUtil.transferCaseRTDevName+       '; Ownership = '+InternalOwnership+'; Role = '  +CasePartyUtil.creditBuyerRoleName, ReadOnlyAccessLevel);

                // find the account ID for the User record being activated.
                map<ID, ID> AccountToUserMap = new map<ID, ID>();
                for(User usr : [SELECT id, AccountID FROM User WHERE ID IN :portalUserIDs AND IsActive = TRUE]){
                    AccountToUserMap.put(usr.AccountID, usr.ID);
                }
                // find the case parties that have been associated with this account.
                // this will be used to identify the list of cases that this portal user should have access to
                map<ID, String> caseIDToRecordTypeMap = new map<ID, String>();
                map<ID, String> caseIDToOwnershipMap = new map<ID, String>();
                map<ID, list<Case_Party__c>> CaseToCasePartyMap = new map<ID, list<Case_Party__c>>();
                for(Case_Party__c cp : [SELECT id, Case__c, Case__r.CaseOwnership__c, Case__r.RecordType.DeveloperName, Role__c, PersonAccount__c FROM Case_Party__c WHERE PersonAccount__c IN :AccountToUserMap.KeySet()]){
                    list<Case_Party__c> cpList = CaseToCasePartyMap.get(cp.Case__c);
                    if(cpList == null){
                        cpList = new list<Case_Party__c>();
                        CaseToCasePartyMap.put(cp.Case__c, cpList);
                    }
                    cpList.add(cp);

                    caseIDToRecordTypeMap.put(cp.Case__c, cp.Case__r.RecordType.DeveloperName);
                    caseIDToOwnershipMap.put(cp.Case__c, cp.Case__r.CaseOwnership__c);
                }
                
                map<ID, map<ID, String>> caseToUserAccessMap = new map<ID, map<ID, String>>();
                // Iterate through each case.
                for(ID caseID : CaseToCasePartyMap.KeySet()){
                    map<ID, String> userAccessMap = new map<ID, String>();
                    caseToUserAccessMap.put(caseID, userAccessMap);

                    String caseRTName = caseIDToRecordTypeMap.get(caseID);
                    String ownership = caseIDToOwnershipMap.get(caseID);
                    // get the list of case party records for this case
                    list<Case_Party__c> cpList = CaseToCasePartyMap.get(caseID);
                    // iterate through each case party for this case
                    for(Case_Party__c cp : cpList){
                        // find the user record for this case party.
                        ID userID = AccountToUserMap.get(cp.PersonAccount__c);
                        if(userID != null){
                            String currentAccess = userAccessMap.get(userID);
                            
                            String roleName = cp.Role__c;
                            String key = 'RecordType = '+caseRTName+'; Ownership = '+ownership+'; Role = '+roleName;

                            String accessLevel = findHighestsAccess(currentAccess, accessLevelsMap.get(key));
                            if(accessLevel != null)
                                userAccessMap.put(userID, accessLevel);
                        }
                    }
                }
                
                // find the case team members WHERE this portal user is listed.
                // this will be used to identify upserts as well as invalid access that needs to be removed.
                // should the query be restricted for only some portal roles? Will a portal user be manually added to the case team?
                // If so, why not add him as a party?
                map<ID, CaseTeamMember> ctmMap = new map<ID, CaseTeamMember>([SELECT ParentID, MemberID, TeamRole.AccessLevel FROM CaseTeamMember WHERE MemberID IN :portalUserIDs]);
                map<ID, list<CaseTeamMember>> existingCaseMemberships = new map<ID, list<CaseTeamMember>>();
                for(CaseTeamMember ctm : ctmMap.Values()){
                    list<CaseTeamMember> ctmList = existingCaseMemberships.get(ctm.ParentID);
                    if(ctmList == null){
                        ctmList = new list<CaseTeamMember>();
                        existingCaseMemberships.put(ctm.ParentID, ctmList);
                    }
                    ctmList.add(ctm);
                }

                list<CaseTeamMember> CaseTeamMembersUpsert = new list<CaseTeamMember>();
                // Iterate through each case.
                for(ID caseID : caseToUserAccessMap.KeySet()){
                    // get the list of users and their access levels for this case
                    map<ID, String> userAccessMap = caseToUserAccessMap.get(caseID);
                    for(ID userID : userAccessMap.KeySet()){
                        // for each user, check if their access has been already setup in the case team.
                        String accessLevel = userAccessMap.get(userID);
                        
                        // find the case team members for this case.
                        list<CaseTeamMember> ctmList = existingCaseMemberships.get(caseID);
                        Boolean toInsert = true;
                        if(ctmList != null){
                            for(CaseTeamMember ctm : ctmList){
                                // If the user ID is already listed as a member, then do not add again.
                                if(userID == ctm.MemberID){
                                    toInsert = false;
                                    // if the access level needs to change, update it.
                                    if(ctm.TeamRole.AccessLevel != accessLevel){
                                        ctm.TeamRoleID = findPortalRoleID(accessLevel);
                                        CaseTeamMembersUpsert.add(ctm);
                                    }
                                    break;
                                }
                            }
                        }
                        // if the user was not identified, add him as a new case team member.
                        if(toInsert){
                            CaseTeamMember ctm = new CaseTeamMember();
                            ctm.MemberID = userID;
                            ctm.ParentID = caseID;
                            ctm.TeamRoleID = findPortalRoleID(accessLevel);
                            CaseTeamMembersUpsert.add(ctm);
                        }
                    }
                }

                // If the user was removed FROM the case party list, time to check and remove them FROM case team.
                list<CaseTeamMember> CaseTeamMembersToRemove = new list<CaseTeamMember>();
                // iterate through the cases
                for(ID caseID : existingCaseMemberships.KeySet()){
                    map<ID, String> userAccessMap = caseToUserAccessMap.get(caseID);
                    // iterate through the existing memberships
                    for(CaseTeamMember ctm : existingCaseMemberships.get(caseID)){
                        // if user is not found, he should not be there.
                        if(userAccessMap == null || !userAccessMap.containsKey(ctm.MemberID)){
                            CaseTeamMembersToRemove.add(ctm);
                        }
                    }
                }
                
                // find case comment to user map
                list<Case_Comment__c> commList = [SELECT id, Case__c FROM Case_Comment__c WHERE Case__c IN :CaseToCasePartyMap.KeySet() AND Private__c = FALSE ORDER BY Case__c];
                map<ID, set<ID>> userToCaseComment = new map<ID, set<ID>>();
                for(Case_Comment__c cc : commList){
                    ID caseID = cc.Case__c;
                    if(caseID != null){
                        map<ID, String> userAccessMap = caseToUserAccessMap.get(caseID);
                        if(userAccessMap != null){
                            for(ID userID : userAccessMap.KeySet()){
                                set<ID> ccIDs = userToCaseComment.get(userID);
                                if(ccIDs == null){
                                    ccIDs = new set<ID>();
                                    userToCaseComment.put(userID, ccIDs);
                                }
                                ccIDs.add(cc.ID);
                            }
                        }
                    }
                }
                
                list<Case_Comment__Share> ccShareList = [SELECT ParentID, UserOrGroupID, RowCause FROM Case_Comment__Share WHERE UserOrGroupID IN :portalUserIDs AND RowCause IN ('CasePartySharing__c', 'Owner')];
                map<ID, map<ID, Case_Comment__Share>> existingCaseCommentShares = new map<ID, map<ID, Case_Comment__Share>>();
                for(Case_Comment__Share ccs : ccShareList){
                    map<ID, Case_Comment__Share> exstCcIds = existingCaseCommentShares.get(ccs.UserOrGroupID);
                    if(exstCcIds == null){
                        exstCcIds = new map<ID, Case_Comment__Share>();
                        existingCaseCommentShares.put(ccs.UserOrGroupID, exstCcIds);
                    }
                    exstCcIds.put(ccs.ParentID, ccs);
                }

                list<Case_Comment__Share> CaseCommentShareInsert = new list<Case_Comment__Share>();
                for(ID userID : userToCaseComment.KeySet()){
                    map<ID, Case_Comment__Share> exstCcIds = existingCaseCommentShares.get(userID);
                    set<ID> ccIDs = userToCaseComment.get(userID);
                    for(ID ccID : ccIDs){
                        if(exstCcIds == null || !exstCcIds.containsKey(ccID)){
                            Case_Comment__Share ccs = new Case_Comment__Share();
                            ccs.ParentID = ccID;
                            ccs.UserOrGroupID = userID;
                            ccs.RowCause = 'CasePartySharing__c';
                            ccs.AccessLevel = 'Read';
                            CaseCommentShareInsert.add(ccs);
                        }
                    }
                }

                // If the user was removed FROM the case party list, time to check and remove them FROM case team.
                list<Case_Comment__Share> CaseCommentShareDelete = new list<Case_Comment__Share>();
                // iterate through the cases
                for(ID userID : existingCaseCommentShares.KeySet()){
                    map<ID, Case_Comment__Share> exstCcIds = existingCaseCommentShares.get(userID);
                    set<ID> ccIDs = userToCaseComment.get(userID);
                    for(ID ccID : exstCcIds.KeySet()){
                        if(ccIDs == null || !ccIDs.contains(ccID)){
                            Case_Comment__Share ccs = exstCcIds.get(ccID);
                            if(ccs.RowCause != 'Owner'){
                                CaseCommentShareDelete.add(ccs);
                            }
                        }
                    }
                }
                
                if(!CaseTeamMembersUpsert.IsEmpty()){
                    list<Database.UpsertResult> srList = Database.upsert(CaseTeamMembersUpsert,false);
                    logErrors(srList, CaseTeamMembersUpsert, 'CaseTeamMemberSharing');
                }
                if(!CaseTeamMembersToRemove.IsEmpty())
                    delete CaseTeamMembersToRemove;
                if(!CaseCommentShareInsert.IsEmpty()){
                    list<Database.UpsertResult> srList = Database.upsert(CaseCommentShareInsert,false);
                    logErrors(srList, CaseCommentShareInsert, 'CaseCommentSharing');
                }
                if(!CaseCommentShareDelete.IsEmpty())
                    delete CaseCommentShareDelete;
            }
            catch(Exception ex){
                String sourceClass = PortalUserSharingUtil.Class.getName();
                String sourceFunction = 'recalculateCaseSharing';
                String logMessage = 'Exception = '+ex.getMessage()+'; Details = '+String.ValueOf(ex);
                if(ex.getTypeName() == 'DMLException')
                    logMessage += ' and DML Exception';
                String referenceID;
                String referenceInfo;
                String payLoad = '';
                for( ID userID : portalUserIDs ){
                    payLoad += 'ID = '+String.ValueOf(userID) + '; ';
                }
                String logCode;
                Long timeTaken;

                ApplicationLogUtility.logError(sourceClass, sourceFunction, ex, logMessage, referenceID, referenceInfo, payLoad, logCode, timeTaken);
                ApplicationLogUtility.commitLog();
                system.debug('Please contact you administrator. Exception occurred '+ ex.getMessage());
            }
        }
    }

    public static void recalculateAccountSharingByUserID(set<ID> userIDs){
        if(!userIDs.IsEmpty()){
            // Query the user table to identify a map of account to user 
            map<ID, ID> AccountToUserMap = new map<ID, ID>();
            for(User usr : [SELECT id, AccountID FROM User WHERE ID IN :userIDs AND IsActive = TRUE]){
                AccountToUserMap.put(usr.AccountID, usr.ID);
            }
            list<AccountTeamMember> atmInsertList = new list<AccountTeamMember>();
            list<AccountTeamMember> atmDeleteList = new list<AccountTeamMember>();
            set<ID> accountsProcessed = new set<ID>();
            for(AccountTeamMember atm : [SELECT AccountId, UserId FROM AccountTeamMember WHERE UserId IN :userIDs]){
                ID userIDForAccount = AccountToUserMap.get(atm.AccountId);
                accountsProcessed.add(atm.AccountId);
                if(atm.UserId != userIDForAccount){
                    atmDeleteList.add(atm);
                    atmInsertList.add(addAccountTeamMember(atm.AccountId, userIDForAccount));
                }
            }
            for(ID accountID : AccountToUserMap.KeySet()){
                if(!accountsProcessed.contains(accountID)){
                    atmInsertList.add(addAccountTeamMember(accountID, AccountToUserMap.get(accountID)));
                }
            }
            if(!atmDeleteList.IsEmpty())
                Delete atmDeleteList;
            if(!atmInsertList.IsEmpty()){
                list<Database.UpsertResult> srList = Database.upsert(atmInsertList,false);
                logErrors(srList, atmInsertList, 'recalculateAccountSharing');
            }
        }
    }

    public static AccountTeamMember addAccountTeamMember(ID accountID, ID userID){
        AccountTeamMember atm = new AccountTeamMember();
        atm.AccountId = accountID;
        atm.UserID = userID;
        atm.TeamMemberRole = DefaultAccountTeamRole;
        atm.AccountAccessLevel = 'Edit';
        atm.ContactAccessLevel = 'Edit';
        atm.CaseAccessLevel = 'None';
        atm.OpportunityAccessLevel = 'None';
        return atm;
    }

    public static void logErrors(list<Database.UpsertResult> srList, list<SObject> ctmList, String methodName){
        for(Integer i = 0 ; i < srList.size(); i++)
            logError(srList[i], ctmList[i], methodName);
        ApplicationLogUtility.commitLog();
    }

    public static void logError(Database.UpsertResult sr, Object obj, String methodName){
        // Process the save results.
        if(!sr.isSuccess()){
            // Get first save result error.
            Database.Error err = sr.getErrors()[0];

            // Check if the error is related to trival access level.
            // Access level must be more permissive than the object's default.
            // These sharing records are not required and thus an insert exception is acceptable. 
            if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&  
                err.getMessage().contains('AccessLevel')){
                // Indicates success.
            }else{
                // Indicates failure.
                // Log it in application log.
                String sourceClass = PortalUserSharingUtil.Class.getName();
                String sourceFunction = methodName;
                String logMessage = 'Error Message = '+err.getMessage()+' ; Object record = '+String.ValueOf(obj)+' ; Save Result = '+String.ValueOf(sr)+' ; Error record = '+String.ValueOf(err);
                String referenceID;
                String referenceInfo;
                String payLoad = 'ID = '+String.ValueOf(sr.getID());
                String logCode = err.getStatusCode().name();
                Long timeTaken;

                system.debug('Please contact you administrator. Exception occurred '+ logCode + ': ' + err.getMessage());
                ApplicationLogUtility.logError(sourceClass, sourceFunction, null, logMessage, referenceID, referenceInfo, payLoad, logCode, timeTaken);
            }
        }
    }
}
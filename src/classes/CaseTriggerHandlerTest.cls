@IsTest
public class CaseTriggerHandlerTest{
    static Account acc1;
    static Contact cont;
    static Case cs;
    static Case parentCs;
    static Case_Party__c cp;
    
    static testmethod void testBOAMChildAutofills(){
        PortalUserSharingUtil.executeSharingSetup = false;
        ID personAccRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

        acc1 = new Account(
            RecordTypeID=personAccRecTypeId,
            FirstName='FirstName ',
            LastName='LastName from Test Class',
            PersonMailingStreet='Mail Street',
            PersonMailingPostalCode='PostCode',
            PersonMailingCity='MailCity',
            PersonEmail='mailEmailFromClass@test.com',
            Email_Verified__pc = true,
            Business_Unit__pc = 'BOAM'
        ) ;
        insert acc1;

        cont = [select id, AccountID from Contact where AccountID = :acc1.ID];
        
        Id profileId = [SELECT id, name FROM profile where UserType = 'PowerCustomerSuccess' LIMIT 1].get(0).Id;
        
        User userRec1 =  new User(
            Username = acc1.PersonEmail + '.testClassUser',
            FirstName = acc1.firstname,
            LastName = acc1.lastname,
            Email = acc1.PersonEmail,
            alias = 'test1',
            ContactId = cont.ID,
            TimeZoneSidKey = 'Australia/Sydney',
            LocaleSidKey='en_AU', 
            EmailEncodingKey='ISO-8859-1', 
            LanguageLocaleKey='en_US',
            profileId = profileId,
            Business_Unit__c = 'BOAM'
        );

        User usr = [select id from User where ID = :UserInfo.getUserID()];
        system.runAs(usr){
            insert userRec1;
        }
        
            parentCs = new Case(
                Subject = 'Case from Test Class',
                RecordTypeID = CaseUtil.parentCaseRTID,
                ContactID = cont.ID
            );
            insert parentCs;
            
            cp = new Case_Party__c(
                Case__c = parentCs.ID,
                First_Name__c = acc1.FirstName,
                Middle_Names__c = 'test',
                Last_Name__c = acc1.LastName,
                Person_Email__c = acc1.PersonEmail,
                Assessor_accreditation_number__c = '1234',
                RecordTypeID = CasePartyUtil.assessorRTID,
                Role__c = CasePartyUtil.assessorRoleName
            );
            insert cp;
            
            list<Properties__c> propList = new list<Properties__c>();
            Properties__c prop1 = new Properties__c();
            prop1.Case__c = parentCs.ID;
            prop1.Street__c = 'test street';
            prop1.City__c = 'test city';
            prop1.State__c = 'test state';
            prop1.PostCode__c = 'testcode';
            propList.add(prop1);
            
            Properties__c prop2 = new Properties__c();
            prop2.Case__c = parentCs.ID;
            prop2.Street__c = 'test street';
            prop2.City__c = 'test city';
            prop2.State__c = 'test state';
            prop2.PostCode__c = 'testcode';
            propList.add(prop2);
            insert propList;
            
            list<Property_Lots__c> lotList = new list<Property_Lots__c>();
            Property_Lots__c pl1 = new Property_Lots__c();
            pl1.Properties__c = prop1.ID;
            pl1.Case__c = parentCs.ID;
            lotList.add(pl1);
            
            Property_Lots__c pl2 = new Property_Lots__c();
            pl2.Properties__c = prop1.ID;
            pl2.Case__c = parentCs.ID;
            lotList.add(pl2);
            insert lotList;
            
        Test.startTest();
            list<Case> csList = new list<Case>();
            list<Case> csList2 = new list<Case>();
            cs = new Case(
                Subject = 'Case from Test Class',
                RecordTypeID = CaseUtil.assesmentRTID,
                ContactID = cont.ID,
                Related_Cases__c = parentCs.ID
            );
            csList.add(cs);
            
            Case app = new Case(
                Subject = 'Case from Test Class',
                RecordTypeID = CaseUtil.stwAppRTID,
                ContactID = cont.ID,
                Related_Cases__c = parentCs.ID
            );
            csList.add(app);
            
            Case agg = new Case(
                Subject = 'Case from Test Class',
                RecordTypeID = CaseUtil.agreementRTID,
                ContactID = cont.ID,
                Related_Cases__c = parentCs.ID
            );
            csList.add(agg);
            insert csList;
            
            list<Assessed_Credit__c> acList = new list<Assessed_Credit__c>();
            Assessed_Credit__c ac1 = new Assessed_Credit__c();
            ac1.Credit_Type__c = AssessmentRestService.SpeciesCreditType;
            ac1.Species_ID__c = 123;
            ac1.Case__c = cs.ID;
            ac1.Number_of_Credits__c = 10;
            acList.add(ac1);
            
            Assessed_Credit__c ac2 = new Assessed_Credit__c();
            ac2.Credit_Type__c = AssessmentRestService.EcosystemCreditType;
            ac2.PCT_Row_ID__c = 123;
            ac2.Case__c = cs.ID;
            ac2.Number_of_Credits__c = 10;
            acList.add(ac2);
            insert acList;
            
            csList2.add(app);
            csList2.add(agg);
            delete csList2;

            csList = new list<Case>();
            
            app = new Case(
                Subject = 'Case from Test Class',
                RecordTypeID = CaseUtil.stwAppRTID,
                ContactID = cont.ID,
                Related_Cases__c = parentCs.ID
            );
            csList.add(app);
            agg = new Case(
                Subject = 'Case from Test Class',
                RecordTypeID = CaseUtil.agreementRTID,
                ContactID = cont.ID,
                Related_Cases__c = parentCs.ID
            );
            csList.add(agg);
            insert csList;
			
			Properties__c prop3 = new Properties__c();
            prop3.Case__c = parentCs.ID;
            prop3.Street__c = 'test street';
            prop3.City__c = 'test city';
            prop3.State__c = 'test state';
            prop3.PostCode__c = 'testcode';
            insert prop3;
            
            Property_Lots__c pl3 = new Property_Lots__c();
            pl3.Properties__c = prop3.ID;
            pl3.Case__c = parentCs.ID;
            insert pl3;
            
            app.Status = 'Approved';
            update app;
            
            agg.Status = 'Issued';
            update agg;

        Test.stopTest();
    }

    static testmethod void testCreditTransfer(){
        PortalUserSharingUtil.executeSharingSetup = false;
        ID personAccRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

        acc1 = new Account(
            RecordTypeID=personAccRecTypeId,
            FirstName='FirstName ',
            LastName='LastName from Test Class',
            PersonMailingStreet='Mail Street',
            PersonMailingPostalCode='PostCode',
            PersonMailingCity='MailCity',
            PersonEmail='mailEmailFromClass@test.com',
            Email_Verified__pc = true,
            Business_Unit__pc = 'BOAM'
        ) ;
        insert acc1;

        cont = [select id, AccountID from Contact where AccountID = :acc1.ID];
        
        Id profileId = [SELECT id, name FROM profile where UserType = 'PowerCustomerSuccess' LIMIT 1].get(0).Id;
        
        User userRec1 =  new User(
            Username = acc1.PersonEmail + '.testClassUser',
            FirstName = acc1.firstname,
            LastName = acc1.lastname,
            Email = acc1.PersonEmail,
            alias = 'test1',
            ContactId = cont.ID,
            TimeZoneSidKey = 'Australia/Sydney',
            LocaleSidKey='en_AU', 
            EmailEncodingKey='ISO-8859-1', 
            LanguageLocaleKey='en_US',
            profileId = profileId,
            Business_Unit__c = 'BOAM'
        );

        Case agg = new Case(
            Subject = 'Case from Test Class',
            RecordTypeID = CaseUtil.agreementRTID,
            ContactID = cont.ID
        );
        insert agg;
        
        Credit_Holding__c ch = new Credit_Holding__c();
        ch.Agreement__c = agg.ID;
        insert ch;
        
        list<Assessed_Credit__c> acList = new list<Assessed_Credit__c>();
        Assessed_Credit__c ac1 = new Assessed_Credit__c();
        ac1.Credit_Type__c = AssessmentRestService.SpeciesCreditType;
        ac1.Species_ID__c = 123;
        ac1.Case__c = agg.ID;
        ac1.Number_of_Credits__c = 10;
        ac1.Approved_Credits__c = 10;
        ac1.Credit_Holding__c = ch.ID;
        acList.add(ac1);
        
        Assessed_Credit__c ac2 = new Assessed_Credit__c();
        ac2.Credit_Type__c = AssessmentRestService.EcosystemCreditType;
        ac2.PCT_Row_ID__c = 123;
        ac2.Case__c = agg.ID;
        ac2.Number_of_Credits__c = 10;
        ac2.Approved_Credits__c = 10;
        ac2.Credit_Holding__c = ch.ID;
        acList.add(ac2);
        insert acList;
        
        cp = new Case_Party__c(
            Case__c = agg.ID,
            First_Name__c = acc1.FirstName,
            Middle_Names__c = 'test',
            Last_Name__c = acc1.LastName,
            Person_Email__c = acc1.PersonEmail,
            Assessor_accreditation_number__c = '1234',
            RecordTypeID = CasePartyUtil.landholderRTID,
            Role__c = CasePartyUtil.landholderRoleName,
            Credit_Holding__c = ch.ID
        );
        insert cp;
        
        Test.startTest();
            Case tfCase = new Case(
                Subject = 'Case from Test Class',
                RecordTypeID = CaseUtil.transferCaseRTID,
                ContactID = cont.ID
            );
            insert tfCase;
            
            list<Credit_Transaction__c> ctxList = [select id from Credit_Transaction__c where Case__c = :tfCase.ID];
            system.assertEquals(2, ctxList.size());
            ctxList[0].Number_To_Transfer_Retire__c = 1;
            update ctxList[0];
            
            cp = new Case_Party__c(
                Case__c = tfCase.ID,
                First_Name__c = acc1.FirstName,
                Middle_Names__c = 'test',
                Last_Name__c = acc1.LastName,
                Person_Email__c = acc1.PersonEmail,
                Assessor_accreditation_number__c = '1234',
                RecordTypeID = CasePartyUtil.creditBuyerRTID,
                Role__c = CasePartyUtil.creditBuyerRoleName
            );
            insert cp;
        
            tfCase.Status = 'Submitted';
            update tfCase;
            
            tfCase.Status = 'Approved';
            update tfCase;
            
            tfCase.Status = 'Refused';
            update tfCase;
            
        Test.stopTest();
    }
}
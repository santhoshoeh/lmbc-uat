/**
 * Created by junzhou on 16/6/17.
 */

public with sharing class SelectionController {
    //Get Account Organisation Type Picklist Values
    @AuraEnabled
    public static List<SelectItem> getOrganisationType() {
        List<SelectItem> selectItems = new List<SelectItem>();

        SelectItem defaultItem = new SelectItem();
        defaultItem.label = '--Please Select Organisation Type--';
        defaultItem.value = '';
        selectItems.add(defaultItem);

        Schema.DescribeFieldResult fieldResult = Account.Organisation_Type__c.getDescribe();
        List<Schema.PicklistEntry> picklistValues = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry picklistValue: picklistValues) {
            SelectItem selectItem = new SelectItem();
            selectItem.label = picklistValue.getLabel();
            selectItem.value = picklistValue.getValue();
            selectItems.add(selectItem);
        }
        return selectItems;
    }

    //Get Training Course Picklist Values
    @AuraEnabled
    public static List<SelectItem> getTrainingCourses() {
        List<SelectItem> selectItems = new List<SelectItem>();

        List<Training_Modules__c> trainingModules = TrainingModuleSelector.getTrainingModules();
        if(trainingModules != null && !trainingModules.isEmpty()) {
            for(Training_Modules__c trainingModule : trainingModules) {
                selectItem selectItem = new SelectItem();
                selectItem.label = trainingModule.Name;
                selectItem.value = trainingModule.Id;
                selectItem.compulsory = trainingModule.Compulsory__c;
                selectItems.add(selectItem);
            }
        }

        SelectItem defaultItem = new SelectItem();
        defaultItem.label = 'Other';
        defaultItem.value = 'Other';
        defaultItem.compulsory = false;
        selectItems.add(defaultItem);
//        Schema.DescribeFieldResult fieldResult = Training__c.Training_Courses__c.getDescribe();
//        List<Schema.PicklistEntry> picklistValues = fieldResult.getPicklistValues();
//        for (Schema.PicklistEntry picklistValue: picklistValues) {
//            SelectItem selectItem = new SelectItem();
//            selectItem.label = picklistValue.getLabel();
//            selectItem.value = picklistValue.getValue();
//            selectItems.add(selectItem);
//        }
        return selectItems;
    }

    //Get Work locality Picklist Values
    @AuraEnabled
    public static List<SelectItem> getWorklocality() {
        List<SelectItem> selectItems = new List<SelectItem>();

        SelectItem defaultItem = new SelectItem();
        defaultItem.label = '--Please Select Work Locality--';
        defaultItem.value = '';
        selectItems.add(defaultItem);

        Schema.DescribeFieldResult fieldResult = Account.Work_locality__c.getDescribe();
        List<Schema.PicklistEntry> picklistValues = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry picklistValue: picklistValues) {
            SelectItem selectItem = new SelectItem();
            selectItem.label = picklistValue.getLabel();
            selectItem.value = picklistValue.getValue();
            selectItems.add(selectItem);
        }
        return selectItems;
    }

    public class SelectItem {
        @AuraEnabled
        public String label {get; set;}
        @AuraEnabled
        public String value {get; set;}
        @AuraEnabled
        public Boolean selected {get; set;}
        @AuraEnabled
        public Boolean compulsory {get; set;}
    }
}
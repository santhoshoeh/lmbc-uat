@isTest
private class ApplicationLogUtilityTest{
    static testmethod void testAllMethods(){
        Test.StartTest();
            String sourceClass = ApplicationLogUtilityTest.Class.getName();
            String sourceFunction = 'recalculateCaseSharing';
            String logMessage = 'test message';
            String referenceID = 'test ref ID';
            String referenceInfo = 'rest ref Info';
            String payLoad = 'payload from apex class';
            String logCode = 'test log code';
            Long timeTaken = 1234;
            ApplicationLogUtility.logError(sourceClass, sourceFunction, null, logMessage, referenceID, referenceInfo, payLoad, logCode, timeTaken);
            ApplicationLogUtility.logInfo(sourceClass, sourceFunction, logMessage, referenceID, referenceInfo, payLoad, logCode, timeTaken);
            ApplicationLogUtility.logWarn(sourceClass, sourceFunction, logMessage, referenceID, referenceInfo, payLoad, logCode, timeTaken);
            ApplicationLogUtility.logDebug(sourceClass, sourceFunction, logMessage, referenceID, referenceInfo, payLoad, logCode, timeTaken);
            ApplicationLogUtility.commitLog();
        Test.StopTest();
    }
}
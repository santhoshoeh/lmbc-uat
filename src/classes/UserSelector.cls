/**
 * Created by junzhou on 6/6/17.
 */

public with sharing class UserSelector {
    public List<User> getTrainingProviderUsers() {
        Profile trainingProfile = ProfileSelector.trainingProviderProfile;
        List<User> trainingProviderUsers = [
                SELECT Id
                FROM User
                Where ProfileId =: trainingProfile.Id
                And isActive=true
        ];
        return trainingProviderUsers;
    }

    public static User getUser() {
        User user = [
                SELECT Id, Username, ContactId, Email
                FROM User
                WHERE Id = :UserInfo.getUserId()
        ];
        return user;
    }
}
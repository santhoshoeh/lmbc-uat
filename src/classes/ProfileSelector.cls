/**
 * Created by junzhou on 6/6/17.
 */

public with sharing class ProfileSelector {
    public static Profile trainingProviderProfile{
        get{
            if(trainingProviderProfile ==null) {
                trainingProviderProfile = [
                        SELECT id, Name, UserType
                        FROM   Profile
                        WHERE  Name ='~ Training Provider Community User'
                ];
            }
            return trainingProviderProfile;
        }
    }

    public static Profile accreditedAssessorProfile{
        get{
            if(accreditedAssessorProfile ==null) {
                accreditedAssessorProfile = [
                        SELECT id, Name, UserType
                        FROM   Profile
                        WHERE  Name ='~ Accredited Assessor User'
                ];
            }
            return trainingProviderProfile;
        }
    }
}
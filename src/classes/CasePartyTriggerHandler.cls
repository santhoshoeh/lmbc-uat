/*--- Trigger Handler class for the Case_Party__c Object ---*/
/* Description:     This class will contain methods which will contain all the logic for trigger events.
*                   This is recommended to control the order of execution of logic for the same event.
*                   This trigger also uses trigger flags logic to allow easy deactivation of trigger modules using custom settings.
*/
public class CasePartyTriggerHandler{

    private boolean isExecuting = false;
    private integer BatchSize = 0;
    private list<Case_Party__c> trgOld;
    private list<Case_Party__c> trgNew;
    private map<ID, Case_Party__c> trgOldMap;
    private map<ID, Case_Party__c> trgNewMap;

    private static set<ID> triggeredSetBeforeUpdate = new set<ID>();
    private static set<ID> triggeredSetAfterUpdate = new set<ID>();
    
    public static final String personAccountRecTypeName;
    public static final ID personAccountRecTypeID;
    
    // Static block to populate variables for usage by trigger methods
    // Executing as part of static block so that repeated executions of the trigger do not rerun this code
    static{
        personAccountRecTypeName = 'Person Account';
        personAccountRecTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(personAccountRecTypeName).getRecordTypeId();
    }

    // Method to reset the processed records cache FROM the trigger variables.
    // This will especially be useful in test classes to perform repeated executions.
    public void resetTriggerState(){
        triggeredSetBeforeUpdate.clear();
        triggeredSetAfterUpdate.clear();
    }

    // Method to log the processed records. This will be used to prevent recursion
    private void computeTriggeredSet(list<Case_Party__c> recList, set<ID> triggeredSet){
        for(Case_Party__c rec: recList){
            triggeredSet.add(rec.id);
        }
    }

    // Constructor that will be called FROM trigger
    public CasePartyTriggerHandler(boolean isExecuting, integer size, list<Case_Party__c> trgOld, list<Case_Party__c> trgNew, map<ID, Case_Party__c> trgOldMap, map<ID, Case_Party__c> trgNewMap){
        this.isExecuting = isExecuting;
        this.BatchSize = size;
        this.trgOld = trgOld;
        this.trgNew = trgNew;
        this.trgOldMap = trgOldMap;
        this.trgNewMap = trgNewMap;
    }

    public void OnBeforeInsert(){
        tagPersonAccount(trgNew, trgOldMap, false);
    }

    public void OnAfterInsert(){
        recalculateSharing(trgNew, trgOldMap, false);
        syncCaseParty(trgNew, trgOldMap, false);
        
    }

    public void OnBeforeUpdate(){
        tagPersonAccount(trgNew, trgOldMap, true);

        computeTriggeredSet(trgNew, triggeredSetBeforeUpdate);
    }

    public void OnAfterUpdate(){
        recalculateSharing(trgNew, trgOldMap, true);

        computeTriggeredSet(trgNew, triggeredSetAfterUpdate);
    }

    public void OnBeforeDelete(){
    }

    public void OnAfterDelete(){
        recalculateSharing(trgOld, trgOldMap, false);
    }
    
    public void OnAfterUndelete(){
        recalculateSharing(trgNew, trgOldMap, false);
    }

    private void tagPersonAccount(list<Case_Party__c> trgNew, map<ID, Case_Party__c> trgOldMap, Boolean IsUpdate){
        //storing valid Party records
        List<Case_Party__c> PartyListVar = new List<Case_Party__c>();
        for(Case_Party__c pty : trgNew){
            if(IsUpdate){
                if(trgOldMap.get(pty.Id).Person_Email__c != pty.Person_Email__c ){
                    PartyListVar.add(pty);
                }
            }
            else{
                PartyListVar.add(pty);
            }
        }

        // calling trigger handler to perform account id update on Party records
        CasePartyTriggerHandler.updateAccountId(PartyListVar);
    }

    public static void updateAccountId(List<Case_Party__c> PartyListVar){
        // map to store email and corresponding Account
        Map<String,Id> emailToAccountIdMap = new Map<String,Id>();
        // Set to store all emails from Party being inserted/updated
        map<String, Case_Party__c> PartyMap = new map<String, Case_Party__c>();
        // fetching all emails from Party records
        for(Case_Party__c pty : PartyListVar){
            if(pty.Person_Email__c != null)
                PartyMap.put(pty.Person_Email__c.toLowerCase(), pty);
            else
                pty.PersonAccount__c = null;
        }
        try{
            set<String> emailsToCreate = new set<String>();
            emailsToCreate.addAll(PartyMap.KeySet());
            // getting all accounts based on email address received from Party records
            List<Account> accList = [SELECT ID, PersonEmail from Account WHERE PersonEmail IN :PartyMap.KeySet() ORDER BY LastModifiedDate ];

            // creating Map of email and corresponding Account Id
            for(Account accObj : accList){
                emailToAccountIdMap.put(accObj.PersonEmail.toLowerCase(),accObj.Id);
            }
            emailsToCreate.removeAll(emailToAccountIdMap.KeySet());
            list<Account> newAccounts = new list<Account>();
            for(String email : emailsToCreate){
                Account acc = new Account();
                acc.RecordTypeID = CasePartyTriggerHandler.personAccountRecTypeID;
                acc.PersonEmail = email;
                acc.LastName = PartyMap.get(email).Last_Name__c;
                acc.FirstName = PartyMap.get(email).First_Name__c;
                if(acc.LastName != null)
                    newAccounts.add(acc);
            }
            insert newAccounts;
            for(Account acc : newAccounts){
                emailToAccountIdMap.put(acc.PersonEmail.toLowerCase(),acc.Id);
            }
            system.debug('emailToAccountIdMap = ' + emailToAccountIdMap );
            
            // storing accound id on Party object records
            for( Case_Party__c pty : PartyListVar ){
                if(pty.Person_Email__c != null)
                    pty.PersonAccount__c = emailToAccountIdMap.get(pty.Person_Email__c.toLowerCase());
            }
        }
        catch(Exception ex){
            String sourceClass = CasePartyTriggerHandler.Class.getName();
            String sourceFunction = 'updateAccountId';
            String logMessage = ex.getMessage();
            String referenceID;
            String referenceInfo;
            String payLoad = '';
            for( Case_Party__c pty : PartyListVar ){
                payLoad += 'ID = '+String.ValueOf(pty.ID);
            }
            String logCode;
            Long timeTaken;

            ApplicationLogUtility.logError( sourceClass, sourceFunction, ex, logMessage, referenceID, referenceInfo, payLoad, logCode, timeTaken);
            ApplicationLogUtility.commitLog();
            system.debug('Please contact you administrator. Exception occurred '+ ex.getMessage() );
        }
    }

    private void recalculateSharing(list<Case_Party__c> trgNew, map<ID, Case_Party__c> trgOldMap, Boolean IsUpdate){
        //storing valid Party records
        set<ID> accountIds = new set<ID>();
        for(Case_Party__c pty : trgNew){
            if(IsUpdate){
                ID oldAccID = trgOldMap.get(pty.Id).PersonAccount__c;
                if(pty.PersonAccount__c != oldAccID){
                    if(pty.PersonAccount__c != null)
                        accountIds.add(pty.PersonAccount__c);
                    if(oldAccID != null)
                        accountIds.add(oldAccID);
                }

                ID oldCaseID = trgOldMap.get(pty.Id).Case__c;
                if(pty.Case__c != oldCaseID){
                    accountIds.add(pty.PersonAccount__c);
                }
                
                String oldRole = trgOldMap.get(pty.Id).Role__c;
                if(pty.Role__c != oldRole){
                    accountIds.add(pty.PersonAccount__c);
                }
            }
            else{
                if(pty.PersonAccount__c != null){
                    accountIds.add(pty.PersonAccount__c);
                }
            }
        }
        system.debug('@@@@@@@@@@ accountIds = '+accountIds);
        PortalUserSharingUtil.recalculateSharingByAccountID(accountIds);
    }
    
    
    /* This method is used to copy the Case party from parent case to child case whenver case is created
     * Case Party will be added to parent case only
     * Query the child cases and add case party record
     * - Suraj
     */ 
    public void syncCaseParty(list<Case_Party__c> trgNew, map<ID, Case_Party__c> trgOldMap, Boolean IsUpdate){
        system.debug(' entry point syncCaseParty');
        
        //get the parent case from case party
        Map<Id, Case_Party__c> caseIdMap = new Map<Id, Case_Party__c>();
        for(Case_Party__c thisElem : trgNew){
            caseIdMap.put(thisElem.Case__c, thisElem);
        }
        
        List<Case_Party__c> casePartyList = new List<Case_Party__c>();
        
        //loop through the parent cases and get child details
        for(Case thisCase : [Select Id, Related_Cases__c 
                                FROM Case 
                                WHERE Related_Cases__c IN: caseIdMap.keySet()]){
                                    
            Case_Party__c newCaseParty = caseIdMap.get(thisCase.Related_Cases__c).clone(); 
            newCaseParty.Case__c = thisCase.Id;
            newCaseParty.Parent_Case_Party__c = caseIdMap.get(thisCase.Related_Cases__c).Id;
            casePartyList.add(newCaseParty);                
        }
        
        //insert case parties
        if(casePartyList != null){
            insert casePartyList;
        }
    }

}
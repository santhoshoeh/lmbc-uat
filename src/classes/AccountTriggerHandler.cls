/* This class contains method for Account trigger logic
 * - Suraj
 * 25 Sept 2017
 */ 
public class AccountTriggerHandler {

    //update user assessor flag
    //change the user profile to Approved accredit assessor
    //verify if Acount is approved && has a valid assessor no. && has valid date
    public static void updateUserAsAssessor(List<Account> accntList, Map<Id, Account> oldAccntMap){
        
        Set<Id> accntIdSet = new Set<Id>();
            
        for(Account thisAccnt : accntList){
       		system.debug('Accredited_Assessor__pc ' + thisAccnt.Accredited_Assessor__pc);
       		system.debug('Valid_To__pc ' + thisAccnt.Valid_To__pc);
       		system.debug('Valid_From__pc ' +thisAccnt.Valid_From__pc);
       		system.debug('thisAccnt Assessor_Certification_Number__pc' + thisAccnt.Assessor_Certification_Number__pc);
        	//check if Asessor account has been Approved
            if(thisAccnt.Accredited_Assessor__pc && 
               (oldAccntMap != Null && oldAccntMap.get(thisAccnt.Id).Accredited_Assessor__pc != thisAccnt.Accredited_Assessor__pc 
               		|| oldAccntMap == Null)//for insert event olmap will be null
              	&& thisAccnt.Assessor_Certification_Number__pc != null && thisAccnt.Valid_From__pc != null
              	&& thisAccnt.Valid_To__pc != null && thisAccnt.Valid_To__pc > System.today()){
                   
                accntIdSet.add(thisAccnt.Id);    
                    

        	}
        }
        
        //update user records
        if(accntIdSet.size() > 0){
        	profile assessorProfile = [SELECT ID FROM profile WHERE name='~ Accredited Assessor'];//00e0l000000DgZaAAK
            updateUser(accntIdSet, assessorProfile.Id);
        }
    }
    
    
    //future method to prevent mixed dml operation
    @future 
    public static void updateUser(set<Id> accntIdSet, Id profileId)
    {
		List<User> userListToUpdate = new List<User>();
        
        //Set user flag as approved and change the profile
		for(User thisUser : [SELECT Id, profileID, Name,FirstName, LastName, AccountId
              					 FROM User WHERE AccountId IN: accntIdSet])
        {
            system.debug('userRec ' + thisUser);
            thisUser.profileID = profileId;
            thisUser.IsAssessor__c = true;
            userListToUpdate.add(thisUser);
        }
        
        if(userListToUpdate.size() > 0){
            if(! Test.isRunningTest())// cant test this in test class as it throws mixed dml operation error
			update userListToUpdate;        
        }

    }
}
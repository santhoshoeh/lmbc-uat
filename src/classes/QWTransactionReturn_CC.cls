public without sharing class QWTransactionReturn_CC {

    public String transId{get;set;}

    public String summaryCode;
    public String responseDescription{get;set;}
    public String summaryDescription{get;set;}
    public LIST<ParameterWrapper> paramList{get;set;}
    //public String isInConsole {get;set;}
    public String consoleVal;

    public String redirectUrl {get; set;}

    public Boolean shouldRedirect {public get; private set;}


    public QWTransactionReturn_CC() {
        paramList = new LIST<ParameterWrapper>();
        Map <String, String> params = ApexPages.currentPage().getParameters();
        Map <String, String> SummaryDesc = new Map <String, String>{
                '0' => 'Approved by financial institution',
                '1' => 'Declined by financial institution',
                '2' => 'Unknown/pending',
                '3' => 'Rejected'
        };
        Transaction__c tran = TransactionSelector.getTransactionByReferenceNumber(params.get('paymentReference'));
        //OppId = params.get('id');
        summaryCode = params.get('summaryCode');
        summaryDescription = summaryDesc.get(summaryCode);
        responseDescription = summaryDesc.get(summaryCode) +' - '+params.get('responseDescription');

        paramList.add(new ParameterWrapper('Summary Description', summaryDesc.get(params.get('summaryCode'))));
        paramList.add(new ParameterWrapper('Payment Amount', '$ '+params.get('paymentAmount')));
        paramList.add(new ParameterWrapper('Receipt Number', params.get('receiptNumber')));

        paramList.add(new ParameterWrapper('Response Description',params.get('responseDescription')));
        paramList.add(new ParameterWrapper('Created Date Time', params.get('createdDateTime')));
        paramList.add(new ParameterWrapper('Masked Card Number',params.get('maskedCardNumber')));
        paramList.add(new ParameterWrapper('Card Scheme', params.get('cardScheme')));
        paramList.add(new ParameterWrapper('Reference Number', params.get('customerReferenceNumber')));
        //Create redirect url
        string baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        //updateTransaction(params);
        redirectUrl = baseURL + '/baas/s/case/'+tran.Case__c;

    }

    //public void updateTransaction()
    //{
    //    map <String, String> params = ApexPages.currentPage().getParameters();
    //    Transaction__c trans = new Transaction__c();
    //    trans.Reference_Number__c = params.get('customerReferenceNumber');
    //    trans.Summary_Code__c = params.get('summaryCode');
    //    trans.Payment_Amount__c = Decimal.valueOf(params.get('paymentAmount'));
    //    trans.Receipt_Number__c = params.get('receiptNumber');
    //    trans.Response_Description__c = params.get('responseDescription');
    //    trans.Surcharge_Amount__c = Decimal.valueOf(params.get('surchargeAmount'));
    //    trans.Card_Scheme__c = params.get('cardScheme');
    //    string paymentDate = params.get('settlementDate');
    //    trans.Payment_Date__c = Date.newInstance(Integer.valueOf(paymentDate.substring(0,4)),
    //            Integer.valueOf(paymentDate.substring(4,6)),
    //            Integer.valueOf(paymentDate.substring(6,8)));

    //    if (params.get('summaryCode') == '0')
    //    {
    //        trans.Payment_Status__c = SystemConstants.CDS_PAID_STATUS;
    //    }
    //    upsert trans Reference_Number__c;

    //}

    public class ParameterWrapper {
        public String key {get;set;}
        public String value {get;set;}

        public ParameterWrapper(String pKey, String pValue) {
            key = pKey;
            value = pValue;
        }
    }
}
/**
 * Created by junzhou on 29/6/17.
 */

public without sharing class CaseSelector {
    private static String QUERY_STRING = 'Select id, Status, Business_Unit__c from Case';
    private static String PAYMENT_QUERY_STRING = 'SELECT Id, Status, Subscription_fee_payment_status__c, Certification_fee_payment_status__c from Case';

    public static Case getCaseDetails(Id caseId) {
        String queryString  = QUERY_STRING + ' WHERE Id =: caseId';
        System.debug('queryString ='+ queryString);
        Case submittedCase = Database.query(queryString);
        return submittedCase;
    }

    public static Case getCasePaymentDetails(Id caseId) {
        String queryString  = PAYMENT_QUERY_STRING + ' WHERE Id =: caseId';
        System.debug('queryString ='+ queryString);
        Case submittedCase = Database.query(queryString);
        return submittedCase;
    }

    public static List<Case> getCasesPaymentDetails(List<Id> caseIds) {
        String queryString  = PAYMENT_QUERY_STRING + ' WHERE Id in: caseIds';
        System.debug('queryString ='+ queryString);
        system.debug('caseIds ='+ caseIds);
        List<Case> cases = Database.query(queryString);
        return cases;
    }
}
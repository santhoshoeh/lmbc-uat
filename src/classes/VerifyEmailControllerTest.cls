@isTest
private class VerifyEmailControllerTest
{
	@testSetup
	static void createPersonAccount() {
		TestDataGenerator.DefaultDummyRecordSet rs = TestDataGenerator.createDefaultDummyRecordSet();
	}
	@isTest
	static void testUpdateEmailVerifiedFlag() {
		Account personAccount = [SELECT Id from Account where PersonEmail = 'PersonAccount_0@baastest.com'];
		Test.setCurrentPage(Page.VerifyEmailPage);
		ApexPages.currentPage().getParameters().put('customerid', personAccount.Id);
		VerifyEmailController vEmailController = new VerifyEmailController();
		
		Test.startTest();
		VerifyEmailController.updateEmailVerifiedFlag();
		system.assert(VerifyEmailController.messageStatus);

		ApexPages.currentPage().getParameters().put('customerid', '');
		VerifyEmailController v1EmailController = new VerifyEmailController();
		
		VerifyEmailController.updateEmailVerifiedFlag();
		system.assert(!VerifyEmailController.messageStatus);
		Test.stopTest();
	}
}
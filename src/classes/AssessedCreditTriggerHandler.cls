public class AssessedCreditTriggerHandler{
    public static void populateCaseLookups(list<Assessed_Credit__c> recList){
        set<ID> assessmentIDs = new set<ID>();
        for(Assessed_Credit__c ascr : recList){
            assessmentIDs.add(ascr.Case__c);
        }
        if(!assessmentIDs.IsEmpty()){
            map<ID, ID> assesToParentMap = new map<ID, ID>();
            list<Case> parentCases = [select Related_Cases__c from Case where ID in :assessmentIDs];
            if(!parentCases.IsEmpty()){
                for(Case cs : parentCases){
                    if(cs.Related_Cases__c != null)
                        assesToParentMap.put(cs.ID, cs.Related_Cases__c);
                }
            }
            if(!assesToParentMap.IsEmpty()){
                map<ID, Case> parentCaseMap = new map<ID, Case>([SELECT ID, (select ID, RecordTypeID FROM Child_Cases__r WHERE RecordTypeID IN :CaseUtil.boamAppsAndAgreementRTIDs) FROM Case WHERE ID = :assesToParentMap.Values()]);
                for(Assessed_Credit__c ascr : recList){
                    ID parentCaseID = assesToParentMap.get(ascr.Case__c);
                    if(parentCaseID != null){
                        ascr.Related_Parent_Case__c = parentCaseID;
                        Case parentCase = parentCaseMap.get(parentCaseID);
                        for(Case child : parentCase.Child_Cases__r){
                            if(CaseUtil.boamApplicationRTIDs.contains(child.RecordTypeID)){
                                ascr.Related_Application__c = child.ID;
                            }else if(CaseUtil.agreementRTID == child.RecordTypeID){
                                ascr.Related_Agreement__c = child.ID;
                            }
                        }
                    }
                }
            }
        }
    }
}
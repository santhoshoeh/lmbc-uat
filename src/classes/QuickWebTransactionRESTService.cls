@RestResource(urlMapping='/QWebTransactions/*')
global without sharing class QuickWebTransactionRESTService
{

  /*------------------------------------------------------------
  Author:         Santhosh
  Company:        System Partners
  Description:    Service class for QuickWeb notification;Generic class for transaction object;
  Inputs:         n/a
  Test Class:     QuickWebTransactionRESTService_Test;
  History
  <Date>          <Authors Name>      <Brief Description of Change>
  07-Feb-17       Santhosh            Created
  ------------------------------------------------------------*/

    static final Integer HTTP_OK = 200;
    static final Integer HTTP_INTERNAL_SERVER_ERROR = 499;

    public QuickWebTransactionRESTService() {

    }
    @HttpPost
    global static String createTransaction()
    {
        RestRequest req = RestContext.request;
        RestResponse res = Restcontext.response;

        Transaction__c trans = new Transaction__c();

        try
        {
            //system.debug('returned request body is; '+ req.requestBody);
            res.statusCode = HTTP_OK;

            Blob rBody = req.requestBody;

            if (rBody != null)
            {
                //read the dom object and get the response;
                trans = createTransactionFromXML(rBody.toString(), trans);
            }
            else
            {
                //read the response from req.parameters; */
                trans = createTransactionFromParameters(req, trans);
            }

            if (trans != null)
            {
                upsert trans Reference_Number__c;
            }

        }
        catch (Exception e) {
            res.statusCode = HTTP_INTERNAL_SERVER_ERROR;
            //return String.valueOf(res.statusCode);
            return String.valueOf(e.getMessage());
        }

        res.responseBody = Blob.valueOf('' + res.statusCode);
        return String.valueOf(res.statusCode);

    }



    public static Transaction__c createTransactionFromParameters(RestRequest req, Transaction__c transactionRec)
    {
        system.debug('req ='+ req);
        Map<string,string> reqParams = req.params;
        system.debug('reqParams ='+ reqParams);
        transactionRec.Receipt_Number__c = reqParams.get('receiptNumber');
        transactionRec.Reference_Number__c = reqParams.get('customerReferenceNumber');
        transactionRec.Payment_Amount__c = Decimal.valueOf(reqParams.get('paymentAmount'));
        transactionRec.Surcharge_Amount__c = Decimal.valueOf(reqParams.get('surchargeAmount'));
        transactionRec.Card_Scheme__c = reqParams.get('cardScheme');

        string paymentDate = reqParams.get('settlementDate');

        transactionRec.Payment_Date__c = Date.newInstance(Integer.valueOf(paymentDate.substring(0,4)),
                Integer.valueOf(paymentDate.substring(4,6)),
                Integer.valueOf(paymentDate.substring(6,8)));

        transactionRec.Response_Code__c = reqParams.get('responseCode');

        transactionRec.Response_Description__c = reqParams.get('responseDescription');

        transactionRec.Summary_Code__c = reqParams.get('summaryCode');

        if (reqParams.get('summaryCode') == '0')
        {
            transactionRec.Payment_Status__c = SystemConstants.CDS_PAID_STATUS;
        }

        return transactionRec;

    }

    public static Transaction__c createTransactionFromXML(string xml, Transaction__c transactionRec)
    {

	   system.debug('xml is: '+ xml);
        DOM.Document doc = new DOM.Document();
        doc.load(xml);
        DOM.XmlNode root = doc.getRootElement();

        //transactionRec.Reference_Number__c = root.getChildElement('customerReferenceNumber', null).getText();
	   transactionRec.Reference_Number__c = root.getChildElement('paymentReference', null).getText();

        transactionRec.Receipt_Number__c = root.getChildElement('receiptNumber', null).getText();
        transactionRec.Payment_Amount__c = Decimal.valueOf(root.getChildElement('paymentAmount', null).getText());

        transactionRec.Surcharge_Amount__c = Decimal.valueOf(root.getChildElement('surchargeAmount', null).getText());

        transactionRec.Card_Scheme__c = root.getChildElement('cardScheme', null).getText();

        string paymentDate = root.getChildElement('settlementDate', null).getText();

        transactionRec.Payment_Date__c = Date.newInstance(Integer.valueOf(paymentDate.substring(0,4)),
                Integer.valueOf(paymentDate.substring(4,6)),
                Integer.valueOf(paymentDate.substring(6,8)));

        transactionRec.Response_Code__c = root.getChildElement('responseCode', null).getText();

        transactionRec.Response_Description__c = root.getChildElement('responseDescription', null).getText();

        transactionRec.Summary_Code__c = root.getChildElement('summaryCode', null).getText();

        boolean successflg = Boolean.valueOf(root.getChildElement('successFlag', null).getText());
        if (successflg)
        {
            transactionRec.Payment_Status__c = SystemConstants.CDS_PAID_STATUS;
        }
        else
        {
            transactionRec.Payment_Status__c = SystemConstants.CDS_PAYMENT_NOT_PROCESSED;
        }

        return transactionRec;


    }

    //Doesn't find any place to use it.
    //private static DateTime StringToDateTime(string dateValue)
    //{
    //    Map<String,Integer> monthNames = new Map<String,Integer>{'Jan'=>1, 'Feb'=>2, 'Mar'=>3, 'Apr'=>4,
    //    'May'=>5, 'Jun'=>6, 'Jul'=>7, 'Aug'=>8, 'Sep'=>9, 'Oct'=>10, 'Nov'=>11, 'Dec'=>12};
    //    List<String> stringParts = dateValue.split(' ');
    //    List<String> timeParts = stringParts[3].Split(':');
    //    double offset = Timezone.gettimezone('Australia/Sydney').getoffset(datetime.now())/(60*60*1000);
    //    DateTime result = DateTime.newInstanceGMT(Integer.valueOf(stringParts[2]),
    //            monthNames.get(stringParts[1]),
    //            Integer.valueOf(stringParts[0]),
    //            Integer.valueOf(timeParts[0]),
    //            Integer.valueOf(timeParts[1]),
    //            Integer.valueOf(timeParts[2])
    //    );
    //    Datetime result_GMT0;
    //    result_GMT0 = result - (offset/24.0);

    //    System.debug('convertTime '+result_GMT0);
        
    //    /*String localTime = result.format('yyyy-MM-dd HH:mm:ss','GMT-10');
    //    System.debug('localtime '+localTime);
    //    result_GMT0 = DateTime.valueOfGmt(localTime);*/
    //    return result_GMT0;
    //}


}
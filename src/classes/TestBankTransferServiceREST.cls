@isTest


private class TestBankTransferServiceREST {

    static testMethod void testBankService() 
    {
        TestDataGenerator.createRecordSet(new TestDataGenerator.DefaultDummyRecordSet());
	   Account acct = [Select Id from Account where IsPersonAccount = true limit 1];
        Case cs = [Select Id from Case limit 1];
        //create transaction'
        Transaction__c trans = TestDataGenerator.createTransaction(acct.Id, 1000.00);
        trans.Reference_Number__c = 'BAAS999999';
        trans.Purchase_Item__c = 'Subscription Fee';
        trans.Case__c = cs.Id;
        insert trans;
		

		list<BankTransferServiceREST.banktransfertransaction> bankTransfers = new list<BankTransferServiceREST.banktransfertransaction>();
		BankTransferServiceREST.banktransfertransaction bankTransfer = new BankTransferServiceREST.banktransfertransaction();
		bankTransfer.referencenumber = 'BAAS999999';
        bankTransfer.paymentamount = '1000.00';
		bankTransfer.bankbranchdetails = 'Test';
		bankTransfer.accountnumber = '0020000';
		bankTransfer.statementnumber = '123444';
		bankTransfer.paymentdate = '01/01/2017';
		bankTransfer.bankdescription1 = 'Test';
		bankTransfer.bankdescription2 = 'Test';
		bankTransfer.bankdescription3 = 'Test';
		bankTransfer.bankdescription4 = 'Test';
		bankTransfer.bookingdate = '01/01/2017';
		bankTransfer.serialnumber = '111';
		bankTransfer.transactioncode = '111';
		bankTransfers.add(bankTransfer);
		String jsonPaymentDetails = JSON.serialize(bankTransfers);
 	    Test.startTest();
	    RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();
		
	    req.requestURI = '/services/apexrest/banktransfertransactions';  
	    req.httpMethod = 'POST';
	    req.requestBody = Blob.valueof(jsonPaymentDetails);
	    RestContext.request = req;
	    RestContext.response = res;
	    BankTransferServiceREST.processbanktransfer(bankTransfers);
	    system.debug(RestContext.response);
	    system.debug(res);


    }

}
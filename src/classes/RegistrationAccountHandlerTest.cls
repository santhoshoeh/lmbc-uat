@isTest
public class RegistrationAccountHandlerTest {

    //tests if person account, account is created successfully
    static testMethod void validateAccountCreationSuccess() {
        
        test.startTest();
        registrationAccountHandler.createAccount(null);
		
        String regoJSONstr = '{"firstsName": "TestFirst"}';
        registrationAccountHandler.createAccount(regoJSONstr);

        regoJSONstr = '{"firstsName": "TestFirst", "mobile": ""}';
        registrationAccountHandler.createAccount(regoJSONstr);

        regoJSONstr = '{"firstsName": "TestFirst", "emailAddress": ""}';
        registrationAccountHandler.createAccount(regoJSONstr);
        
        regoJSONstr = '{"firstName": "TestFirst","lastName": "testLast","mobile": "0451123123","phone": "0212345678","emailAddress": "test@test.com","accountName" : "TestCo","existingAssessor": "no","trainingInterested": "yes","assessorCertificationNumber": "BAAS01234", "businessUnit":"BAAS"}';
		//this method will create person account, Account and Account Contct relation for above JSON data
        registrationAccountHandler.createAccount(regoJSONstr);

        List<Account> perAccnt = [SELECT id, firstName, lastName, PersonMobilePhone, PersonEmail, recordtype.name FROM account WHERE recordtype.name = 'Person Account'];
        List<Account> buisAccnt = [SELECT id, name, lastName, PersonMobilePhone, PersonEmail, recordtype.name FROM account WHERE recordtype.name = 'Business Account'];
        List<Training__c> trainingList = [SELECT id from Training__c WHERE Account__c =: perAccnt.get(0).Id];
        
        //Assert for results
        
        //Person account should be created with provided details
        System.assert(perAccnt.size() == 1);
        System.assertEquals(perAccnt.get(0).firstName, 'TestFirst');
        System.assertEquals(perAccnt.get(0).lastName, 'testLast');
        System.assertEquals(perAccnt.get(0).PersonMobilePhone, '0451123123');
        System.assertEquals(perAccnt.get(0).PersonEmail, 'test@test.com');
        
        //Buisness account should be created
        System.assert(buisAccnt.size() == 1);
        System.assertEquals(buisAccnt.get(0).name, 'TestCo');
        
        //Verify if training is created
        System.assert(trainingList.size() == 1);
        
        String responseString = registrationAccountHandler.createAccount(regoJSONstr);
        system.assertNotEquals(responseString, null);
        test.stopTest();
        
    }
    
     
    //tests if person account, account is created successfully
    static testMethod void validateAccountCreationWithoutCompany() {
        
        test.startTest();
        
		String regoJSONstr = '{"firstName": "TestFirst","lastName": "testLast","mobile": "0451123123","phone": "0212345678","emailAddress": "test@test.com","accountName" : "","existingAssessor": "no","trainingInterested": "yes","assessorCertificationNumber": "BAAS01234", "businessUnit":"BAAS"}';
		//this method will create person account, Account and Account Contct relation for above JSON data
        registrationAccountHandler.createAccount(regoJSONstr);

        List<Account> perAccnt = [SELECT id, firstName, lastName, PersonMobilePhone, PersonEmail, recordtype.name FROM account WHERE recordtype.name = 'Person Account'];
        List<Account> buisAccnt = [SELECT id, name, lastName, PersonMobilePhone, PersonEmail, recordtype.name FROM account WHERE recordtype.name = 'Business Account'];
        List<Training__c> trainingList = [SELECT id from Training__c WHERE Account__c =: perAccnt.get(0).Id];
        //Assert for results
        
        //Person account should be created with provided details
        System.assert(perAccnt.size() == 1);
        System.assertEquals(perAccnt.get(0).firstName, 'TestFirst');
        System.assertEquals(perAccnt.get(0).lastName, 'testLast');
        System.assertEquals(perAccnt.get(0).PersonMobilePhone, '0451123123');
        System.assertEquals(perAccnt.get(0).PersonEmail, 'test@test.com');
        
        //Buisness account should be created
        System.assert(buisAccnt.size() == 0);
        
        //Verify if training is created
        System.assert(trainingList.size() == 1);
        test.stopTest();
        
    }
    
    static testMethod void communityUserLoginTest() {
        /*
        //create community account
        Account accnt = new Account(Name = 'Community Account');
        insert accnt;
        
        //create test contact
        Contact cont = new Contact(FirstName = 'TestCont', LastName = 'TestLast');
        cont.AccountId = accnt.Id;
        insert cont;
        
        //profile for community user
        Id profileId = [SELECT id, name FROM profile WHERE name = '~ Accredited Assessor Community User' LIMIT 1].get(0).Id;

        
        //create test user
        User userRec =  new User(
                            Username = 'testuser@baas.com',
                            FirstName = 'testFirst',
                            LastName = 'testLast',
                            CommunityNickname =  'testNick',
                            Email = 'test@comm.com',
                            ContactId = cont.Id,
                            Alias = 'ali', 
                            TimeZoneSidKey = 'Australia/Sydney',
                                LocaleSidKey='en_AU', 
                                EmailEncodingKey='ISO-8859-1', 
                                LanguageLocaleKey='en_US',
                            profileId = profileId,
                            Business_Unit__c = 'BAAS');
        insert userRec;*/
        
        RegistrationAccountHandler.communityUserLogin('testUser', 'testPassword', 'BAAS');
        
    }

    
    
}
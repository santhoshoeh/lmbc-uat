/*--- Trigger Handler class for the User Object ---*/
/* Description:     This class will contain methods which will contain all the logic for trigger events.
*                   This is recommended to control the order of execution of logic for the same event.
*                   This trigger also uses trigger flags logic to allow easy deactivation of trigger modules using custom settings.
*/
public class UserTriggerHandler{

    private boolean isExecuting = false;
    private integer BatchSize = 0;
    private list<User> trgOld;
    private list<User> trgNew;
    private map<ID, User> trgOldMap;
    private map<ID, User> trgNewMap;

    private static set<ID> triggeredSetBeforeUpdate = new set<ID>();
    private static set<ID> triggeredSetAfterUpdate = new set<ID>();
    
    public static final String PortalUserType;

    // Static block to populate variables for usage by trigger methods
    // Executing as part of static block so that repeated executions of the trigger do not rerun this code
    static{
        PortalUserType = 'PowerCustomerSuccess';
    }

    // Method to reset the processed records cache FROM the trigger variables.
    // This will especially be useful in test classes to perform repeated executions.
    public void resetTriggerState(){
        triggeredSetBeforeUpdate.clear();
        triggeredSetAfterUpdate.clear();
    }

    // Method to log the processed records. This will be used to prevent recursion
    private void computeTriggeredSet(list<User> recList, set<ID> triggeredSet){
        for(User rec: recList){
            triggeredSet.add(rec.id);
        }
    }

    // Constructor that will be called FROM trigger
    public UserTriggerHandler(boolean isExecuting, integer size, list<User> trgOld, list<User> trgNew, map<ID, User> trgOldMap, map<ID, User> trgNewMap){
        this.isExecuting = isExecuting;
        this.BatchSize = size;
        this.trgOld = trgOld;
        this.trgNew = trgNew;
        this.trgOldMap = trgOldMap;
        this.trgNewMap = trgNewMap;
    }

    public void OnBeforeInsert(){
    }

    public void OnAfterInsert(){
        findActivePortalUsers(trgNew, trgOldMap, true);
    }

    public void OnBeforeUpdate(){
        computeTriggeredSet(trgNew, triggeredSetBeforeUpdate);
    }

    public void OnAfterUpdate(){
        findActivePortalUsers(trgNew, trgOldMap, false);

        computeTriggeredSet(trgNew, triggeredSetAfterUpdate);
    }

    private void findActivePortalUsers(list<User> trgNew, map<ID, User> trgOldMap, Boolean isInsert){
        //storing valid User IDs
        set<ID> portalUserIds = new set<ID>();

        for(User usr : trgNew){
            if(usr.IsActive && usr.UserType == PortalUserType){
                if(isInsert){
                    portalUserIds.add(usr.ID);
                }
                else{
                    if(usr.IsActive != trgOldMap.get(usr.Id).IsActive){
                        portalUserIds.add(usr.ID);
                    }
                }
            }
        }
        // calling trigger handler to perform account id update on User records
        if(!portalUserIds.IsEmpty())
            UserTriggerHandler.futureShareCasesForPortalUser(portalUserIds);
    }

    // future method as this would be a mixed DML of User object and other non setup objects.
    @future
    public static void futureShareCasesForPortalUser(set<ID> portalUserIds){
        PortalUserSharingUtil.recalculateSharingByUserID(portalUserIds);
    }
}
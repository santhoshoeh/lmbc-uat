/*------------------------------------------------------------
Author:         Santhosh
Company:        System Partners
Description:    Utility class to help log consistent messages into the Application_Log__c object
Inputs:         logLevel        - Debug, Error, Info, Warning
                sourceClass     - Originating trigger or utility class
                sourceFunction  - Method in class above that caused the message
                ex              - the standard exception object for errors
                logMessage      - the message associated with the log record
                referenceId     - Process Identifier (e.g. Job Id)
                referenceInfo   - Process information
                payLoad         - Optional based on integration messages
                logCode         - associated custom defined error code
                timeTaken       - The time in milliseconds of the transaction              
Test Class:     TestApplicationLogUtility
History
<Date>      <Authors Name>      <Brief Description of Change>
05-Jun-17   Santhosh    created
------------------------------------------------------------*/
public class ApplicationLogUtility {

    public static final String LOGLEVEL_INFO = 'Info';
    public static final String LOGLEVEL_WARN = 'Warn';
    public static final String LOGLEVEL_ERROR = 'Error';
    public static final String LOGLEVEL_DEBUG = 'Debug';
    
    private static List<ApplicationLogWrapper> appLogWrappers = new List<ApplicationLogWrapper>();
    //to log errors;
    public static void logError(String sourceClass, 
                                String sourceFunction, 
                                Exception ex, 
                                String logMessage, 
                                String referenceID,
                                String referenceInfo,
                                String payLoad, 
                                String logCode,
                                Long timeTaken) {

        insertLog(LOGLEVEL_ERROR, sourceClass, sourceFunction, ex, logMessage, referenceID, referenceInfo, payLoad, logCode, timeTaken);
    }
    //to log info;
    public static void logInfo( String  sourceClass, 
                                String  sourceFunction, 
                                String  logMessage, 
                                String  referenceID,
                                String  referenceInfo,
                                String  payLoad, 
                                String  logCode,
                                Long    timeTaken) {

        insertLog(LOGLEVEL_INFO, sourceClass, sourceFunction, null, logMessage, referenceID, referenceInfo, payLoad, logCode, timeTaken);
    }
    //to log warn;
    public static void logWarn( String  sourceClass, 
                                String  sourceFunction, 
                                String  logMessage, 
                                String  referenceID,
                                String  referenceInfo,
                                String  payLoad, 
                                String  logCode,
                                Long    timeTaken) {

        insertLog(LOGLEVEL_WARN, sourceClass, sourceFunction, null, logMessage, referenceID, referenceInfo, payLoad, logCode, timeTaken);
    }
    //log debug logs;
    public static void logDebug(String  sourceClass, 
                                String  sourceFunction, 
                                String  logMessage, 
                                String  referenceID,
                                String  referenceInfo,
                                String  payLoad, 
                                String  logCode,
                                Long    timeTaken) {

        insertLog(LOGLEVEL_DEBUG, sourceClass, sourceFunction, null, logMessage, referenceID, referenceInfo, payLoad, logCode, timeTaken);
    }
    //insert log;
    private static void insertLog(  String logLevel, 
                                    String sourceClass, 
                                    String sourceFunction, 
                                    Exception ex, 
                                    String logMessage, 
                                    String referenceID,
                                    String referenceInfo,
                                    String payLoad, 
                                    String logCode,
                                    Long timeTaken) {
        try {
            /*
            Application_Log__c appLog = new Application_Log__c();
            if(logLevel!=null) appLog.Debug_Level__c = logLevel;
            if(logCode!=null) appLog.Log_Code__c = logCode;
            if(logMessage!=null) appLog.Message__c = logMessage;
            if(payLoad!=null) appLog.Payload__c = payLoad;
            if(referenceID!=null) appLog.Reference_ID__c = referenceID;
            if(referenceInfo!=null) appLog.Reference_Info__c = referenceInfo;
            if(sourceFunction!=null) appLog.Source_Function__c = sourceFunction;
            if(sourceClass!=null) appLog.Source__c = sourceClass;
            if(ex!=null) appLog.Stack_Trace__c = ex.getStackTraceString();
            if(timeTaken!=null) appLog.Timer__c = timeTaken;
            insert appLog;
            */
            ApplicationLogWrapper appLogWrapper = new ApplicationLogWrapper();
            if(logLevel!=null) appLogWrapper.debugLevel = logLevel;
            if(logCode!=null) appLogWrapper.logCode = logCode;
            if(logMessage!=null) appLogWrapper.message = logMessage;
            if(payLoad!=null) appLogWrapper.payLoad = payLoad;
            if(referenceID!=null) appLogWrapper.referenceId = referenceID;
            if(referenceInfo!=null) appLogWrapper.referenceInfo = referenceInfo;
            if(sourceFunction!=null) appLogWrapper.sourceFunction = sourceFunction;
            if(sourceClass!=null) appLogWrapper.source = sourceClass;
            if(ex!=null) appLogWrapper.stackTrace = ex.getStackTraceString();
            if(timeTaken!=null) appLogWrapper.timer = timeTaken;
            appLogWrappers.add(appLogWrapper);
        }
        catch(Exception e) {
            System.debug('Failed to insert Application_Log__c. ' 
                                    + ' Error = ' + e.getMessage() 
                                    + ' logLevel='+logLevel 
                                    + ' sourceClass='+sourceClass
                                    + ' sourceFunction='+sourceFunction
                                    + ' ex='+ex
                                    + ' logMessage='+logMessage
                                    + ' referenceID='+referenceID
                                    + ' referenceInfo='+referenceInfo
                                    + ' payLoad='+payLoad
                                    + ' logCode='+logCode
                                    + ' timeTaken='+timeTaken);
        }
    }
    //add to the log object;
    public static void commitLog() {
        if (!appLogWrappers.isEmpty()) {
            List<Application_Log__c> applicationLogs = new List<Application_Log__c>();
            for (ApplicationLogWrapper log : appLogWrappers) {
                applicationLogs.add(new Application_Log__c(
                    Debug_Level__c = log.debugLevel,
                    Log_Code__c = log.logCode,
                    Message__c = log.message,
                    Payload__c = log.payLoad,
                    Reference_ID__c = log.referenceId,
                    Reference_Info__c = log.referenceInfo,
                    Source__c = log.source,
                    Source_Function__c = log.sourceFunction,
                    Stack_Trace__c = log.stackTrace,
                    Timer__c = log.timer
                ));
            }
            insert applicationLogs;

        }
        
    }

    //wrapper class for application log;
    public class ApplicationLogWrapper {
        public String debugLevel {get; set;}
        public String logCode {get; set;}
        public String message {get; set;}
        public String payLoad {get; set;}
        public String referenceId {get; set;}
        public String referenceInfo {get; set;}
        public String source {get; set;}
        public String sourceFunction {get; set;}
        public String stackTrace {get; set;}
        public Long timer {get; set;}

    }
}
@isTest
private class TransactionSelectorTest
{
	static TestDataGenerator.DefaultDummyRecordSet rs = TestDataGenerator.createDefaultDummyRecordSet();
	
	@testSetup
	static void createTransactionRecord() {
		System.runAs(TestDataGenerator.Admin) {
			Transaction__c trans = new Transaction__c();
			trans.Case__c = rs.cases[0].Id;
			trans.Purchase_Item__c = 'Subscription Fee';
			trans.Payment_Status__c = 'Pending Payment';
			trans.Reference_Number__c = 'baas098787';
			insert trans;
		}
	}
	@isTest
	static void testTransactionSelector() {
		System.runAs(TestDataGenerator.Admin) {
			Transaction__c trans = TransactionSelector.getTransactionByReferenceNumber('baas098787');
			system.assertEquals(trans.Payment_Status__c, 'Pending Payment');

			trans = TransactionSelector.getTransactionById(trans.Id);
			system.assertEquals(trans.Purchase_Item__c, 'Subscription Fee');
		}
	}
}
@isTest
public class ApproveUserControllerTest{
    
    public static testmethod void testApproveUser(){
        // create test data for account
        String personAccRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        
        list<Account> accList = new list<Account>();
        
        for(Integer i = 0; i < 10; i++){
            Account acc = new Account(
                RecordTypeID=personAccRecTypeId,
                FirstName='FirstName ',
                LastName='LastName from Test Class'+String.ValueOf(i),
                PersonMailingStreet='Mail Street',
                PersonMailingPostalCode='PostCode',
                PersonMailingCity='MailCity',
                PersonEmail='mailEmailFromClass'+String.ValueOf(i)+'@test.com',
                Email_Verified__pc = true,
                Business_Unit__pc = 'BAAS'
            );
            accList.add(acc);
        }
        System.runAs(TestDataGenerator.Admin){
            insert accList;
        }

        map<ID, ID> contToAccIDMap = new map<ID, ID>();
        map<ID, Account> accountMap = new map<ID, Account>([SELECT ID, FirstName, LastName, PersonEmail, Business_Unit__pc, (SELECT Id FROM Contacts) FROM Account where ID = :accList]);
        for(Account acc : accountMap.Values()){
            contToAccIDMap.put(acc.Contacts[0].ID, acc.ID);
        }

        Test.startTest();
            System.runAs(TestDataGenerator.Admin){
                ApexPages.StandardSetController stdController = new ApexPages.StandardSetController(accList);
                ApproveUserController cont = new ApproveUserController(stdController);
                cont.selectedAccountsjson = JSON.serialize(cont.allAccounts);
                cont.updateSelectedAccounts();
                cont.ApproveUsers();
                
                cont.selectedAccountsjson = JSON.serialize(new ApproveUserController.AccountWrapper[]{});
                cont.getAccountsToApproveOrReject();
                cont.ApproveUsers();
                
                cont.userBuisUnit = 'BOAM';
                cont.ApproveUsers();
                cont.userBuisUnit = 'NVRM';
                cont.ApproveUsers();

                cont.selectedAccountsjson = JSON.serialize(new Account());
                cont.updateSelectedAccounts();
                cont.ApproveUsers();
                
                cont.userBuisUnit = null;
                cont.getAccountsToApproveOrReject();
                
                set<ID> contIDs = new set<ID>();
                list<User> userList = [select id, Username, FirstName, LastName, Email, ContactId, Business_Unit__c from User where ContactId = :contToAccIDMap.KeySet()];
                
                for(User usr : userList){
                    ID ContactID = usr.ContactID;
                    ID AccountID = contToAccIDMap.get(ContactID);
                    Account acc = accountMap.get(AccountID);
                    
                    system.assertEquals(acc.PersonEmail, usr.Username);
                    system.assertEquals(acc.FirstName, usr.FirstName);
                    system.assertEquals(acc.LastName, usr.LastName);
                    system.assertEquals(acc.PersonEmail, usr.Email);
                    system.assertEquals(acc.Business_Unit__pc, usr.Business_Unit__c);
                }
            }
        Test.stopTest();
    }
}
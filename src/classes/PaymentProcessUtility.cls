/**
 * Created by junzhou on 6/7/17.
 */

public with sharing class PaymentProcessUtility {

    public static Contact getCurrentContact(){
        User user = UserSelector.getUser();
        if (user != null)
            return getContact(user.ContactId);

        return null;
    }

    //Method to retrieve the contact record;
    public static Contact getContact(Id contactId){
        for (Contact c: [Select Id, LastName, FirstName, Email, MobilePhone, Phone, Fax, Title, Salutation, AccountId From Contact Where Id = :contactId ])
            return c;

        return null;
    }

    //Method to generate random 7 digit string for CDS Project;
    public static String generateRandomString() {
        Integer defaultLength = 6;
        final String defaultChars = '0123456';
        String randomString = '';
        while (randomString.length() < defaultLength)
        {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), defaultChars.length());
            randomString += defaultChars.substring(idx,idx+1);
        }

        return randomString;
    }
}
/**
* @author Sakshi Chauhan
* @date 10 July 2017
* @description Batch Class to update the clock status for each case record for NVRM
*/

global class NVRM_CaseClockDailyUpdate implements Database.Batchable<sObject> {
    
    String query;
    public Set<String> caseStatusSet;
    
    global NVRM_CaseClockDailyUpdate() {
        query ='SELECT NVRM_Application_Accepted_Date__c,Status,Total_clock_days__c,Business_Unit__c FROM Case WHERE Business_Unit__c=\'NVRM\' AND NVRM_Application_Accepted_Date__c != null';
        caseStatusSet = new Set<String>();
        caseStatusSet = NVRM_Utility.getCaseClockRunningStatus();
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug('###### query ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Case> scope) {
        List<Case>  caseListToUpdate = new List<Case>();
        system.debug('###### scope '+ scope);
        for(Case caseVar : scope){
            system.debug(caseVar.Id + ' caseVar Status '+ caseVar.Status + 'NVRM_Application_Accepted_Date__c '+ caseVar.NVRM_Application_Accepted_Date__c );
            if( caseVar.NVRM_Application_Accepted_Date__c != null && caseVar.Status=='Request Further Information'){
                if( caseVar.Total_clock_days__c  == null)
                        caseVar.Total_clock_days__c = 1;
                else if( caseVar.Total_clock_days__c > 25 ) {
                    caseVar.Total_clock_days__c += 1;
                    
                }
                caseListToUpdate.add( caseVar );
                    
            }
            else if( caseVar.NVRM_Application_Accepted_Date__c != null &&  caseStatusSet.contains( caseVar.Status) ){
                if( caseVar.Total_clock_days__c  == null)
                    caseVar.Total_clock_days__c = 1;
                else
                    caseVar.Total_clock_days__c += 1;
                caseListToUpdate.add( caseVar );
            }
        }
        update caseListToUpdate;



    }
    
    global void finish(Database.BatchableContext BC) {
        
    }

    
    
}
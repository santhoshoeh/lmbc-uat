global class UtilityClass {


/**
 * Used by other apex classes to maintain reusable methods;
 * @author  Santhosh Lakshmanan
 * @date    10/03/2016
 */

    //recurring outlook synch stopper;
    public static boolean isOutlookSynchComplete = false;
    public static boolean isAttachmentSynchComplete = false;  
    public static boolean isTransactionUpdateRecursive = false;  
    public static final String BAAS_STOP_CLOCK_CASE_STATUS = 'Panel Requests Additional Info';

    public static String stripJsonNulls(String jsonString)
    {
        if (String.isNotBlank(jsonString))
        {
            jsonString = JsonString.replaceAll('\"[^\"]*\":null',''); //basic removeal of null values
            jsonString = JsonString.replaceAll(',{2,}', ','); //remove duplicate/multiple commas
            jsonString = JsonString.replace('{,', '{'); //prevent opening brace from having a comma after it
            jsonString = JsonString.replace(',}', '}'); //prevent closing brace from having a comma before it
            jsonString = JsonString.replace('[,', '['); //prevent opening bracket from having a comma after it
            jsonString = JsonString.replace(',]', ']'); //prevent closing bracket from having a comma before it
        }

        return jsonString;
    }
    
    // Returns a map of Record Types for a list of sObjects -  can be single or multiple sObject(s)
    public static map<String, Id> getRecordTypeMap(list<string> objNames) {
        // Example: Pass the string as a String List eg. String[] objNames = new String[]{'Account','Contact'};
        map<String, Id> RTMap = new Map<String, Id>();
        for (RecordType RT : [Select Id, DeveloperName From RecordType where SobjectType IN :objNames AND IsActive = true]) {
            RTMap.put(RT.DeveloperName, RT.Id);
        }
        return RTMap;
    }
    
    //SL added to retrieve current logged in user's bu.
    public static String currentLoginBU()
    {
        List<User> userList = new List<User>([Select Id from User where Id = :UserInfo.getUserId()]);
       
            return 'NPWS';
    } 
    
    /*------------------------------------------------------------
    Author:         Santhosh
    Company:        System Partners
    Description:    Method added to field metadata type and create queries dynamically;
    Inputs:         n/a
    Test Class:     
    History
    <Date>      <Authors Name>      <Brief Description of Change>
    20-Jan-17    Santhosh           Created
    ------------------------------------------------------------*/ 
    
    // Returns a map of fields for the sObject name that is passed in
    public static map<String, SobjectField> getFieldMap (String objName) {
        // Method returns a field map for the sObject name that is passed
        Schema.Sobjecttype objToken = Schema.getGlobalDescribe().get(objName);
        // Describe sObject to get the fields mapset.
        Map<String, SobjectField> fieldMap = objToken.getDescribe().fields.getMap(); 
        return fieldMap;     
    }
    
    //Method to find if the user is authenticated or not;
    public static Boolean isAuthenticated()
    {
        return UserInfo.getUserType() != 'Guest';       
    } 

    //Method to get the current user;
    public static User getLoggedInUser()
    {
        for (user u : [Select Id, Username, ContactId, Email From User Where Id = :UserInfo.getUserId()])
            return u;
        
        return null;
    } 
    //Method to get the contact of the logged in user;
    public static Contact getCurrentContact(){
        User u = getLoggedInUser();
        if (u != null)
            return getContact(u.ContactId);

        return null;    
    }
    //Method to retrieve the contact record;
    public static Contact getContact(Id contactId){
        for (Contact c: [Select Id, LastName, FirstName, Email, MobilePhone, Phone, Fax, Title, Salutation, AccountId From Contact Where Id = :contactId ])
            return c;
    
        return null;
    }
    //Get the community user account id;
    public static Account getAccount(){
        Contact c = getCurrentContact();
        if (c != null)
            return getAccount(c.AccountId);
            
        return null;    
    }
    //Get the account record;
    public static Account getAccount(String accountId)
    {
        for (Account a : [SELECT Id FROM Account WHERE Id = :accountId])

            return a;
        
        return null;
    }       

    //Get the community user contactId;
    public static String getCurrentContactId(){
        User u = getLoggedInUser();
        if (u != null)
            return u.ContactId;

        return '';  
    }
    //Get the community user contactId;
    public static String getCurrentAccountId(){
        Contact c = getCurrentContact();
        if (c != null)
            return c.AccountId;
            
        return '';    
    }

    public static Date convertStringToDate(String dateString){
       
       //String should be in the format - DD/MM/YYYY
       try
       {
           if (string.isBlank(dateString))
                return system.today();
    
           list<string> dateParts = dateString.split('/');
           return Date.newInstance(integer.valueOf(dateParts[2]),
                                    integer.valueOf(dateParts[1]), 
                                    integer.valueOf(dateParts[0]));
       }
       catch (exception ex)
       {
           return system.today();
       }
        
    }


    public static String convertDateToString(Date dt)
    {
        if (dt == null) return 'null date';

        Datetime dtime = datetime.newInstance(dt.year(), dt.month(),dt.day());

        return dtime.format('dd/MM/yyyy');

    }

    // Returns a string for use in dynamic queries
    public static String getQueryString (Map<String, SobjectField> fieldMap,string additionalFields) {
        // Method returns a dynamic query string including all the field names in the object, removes the need to hardcode fieldnames.
        String sQryString = 'Select ';
        for(String fieldName : fieldMap.keySet()) {
              if (fieldName != 'Id')    
                sQryString = sQryString + fieldName + ', ';
        }
        if (additionalFields != null)
            sQryString = sQryString + additionalFields + ', ' + 'Id from ';
        else
            sQryString = sQryString + 'Id from ';
        return sQryString;    
    }
    
    
    // Returns a map of an object records with name and object in a map.
    public static map<String, SObject> getSObjectRecs(String objName, string additionalFields, string keyfield) {
        
        map <String, SObject> SObjectMap = new map<String, SObject>();
        List<SObject> SObjectList = new List<SObject>();
        SObjectList = Database.query(getQueryString(getFieldMap(objName), additionalFields) + objName);
        for (SObject obj : SObjectList) {
            String sName = (String) obj.get(keyfield);
            SObjectMap.put(sName, obj);
        }
        return SObjectMap;
    } 

    // Returns a map of an object records with name and object in a map for a custommetadatatype.
    public static map<String, SObject> getMetadataTypeRecs(String objName, string additionalFields) {
        
        map <String, SObject> SObjectMap = new map<String, SObject>();
        List<SObject> SObjectList = new List<SObject>();
        SObjectList = Database.query(getQueryString(getFieldMap(objName), additionalFields) + objName);
        for (SObject obj : SObjectList) {
            String sName = (String) obj.get('DeveloperName');
            SObjectMap.put(sName, obj);
        }
        return SObjectMap;
    }               

    public static list<SelectOption> ausStates()
    {
        list<SelectOption> states = new list<SelectOption>();
        states.add(new SelectOption('NSW','NSW'));
        states.add(new SelectOption('VIC','VIC'));
        states.add(new SelectOption('ACT','ACT'));
        states.add(new SelectOption('QLD','QLD'));
        states.add(new SelectOption('SA','SA'));
        states.add(new SelectOption('WA','WA'));
        states.add(new SelectOption('NT','NT'));
        states.add(new SelectOption('TAS','TAS'));

        return states;

    }

    public static list<SelectOption> supplierTypes()
    {
        list<SelectOption> supplierType = new list<SelectOption>();
        supplierType.add(new SelectOption('Brand Owner','Brand Owner'));
        supplierType.add(new SelectOption('Distributor','Distributor'));
        supplierType.add(new SelectOption('Retailer','Retailer'));
        supplierType.add(new SelectOption('Other','Other'));

        return supplierType;

    }

    public static list<SelectOption> contactTitles()
    {
        list<SelectOption> contactTitle = new list<SelectOption>();
        contactTitle.add(new SelectOption('Mr','Mr.'));
        contactTitle.add(new SelectOption('Ms','Ms.'));
        contactTitle.add(new SelectOption('Mrs','Mrs.'));
        contactTitle.add(new SelectOption('Miss','Miss'));

        return contactTitle;

    } 

    public static list<SelectOption> countries()
    {
        list<SelectOption> countries = new list<SelectOption>();
        countries.add(new SelectOption('Australia','Australia'));

        return countries;

    }      
   

    //Method to generate random 7 digit string for BAAS Project;
    public static String generateRandomString()
    {
        Integer defaultLength = 7;
        final String defaultChars = '0123456';
        String randomString = '';
        while (randomString.length() < defaultLength)
        {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), defaultChars.length());
            randomString += defaultChars.substring(idx,idx+1);           
        }

        return randomString;
    }

    //get the email template id;
    public static Id getEmailTemplateId(String templateDevName)
    {
        list<EmailTemplate> templateId = new list<EmailTemplate>([Select Id, DeveloperName from EmailTemplate where DeveloperName = :templateDevName]);
        if (!templateId.isEmpty()) return templateId[0].Id;
        else return null;
    } 
    //get the org wide email address;
    public static Id getOrgWideEmailAddress(String emailDisplayName)
    {
        list<OrgWideEmailAddress> orgEmailId = new list<OrgWideEmailAddress>([select id, Address, DisplayName from OrgWideEmailAddress where DisplayName = :emailDisplayName limit 1]);
        if (!orgEmailId.isEmpty()) return orgEmailId[0].Id;
        else return null;
    }

    public static Id getCommunityNetworkId(String communityName) {
        String queryString = 'SELECT Id,Name FROM Network WHERE Name=: communityName';
        Network communityNetwork = Database.query(queryString);
        return communityNetwork.Id;
    }
}
/* This class is used to create Person Account, Company Account and Account Contact Relationship
 * Validation for checking duplicate Account ? - TODO
 * Test Class: RegistrationAccountHandlerTest
 *
 * @author: Suraj 01/06/2017
 */


public without sharing class RegistrationAccountHandler {

    //get custom setting
	private static Community_Settings__mdt cSettings = CommunitySettingsSelector.getCommunitySettings();
    private static Id BAASRecordownerId{
        get{
            if(BAASRecordownerId == null) {
                if(cSettings == null) {
                    BAASRecordownerId = userInfo.getUserId();
                } else {
                    BAASRecordownerId = cSettings.BAAS_Admin_User_ID__c;
                }
            }
            return BAASRecordownerId;
        }
    }

    //constructor
    public registrationAccountHandler(){}

    /* This method is aura enabled and can be used in lightning components
     * Takes JSON string as input param
     * The JSON String is parsed into the wrapper class object
     * Create person account, company account and Account Contact Relationship
     * Input Param: JSON String
     * Return : JSON String with error or success code and message
     * Sample JSON:'{"firstName": "New Contact","lastName": "new Last","mobile": "11111","phone": "22222","emailAddress": "aaaa@sss.com","accountName" : "zzzz","existingAssessor": "no","trainingInterested": "no","assessorCertificationNumber": "123123","validFrom": "","validTo":""}';
     */
    @AuraEnabled
    public static String createAccount(String regoJSONstr) {
        ResultWrapper rWrapper;
        //server side validations
		if(regoJSONstr == null || regoJSONstr.length() == 0){
            rWrapper = new ResultWrapper('Failed',null, null, null, 'null JSON recieved ');
            return JSON.serialize(rWrapper);
        }

        Savepoint sp = null;
        try {
            sp = Database.setSavepoint();//roll back to this point if there is anyexception
            registrationJSONHandler formDataObject = (registrationJSONHandler)JSON.deserialize(regoJSONstr, registrationJSONHandler.class);

            //server side validations

            if(formDataObject == null){//parsed object cant be null
            	rWrapper = new ResultWrapper('Failed',null, null, null, 'Issue in parsing Jason');
                return JSON.serialize(rWrapper);
            }

            if(formDataObject.emailAddress == null){//email cant be null
                rWrapper = new ResultWrapper('Failed',null, null, null, 'Email Address is null');
                return JSON.serialize(rWrapper);
            }

            if(formDataObject.mobile == null){//mobile cant be null
                rWrapper = new ResultWrapper('Failed',null, null, null, 'Mobile is null');
                return JSON.serialize(rWrapper);
            }

            //get community url based on business unit
            //get the community id based on the name pased in login method
            //List<Network> commIdList = [SELECT Id FROM Network WHERE name =: personAccount.business_unit__pc];

            //if(commIdList.size() == 0){
            //    rWrapper = new ResultWrapper('Failed',null, null, null, 'Business Unit is Missing for the existing person Account');
            //    return JSON.serialize(rWrapper);
            //}
            Id communityId = UtilityClass.getCommunityNetworkId('BAAS');  //Assessment
            //construct the url for directing to community landing page
            String commUrl = Network.getLoginUrl(communityId);
            commUrl = commUrl.substring(0, commUrl.lastIndexOf('/')+1) ;

            //if Person Acount with email address exists use that one, else create new
            Account personAccount = new Account();
            Id personAccountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

            List<Account> accntList = [
                        SELECT id, name, business_unit__pc, business_unit__c FROM account
                        WHERE PersonEmail =: formDataObject.emailAddress
                        AND RecordTypeId =: personAccountRecTypeId
                ];

            if (accntList.size() > 0 && !accntList.isEmpty()){
            	personAccount = accntList[0];

                rWrapper = new ResultWrapper('Success',personAccount.Id, '201', commUrl+'s/','Thanks for your registration. We notice that you are a registered user. Please use this link to login with your biodiversity username and password.');
                return JSON.serialize(rWrapper);
            }else{
            	// for person accounts we can not update the Name field instead we have to update the FirstName and LastName individually
               	personAccount = new Account();
               	personAccount.RecordTypeId = personAccountRecTypeId;
               	personAccount.FirstName = formDataObject.firstName;
                //personAccount.MiddleName = formDataObject.middleName;
                personAccount.LastName = formDataObject.lastName;
               	personAccount.Phone = formDataObject.phone;
               	personAccount.PersonMobilePhone = formDataObject.mobile;
            	personAccount.PersonEmail = formDataObject.emailAddress;
                personAccount.Organisation_Type__c = formDataObject.organisationType;
                personAccount.Work_locality__c = formDataObject.workLocality;
                personAccount.Existing_Assessor__pc = (formDataObject.existingAssessor != null &&
                                                       formDataObject.existingAssessor.equalsIgnorecase('yes') ? true : false);
                personAccount.Be_Accredited_Assessor__pc = (formDataObject.trainingInterested != null &&
                                                       formDataObject.trainingInterested.equalsIgnorecase('yes') ? true : false);
				personAccount.is_Public_Available__c = (formDataObject.isPublicAvaliable != null &&
													   formDataObject.isPublicAvaliable.equalsIgnorecase('yes') ? true : false);
               	personAccount.Assessor_Certification_Number__pc = formDataObject.assessorCertificationNumber;
//               	personAccount.Valid_From__pc = (formDataObject.validFrom != null && !String.isEmpty(formDataObject.validFrom) ? date.parse(formDataObject.validFrom) : null);
//               	personAccount.Valid_To__pc = (formDataObject.validTo != null && !String.isEmpty(formDataObject.validTo)? date.parse(formDataObject.validTo) : null);
                personAccount.Business_Unit__pc = formDataObject.businessUnit;
                personAccount.Business_Unit__c = formDataObject.businessUnit;
                personAccount.OwnerId = BAASRecordownerId;
                insert personAccount;

                //create training record if User opted for training
                createTrainingRecord(personAccount);

            }

			system.debug('inserted personAccount ' + personAccount);
            //we will need to check for duplicate person account - TODO

            //create company Account if Company Name is provided
            if(formDataObject.accountName != null && !String.isEmpty(formDataObject.accountName) ){
            	Account newCompanyAccount = new Account();
                newCompanyAccount.Name = formDataObject.accountName;
                newCompanyAccount.Business_Unit__c = formDataObject.businessUnit;
                newCompanyAccount.OwnerId = BAASRecordownerId;
                newCompanyAccount.ABNACN__c = formDataObject.ABNACN;
                newCompanyAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
                insert newCompanyAccount;

                //create Account Contact Relationship
                //get contact id for person account
                Account personAcc = [Select PersonContactId From Account Where Id = :personAccount.Id];

                AccountContactRelation accntContRelation = new AccountContactRelation();
                accntContRelation.ContactId = personAcc.PersonContactId;
                accntContRelation.AccountId = newCompanyAccount.Id;
                accntContRelation.Roles = 'Business User';
                insert accntContRelation;
             }

            //at this point everything should have inserted properly, return success message
            rWrapper = new ResultWrapper('Success',personAccount.Id, '200', commUrl, ' Account created successfully');
            return JSON.serialize(rWrapper);
        } catch (DMLException dmExp){
        	Database.rollback(sp);//roll back in case of exception and return exception message
            rWrapper = new ResultWrapper('Failed',null, null, null, dmExp.getMessage());
            return JSON.serialize(rWrapper);
        } catch (Exception ex) {
            system.debug('in catch block exception ::: ' + ex.getMessage());
            Database.rollback(sp);//roll back in case of exception and return exception message

            rWrapper = new ResultWrapper('Failed',null, null, null, 'Issue in registration');
            return JSON.serialize(rWrapper);
        }
    }

    // this method is used to login community user
    // Redirect to community home page in case of successfull login
    // We need to get third param from fron end that will specify the community name,
    // this will decide where to redirect after successfull login
	@AuraEnabled
    public static String communityUserLogin(String userName, String password, String businessUnit){
        ResultWrapper rWrapper;
        system.debug('entry userName' + userName);
        system.debug('entry password' + password);

        ApexPages.PageReference lgn;
        try{

            //get the community id based on the name pased in login method
            List<Network> commIdList = [SELECT Id FROM Network WHERE name =: businessUnit];
            //List<Network> commIdList = [SELECT Id FROM Network WHERE name = 'Assessment'];
            if(commIdList.size() == 0){
                rWrapper = new ResultWrapper('Failed',null, null, null, 'Business Unit is Missing');
                return JSON.serialize(rWrapper);
            }

            //construct the url for directing to community landing page
            String commUrl = Network.getLoginUrl(commIdList.get(0).Id);
            commUrl = commUrl.substring(0, commUrl.lastIndexOf('/')+1) ;
			system.debug('commUrl ' + commUrl);

            lgn = Site.login(userName, password, commUrl+'s/');
            //https://masterdev-nvrmcms.cs58.force.com/NVRM/s/
			//https://masterdev-nvrmcms.cs58.force.com/baas/
			system.debug('login response ' + lgn);
            if(lgn != null){
				system.debug('redirecting to community ' + lgn);
                aura.redirect(lgn);//redirect to homepage component
                return null;
            }else{
                rWrapper = new ResultWrapper('Failed',null, null, null, 'Invalid username or password');
                return JSON.serialize(rWrapper);
            }
        } catch(Exception e){
            rWrapper = new ResultWrapper('Failed',null, null, null, 'Invalid username or password ');
            return JSON.serialize(rWrapper);
        }
    }


    // this method is used to login community user
    public static void createTrainingRecord(Account trainingAccnt){
        system.debug('entry point createTrainingRecord');
        Training__c newTrainingRec = new Training__c();
        newTrainingRec.Account__c = trainingAccnt.Id;
        newTrainingRec.Account_Email__c = trainingAccnt.PersonEmail;
        newTrainingRec.Request_Training__c = true;
//        newTrainingRec.Training_Other__c = formDataObject.trainingComment;
//        newTrainingRec.Willing_To_Travel_To_Training__c	 = (formDataObject.willingToTravel != null &&
//                formDataObject.willingToTravel.equalsIgnorecase('yes') ? true : false);
        insert newTrainingRec;
    }

    //public static void createAssessorTrainingModule(Training__c training, registrationJSONHandler formDataObject) {
    //    List<Assessor_Training_Module__c> aTrainingModules = new List<Assessor_Training_Module__c>();
    //    if(formDataObject.trainingCourses != null && !formDataObject.trainingCourses.isEmpty()) {
    //        for(String trainingModuleId : formDataObject.trainingCourses) {
    //            if(trainingModuleId != 'Other') {
    //                Assessor_Training_Module__c aTrainingModule = new Assessor_Training_Module__c();
    //                aTrainingModule.Training_Module__c = trainingModuleId;
    //                aTrainingModule.Training__c = training.Id;
    //                aTrainingModule.Person_Account_Training_Modules__c = training.Account__c;

    //                aTrainingModules.add(aTrainingModule);
    //            }
    //        }
    //    }
    //    insert aTrainingModules;
    //}

    @AuraEnabled
    public static Boolean isAuthenticated() {
        if(UserInfo.getUserType() == 'Guest' || UserInfo.getUserType() == 'Standard') return false;
        return true;
    }

    //Wrapper for parsing JSON response from Registration Page
    public class registrationJSONHandler{
		@AuraEnabled
        public String firstName{get; set;}
        @AuraEnabled
        public String middleName{get; set;}
		@AuraEnabled
     	public String lastName{get; set;}
		@AuraEnabled
     	public String mobile{get; set;}
		@AuraEnabled
     	public String phone{get; set;}
		@AuraEnabled
    	public String emailAddress{get; set;}
		@AuraEnabled
    	public String accountName{get; set;}
        @AuraEnabled
        public String organisationType{get; set;}
        @AuraEnabled
        public String ABNACN{get; set;}
        @AuraEnabled
        public String workLocality{get; set;}
		@AuraEnabled
    	public String existingAssessor{get; set;}
		@AuraEnabled
    	public String trainingInterested{get; set;}
		@AuraEnabled
		public String assessorCertificationNumber{get; set;}
//		@AuraEnabled
// 		public String validFrom{get; set;}
//		@AuraEnabled
//     	public String validTo{get; set;}
        @AuraEnabled
        public String businessUnit{get; set;}
        @AuraEnabled
        public List<String> trainingCourses{get;set;}
        @AuraEnabled
        public String willingToTravel{get; set;}
        @AuraEnabled
        public String trainingComment{get; set;}
		@AuraEnabled
		public String isPublicAvaliable{get; set;}
    }

    //wrapper for retuning response to the calling method
       public class ResultWrapper{
       public String status {get;set;}
       public String recId {get;set;}
       public String url {get;set;}
       public String msg {get;set;}
       public String statusCode {get;set;}

       public ResultWrapper(String status, String recId, String statusCode, String url, String msg){
           this.status = status;
           this.statusCode = statusCode;
           this.recId = recId;
           this.url = url;
           this.msg = msg;
       }

   }
}

@isTest
private class AccountControllerTest
{
    @testSetup static void createAccreditedAssessorAccount() {
        RecordTypeSelector rTypeSelector = new RecordTypeSelector();
        List<RecordType> rTypeList = rTypeSelector.fetchRecordTypesByName('PersonAccount');
        Id PersonAccountRecordTypeId = rTypeList.get(0).Id;
        Account accreditedAssessorAccount = new Account(RecordTypeId=PersonAccountRecordTypeId, 
                                                         FirstName='Accredited',
                                                         LastName = 'Assessor',
                                                         Accredited_Assessor__pc = true,
                                                       Assessor_Certification_Number__pc = 'BAAS01234',
                                                       Valid_From__pc = system.today().addDays(-10),
                                                       Valid_To__pc = system.today().addDays(10));
        insert accreditedAssessorAccount;
    }

    @isTest
    static void testGetAccreditedAssessorList(){
        AccountController aControler = new AccountController();

        system.assertEquals(aControler.accreditedAssessor.size(), 1);
    }
}
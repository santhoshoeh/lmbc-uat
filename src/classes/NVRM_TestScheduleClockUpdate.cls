@isTest
private class NVRM_TestScheduleClockUpdate {
    public static String testEmail = 'daveAdm@gmail.com';
     
    @testSetup static void createCasesTestData() {
        List<Case> caseList = new List<Case>();

        for(Integer i =0;i<20;i++){
            Case caseVar = new Case();
            caseVar.Business_Unit__c = 'NVRM';
            caseVar.recordTypeID = NVRM_Utility.getCaseRecordTypeID('LIMR');
            if( i <= 5){
                caseVar.Status='Application Accepted';
                caseVar.NVRM_Application_Accepted_Date__c = system.today();
            }

            else if( i <= 10 ){
                caseVar.Status='Request Further Information';
                caseVar.NVRM_Application_Accepted_Date__c = system.today()-3;
            }
            else if( i <= 15 )
                caseVar.Status='Application Rejected';
            else if( i <= 20 )
                caseVar.Status='Determination Sent For Approval';

            caseList.add(caseVar);

        }
        insert caseList;

    }
    
    @isTest static void test_method_one() {
        Test.StartTest();
            NVRM_ScheduleClockUpdate clockUpdate1 = new NVRM_ScheduleClockUpdate();
            String sch = '0 0 23 * * ?'; 
            system.schedule('Test Territory Check', sch, clockUpdate1); 
            clockUpdate1.execute(null);
        Test.stopTest();
    }
    @isTest static void test_method_two() {
        Test.StartTest();
            List<Case> caseList = [ SELECT NVRM_Application_Accepted_Date__c,Status,Total_clock_days__c,Business_Unit__c FROM Case ];
            Database.executeBatch(new NVRM_CaseClockDailyUpdate(), 200);

            NVRM_CaseClockDailyUpdate cb = New NVRM_CaseClockDailyUpdate();

            Database.QueryLocator ql = cb.start(null);
            cb.execute(null,caseList);
            cb.Finish(null);

        Test.stopTest();
    }
    
    testMethod static void testCaseClock(){
        Profile pf = [Select id,name from Profile where Name='NVRM_Admin' limit 1];
        Id roleId = [ SELECT  Name FROM UserRole WHERE DeveloperName='NVRM_Admin' LIMIT 1].ID;
        User userObj = new User(username=testEmail, firstname='Smith',
        lastname='Dave', email=testEmail,communityNickname = 'Dave' + '_fn' ,
        alias = 'adm', profileid = pf.Id, emailencodingkey='UTF-8',
        languagelocalekey='en_US', localesidkey='en_US', timezonesidkey='America/Los_Angeles',Business_Unit__c='NVRM',UserRoleId=roleId);
        
        insert userObj; 
        Test.startTest();
            system.runAs(userObj){
                // insert Account 
                Account acc = new Account( lastname='Dave',firstname='Smith', personemail=testEmail);
                acc.Identification_provided__c = 'Medicare card';
                acc.Medicare_card_number__c ='MD1212';
                acc.Position_on_Medicare_card__c = 'Valid';
                acc.ShippingCity = 'Sydney';
                acc.ShippingCountry = 'AU';
                acc.ShippingPostalCode = '2000';
                acc.ShippingState = 'NSW';
                acc.ShippingStreet = '34 Ramsay street';
                acc.Phone = '0423334546';
                insert acc;
                Contact contactAcc = [ SELECT Name FROM Contact where AccountID =: acc.Id];
                // create newCase case
                Case newCase = new Case();
                newCase.recordTypeId = NVRM_Utility.getCaseRecordTypeID('LIMR');
                newCase.Status = 'New';
                newCase.Business_Unit__c = 'NVRM';
                newCase.Applicant_Type__c = 'Landholder';
                newCase.ContactId = contactAcc.Id;
                newCase.Attach_a_map_with_area__c = true;
                insert newCase;
                Id caseId = newCase.Id;
                // Application submitted
                newCase = [ SELECT OwnerID,Status,Business_Unit__c FROM Case WHERE Id = :caseId ];
                newCase.Status = 'Submitted';
                newCase.Submitted_Date__c = system.today();
                update newCase;
                system.debug('***Case new' + newCase);
                
                Case caseVar = [ SELECT OwnerID,Status,Business_Unit__c FROM Case WHERE Id =: newCase.ID ];
                system.debug('***caseVar ' + caseVar);
                // Application accepted to start the clock
                Case newCERCase2 = [ SELECT OwnerID,Status,Business_Unit__c FROM Case WHERE Id = :newCase.ID ];
                system.debug('After query ### '+ newCase);
                newCERCase2.Status = 'Application Accepted';
               
                update newCERCase2;
                Database.executeBatch(new NVRM_CaseClockDailyUpdate(), 200);

                newCERCase2.Status = NVRM_Utility.caseStatus_RFI;
                newCERCase2.Total_clock_days__c = 26;
                update newCERCase2;
                // run the clock
                Database.executeBatch(new NVRM_CaseClockDailyUpdate(), 200);
            
                
            }
        Test.stopTest();
        
        
        
    }
    
    
    
}
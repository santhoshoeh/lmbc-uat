/**
 * Created by junzhou on 20/6/17.
 */

public with sharing class RecordTypeSelector {
    private String queryStr='select id, Name, developerName from RecordType ';

    public list<RecordType> fetchRecordTypesByName(list<String> developerNames){
        String queryString=queryStr+' where developerName in : developerNames';
        list<RecordType> recordTypes=new list<RecordType>();
        recordTypes=database.query(queryString);
        return recordTypes;
    }

    public list<RecordType> fetchRecordTypesByName(String developerName){
        list<String> developerNames=new list<String>();
        developerNames.add(developerName);
        return fetchRecordTypesByName(developerNames);
    }

    public list<RecordType> fetchRecordTypeList(){
        List<RecordType> rtList = new List<RecordType>();
        rtList = Database.query(queryStr);
        return rtList;
    }

    public List<RecordType> fetchRecordTypesByObjectName(String objectname) {
        String queryString=queryStr+' where sobjecttype =: objectname';
        List<RecordType> rtList = new List<RecordType>();
        rtList = Database.query(queryString);
        return rtList;
    }
}
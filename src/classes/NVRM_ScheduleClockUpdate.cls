/**
* @author Sakshi Chauhan
* @date 10 July 2017
* @description Schedulable Class to run batch class (NVRM_CaseClockDailyUpdate)  every morning of the day to update clock status
*/

global class NVRM_ScheduleClockUpdate implements Schedulable {
	
	global void execute(SchedulableContext sc) {
		
		NVRM_CaseClockDailyUpdate clockUpdateBatch = new NVRM_CaseClockDailyUpdate();
		database.executebatch( clockUpdateBatch );
	}
}
@isTest
private class TrainingHandlerTest
{
	@isTest
	static void test_TrainingHandler() {
		User trainingProvider = TestDataGenerator.trainingProvider;
		Id personAccountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
       	Account account = new Account(FirstName='test', LastName='test', RecordTypeId=personAccountRecTypeId);
       	insert account;

       	RegistrationAccountHandler.createTrainingRecord(account);

       	List<AccountShare> aShare = [SELECT Id, AccountAccessLevel, AccountId, UserOrGroupId FROM AccountShare where accountid=:account.Id];

       	system.assert(aShare.size()>0);
       	TrainingHandler tHandler = new TrainingHandler();
       	tHandler.bulkBefore();
       	tHandler.beforeInsert(account);
       	tHandler.beforeUpdate(account, account);
       	tHandler.beforeDelete(account);
       	tHandler.afterUpdate(account, account);
       	tHandler.afterDelete(account);
       	tHandler.afterUndelete(account);
	}
}
@isTest
private class PortalUserSharingUtilTest{
    public static list<Account> accList;
    public static list<User> userList;
    public static list<Case> caseList;
    public static User usr;
    public static Integer recordCount;

    public static void setupTestData(){
        String personAccRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

        accList = new list<Account>();
        for(Integer i = 0; i < recordCount; i++){
            Account acc1 = new Account(
                RecordTypeID=personAccRecTypeId,
                FirstName='FirstName ',
                LastName='LastName from Test Class'+String.ValueOf(i),
                PersonMailingStreet='Mail Street',
                PersonMailingPostalCode='PostCode',
                PersonMailingCity='MailCity',
                PersonEmail='mailEmailFromClass'+String.ValueOf(i)+'@test.com',
                Email_Verified__pc = true,
                Business_Unit__pc = 'BOAM'
            );
            accList.add(acc1);
        }
        insert accList;

        Id profileId = [SELECT id, name FROM profile where UserType = 'PowerCustomerSuccess' LIMIT 1].get(0).Id;
        userList = new list<User>();
        
        map<ID, ID> accContMap = new map<ID, ID>();
        for(Contact cont : [select id, AccountID from Contact where AccountID IN :accList]){
            accContMap.put(cont.AccountID, cont.ID);
        }

        for(Integer i = 0; i < recordCount; i++){
            Account acc1 = accList[i];
            User userRec1 =  new User(
                Username = acc1.PersonEmail + '.testClassUser',
                FirstName = acc1.firstname,
                LastName = acc1.lastname,
                Email = acc1.PersonEmail,
                alias = 'test1',
                ContactId = accContMap.get(acc1.ID),
                TimeZoneSidKey = 'Australia/Sydney',
                LocaleSidKey='en_AU', 
                EmailEncodingKey='ISO-8859-1', 
                LanguageLocaleKey='en_US',
                profileId = profileId,
                Business_Unit__c = 'BOAM'
            );
            userList.add(userRec1);
        }

        usr = [select id from User where ID = :UserInfo.getUserID()];
        system.runAs(usr){
            insert userList;
        }
        
        caseList = new list<Case>();
        for(Integer i = 0; i < recordCount; i++){
            Case cs1 = new Case(
                Subject = 'Case from Test Class'+String.ValueOf(i),
                RecordTypeID = CaseUtil.parentCaseRTID,
                Business_Unit__c = 'BOAM'
            );
            caseList.add(cs1);
        }
        insert caseList;  
    }

    static testmethod void casePartyTest(){
        recordCount = 8;
        setupTestData();
        Test.StartTest();
            list<Case_Party__c> cpList = new list<Case_Party__c>();
            // add party records.
            for(Integer i = 0; i < recordCount; i++){
                Case_Party__c cp = new Case_Party__c();
                cp.First_Name__c = 'test';
                cp.Last_Name__c = 'name';
                cp.Person_Email__c = accList[i].PersonEmail;
                cp.RecordTypeID = CasePartyUtil.assessorRTID;
                cp.Role__c = 'Assessor';
                cp.Assessor_accreditation_number__c = '1234';
                cp.Case__c = caseList[i].ID;
                cpList.add(cp);
            }
            insert cpList;

            // add new share records.
            list<Case_Party__c> cpList2 = new list<Case_Party__c>();
            for(Integer i = 0; i < recordCount; i++){
                Case_Party__c cp = new Case_Party__c();
                cp.First_Name__c = 'test';
                cp.Last_Name__c = 'name';
                cp.Person_Email__c = accList[recordCount-1-i].PersonEmail;
                cp.RecordTypeID = CasePartyUtil.assessorRTID;
                cp.Role__c = 'Assessor';
                cp.Assessor_accreditation_number__c = '1234';
                cp.Case__c = caseList[i].ID;
                cpList2.add(cp);
            }
            insert cpList2;

            list<Case_Party__c> cpListAll = new list<Case_Party__c>();
            cpListAll.addAll(cpList);
            cpListAll.addAll(cpList2);

            // Update to new party
            for(Integer i = 0; i < recordCount/2; i++){
                Case_Party__c cp = cpList[i];
                cp.Person_Email__c = accList[i+1].PersonEmail;
                cp.RecordTypeID = CasePartyUtil.interestHolderRTID;
            }
            for(Integer i = 0; i < recordCount/2; i++){
                Case_Party__c cp = cpList2[i];
                cp.Person_Email__c = null;
            }
            update cpListAll;

            delete cpList;
            
            undelete cpList;

            CasePartyTriggerHandler cpt = new CasePartyTriggerHandler(false, 0, null, null, null, null);
            cpt.resetTriggerState();
        Test.StopTest();
    }

    static testmethod void userTest(){
        recordCount = 8;
        setupTestData();
        list<Case_Party__c> cpList = new list<Case_Party__c>();
        // add party records.
        for(Integer i = 0; i < recordCount; i++){
            Case_Party__c cp = new Case_Party__c();
            cp.First_Name__c = 'test';
            cp.Last_Name__c = 'name';
            cp.Person_Email__c = accList[i].PersonEmail;
            cp.RecordTypeID = CasePartyUtil.assessorRTID;
            cp.Role__c = 'Assessor';
            cp.Assessor_accreditation_number__c = '1234';
            cp.Case__c = caseList[i].ID;
            cpList.add(cp);
        }
        insert cpList;

        Test.StartTest();
            system.runAs(usr){
                Integer startCount = 0;
                Integer endCount = 2*userList.size()/3;
                // Remove person account from some.
                for(Integer i = startCount; i < endCount; i++){
                    userList[i].IsActive = false;
                }
                update userList;

                for(Integer i = startCount; i < endCount; i++){
                    userList[i].IsActive = true;
                }
                update userList;

                UserTriggerHandler ust = new UserTriggerHandler(false, 0, null, null, null, null);
                ust.resetTriggerState();
            }
        Test.StopTest();
    }

    static testmethod void caseCommentTest(){
        recordCount = 8;
        setupTestData();
        list<Case_Party__c> cpList = new list<Case_Party__c>();
        // add party records.
        for(Integer i = 0; i < recordCount; i++){
            Case_Party__c cp = new Case_Party__c();
            cp.First_Name__c = 'test';
            cp.Last_Name__c = 'name';
            cp.Person_Email__c = accList[i].PersonEmail;
            cp.RecordTypeID = CasePartyUtil.assessorRTID;
            cp.Role__c = 'Assessor';
            cp.Assessor_accreditation_number__c = '1234';
            cp.Case__c = caseList[i].ID;
            cpList.add(cp);
        }
        insert cpList;

        Test.StartTest();
            list<Case_Comment__c> ccmList = new list<Case_Comment__c>();
            // add party records.
            for(Integer i = 0; i < recordCount; i++){
                Case_Comment__c ccm = new Case_Comment__c();
                ccm.Case__c = caseList[i].ID;
                ccmList.add(ccm);
            }
            insert ccmList;
            
            Integer startCount = 0;
            Integer endCount = ccmList.size()/3;
            for(Integer i = startCount; i < endCount; i++){
                ccmList[i].Case__c = caseList[i+1].ID;
            }

            startCount = endCount;
            endCount = 2*ccmList.size()/3;
            for(Integer i = startCount; i < endCount; i++){
                ccmList[i].Private__c = true;
            }
            update ccmList;
        Test.StopTest();
    }
    
    static testmethod void credHoldingTest(){
        recordCount = 8;
        setupTestData();

        Test.StartTest();
            list<Credit_Holding__c> chList = new list<Credit_Holding__c>();
            // add party records.
            for(Integer i = 0; i < recordCount; i++){
                Credit_Holding__c ch = new Credit_Holding__c();
                ch.Agreement__c = caseList[i].ID;
                chList.add(ch);
            }
            insert chList;
            
            list<Case_Party__c> cpList = new list<Case_Party__c>();
            // add party records.
            for(Integer i = 0; i < recordCount; i++){
                Case_Party__c cp = new Case_Party__c();
                cp.First_Name__c = 'test';
                cp.Last_Name__c = 'name';
                cp.Person_Email__c = accList[i].PersonEmail;
                cp.RecordTypeID = CasePartyUtil.assessorRTID;
                cp.Role__c = 'Assessor';
                cp.Assessor_accreditation_number__c = '1234';
                cp.Case__c = caseList[i].ID;
                cp.Credit_Holding__c = chList[i].ID;
                cpList.add(cp);
            }
            insert cpList;
        Test.StopTest();
    }
}
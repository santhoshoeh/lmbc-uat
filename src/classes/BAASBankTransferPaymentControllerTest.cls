@isTest
private class BAASBankTransferPaymentControllerTest
{
	static TestDataGenerator.DefaultDummyRecordSet rs = TestDataGenerator.createDefaultDummyRecordSet();
	static Transaction__c trans;
	@testSetup
	static void createRequestParameters() {
		trans = new Transaction__c();
		trans.Case__c = rs.cases[0].Id;
		trans.Purchase_Item__c = 'Subscription Fee';
		trans.Payment_Status__c = 'Pending Payment';
		trans.Reference_Number__c = 'baas098787';
		insert trans;
	}
	
	@isTest
	static void testBAASBankTransferPaymentController() {
		Transaction__c trans = TransactionSelector.getTransactionByReferenceNumber('baas098787');
		
		Test.setCurrentPage(Page.BAASBankTransferPayment);
		ApexPages.currentPage().getParameters().put('Id',trans.Id);

		BAASBankTransferPaymentController bBAnkTransferPayment = new BAASBankTransferPaymentController();
		system.assertEquals(bBAnkTransferPayment.referenceNumber, 'baas098787');

		PageReference pReference = bBAnkTransferPayment.finish();
		System.assertNotEquals(pReference, NULL);
	}
}
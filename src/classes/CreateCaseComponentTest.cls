@isTest
public class CreateCaseComponentTest {
    
    public static id caseId;
    //intsert test data
    static{
       Case testCase = new Case();
       testCase.Status = 'New'; 
       insert testCase; 
        
       caseId = testCase.id; 
    }
    
    @isTest
    static void testcreateAssessment(){
        test.startTest();
        
        // Create parent case
        Case parentCase = new Case(Status = 'New');
        insert parentCase;
        
        String retCase = CreateCaseComponent.createAssessment(parentCase.Id, CaseUtil.assesmentRTID, 'Test');
        system.assert(retCase != null);
        
        String retCase2 = CreateCaseComponent.validateAssessment(parentCase.Id);
        system.assert(retCase2 != null);
        
        test.stopTest();
    }
    
    @isTest
    static void testAuraMethods() {
        test.startTest();
        CreateCaseComponent.isAssessor();
        
		List<String> statusList = CreateCaseComponent.getCaseStatus();
        system.assert(statusList != null);
        
        Case retCase = CreateCaseComponent.getCaseDetails(caseId);
        system.assert(retCase != null);
        system.assert(retCase.status.equalsIgnoreCase('New'));
        
        Map<String, Id> recTypeMap = CreateCaseComponent.getRecTypeIdMap();
		system.assert(!recTypeMap.isEmpty());
        
        
        Id recTypeId = CreateCaseComponent.getAssessmentCaseDetails();
		system.assert(recTypeId != null);
        

        test.stopTest();
    }
    
}
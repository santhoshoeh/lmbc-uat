@isTest
public class NVRM_TestSubmitApplication{
    public static String userName = 'demo1@demopods.com';
    public static String blankValue = '<br/>';
    
    @testSetup
    public static void  createTestData(){
        
        User userVar = new user(alias='demo1',LastName='Demo1',Username=userName,ProfileId = NVRM_Utility.NVRM_ADMIN_PROFILE,EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',Email=userName,LocaleSidKey='en_AU',TimeZoneSidKey='Australia/Sydney');
        insert userVar;
        system.runAs( userVar ){
            Account personAccount = new Account();
            personAccount.recordTypeID = NVRM_Utility.PERSON_ACC_RECORDTYPE;
            //personAccount.FirstName= 'Jack';
            personAccount.LastName = 'Wayne';
            insert personAccount;
            Contact contactVar  = [ SELECT Id FROM COntact WHERE AccountId = :personAccount.Id ];
            Case caseLIMR = new Case();
            caseLIMR.recordTypeID = NVRM_Utility.LIMR_RECORDTYPE;
            caseLIMR.status= 'New';
            caseLIMR.ContactId = contactVar.Id;
            insert caseLIMR;
            
        }
        
    }
    
    /* Scenario 1: Account person identity, postal add and phone details are not present
    *   Landholder details are not available
    *   Case Applicant details,map provided method are ot provided.
    *
    */
    testMethod public static void testCaseValidationMess1(){
        User userVar = [ SELECT Id FROM User WHERE Username = :userName];
        Case caseVar = [ SELECT Id FROM Case LIMIT 1];
        caseVar.Applicant_Type__c = 'Agent';
        update caseVar;
        String messReceived = '';
        Test.startTest();
        system.runAs( userVar ){
            messReceived = NVRM_SubmitApplication.caseSubmitValidation( caseVar.Id );
        }
        Test.stopTest();
        system.debug('messReceived ' + messReceived);
        String expectedMess = '';
        
        expectedMess += NVRM_Utility.LOTMETHOD + blankValue;
        expectedMess += NVRM_Utility.FIRSTNAME_ERRORMESS + blankValue ;
        expectedMess += NVRM_Utility.IDENTITY_ERRORMESS + blankValue;
        expectedMess += NVRM_Utility.ADD_ERRORMESS + blankValue;
        expectedMess += NVRM_Utility.PHONE_ERRORMESS + blankValue;
        expectedMess += NVRM_Utility.LANDHOLDER_ERRORMESS + blankValue;
        
        system.assertEquals(messReceived,expectedMess);
        
        
        
    }
    
    /* Scenario 2: Contact Name is not provided
    *   Landholder details are not available
    *   Case Applicant details,map provided method are ot provided.
    *
    */
    testMethod public static void testCaseValidationMess2(){
        User userVar = [ SELECT Id FROM User WHERE Username = :userName];
        
        String messReceived = '';
        Test.startTest();
        system.runAs( userVar ){
            Case caseLIMR2 = new Case();
            caseLIMR2.recordTypeID = NVRM_Utility.LIMR_RECORDTYPE;
            caseLIMR2.status= 'New';
            insert caseLIMR2;
                
            Case_Party__c landholder = new Case_Party__c();
            landholder.Case__c = caseLIMR2.Id;
            landholder.Last_Name__c = 'Dane';
            insert landholder;
            
            messReceived = NVRM_SubmitApplication.caseSubmitValidation( caseLIMR2.Id );
        }
        Test.stopTest();
        system.debug('messReceived ' + messReceived);
        String expectedMess = '';
        Case_Party__c landholder = [ SELECT First_Name__c,Last_Name__c FROM Case_Party__c LIMIT 1];
        
        expectedMess += NVRM_Utility.APPLICANT_ERRORMESS + blankValue;
        expectedMess += NVRM_Utility.LOTMETHOD + blankValue;
        expectedMess += NVRM_Utility.LANDHOLDER_IDENTITY_ERRORMESS + blankValue;
        expectedMess += NVRM_Utility.LANDHOLDER_POSTAL_ADD_ERRORMESS + blankValue;
        expectedMess += NVRM_Utility.LANDHOLDER_PHONE_ERRORMESS + blankValue;
        
        system.assertEquals(messReceived,expectedMess);
    }
    
    /* Scenario 3: CAse Id is null
    *   Landholder details are not available
    *   Case Applicant details,map provided method are ot provided.
    *
    */
    testMethod public static void testCaseValidationMess3(){
        User userVar = [ SELECT Id FROM User WHERE Username = :userName];
        String messReceived ='';
        Test.startTest();
        system.runAs( userVar ){
            messReceived = NVRM_SubmitApplication.caseSubmitValidation( null);  
        }
        Test.stopTest();
        system.assertEquals(messReceived,null);
    }
    /* Scenario 4: All  Account proper details are available
    *   Landholder details are available, 
    *   Case Applicant details,map provided method are provided.
    *
    */
    testMethod public static void testNoLotDpErrorMess(){
        User userVar = [ SELECT Id FROM User WHERE Username = :userName];
        String messReceived;
        Test.startTest();
        system.runAs( userVar ){
            Account personAccount = new Account();
            personAccount.recordTypeID = NVRM_Utility.PERSON_ACC_RECORDTYPE;
            personAccount.FirstName= 'Jack';
            personAccount.LastName = 'Wayne';
            personAccount.Identification_provided__c = 'Drivers license';
            personAccount.PersonEmail = 'testuser@gmail.com';
            personAccount.DL_Expiry_date__c = system.today() + 20;
            personAccount.Drivers_license_number__c = 'D1212';
            personAccount.Phone = '04467882799';
            insert personAccount;
            Contact contactVar  = [ SELECT Id FROM COntact WHERE AccountId = :personAccount.Id ];
            Case caseLIMR = new Case();
            caseLIMR.recordTypeID = NVRM_Utility.LIMR_RECORDTYPE;
            caseLIMR.status= 'New';
            caseLIMR.ContactId = contactVar.Id;
            caseLIMR.Attach_a_map_with_area__c = true;
            caseLIMR.Applicant_Type__c = 'Agent';
            caseLIMR.ContactId = contactVar.Id;
            insert caseLIMR;
            Case_Party__c caseParty = new Case_Party__c();
            caseParty.First_NAme__c='Landholder';
            caseParty.Last_Name__c='test';
            caseParty.Identification_provided__c = 'Drivers License';
            caseParty.Drivers_license_number__c = 'D121323';
            caseParty.DL_Expiry_date__c = system.today() + 100;
            caseParty.Street_Number__c = '12';
            caseParty.Street_Name__c = 'York Street';
            caseParty.City_Town__c = 'Sydney';
            caseParty.State__c = 'NSW';
            caseParty.Country__c = 'AU';
            caseParty.Postcode__c = '2000';
            caseParty.Phone__c='0423242424';
            caseParty.Case__c = caseLIMR.Id;
            insert caseParty;
            messReceived = NVRM_SubmitApplication.caseSubmitValidation( caseLIMR.Id );  
            
        
        }
        Test.stopTest();
        system.assertEquals(messReceived,null);
    }
    
    /* Scenario 5: Case Id is null
    *   Landholder details are not available
    *   Case Applicant details,map provided method are ot provided.
    *
    */
    testMethod public static void testSubmitCAse1(){
        User userVar = [ SELECT Id FROM User WHERE Username = :userName];
        String messReceived;
        Test.startTest();
        system.runAs( userVar ){
            messReceived = NVRM_SubmitApplication.submitCaseMethod( null ); 
        
        
        }
        Test.stopTest();
        system.assertEquals(messReceived,'Case Id is Null');
    }
    /* Scenario 6: Case Id is null
    *   Landholder details are not available
    *   Case Applicant details,map provided method are ot provided.
    *
    */
    testMethod public static void testSubmitCase2(){
        
        User userVar = [ SELECT Id FROM User WHERE Username = :userName];
        String messReceived;
        String errorMess;
        Test.startTest();
        system.runAs( userVar ){
            Account personAccount = new Account();
            personAccount.recordTypeID = NVRM_Utility.PERSON_ACC_RECORDTYPE;
            personAccount.FirstName= 'Jack';
            personAccount.LastName = 'Wayne';
            personAccount.Identification_provided__c = 'Drivers license';
            personAccount.PersonEmail = 'testuser@gmail.com';
            personAccount.DL_Expiry_date__c = system.today() + 20;
            personAccount.Drivers_license_number__c = 'D1212';
            personAccount.Phone = '04467882799';
            insert personAccount;
            Contact contactVar  = [ SELECT Id FROM COntact WHERE AccountId = :personAccount.Id ];
            Case caseLIMR = new Case();
            caseLIMR.recordTypeID = NVRM_Utility.LIMR_RECORDTYPE;
            caseLIMR.status= 'New';
            caseLIMR.ContactId = contactVar.Id;
            caseLIMR.Enter_Lot_DP_details__c = true;
            caseLIMR.Applicant_Type__c = 'Landholder';
            caseLIMR.ContactId = contactVar.Id;
            insert caseLIMR;
            Case_Party__c caseParty = new Case_Party__c();
            caseParty.First_NAme__c='Landholder';
            caseParty.Last_Name__c='test';
            caseParty.Identification_provided__c = 'Drivers License';
            caseParty.Drivers_license_number__c = 'D121323';
            caseParty.DL_Expiry_date__c = system.today() + 100;
            caseParty.Street_Number__c = '12';
            caseParty.Street_Name__c = 'York Street';
            caseParty.City_Town__c = 'Sydney';
            caseParty.State__c = 'NSW';
            caseParty.Country__c = 'AU';
            caseParty.Postcode__c = '2000';
            caseParty.Phone__c='0423242424';
            caseParty.Case__c = caseLIMR.Id;
            insert caseParty;
            messReceived = NVRM_SubmitApplication.caseSubmitValidation( caseLIMR.Id );  
            errorMess = NVRM_SubmitApplication.submitCaseMethod( caseLIMR.Id ); 
            
        
        }
        Test.stopTest();
        system.assertEquals(errorMess,'Success');
    }
    
    /* Scenario 7: All  Account proper details are available
    *   Landholder details are available, LOt DP info not entered
    *   Case Applicant details,map provided method are provided.
    *
    */
    testMethod public static void testNoErrorMess(){
        User userVar = [ SELECT Id FROM User WHERE Username = :userName];
        String messReceived;
        Test.startTest();
        system.runAs( userVar ){
            Account personAccount = new Account();
            personAccount.recordTypeID = NVRM_Utility.PERSON_ACC_RECORDTYPE;
            personAccount.FirstName= 'Jack';
            personAccount.LastName = 'Wayne';
            personAccount.Identification_provided__c = 'Drivers license';
            personAccount.PersonEmail = 'testuser@gmail.com';
            personAccount.DL_Expiry_date__c = system.today() + 20;
            personAccount.Drivers_license_number__c = 'D1212';
            personAccount.Phone = '04467882799';
            personAccount.ShippingCity = 'Sydney';
            insert personAccount;
            Contact contactVar  = [ SELECT Id FROM COntact WHERE AccountId = :personAccount.Id ];
            Case caseLIMR = new Case();
            caseLIMR.recordTypeID = NVRM_Utility.LIMR_RECORDTYPE;
            caseLIMR.status= 'New';
            caseLIMR.ContactId = contactVar.Id;
            caseLIMR.Enter_Lot_DP_details__c = true;
            caseLIMR.Applicant_Type__c = 'Agent';
            caseLIMR.ContactId = contactVar.Id;
            insert caseLIMR;
            Case_Party__c caseParty = new Case_Party__c();
            caseParty.First_NAme__c='Landholder';
            caseParty.Last_Name__c='test';
            caseParty.Identification_provided__c = 'Drivers License';
            caseParty.Drivers_license_number__c = 'D121323';
            caseParty.DL_Expiry_date__c = system.today() + 100;
            caseParty.Street_Number__c = '12';
            caseParty.Street_Name__c = 'York Street';
            caseParty.City_Town__c = 'Sydney';
            caseParty.State__c = 'NSW';
            caseParty.Country__c = 'AU';
            caseParty.Postcode__c = '2000';
            caseParty.Phone__c='0423242424';
            caseParty.Case__c = caseLIMR.Id;
            insert caseParty;
            messReceived = NVRM_SubmitApplication.caseSubmitValidation( caseLIMR.Id );  
            
        
        }
        Test.stopTest();
        String expectedMess = NVRM_Utility.LOT_DP_EMPTY + '<br/>';
        expectedMess += NVRM_Utility.POSTAL_ADD_ERRORMESS + '<br/>';
        system.assertEquals(messReceived,expectedMess);
    }
    

}
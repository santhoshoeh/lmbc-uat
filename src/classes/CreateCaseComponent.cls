/* This class is handler for lightning component CreateAssessment, CreateApplication, CreateAgreement
 * 
 * 
 * @Author: Suraj 
 * @Date: 16/06/2017
 */ 

public class CreateCaseComponent {
    
     //this method crates the assessment case and returns the case id
    //Params: parentcase id, rectype Id and case type
    @AuraEnabled
    public static String createAssessment(Id parentID, Id recTypeID, String caseType){
        system.debug('entry point parentID ' + parentID);
        system.debug('entry point recTypeID ' + recTypeID);
        system.debug('entry point caseType ' + caseType);
        Case newCase = new Case();
        newCase.Related_Cases__c = parentID;
		newCase.RecordTypeId  = recTypeID;
        newCase.Type = caseType;
        insert newCase;
        
        system.debug('return case id ' + newCase);
		return newCase.Id;
    }
     
    
    //If logged in user is assessor return true
    @AuraEnabled
    public static boolean isAssessor(){
        //return true;
        system.debug('entry point isAssessor ');        
        List<User> thisuser =  [SELECT isAssessor__c FROM User WHERE id =: UserInfo.getUserId()];   
        
        return thisuser.get(0).isAssessor__c;
    }
        
    // this method returns the picklist values for Case Status
    @AuraEnabled
	public static List<String> getCaseStatus(){
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = case.status.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f: ple) {
            options.add(f.getLabel());
        }
        
        return options;
    }
    
    
    //this method is used to vlidate properties, lots before creating assessment
    @AuraEnabled
    public static String validateAssessment(Id caseId){
        String retMsg;
        
        //get plot and lot details
		Case parentCase = [SELECT id, Status, 
                           	(SELECT id FROM Properties__r),
                           	(SELECT id FROM Property_Lots__r)
                           	FROM Case WHERE id =: caseId];
        
        //check if no property details attached show error
        if(parentCase.Properties__r == null || parentCase.Properties__r.isEmpty()){
            retMsg = 'Please add Property and Lots to the parent case before creating assessment';
        }        
        
        return retMsg;
    }
    
    
    @AuraEnabled
    public static Case getCaseDetails(Id caseId){
        system.debug('thisCaseId  entry ' + caseId);
        Case thisCase = [Select Id, RecordTypeId, Related_Cases__c 
                         FROM Case 
                         WHERE ID =: caseId];
        system.debug('thisCase  entry ' + thisCase);
        if(thisCase.Related_Cases__c !=null){
			caseId = (CaseUtil.boamAssessmentRTIDs.contains(thisCase.RecordTypeId)
                         ? thisCase.Related_Cases__c : caseId);        
        }
        
        system.debug('caseId   ' + caseId);
        return [SELECT Id, ParentId, Related_Cases__c, Submitted_Date__c,
                	(SELECT Id, Recordtype.Name FROM Agreement__r),
                	(SELECT Id, Recordtype.Name FROM Child_Cases__r 
                     	WHERE RecordtypeId IN: CaseUtil.boamApplicationRTIDs LIMIT 1), 
                	Contact.Account.Assessor_Certification_Number__pc, Application_Received_Date__c,
                	Type, Status, RecordType.Name 
                	FROM Case WHERE Id =: caseId LIMIT 1] ;
    }
    
    // this method returns the map of rectype name and id 
    @AuraEnabled
    public static Map<String, Id> getRecTypeIdMap(){
        Map<String, Id> retMap = new Map<String, Id>();
        for(RecordType thiElem:  [SELECT Id, Name FROM RecordType WHERE sObjectType = 'Case']){
            retMap.put(thiElem.Name, thiElem.Id);
        }
        return retMap;
    }
    
    // this method returns rectype id for Assessment   
    @AuraEnabled
    public static Id getAssessmentCaseDetails(){
       RecordType thisRecType = [SELECT Id, Name 
                FROM RecordType 
                WHERE sObjectType = 'Case' 
                AND Name = 'Assessment' LIMIT 1];
        
        return thisRecType.Id;
    }
}
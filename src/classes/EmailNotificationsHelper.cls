/*------------------------------------------------------------
    Author:         Sakshi Chauhan
    Company:        System Partners
    Description:    Email Notification Utility
    Inputs:         n/a
    Test Class:     
    History
    <Date>      <Authors Name>      <Brief Description of Change>
    14-Aug-17    Sakshi Chauhan          Created
    ------------------------------------------------------------*/  
    
public class EmailNotificationsHelper {
    
    /*************************************************************      *********************************
    * @description prepare email properties
    * 
    * @param Template Id,Email recipeint address, merged fields,target object id,Org wide FRom email address, Save as activity property value
    * @return Email Message
    * @ Created By : Sakshi Chauhan ( 14th Aug 2017 )
    */
    public static Messaging.SingleEmailMessage sendSingleEmail(String templateId, String toEmail,
          Map<String, String> mapSubjectCustomMergeFields, Map<String,
          String> mapBodyCustomMergeFields, String targetObjectId, String orgWideEmailAddressId, Boolean saveAsActivity) {
          
          // Create email object and assign the template to get the subject/body
          Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          mail.setTemplateId(templateId);
          mail.setToAddresses(new string[]{toEmail});
          //mail.setBccAddresses(new string[]{'santhosh.lakshmanan@systempartners.com'});
          mail.setTargetObjectId(targetObjectId);
          mail.setSaveAsActivity(saveAsActivity);
          if( orgWideEmailAddressId != null)
              mail.setOrgWideEmailAddressId(orgWideEmailAddressId);
          if (mapSubjectCustomMergeFields !=null || mapBodyCustomMergeFields!=null){
              mail = renderEmailMessage(mail);
              String subject = mail.getSubject();
              String emailBody = mail.getHtmlBody();
              if (String.isBlank(emailBody)) {
                   emailBody = mail.getPlainTextBody();
              }
              // Apply merge fields to the email subject if necessary
              if (mapSubjectCustomMergeFields != null) {
                   subject = applyMergeFields(subject, mapSubjectCustomMergeFields);
              }
              // Apply merge fields to the email body if necessary
              if (mapBodyCustomMergeFields != null) {
                   emailBody = applyMergeFields(emailBody, mapBodyCustomMergeFields);
              }
              mail.setSubject(subject);
              mail.setHTMLBody(emailBody);
          }  
          system.debug('mail is: '+mail);
          return mail;
     }
    
     private static String applyMergeFields(String textToModify, Map<String, String> mapCustomMergeFields) {
          system.debug('texttomodify is;'+textToModify);
          String newText = textToModify;
          for (String mergeField : mapCustomMergeFields.keySet()) {
               newText = newText.replace(mergeField, mapCustomMergeFields.get(mergeField));
          }
          return newText;
     }

     public static Messaging.SingleEmailMessage renderEmailMessage (Messaging.SingleEmailMessage message) {
          system.assertNotEquals(null, message.getTemplateId());
          Savepoint sp = Database.setSavepoint();
          Messaging.sendEmail(new Messaging.SingleEmailMessage[] { message });
          Database.rollback(sp);
          message.setTemplateId(null);
          return message;
     }   
    
}
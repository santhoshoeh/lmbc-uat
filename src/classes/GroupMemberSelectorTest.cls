@isTest
private class GroupMemberSelectorTest
{
	@testSetup
	static void createGroupAndMembers() {
		Group groupRecord = new Group();
		groupRecord.Name = 'Test Group';
		insert groupRecord;

		GroupMember gMember = new GroupMember();
		gMember.GroupId = groupRecord.Id;
		gMember.UserOrGroupId = TestDataGenerator.Admin.Id;
		insert gMember;
	}
	
	@isTest
	static void testGroupMemberSelectorTest() {
		List<GroupMember> gMembers = GroupMemberSelector.getGroupMemberByGroupName('Test Group');
		system.assertEquals(gMembers.size(), 1);

		Group groupRecord = GroupMemberSelector.getGroupByGroupName('Test Group');
		system.assertNotEquals(groupRecord, null);
	}	
}
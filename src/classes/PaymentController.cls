/**
 * Created by junzhou on 5/7/17.
 */

public with sharing class PaymentController {
    @AuraEnabled
    public static PaymentOption getPaymentOption(String caseId) {
        PaymentOption pOption = new PaymentOption();
        Community_Settings__mdt BAASComunitySetting = CommunitySettingsSelector.getPaymentSettings();
        Case currentCase = CaseSelector.getCasePaymentDetails(caseId);

        if(currentCase.Status == 'Submitted' || currentCase.Status == 'Closed') {
            pOption.message = 'Application is submitted or closed.';
        }else {
            if(currentCase.Subscription_fee_payment_status__c == 'Pending Payment') {
                pOption.businessUnit = 'BAAS';
                pOption.paymentItem = 'Subscription Fee';
                pOption.applicationFee = String.valueOf(BAASComunitySetting.Subscription_Fees__c);
                pOption.paymentMethod = new List<String>{'Credit Card', 'Bank Transfer'};
                pOption.message = 'Success';

            }else if(currentCase.Subscription_fee_payment_status__c == 'Paid' && currentCase.Certification_fee_payment_status__c =='Pending Payment') {
                pOption.businessUnit = 'BAAS';
                pOption.paymentItem = 'Certification Fee';
                pOption.applicationFee = String.valueOf(BAASComunitySetting.Certification_Fee__c);
                pOption.paymentMethod = new List<String>{'Credit Card','Bank Transfer'};
                pOption.message = 'Success';

            }else {
                pOption.message = 'Incorrect payment status, please contact Admin';
            }
        }
        return pOption;
    }

    @AuraEnabled
    public static String processPayment(String sPaymentJSONString) {
        String commUrl = Network.getLoginUrl(UtilityClass.getCommunityNetworkId('BAAS'));
        commUrl = commUrl.substring(0, commUrl.lastIndexOf('/')+1) ;
        PageReference pr;
        SelectPaymenet sPayment = (SelectPaymenet)JSON.deserialize(sPaymentJSONString, SelectPaymenet.class);
        PaymentProcesses.requestParameters reqs = new PaymentProcesses.requestParameters();
        reqs.caseId = sPayment.caseId;
        reqs.finalAmount = sPayment.approvalFee;
        reqs.customerName = ((PaymentProcessUtility.getCurrentContact().FirstName + ' '+ PaymentProcessUtility.getCurrentContact().LastName).length()>50? 
                            (PaymentProcessUtility.getCurrentContact().FirstName + ' '+ PaymentProcessUtility.getCurrentContact().LastName).left(50) :
                            (PaymentProcessUtility.getCurrentContact().FirstName + ' '+ PaymentProcessUtility.getCurrentContact().LastName) );
        reqs.customerId = PaymentProcessUtility.getCurrentContact().AccountId;
        reqs.referenceNumber =  'BAAS'+PaymentProcessUtility.generateRandomString();
        reqs.purchaseItem = sPayment.paymentItem;
//        system.debug('paymentoption is:' +paymentOption);
        reqs.paymentstatus = 'Pending Payment';

        if (sPayment.paymentMethod == 'Credit Card') {
            PaymentProcesses pProcesses = new PaymentProcesses();
            String redirectURL = pProcesses.processQuickWebRequest(reqs);
            pr = new PageReference(redirectURL);
            pr.setRedirect(true);

            if(pr != null) {
                TransactionService tService = new TransactionService();
                Transaction__c transactionRec = tService.CreateTransaction(reqs,'Credit_Card');
            }

        }
        else if (sPayment.paymentMethod == 'Bank Transfer') {
            //Invoke the bank transfer page;
            String redirectURL = commUrl+'BAASBankTransferPayment';
            pr = new PageReference(redirectURL);
            Transaction__c transactionRec = new Transaction__c();
            if(pr != null) {
                TransactionService tService = new TransactionService();
                transactionRec = tService.CreateTransaction(reqs, 'Bank_Transfer');
            }
            pr.getParameters().put('Id', transactionRec.Id);
            pr.setRedirect(false);
        }
        return pr.getUrl();
    }

    public class PaymentOption{
        @AuraEnabled
        public String businessUnit{get; set;}
        @AuraEnabled
        public String paymentItem{get; set;}
        @AuraEnabled
        public String applicationFee{get; set;}
        @AuraEnabled
        public String approvalFee{get; set;}
        @AuraEnabled
        public List<String> paymentMethod{get; set;}
        @AuraEnabled
        public String message {get; set;}
    }

    public class SelectPaymenet{
        private String caseId;
        private String businessUnit;
        private String paymentItem;
        private decimal applicationFee;
        private decimal approvalFee;
        private String paymentMethod;
    }
}
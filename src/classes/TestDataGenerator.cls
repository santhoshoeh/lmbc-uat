@isTest
public with sharing class TestDataGenerator{
    public static Integer defaultDummyRecordCount{get;set;}
    
    static{
        defaultDummyRecordCount = 1;
    }
    public class DefaultDummyRecordSet{
        public List<User> users {get; set;}
        public List<Account> clients {get; set;}
        public List<Contact> contacts {get; set;}
        public List<Case> cases {get; set;}
 
        public Set<Id> userIdSet {get; set;}
        public Set<Id> clientIdSet {get; set;}
        public Set<id> contactIdSet {get; set;}
        public Set<id> caseIdSet {get; set;}

        public Map<Id, user> userMap {get; set;}
        public Map<Id, Account> clientMap {get; set;}
        public Map<Id, Contact> contactMap {get; set;}
        public Map<Id, Case> caseMap {get; set;}


        public DefaultDummyRecordSet() {
            users = new List<User>();
            clients = new List<Account>();
            contacts = new List<Contact>();
            cases = new List<Case>();

            userIdSet = new Set<Id>();
            clientIdSet = new Set<Id>();
            contactIdSet = new Set<Id>();
            caseIdSet = new Set<Id>();

            userMap = new Map<Id, user>();
            clientMap = new Map<Id, Account>();
            contactMap = new Map<Id, Contact>();
            caseMap = new Map<Id, Case>();
        }
    }
    
    public static User admin{
        get{
            system.debug('admin ='+ admin);
            if(admin ==null){
                Integer rand = Math.round(Math.random()*1000);


                admin = new User(Username='oehbasssadminTest'+rand+'@oeh.com',
                        LastName='lnAdmin',Email='oehbasssadmin'+rand+'@oeh.com', Alias='oehad',
                        CommunityNickname='coehbasssadmin'+rand+'@oeh.com',
                        TimeZoneSidKey='Australia/Sydney', LocaleSidKey='en_AU',
                        EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US',
                        ProfileId= adminProfile.Id, isActive=true, Business_Unit__c='BAAS');
                insert admin;
            }
            return admin;
        }    
    }
    
    public static User trainingProvider{
        get{
            if(trainingProvider ==null){
                Integer rand = Math.round(Math.random()*1000);
                Account ac = new Account(name ='Grazitti') ;
                insert ac; 
       
                Contact con = new Contact(LastName ='testCon',AccountId = ac.Id);
                insert con;
                
                trainingProvider = new User(Username='oehbassstrainingprovider'+rand+'@oeh.com',
                        LastName='lnAdmin',Email='oehbassstrainingprovider'+rand+'@oeh.com', Alias='oehtp',
                        CommunityNickname='oehbassstp'+rand+'@oeh.com',
                        TimeZoneSidKey='Australia/Sydney', LocaleSidKey='en_AU',
                        EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US', Business_Unit__c = 'BAAS',
                        ProfileId= ProfileSelector.trainingProviderProfile.Id, isActive=true,  ContactId = con.Id);
                insert trainingProvider;
            }
            return trainingProvider;
        }    
    }

    public static User accreditedAssessor{
        get{
            if(accreditedAssessor ==null){
                Integer rand = Math.round(Math.random()*1000);
                Account ac = new Account(name ='Accredited') ;
                insert ac; 
       
                Contact con = new Contact(FirstName='Accredited', LastName ='Assessor',AccountId = ac.Id);
                insert con;
                
                trainingProvider = new User(Username='oehbasssaa'+rand+'@oeh.com',
                        LastName='lnAdmin',Email='oehbasssaa'+rand+'@oeh.com', Alias='oehaa',
                        CommunityNickname='oehbasssaa'+rand+'@oeh.com',
                        TimeZoneSidKey='Australia/Sydney', LocaleSidKey='en_AU',
                        EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US',Business_Unit__c = 'BAAS',
                        ProfileId= ProfileSelector.accreditedAssessorProfile.Id, isActive=true,  ContactId = con.Id);
                insert trainingProvider;
            }
            return trainingProvider;
        }    
    }

    public static User BAASAdmin{
        get{
            if(BAASAdmin ==null){
                Integer rand = Math.round(Math.random()*1000);
                Account ac = new Account(name ='BAASAdmin') ;
                insert ac; 
       
                Contact con = new Contact(FirstName='BAAS', LastName ='Admin',AccountId = ac.Id);
                insert con;
                system.debug('profileId ='+ BAASadminProfile.Id);
                BAASAdmin = new User(Username='oehbasssadmin'+rand+'@oeh.com',
                        LastName='lnAdmin',Email='oehbasssadmin'+rand+'@oeh.com', Alias='oehaa',
                        CommunityNickname='oehbasssadmin'+rand+'@oeh.com',
                        TimeZoneSidKey='Australia/Sydney', LocaleSidKey='en_AU',
                        EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US',
                        Business_Unit__c = 'BAAS',
                        ProfileId= BAASadminProfile.Id, UserRoleID = adminRole.Id, isActive=true);
                insert BAASAdmin;
            }
            return BAASAdmin;
        }    
    }

    private static Profile adminProfile{
        get{
            if(adminProfile ==null){
                adminProfile = [select Id, Name from Profile where UserType='Standard' and Name='System Administrator' limit 1];
            }
            return adminProfile;
        }    
    }

    private static Profile BAASadminProfile{
        get{
            if(BAASadminProfile ==null){
                BAASadminProfile = [select Id, Name from Profile where UserType='Standard' and Name='~ BAAS Admins' limit 1];
            }
            return BAASadminProfile;
        }    
    }

  private static UserRole adminRole{
        get{
            if(adminRole ==null){
                adminRole = [select Id, Name from UserRole WHERE ParentRoleID = null ORDER BY Name ASC LIMIT 1 ];
            }
            return adminRole;
        }    
    }

    public static DefaultDummyRecordSet createDefaultDummyRecordSet(){
        DefaultDummyRecordSet rs = new DefaultDummyRecordSet();
        // Create Users
        //createUsers(rs);

        if (admin == null) {
            system.debug(LoggingLevel.WARN, 'When deploying code to brand new instance, DummyRecordCreator need the ability to create super admin and use it to create test data in order to pass all tests');
            createRecordSet(rs);
        } else {
            System.runAs(admin){
                createRecordSet(rs);
            }
        }

        return rs;
    }

    public static void createRecordSet(DefaultDummyRecordSet rs) {
        // Create accounts
        createPersonAccounts(rs);

        //// Create contacts
        generatePersonAccountsContacts(rs);

        createCases(rs);

        //createCandidates(rs);

        //createVacancies(rs);

        //createResumes(rs);

        //createSkillGroups(rs);
    }

    private static void createPersonAccounts(DefaultDummyRecordSet rs){
        Id personAccountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        for (Integer i = 0; i < defaultDummyRecordCount; i++) {
            Account acct = new Account();
            acct.RecordTypeId = personAccountRecTypeId;
            acct.LastName = 'PersonAccount_'+i;
            acct.Personemail = 'PersonAccount_'+i+'@baastest.com';
            acct.Business_Unit__pc = 'BAAS';
            acct.Business_Unit__c = 'BAAS';
            rs.clients.add(acct);
        }
        insert rs.clients;
        // Create a map for quicker access to accounts by their ids
        rs.clientMap = new Map<Id, Account>(rs.clients);
        for(Account acct: rs.clients) {
            rs.clientIdSet.add(acct.id);
        }
    }

    private static void generatePersonAccountsContacts(DefaultDummyRecordSet rs) {
        for(Account acct: [SELECT Name, Id, PersonMobilePhone, Business_Unit__pc,
                           (SELECT Id, Email,FirstName,LastName FROM Contacts) FROM Account 
                            WHERE Approved__pc = false
                            AND Email_Verified__pc = true
                            AND Business_Unit__pc = 'BAAS'
                            AND RecordTypeId =: Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId()]) {
            rs.contacts.add(acct.Contacts[0]);
            rs.contactIdSet.add(acct.Contacts[0].Id);
        }

        rs.contactMap = new Map<Id, Contact>(rs.contacts);
    }

    private static void createCases(DefaultDummyRecordSet rs) {
        RecordTypeSelector rTypeSelector = new RecordTypeSelector();
        List<RecordType> AAapplicationRecType = rTypeSelector.fetchRecordTypesByName('Accreditor_Assessor_Application');
        for(Integer i = 0; i < defaultDummyRecordCount; i++) {
            Case personAccountCase = new Case();
            personAccountCase.subject = 'PersonAccount_Case_'+i;
            personAccountCase.RecordTypeId = AAapplicationRecType[0].Id;
            personAccountCase.Origin = 'Web';
            personAccountCase.Business_Unit__c = 'BAAS';
            personAccountCase.Subscription_fee_payment_status__c = 'Pending Payment';
            rs.cases.add(personAccountCase);
        }
        insert rs.cases;

        rs.caseMap = new Map<Id, Case>(rs.cases);
        for(Case ca: rs.cases) {
            rs.caseIdSet.add(ca.id);
     }
	        
    }
    
    public static Transaction__c createTransaction(Id AccountId, Decimal Amount)
    {
        
        Transaction__c transactionRec = new Transaction__c();
        transactionRec.Reference_Number__c =  'BAAS'+PaymentProcessUtility.generateRandomString();
        transactionRec.Account__c = AccountId;
        transactionRec.Payment_Amount__c = Amount;
        transactionRec.Payment_Status__C = 'Pending Payment';
        transactionRec.Bank_Payment_Status__c = 'Pending Payment';
        transactionRec.of_Records_Linked__c = '1';

        return transactionRec;        
    }
}
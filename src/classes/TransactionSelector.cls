/**
 * Created by junzhou on 18/7/17.
 */

public without sharing class TransactionSelector {
    private static String QUERY_STRING = 'SELECT Id, Reference_Number__c, Purchase_Item__c, Payment_Amount__c, Case__c, Payment_Status__c, Bank_Payment_Status__c FROM Transaction__c';

    public static Transaction__c getTransactionById(Id transactionId) {
        Transaction__c tRecord = new Transaction__c();

        String queryString = QUERY_STRING + ' WHERE Id=: transactionId';
        tRecord = Database.query(queryString);
        return tRecord;
    }

    public static Transaction__c getTransactionByReferenceNumber(String referenceNumber) {
    	Transaction__c tRecord = new Transaction__c();

        String queryString = QUERY_STRING + ' WHERE Reference_Number__c=: referenceNumber';
        tRecord = Database.query(queryString);
        return tRecord;
    }
}
@RestResource(urlMapping='/creditcardtransactions/*')
global class CreditCardServiceREST 
{
    /*------------------------------------------------------------------------
    Author:        Santhosh
    Company:       System Partners
    Description:   This REST service allows OEH MW (Biztalk) to post credit card responses to Salesforce; 
    Test Class:    
    History
    <Date>      <Authors Name>     <Brief Description of Change>
    10-Jul-2017    Santhosh         Initial Build
    --------------------------------------------------------------------------*/
    
    //====================================================================================
    // *** REST POST *** : Method to create a new customer;
    //====================================================================================
    
    list<Application_Log__c> exceptionLogs = new list<Application_Log__c>();
    
    /*********TO DO WRITE TO LOGS AND TO SET THE BEVERAGE TRANSACTIONS **********/
    
    @HttpPost
    global static void processCreditCard(creditcardtransaction[] creditcardtransactions)
    {    
       RestResponse res = RestContext.response;
       RestRequest req = RestContext.request;
       res = new RestResponse();
       ServiceResponse result = new ServiceResponse();
       result.cctransactionresponses = new List<transactionresponse>(); 
        
       list<Transaction__c> ccTransactionsToUpdate = new list<Transaction__c>();
        
       map<String, creditcardtransaction> transactions = new map<String, creditcardtransaction>();
        
       try
       {
           res.statusCode = 200;
           for (creditcardtransaction cc : creditcardtransactions)
           {
               //assess if there is a reference number; if not log the message to the exception log;
               if (String.isnotBlank(cc.referencenumber) && cc.referencenumber.contains(SystemConstants.CDS_PROJECT))
               {
                   //assess if this transaction is in the system;
                   transactions.put(cc.referencenumber, cc);
                              
               }
               //send this back as an error to BT and also log this exception;
               else
               {
                   //a transaction reference number was not present or is invalid transaction reference number;
                   result.cctransactionresponses.add(new transactionresponse('FAILED','BAAS001','Not a valid BAAS Transaction reference number',cc.referencenumber,'',''));
               }
           }
           
           //get the transactions;
           if (!transactions.isempty())
           {
               map<String, Transaction__c> transactionObjs = new map<String, Transaction__c>(queryTransactions(transactions.keySet()));
               
               for (creditcardtransaction cc : transactions.values())
               {
                   if (transactionObjs.containskey(cc.referencenumber))
                   {
                       //transaction record is available;Assess if the payment amount matches and if its not paid already;
                       Transaction__c tr = transactionObjs.get(cc.referencenumber);
                       if(tr.Bank_Payment_Status__c != 'Paid')
                       {
                           if (tr.Payment_Amount__c == Decimal.valueOf(cc.paymentamount))
                           {
                               //now set all values;
                               tr.Receipt_Number__c = cc.receiptnumber;
                               tr.Response_Code__c = cc.responsecode;
                               tr.Response_Description__c = cc.responsedescription;
                               tr.Payment_Date__c = UtilityClass.convertStringToDate(cc.paymentdate);
                               tr.Summary_Code__c = cc.summarycode;
                               tr.Surcharge_Amount__c = Decimal.valueOf(cc.surchargeamount);
                               tr.Bank_Customer_Name__c = cc.bankcustomername;
                               tr.CardHolder_Name__c = cc.cardholdername;
                               tr.Card_Scheme__c = cc.cardscheme;
                               tr.Credit_Card_Last_4_digits__c = cc.cardlast4digits;
                               tr.Source_Product__c = cc.sourceproduct;
                               tr.Supplier_Business_Code__c = cc.supplierbusinesscode;
                               tr.Payment_Method__c = SystemConstants.CDS_CC_PAYMENT_METHOD;
                               tr.Bank_Payment_Status__c = SystemConstants.CDS_PAID_STATUS;
                               tr.Payment_Status__c = SystemConstants.CDS_PAID_STATUS;
                               tr.Bank_Payment_Status_Updated_Date__c = system.today();
                               ccTransactionsToUpdate.add(tr);
                               
                           }
                           else
                           {
                               //amount does not match; reject this;
                               result.cctransactionresponses.add(new transactionresponse('FAILED','BAAS002','Payment Amount in Salesforce does not match the amount sent',cc.referencenumber,String.valueOf(tr.Payment_Amount__c),cc.paymentAmount));

                           }
                       }
                       else //already paid transaction. error;
                       {
                          result.cctransactionresponses.add(new transactionresponse('FAILED','BAAS003','Cannot update a paid transaction.',cc.referencenumber,'',''));

                       }
                       
                   }
                   else{
                       
                       //not a valid transaction, respond to the message;
                       result.cctransactionresponses.add(new transactionresponse('FAILED','BAAS004','A record matching the reference number was not found.',cc.referencenumber,'',''));
                   }
               }
           }
           
           if (!ccTransactionsToUpdate.isEmpty())
           {
               //list<DatabaseSaveResult> updResult = new list<DatabaseSaveResult>
               list<Database.SaveResult> updResults = Database.update(ccTransactionsToUpdate, false);
               
               for (integer i=0;i<updResults.size();i++)
               {
                   if (updResults[i].isSuccess())
                   {
                       result.cctransactionresponses.add(new transactionresponse('SUCCESS',null,null,ccTransactionsToUpdate[i].Reference_Number__c,'',''));
                   }
                   else
                   {
                      result.cctransactionresponses.add(new transactionresponse('FAILED','BAAS005','Update Error: An error occurred when updating Salesforce. '+updResults[i].getErrors()[0].getMessage(),ccTransactionsToUpdate[i].Reference_Number__c,'',''));                   
                   }
               }
                   
                   
           }
           
  
       }
       catch (exception ex)
       {
           Application_Log__c exLog = new Application_Log__c();

       }
       RestContext.response.addHeader('Content-Type', 'application/json');
       RestContext.response.responseBody = Blob.valueOf(UtilityClass.stripJsonNulls(JSON.serialize(result)));        
       //res.addHeader('Content-Type', 'application/json');
       //res.responseBody = Blob.valueOf(UtilityClass.stripJsonNulls(JSON.serialize(result)));     
    }
    
    
 



    //====================================================================================
    // Method to retrieve the transaction records;
    //====================================================================================     
    
    public static map<string, Transaction__c> queryTransactions (set<String> referenceIds)
    {
         map<string, Transaction__c> transactionObjs = new map<String, Transaction__c>(); 
         string transactionQuery = UtilityClass.getQueryString(UtilityClass.getFieldMap('Transaction__c'), null) + 'Transaction__c where Reference_Number__c IN :referenceIds';
         system.debug('BAAS Project: '+ transactionquery);
                   
         //get the list of transactions based on the transaction reference number;
         for (Transaction__c tr : Database.query(transactionquery))
         {
             transactionObjs.put(tr.Reference_Number__c, tr);
         }
      return transactionObjs;
    }
    //====================================================================================
    // Method declaration for customer details;
    //====================================================================================

    global class creditcardtransaction
    {
        public String referencenumber;
        public String paymentamount;
        public String receiptnumber;
        public String responsecode;
        public String responsedescription;
        public String paymentdate;
        public String summarycode;
        public String surchargeamount;
        public String bankcustomername;
        public String cardholdername;
        public String cardscheme;
        public String cardlast4digits;
        public String sourceproduct;
        public String supplierbusinesscode;
    }
    
    global class transactionresponse
    {
        public String status;
        public String errorcode;
        public String errordescription;
        public String referencenumber;
        public String sfamount;
        public String wpamount;
        
        public transactionresponse(String tstatus, String terrorcode, String terrordescription, String treferencenumber, String tsfamount, String twpamount)
        {
            this.status = tstatus;
            this.errorcode = terrorcode;
            this.errordescription = terrordescription;
            this.referencenumber = treferencenumber;
            this.sfamount = tsfamount;
            this.wpamount = twpamount;
        }
        
    }
    
    public class ServiceResponse
    {
        public list<transactionresponse> cctransactionresponses;
    }
}
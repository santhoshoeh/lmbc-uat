@isTest
private class RedirectToAssessmentCalcControllerTest{
    static Case cs;
    static void loadData(){
        ID personAccRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

            Account acc1 = new Account(
                RecordTypeID=personAccRecTypeId,
                FirstName='FirstName ',
                LastName='LastName from Test Class',
                PersonMailingStreet='Mail Street',
                PersonMailingPostalCode='PostCode',
                PersonMailingCity='MailCity',
                PersonEmail='mailEmailFromClass@test.com',
                Email_Verified__pc = true,
                Business_Unit__pc = 'BOAM'
            );
        insert acc1;

        Contact cont = [select id, AccountID from Contact where AccountID = :acc1.ID];

        Case parentCs = new Case(
            Subject = 'Case from Test Class',
            RecordTypeID = CaseUtil.parentCaseRTID,
            ContactID = cont.ID
        );
        insert parentCs;
        
        Case_Party__c cp = new Case_Party__c(
                        Case__c = parentCs.ID,
                        First_Name__c = acc1.FirstName,
                        Middle_Names__c = 'test',
                        Last_Name__c = acc1.LastName,
                        Person_Email__c = acc1.PersonEmail,
                        Assessor_accreditation_number__c = '1234',
                        RecordTypeID = CasePartyUtil.assessorRTID,
                        Role__c = CasePartyUtil.assessorRoleName
            );
        insert cp;

        cs = new Case(
            Subject = 'Case from Test Class',
            RecordTypeID = CaseUtil.assesmentRTID,
            Related_cases__c = parentCs.ID,
            ContactID = cont.ID
        );
        insert cs;
        
    }
    static testmethod void test_Trigger() {
        loadData();
        Test.StartTest();
            ApexPages.StandardController stdController = new ApexPages.StandardController(cs);
            RedirectToAssessmentCalcController cont = new RedirectToAssessmentCalcController(stdController);
        Test.StopTest();
    }
}
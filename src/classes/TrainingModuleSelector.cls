/**
 * Created by junzhou on 4/7/17.
 */

public with sharing class TrainingModuleSelector {
    private static String TRAINING_MODULE_STRING = 'SELECT Id, Name, Compulsory__c FROM Training_Modules__c';

    public static List<Training_Modules__c> getTrainingModules() {
        List<Training_Modules__c> trainingModules = new List<Training_Modules__c>();
        String queryString = TRAINING_MODULE_STRING + ' WHERE Active__c = true';
        trainingModules = Database.query(queryString);
        return trainingModules;
    }
}
// test class for NVRM_CreateCases

@isTest
public class NVRM_TestCreateCases{
    
    public static String testEmail = 'dave@gmail.com';
    public static String userName = 'demo1@demopods.com';
    public static Case caseObj;
    
    @testSetup
    public static void createTestData(){
        
        
        User userVar = new user(alias='demo1',LastName='Demo1',Username=userName,ProfileId = NVRM_Utility.NVRM_ADMIN_PROFILE,EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',Email=userName,LocaleSidKey='en_AU',TimeZoneSidKey='Australia/Sydney',Business_Unit__c='NVRM');
        insert userVar;
        system.runAs( userVar ){
            Account personAccount = new Account();
            personAccount.recordTypeID = NVRM_Utility.PERSON_ACC_RECORDTYPE;
            personAccount.FirstName= 'Jack';
            personAccount.LastName = 'Wayne';
            personAccount.personemail = testEmail;
            insert personAccount;
            Contact contactVar  = [ SELECT Id FROM COntact WHERE AccountId = :personAccount.Id ];
            Case caseLIMR = new Case();
            caseLIMR.recordTypeID = NVRM_Utility.LIMR_RECORDTYPE;
            caseLIMR.status= 'New';
            caseLIMR.Business_Unit__c = 'NVRM';
            caseLIMR.ContactId = contactVar.Id;
            insert caseLIMR;
            
        }
        
    
    }

    @isTest
    public static void testGetCaseDetails(){
        caseObj = [ SELECT Status,Total_clock_days__c, Days_Left__c FROM Case ORDER BY LastModifiedDate LIMIT 1];
        Test.startTest();
            caseObj = NVRM_CreateCases.getCaseDetails(caseObj.Id);
        Test.stopTest();

    }

    
    /*@isTest
    public static void testGetUserContactId(){
        Test.startTest();
            Id profileid = Label.NVRM_Proponent_ProfileId;
            Id roleId = [ SELECT  Name FROM UserRole WHERE PortalType ='CustomerPortal' LIMIT 1].ID;
            Contact con = [select id,name,email,lastname,firstname,accountid from contact where email = :testEmail limit 1];
            User userObj = new User(contactId=con.Id, UserRoleId=roleId,username=con.Email, firstname=con.FirstName,
            lastname=con.LastName, email=con.Email,communityNickname = con.LastName + '_fn' ,
            alias = string.valueof(con.FirstName.substring(0,1) + con.LastName.substring(0,1)), profileid = profileid, emailencodingkey='UTF-8',
            languagelocalekey='en_US', localesidkey='en_US', timezonesidkey='America/Los_Angeles');
            
            insert userObj;
        Test.stopTest();
            
        system.debug(caseObj.Id );
        system.debug('testEmail ' + testEmail);
        
        User userObjtest = [ SELECT Id,ContactID FROM User WHERE email= :testEmail ];
        Id contactId = NVRM_CreateCases.getUserContactId( userObj.Id );
        system.debug('userObj' + userObjtest.ContactId);
        system.debug('contactId' + contactId);
        system.assertEquals(contactId,userObjtest.ContactId);
        
    }*/
    
    
    @isTest
    public static void testRecordType(){
        Id caseID = NVRM_Utility.getCaseRecordTypeID('LIMR');
        Test.startTest();
            ID caseIDTest = NVRM_CreateCases.getCaseRecordType('LIMR');
        Test.stopTest();

        system.assertEquals(caseID,caseIDTest);
    }

    @isTest
    public static void testClockDetailsClockRunning(){
        caseObj = [ SELECT Status,Applicant_Type__c,RecordTypeId,ContactID,Total_clock_days__c, Days_Left__c FROM Case ORDER BY LastModifiedDate LIMIT 1];
        system.debug('@@@caseObj '+ caseObj);
        caseObj.Status = 'Submitted';
        update caseObj;
        caseObj.Status = 'Application Accepted';
        update caseObj;
        caseObj = [ SELECT Status,Applicant_Type__c,RecordTypeId,ContactID,Total_clock_days__c, Days_Left__c FROM Case ORDER BY LastModifiedDate LIMIT 1];
        Set<String> clockRunningStatus = NVRM_Utility.getCaseClockRunningStatus();
        String actualJSON = NVRM_CreateCases.getCaseClockDetails(caseObj.ID );
        system.debug(actualJSON);
      
    }
    @isTest
    public static void testClockDetailsCaseIdNull(){
        String actualJSON;
        Test.startTest();
            actualJSON = NVRM_CreateCases.getCaseClockDetails(null);
        Test.stopTest();
        system.debug(actualJSON);
      
    }

    
    @isTest
    public static void testClockDetailsClockStop(){
        User userVar = [ SELECT Id,Business_Unit__c FROM User WHERE Username= :userName];
        system.debug('User business unit ' + userVar.Business_Unit__c);
         Case caseObjLIMR = new Case();
        
        String actualJSON ;
        Test.startTest();
            system.runAs(userVar){

                Account personAccount = new Account();
                personAccount.recordTypeID = NVRM_Utility.PERSON_ACC_RECORDTYPE;
                personAccount.FirstName= 'Jack';
                personAccount.LastName = 'Wayne';
                personAccount.Identification_provided__c = 'Drivers license';
                personAccount.PersonEmail = 'testuser@gmail.com';
                personAccount.DL_Expiry_date__c = system.today() + 20;
                personAccount.Drivers_license_number__c = 'D1212';
                personAccount.Phone = '04467882799';
                insert personAccount;
                Contact contactVar  = [ SELECT Id FROM COntact WHERE AccountId = :personAccount.Id ];
               
                caseObjLIMR.recordTypeID = NVRM_Utility.LIMR_RECORDTYPE;
                caseObjLIMR.status= 'New';
                caseObjLIMR.ContactId = contactVar.Id;
                caseObjLIMR.Enter_Lot_DP_details__c = true;
                caseObjLIMR.Applicant_Type__c = 'Agent';
                caseObjLIMR.ContactId = contactVar.Id;
                caseObjLIMR.Business_Unit__c = 'NVRM';
                insert caseObjLIMR;
                Case_Party__c caseParty = new Case_Party__c();
                caseParty.First_NAme__c='Landholder';
                caseParty.Last_Name__c='test';
                caseParty.Identification_provided__c = 'Drivers License';
                caseParty.Drivers_license_number__c = 'D121323';
                caseParty.DL_Expiry_date__c = system.today() + 100;
                caseParty.Street_Number__c = '12';
                caseParty.Street_Name__c = 'York Street';
                caseParty.City_Town__c = 'Sydney';
                caseParty.State__c = 'NSW';
                caseParty.Country__c = 'AU';
                caseParty.Postcode__c = '2000';
                caseParty.Phone__c='0423242424';
                caseParty.Case__c = caseObjLIMR.Id;
                caseParty.Use_street_address_as_mailing_address__c = true;
                insert caseParty;
                system.debug(' caseObjLIMR before case update submit ##3' + caseObjLIMR);
                caseObjLIMR.Status='Submitted';
                caseObjLIMR.Submitted_Date__c=system.today();
                update caseObjLIMR;
                
            }
            caseObjLIMR = [ SELECT Business_Unit__c,Status,OwnerID,Owner.name FROM Case WHERE ID = :caseObjLIMR.Id];
                system.debug('caseObjLIMR ### ' +caseObjLIMR.Business_Unit__c );
                caseObjLIMR.Status = 'Application Accepted';
                caseObjLIMR.OwnerID = userVar.ID;
                update caseObjLIMR;
                system.runAs(userVar){    
                    caseObjLIMR.Status = NVRM_Utility.caseStatus_RFI;
                    update caseObjLIMR;

                    actualJSON = NVRM_CreateCases.getCaseClockDetails(caseObjLIMR.ID );

                }
        Test.stopTest();
        NVRM_CreateCases.ClockStatus jsonObj = (NVRM_CreateCases.ClockStatus)JSON.deserialize(actualJSON,NVRM_CreateCases.ClockStatus.class);
        system.assertEquals(jsonObj.clockStopped,true);
        system.debug('jsonObj### ' + jsonObj);

      
    }
    @isTest
    public static void testClockDetailsClockStopStatus(){
        User userVar = [ SELECT Id,Business_Unit__c FROM User WHERE Username= :userName];
        system.debug('User business unit ' + userVar.Business_Unit__c);
         Case caseObjLIMR = new Case();
        
        String actualJSON ;
        Test.startTest();
            system.runAs(userVar){

                Account personAccount = new Account();
                personAccount.recordTypeID = NVRM_Utility.PERSON_ACC_RECORDTYPE;
                personAccount.FirstName= 'Jack';
                personAccount.LastName = 'Wayne';
                personAccount.Identification_provided__c = 'Drivers license';
                personAccount.PersonEmail = 'testuser@gmail.com';
                personAccount.DL_Expiry_date__c = system.today() + 20;
                personAccount.Drivers_license_number__c = 'D1212';
                personAccount.Phone = '04467882799';
                insert personAccount;
                Contact contactVar  = [ SELECT Id FROM COntact WHERE AccountId = :personAccount.Id ];
               
                caseObjLIMR.recordTypeID = NVRM_Utility.LIMR_RECORDTYPE;
                caseObjLIMR.status= 'New';
                caseObjLIMR.ContactId = contactVar.Id;
                caseObjLIMR.Enter_Lot_DP_details__c = true;
                caseObjLIMR.Applicant_Type__c = 'Agent';
                caseObjLIMR.ContactId = contactVar.Id;
                caseObjLIMR.Business_Unit__c = 'NVRM';
                insert caseObjLIMR;
                Case_Party__c caseParty = new Case_Party__c();
                caseParty.First_NAme__c='Landholder';
                caseParty.Last_Name__c='test';
                caseParty.Identification_provided__c = 'Drivers License';
                caseParty.Drivers_license_number__c = 'D121323';
                caseParty.DL_Expiry_date__c = system.today() + 100;
                caseParty.Street_Number__c = '12';
                caseParty.Street_Name__c = 'York Street';
                caseParty.City_Town__c = 'Sydney';
                caseParty.State__c = 'NSW';
                caseParty.Country__c = 'AU';
                caseParty.Postcode__c = '2000';
                caseParty.Phone__c='0423242424';
                caseParty.Case__c = caseObjLIMR.Id;
                caseParty.Use_street_address_as_mailing_address__c = true;
                insert caseParty;
                system.debug(' caseObjLIMR before case update submit ##3' + caseObjLIMR);
                caseObjLIMR.Status='Submitted';
                caseObjLIMR.Submitted_Date__c=system.today();
                update caseObjLIMR;
                
            }
            caseObjLIMR = [ SELECT Business_Unit__c,Status,OwnerID,Owner.name FROM Case WHERE ID = :caseObjLIMR.Id];
                system.debug('caseObjLIMR ### ' +caseObjLIMR.Business_Unit__c );
                caseObjLIMR.Status = 'Application Accepted';
                caseObjLIMR.OwnerID = userVar.ID;
                update caseObjLIMR;
                system.runAs(userVar){    
                    caseObjLIMR.Status = 'Application Rejected';
                    update caseObjLIMR;

                    actualJSON = NVRM_CreateCases.getCaseClockDetails(caseObjLIMR.ID );
                    
                }
        Test.stopTest();
        NVRM_CreateCases.ClockStatus jsonObj = (NVRM_CreateCases.ClockStatus)JSON.deserialize(actualJSON,NVRM_CreateCases.ClockStatus.class);
        system.assertEquals(jsonObj.clockStopped,true);
        system.debug('jsonObj### ' + jsonObj);

      
    }

    @isTest
    public static void testClockDetailsClockException(){
        User userVar = [ SELECT Id,Business_Unit__c FROM User WHERE Username= :userName];
        String actualJSON;
        Test.startTest();
        try{
             actualJSON = NVRM_CreateCases.getCaseClockDetails(userVar.ID );
        }
        catch(Exception e){
            system.debug('exception message ###'+ e.getMessage());
        }
        Test.stopTest();
   }

}
/**
 * Created by junzhou on 20/6/17.
 */

public without sharing class AccountSelector {
    final static String PERSON_ACCOUNT_QUERY_BASE_1 = 'SELECT Id, is_Public_Available__c, Preferred_Contact_Phone__pc, Phone, Name, PersonHomePhone, PersonMobilePhone, PersonEmail, Organisation_Type__c, Work_locality__c, Assessor_Certification_Number__pc, valid_from__pc, Valid_to__pc FROM Account';

    public List<Account> getAccreditedAssessorsAccounts() {
        List<Account> accList = new list<Account>();

        RecordTypeSelector rTypeSelector = new RecordTypeSelector();
        List<RecordType> rTypeList = rTypeSelector.fetchRecordTypesByName('PersonAccount');
        Id PersonAccountRecordTypeId = rTypeList.get(0).Id;

        String queryString = PERSON_ACCOUNT_QUERY_BASE_1 + ' WHERE RecordTypeId =: PersonAccountRecordTypeId And Accredited_Assessor__pc = true And Assessor_Certification_Number__pc != null And valid_from__pc != null And Valid_to__pc>=Today AND is_Public_Available__c = true';
        accList = Database.query(queryString);
        return accList;

    }
}

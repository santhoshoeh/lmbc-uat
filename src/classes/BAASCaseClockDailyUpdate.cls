global with sharing class BAASCaseClockDailyUpdate implements Database.Batchable<sObject>{
	private static String BAAS_CASE_QUERY = 'SELECT BAAS_Clock_Start_Date__c,Status,Total_clock_days__c,Business_Unit__c FROM Case WHERE Business_Unit__c=\'BAAS\' AND BAAS_Clock_Start_Date__c != null';
    public Set<String> caseStatusSet;
    
    global BAASCaseClockDailyUpdate() {
        CaseClockStatus__mdt cClockStatus = CommunitySettingsSelector.getBASSClockRunningSettings();
        caseStatusSet = new Set<String>(cClockStatus.ClockRunningStatus__c.split(';'));
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(BAAS_CASE_QUERY);
    }

    global void execute(Database.BatchableContext BC, List<Case> scope) {
        List<Case>  caseListToUpdate = new List<Case>();
        system.debug('###### scope '+ scope);
        for(Case caseVar : scope){
        	if( caseVar.BAAS_Clock_Start_Date__c != null ){
	        	system.debug( 'caseVar.Total_clock_days__c ###' + caseVar.Total_clock_days__c);
	            if( caseVar.BAAS_Clock_Start_Date__c != null && caseVar.Status=='Panel Requests Additional Info'){
	                if( caseVar.Total_clock_days__c  == null)
	                        caseVar.Total_clock_days__c = 1;
	                //else if( caseVar.Total_clock_days__c > 25 ) {
	                //    caseVar.Total_clock_days__c += 1;
	                    
	                //}
	                caseListToUpdate.add( caseVar );
	                    
	            }
	            else if( caseVar.BAAS_Clock_Start_Date__c != null &&  caseStatusSet.contains( caseVar.Status) ){
	                if( caseVar.Total_clock_days__c  == null)
	                    caseVar.Total_clock_days__c = 1;
	                else
	                    caseVar.Total_clock_days__c += 1;
	                caseListToUpdate.add( caseVar );
	            }
        	}
        }
        update caseListToUpdate;
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
}
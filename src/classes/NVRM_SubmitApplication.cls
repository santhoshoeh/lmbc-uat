/*****************************************************************************
 *** NVRM_SubmitApplication
 *** Submit Application i.e Category Explanation Report/Land Initiated Map Review
 *** @Author: Sakshi Chauhan
 *** @Since: 07/07/17
 *** Copyright: System Partner
 *****************************************************************************/
 
 public class NVRM_SubmitApplication{
     
    /*************************************************************      *********************************
    * @description Checks for all validation before NVRM CAses are submitted i.e. All identityand contact details of Account and landholder details
    * @ used in Submit Case Component NVRM_SubmitApplicationComponent
    * @param Case Id
    * @return String of error mess if any otherwise returns null
    * @ Updated By : Sakshi Chauhan
    */
    
    @AuraEnabled
    public static String caseSubmitValidation(Id caseId){
        String lineBreak = '<br/>';
        system.debug('caseId   in aura ' + caseId);
        String messStr = '';
       
        if(caseId == null){
            return null;
        }
        Case thisCase;
         
        Account accountVar;
        //update the case status as submitted
        try{
            
            //Case thisCase = CaseSelector.getCaseDetails(caseId);
            thisCase = [SELECT id,AccountId,ContactId,Applicant_Type__c,Enter_Lot_DP_details__c,Attach_a_map_with_area__c, Status, Submitted_Date__c FROM Case WHERE id =: caseId];
            List<Lot_DP_Details__c> lotDPList = [ SELECT Lot__c, Deposited_Plan__c , Section__c FROM Lot_DP_Details__c WHERE Application__c = : thisCase.Id];
            
            if(  !String.isBlank(thisCase.ContactID) ){
                Contact contactVar = [ SELECT AccountId FROM Contact WHERE Id = :thisCase.ContactId ];
                accountVar = [ SELECT FirstName,PersonEmail,Identification_provided__c, ShippingCity,ShippingCountry ,ShippingPostalCode,ShippingState,ShippingStreet,Phone,PersonHomePhone,PersonMobilePhone FROM Account WHERE ID = :contactVar.AccountId];
            }
            List<Case_Party__c> caseParty = [ SELECT First_Name__c,Last_Name__c,Identification_provided__c,Street_Number__c,Street_Name__c,City_Town__c,State__c,Country__c, Postcode__c,Phone__c,Home_Phone__c,MobilePhone__c FROM Case_Party__c WHERE Case__c = :thisCase.ID AND recordTypeID= :NVRM_Utility.CASEPARTY_LANDHOLDER_RECTYPE];
            
            if( String.isBlank(thisCase.Applicant_Type__c) || String.isBlank(thisCase.ContactId) ){
                messStr += NVRM_Utility.APPLICANT_ERRORMESS + lineBreak;
                
            }
            if( thisCase.Enter_Lot_DP_details__c == false && thisCase.Attach_a_map_with_area__c ==false ){
                messStr += NVRM_Utility.LOTMETHOD + lineBreak;
                
            }
            if( lotDPList.size() <= 0 && thisCase.Enter_Lot_DP_details__c == true){
                messStr += NVRM_Utility.LOT_DP_EMPTY + lineBreak;
            }
            
            if( accountVar != null ){
                system.debug('#####accountVar ' + accountVar);
                if( String.isBlank(accountVar.FirstName) ){
                    messStr += NVRM_Utility.FIRSTNAME_ERRORMESS + lineBreak ;
                    
                }
                if( String.isBlank(accountVar.Identification_provided__c) ){
                    messStr += NVRM_Utility.IDENTITY_ERRORMESS + lineBreak ;
                    
                }
                // if any of the field is not empty i.e filled
                if( !String.isBlank(accountVar.ShippingCity) ||!String.isBlank(accountVar.ShippingCountry) || !String.isBlank(accountVar.ShippingPostalCode) || !String.isBlank(accountVar.ShippingState) || !String.isBlank(accountVar.ShippingStreet)){
                    // if any of the field is blank i.e all fields should be filled
                    if( String.isBlank(accountVar.ShippingCity) || String.isBlank(accountVar.ShippingCountry) || String.isBlank(accountVar.ShippingPostalCode) || String.isBlank(accountVar.ShippingState) || String.isBlank(accountVar.ShippingStreet)){
                        messStr += NVRM_Utility.POSTAL_ADD_ERRORMESS + lineBreak;
                    }
                }
                if( String.isBlank(accountVar.ShippingCity) && String.isBlank(accountVar.ShippingCountry) && String.isBlank(accountVar.ShippingPostalCode) && String.isBlank(accountVar.ShippingState) && String.isBlank(accountVar.ShippingStreet) &&  String.isBlank(accountVar.PersonEmail) ){
                    messStr += NVRM_Utility.ADD_ERRORMESS + lineBreak;
                }
                if( String.isBlank(accountVar.Phone) && String.isBlank(accountVar.PersonHomePhone) && String.isBlank(accountVar.PersonMobilePhone) ){
                    messStr += NVRM_Utility.PHONE_ERRORMESS + lineBreak;
                }
            }
            if( thisCase.Applicant_Type__c == 'Agent' && caseParty.size() == 0 ){
                system.debug('Inside Case Party size is ' + caseParty.size());
                
            
                messStr += NVRM_Utility.LANDHOLDER_ERRORMESS + lineBreak;
            }
            else{
                system.debug(' Inside else '+ caseParty);
                for( Case_Party__c casePartyVar : caseParty){
                    system.debug('#####3 casePartyVar' + casePartyVar);
                    if( String.isBlank(casePartyVar.Identification_provided__c) ){
                        messStr += NVRM_Utility.LANDHOLDER_IDENTITY_ERRORMESS + lineBreak;
                    }
                    if( String.isBlank(casePartyVar.Street_Number__c) || String.isBlank(casePartyVar.Street_Name__c) || String.isBlank(casePartyVar.City_Town__c)  || String.isBlank(casePartyVar.State__c) || String.isBlank(casePartyVar.Country__c) || casePartyVar.Postcode__c == null ){ 
                       messStr += NVRM_Utility.LANDHOLDER_POSTAL_ADD_ERRORMESS + lineBreak;
                    }
                    if( String.isBlank(casePartyVar.Phone__c) && String.isBlank(casePartyVar.Home_Phone__c) && String.isBlank(casePartyVar.MobilePhone__c) ){
                        messStr += NVRM_Utility.LANDHOLDER_PHONE_ERRORMESS + lineBreak;
                    }
                }
            }
            system.debug('messStr @@@' + messStr);
            if( messStr != ''){
                return messStr;
            } 
            else 
                return null; 
                      
            
        }catch(Exception exp){
            system.debug('Exception MEss ' + exp.getLineNumber() );
            system.debug('Exception MEss ' + exp.getCause() );
            system.debug('Exception MEss ' + exp.getStackTraceString());
            system.debug('Exception MEss ' + exp.getMessage() );
           return null;
        }
    }
    
    /*************************************************************      *********************************
    * @description Change the NVRM CAse status to Submitted and populates submit date for the first time case is submitted
    * @ used in Submit Case Component NVRM_SubmitApplicationComponent
    * @param Case Id
    * @return String of success/error mess 
    * @ Updated By : Sakshi Chauhan
    */
    @AuraEnabled
    public static String submitCaseMethod(Id caseId){
        system.debug('caseId   in aura ' + caseId);
        if(caseId == null){
            return 'Case Id is Null';
        }
        //update the case status as submitted
        try{
            Case thisCase = new Case(Id=caseId);
            thisCase.Status = 'Submitted';
            if( thisCase.Submitted_Date__c == null ){ 
                thisCase.Submitted_Date__c = system.today();
            }
            update thisCase;
            return 'Success';
        }catch(Exception exp){
           return exp.getMessage();
        }
    }

}
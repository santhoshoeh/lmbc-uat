trigger AssessedCreditTrigger on Assessed_Credit__c (before insert){
    AssessedCreditTriggerHandler.populateCaseLookups(trigger.new);
}
/************************ Case_Party__c Object Trigger *********************************************************
/*  This Apex trigger will be the only trigger on the Case_Party__c object.
*   The logic for each event will be implemented in methods called from the trigger handler class
*   This is recommended to control the order of execution of logic for the same event.
 * Author       :   Jerun Stanley
 * Created Date :   Jun 19 2017
  
 ******************************************************************************/

trigger CasePartyTrigger on Case_Party__c (before insert, after insert, before update, after update, before delete, after delete, after undelete){

    CasePartyTriggerHandler handler = new CasePartyTriggerHandler(Trigger.isExecuting, Trigger.size, Trigger.old, Trigger.new, Trigger.oldmap, Trigger.newmap);

    if(Trigger.isBefore){
        if(Trigger.isInsert)
            handler.OnBeforeInsert();
        else if(Trigger.isUpdate)
            handler.OnBeforeUpdate();
        else if(Trigger.isDelete)
            handler.OnBeforeDelete();
    }else{
        if(Trigger.isInsert)
            handler.OnAfterInsert();
        else if(Trigger.isUpdate)
            handler.OnAfterUpdate();
        else if(Trigger.isDelete)
            handler.OnAfterDelete();
        else if(Trigger.isUndelete)
            handler.OnAfterUndelete();
    }
}
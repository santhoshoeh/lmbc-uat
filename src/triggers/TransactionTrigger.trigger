trigger TransactionTrigger on Transaction__c (
    before insert, 
    before update, 
    before delete, 
    after insert, 
    after update, 
    after delete, 
    after undelete) 
{
    List<Transaction__c> transactions = new List<Transaction__c>();
    List<Transaction__c> paidTransactions = new List<Transaction__c>();

    if (Trigger.isAfter && Trigger.isUpdate)
    {
        for (Transaction__c trans : trigger.new)
        {
            //check if bank payment status has come;
            if (trans.Bank_Payment_Status__c == 'Paid' && trigger.oldmap.get(trans.Id).Bank_Payment_Status__c != 'Paid')
            {
                //update the beverage container payment status and set it active;
                //for renewal reset the renewal due flag and set the expiry date and registration status field;
                transactions.add(trans);
                paidTransactions.add(trans);
            }
            else if (trans.Payment_Status__c == 'Paid' && trigger.oldmap.get(trans.Id).Payment_Status__c != 'Paid')
            {
                //credit card payment;
                transactions.add(trans);

            }
            else if ((trans.Payment_Status__c == 'Payment Initiated' && trigger.oldmap.get(trans.Id).Payment_Status__c != 'Payment Initiated') || 
                     (trans.Payment_Status__c == 'Renewal - Payment Initiated' && trigger.oldmap.get(trans.Id).Payment_Status__c != 'Renewal - Payment Initiated'))
            {
                transactions.add(trans);
            }
            else if (trans.Payment_Status__c == 'Cancelled' && trigger.oldMap.get(trans.Id).Payment_Status__c != 'Cancelled')
            {
                transactions.add(trans);
            }
            else if (trans.Payment_Status__c == 'Not Processed' && trigger.oldMap.get(trans.Id).Payment_Status__c != 'Not Processed')
            {
                transactions.add(trans);
            }           
        }
    }

    if(!transactions.isEmpty()) new PaymentProcesses().updateCasePaymentStatus(transactions);
    if(!paidTransactions.isEmpty()) new PaymentProcesses().processInvoices(paidTransactions);

}
trigger CaseTrigger on Case (before insert, after insert,after update) {
    system.debug( ' @@@@User\'s business unit ' + CaseUtil.currUser.Business_Unit__c);
    
    Id systemAdminProfileId = NVRM_Utility.getProfileId('System Administrator');
    // if NVRM new Case is being created, it should have status as 'New'
    if(Trigger.isBefore && (Trigger.isInsert) && CaseUtil.currUser.Business_Unit__c == 'NVRM' && CaseUtil.currUser.profileId != systemAdminProfileId){
        for(Case caseVar : Trigger.new){
            if(caseVar.Status != 'New' && caseVar.Business_Unit__c == 'NVRM'){
                caseVar.addError('Case being created can only have only \'New\' Status');
            }
        }
    }
    
    //synch the related list(Property, lots, Parties) 
    //We need this only on insert event, later related lists will be added at parent level only
    //For synching from parent we have trigger on respective objects
    //- suraj
    if(Trigger.isAfter ){
        // when Application case is created
        system.debug('enetering synch related list flow');

        if(Trigger.isInsert){
            CaseTriggerHandler.syncApplicationCaseDetails(Trigger.new, Trigger.oldmap);//should run only for insert event
            CaseTriggerHandler.addCaseCreaterAsCaseParty(Trigger.new);//should run only for parent case
            CaseTriggerHandler.addCreditsFromAssessment( Trigger.new );
            CaseTriggerHandler.findCreditTransferCases( Trigger.new );
            CaseTriggerHandler.makeCaseContactReadOnly( Trigger.new, null, false );

            // nvrm los copy on LIMR creation from CER/LIMR
            if(CaseUtil.currUser.Business_Unit__c == 'NVRM')
                CaseTriggerHandler.copyCaseLots(  Trigger.new );
            
            
        }

        if(Trigger.isUpdate){
            CaseTriggerHandler.addCreditsToCreditHolding(Trigger.new, Trigger.oldmap);//should run only for Agreement
            CaseTriggerHandler.checkIfOwnershipChanged(Trigger.new, Trigger.oldmap);
            CaseTriggerHandler.checkIfTransferFinalized(Trigger.new, Trigger.oldmap);
            CaseTriggerHandler.checkIfTransferApproved(Trigger.new, Trigger.oldmap);
            CaseTriggerHandler.checkIfTransferCancelled(Trigger.new, Trigger.oldmap);
            CaseTriggerHandler.applicationApproved(Trigger.new, Trigger.oldmap);
            CaseTriggerHandler.makeCaseContactReadOnly( Trigger.new, Trigger.oldmap, true );
        }
    }

    if(Trigger.isAfter && Trigger.isUpdate ){
        // check if Case status value is changed to valid one for NVRM Cases
        if( CaseUtil.currUser.Business_Unit__c == 'NVRM' && CaseUtil.currUser.profileId != systemAdminProfileId ){
            CaseTriggerHandler.checkCaseStatusFlow(Trigger.newMap,Trigger.oldMap);
            CaseTriggerHandler.stampClockStatusData(Trigger.newMap,Trigger.oldMap);
        }else if(CaseUtil.currUser.Business_Unit__c == 'BAAS' && CaseUtil.currUser.profileId != systemAdminProfileId ) {
            CaseTriggerHandler.checkBAASCaseStatusFlow(Trigger.newMap,Trigger.oldMap);
            CaseTriggerHandler.stampClockBAASStatusData(Trigger.newMap,Trigger.oldMap);
        }
        if( CaseTriggerHandler.triggerRun == false){
            CaseTriggerHandler.triggerRun = true;
            CaseTriggerHandler.triggerAssignmentRule(Trigger.newMap,Trigger.oldMap);
        }
    }
    
}
trigger CasePropertiesLotTrigger on Property_Lots__c (after insert) {
	//synch the related list(Property) - suraj
    if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
        // when Application case is created
        system.debug('enetering synch related list flow');
       
        CasePropertyLotTriggerHandler.syncCasePropertyLotDetails(Trigger.new);

    }
}
/************************ User Object Trigger *********************************************************
/*  This Apex trigger will be the only trigger on the User object.
*   The logic for each event will be implemented in methods called from the trigger handler class
*   This is recommended to control the order of execution of logic for the same event.
 * Author       :   Jerun Stanley
 * Created Date :   Jun 19 2017
  
 ******************************************************************************/

trigger UserTrigger on User (before insert, after insert, before update, after update){

    UserTriggerHandler handler = new UserTriggerHandler(Trigger.isExecuting, Trigger.size, Trigger.old, Trigger.new, Trigger.oldmap, Trigger.newmap);

    if(Trigger.isBefore){
        if(Trigger.isInsert)
            handler.OnBeforeInsert();
        else if(Trigger.isUpdate)
            handler.OnBeforeUpdate();
    }else{
        if(Trigger.isInsert)
            handler.OnAfterInsert();
        else if(Trigger.isUpdate)
            handler.OnAfterUpdate();
    }
}
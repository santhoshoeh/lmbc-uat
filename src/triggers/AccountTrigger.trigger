/* Handles trigger logic for Account object
 * Date: 25 Sept 2017
 * - Suraj
 */
trigger AccountTrigger on Account (after update) {//after insert,

    //change the profile of assessor to Approved Assessor when user is Approved
    //We will not take care the insert action now.
    if(Trigger.isAfter ){//&& (Trigger.isInsert || Trigger.isUpdate)
    	//update user profile for assessor once assessor is approved
    	system.debug('updateUserAsAssessor entry point');
    	AccountTriggerHandler.updateUserAsAssessor(Trigger.New, Trigger.OldMap);
    }
        
    
}
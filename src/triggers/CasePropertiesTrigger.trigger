//This trigger is used to sync properties from parent case to child cases
trigger CasePropertiesTrigger on Properties__c (after insert) {    
    //synch the related list(Property) - suraj
    if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
        // when Application case is created
        system.debug('enetering synch related list flow');
                   
        CasePropertyTriggerHandler.syncCasePropertyDetails(Trigger.new);

    }
}
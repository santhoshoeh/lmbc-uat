/*Trigger to share Assessments with Accredited Assessor
 * 
 * @Author : Suraj
 * Date : 14/07/2017
 */

trigger AssessmentShareTrigger on Case (after insert, after update) {

    if(Trigger.isUpdate || Trigger.isInsert ){
	    //call The handler method for manupulating the case sharing
    	AssessmentShareHandler shareHandle = new AssessmentShareHandler();
    	shareHandle.shareCaseRecords(trigger.oldMap, trigger.New);    
    }

    
}
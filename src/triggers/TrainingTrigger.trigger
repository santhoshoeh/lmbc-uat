trigger TrainingTrigger on Training__c (after insert) {
    TriggerFactory.createAndExecuteHandler(Training__c.sObjectType);
}
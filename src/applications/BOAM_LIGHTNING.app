<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Lightning App for BOAM</description>
    <label>BOAM APP</label>
    <tab>standard-home</tab>
    <tab>standard-Account</tab>
    <tab>standard-Case</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Case_Party__c</tab>
    <tab>Properties__c</tab>
    <tab>Transaction__c</tab>
</CustomApplication>

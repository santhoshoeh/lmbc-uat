({  
   doInit : function (component, event, helpler) {
       var caseId = component.get("v.caseId");
       var action = component.get("c.getCaseDetails");
       var getSubmissionPeriodaction = component.get("c.getSubmissionPeriod");

       var submissionStartDate;
       var submissionEndDate;

       component.set('v.errorMessage','');
       getSubmissionPeriodaction.setCallback(this, function(response) {
           var rtnValue = response.getReturnValue();
           submissionStartDate = rtnValue['submissionStartDate'] == undefined ? undefined: rtnValue['submissionStartDate'];
           submissionEndDate = rtnValue['submissionEndDate'] == undefined? undefined : rtnValue['submissionEndDate'];

           action.setParams({"caseId":caseId});
           action.setCallback(this, function(a){
               //$A.get("e.force:closeQuickAction").fire();
               var rtnValue = a.getReturnValue();
               //alert('getting case details  rtnValue ' + rtnValue);
               if (rtnValue !== null) {
                   component.set("v.caseStatus",rtnValue.Status);
                   
		         /*if(rtnValue.Status != 'Submitted' && rtnValue.RecordType.Name != 'Agreement') {
                       component.set('v.errorMessage','Application has to be submitted before it can be Approved.');
                   
                    }
                   
                   if(rtnValue.Status == 'Approved') {
                       component.set('v.errorMessage','This case has already been Approved.');
                   }*/
               }else {
                   component.set('v.errorMessage','This case does not exist.');
               }
           });
           $A.enqueueAction(action);
       });
       $A.enqueueAction(getSubmissionPeriodaction);
    },

   backToCase: function (component, event, helpler) {
        helpler.backToCase(component, event, helpler);
   },

   approve: function (component, event, helpler) {
        helpler.submitCase(component, event, helpler);
        helpler.backToCase(component, event, helpler);
        helpler.showMyToast(component, event, helpler);
       
   }
})
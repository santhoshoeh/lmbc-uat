({
    backToCase : function (component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        var caseID = component.get("v.recordId");
        console.log(caseID);
        navEvt.setParams({
            "recordId": component.get("v.recordId"),
            "slideDevName": "related"
        });
        navEvt.fire();
    },
    cloneCase : function (component, event, helpler) {
        var caseObj = component.get("v.CaseRec");
        console.log('caseObj.Business_Unit__c ' + caseObj.Business_Unit__c);
        console.log('caseObj.Applicant_Type__c ' + caseObj.Applicant_Type__c);
        console.log('caseObj.ContactId ' + caseObj.ContactId);
        console.log('caseObj.Origin ' + caseObj.Origin);
        console.log('caseObj.Subject ' + caseObj.Subject);
        console.log('caseObj.Landholder__c ' + caseObj.Landholder__c);
        
        $A.get("e.force:closeQuickAction").fire();
        console.log('caseObj.Id' + caseObj.Id);
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Case",
            "defaultFieldValues": {
                'Business_Unit__c' : 'NVRM',
                'Applicant_Type__c':caseObj.Applicant_Type__c,
                'ContactId':caseObj.ContactId,
                'Origin':caseObj.Origin,
                'Subject':caseObj.Subject,
                'Landholder__c': caseObj.Landholder__c,
                'ParentId': caseObj.Id,
                'Description': caseObj.Description
            },
            "recordTypeId" : "0120l00000008lT"
        });
        createRecordEvent.fire(); 
    },
})
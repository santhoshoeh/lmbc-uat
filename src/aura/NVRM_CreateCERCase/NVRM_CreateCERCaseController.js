({
    doInit : function (component, event, helpler) {
        var caseID = component.get("v.recordId");
        var action = component.get("c.getCaseDetails");
        action.setParams({ "caseId"  : caseID });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var caseObj = response.getReturnValue();
                component.set("v.CaseRec",caseObj);
                console.log('caseObj.status' + caseObj.Status );
                
            }
            else{
                console.log('Case not found');
            }
        });
        $A.enqueueAction(action);
        
    },
	backToCase: function (component, event, helpler) {
        helpler.backToCase(component, event, helpler);
        var caseID = component.get("v.recordId");
        console.log('Inside component controler' + caseID);
    },
    cloneCase : function (component, event, helpler) {
        
        helpler.cloneCase(component, event, helpler);
    },
})
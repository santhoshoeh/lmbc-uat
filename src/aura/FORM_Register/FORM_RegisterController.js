/**
 * Created by junzhou on 31/5/17.
 */
({
    init: function(component, event, helper) {
        component.set('v.communitylink', window.location.pathname.split('/')[1]);
        component.set('v.form', {
            'businessUnit':'BAAS',
            'firstName': '',
            'middleName': '',
            'lastName': '',
            'mobile': '',
            'phone': '',
            'emailAddress': '',

            'organisation' : '',
            'ABNACN' : '',
            'organisationType' : '',
            'workLocality' : '',

            'existingAssessor': 'no',
            'trainingInterested': 'no',
			'isPublicAvaliable': 'no',
            'assessorCertificationNumber': '',
            'trainingCourses': [],
            'willingToTravel' : 'no'
        }), component.set('v.isValid', true)
    },

    submit: function(component, event, helper) {
        var action = component.get('c.createAccount');
        var validationCheckEvent = $A.get("e.c:ValidationForm");
        validationCheckEvent.fire();

        var isValid = component.get('v.isValid');
        if(isValid) {
            var paramJSON = JSON.stringify(component.get('v.form'));
            var param = {
                regoJSONstr : paramJSON
            };
            action.setParams(param);
            action.setCallback(this, function(response) {
                var state = response.getState();
                var rtnValue = response.getReturnValue();
                var rtnObject = JSON.parse(rtnValue);
                if(rtnObject.status === 'Success') {
                    var recId = rtnObject.recId;
                    var statusCode = rtnObject.statusCode;
                    var url = rtnObject.url;
                    window.location.href = "/" + component.get('v.communitylink') + "/userresponse?code="+statusCode+'&url='+url+'&recId='+recId;

                }else {
                    component.set('v.errorMessage', rtnValue.msg);
                }
            });
            $A.enqueueAction(action);
        }
    }


})

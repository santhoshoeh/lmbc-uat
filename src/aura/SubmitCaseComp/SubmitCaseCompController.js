({  
   doInit : function (component, event, helpler) {
       var caseId = component.get("v.caseId");
       var action = component.get("c.getBAASCaseDetails");
       var getSubmissionPeriodaction = component.get("c.getSubmissionPeriod");

       var submissionStartDate;
       var submissionEndDate;

       component.set('v.errorMessage','');
       getSubmissionPeriodaction.setCallback(this, function(response) {
           var rtnValue = response.getReturnValue();
           submissionStartDate = rtnValue['submissionStartDate'] == undefined ? undefined: rtnValue['submissionStartDate'];
           submissionEndDate = rtnValue['submissionEndDate'] == undefined? undefined : rtnValue['submissionEndDate'];

           action.setParams({"caseId":caseId});
           action.setCallback(this, function(a){
               //$A.get("e.force:closeQuickAction").fire();
               var rtnValue = a.getReturnValue();
               //alert('getting case details  rtnValue ' + rtnValue);
               if (rtnValue !== null) {
                   component.set("v.caseStatus",rtnValue.Status);
                   if(rtnValue.Status == 'Submitted') {
                       component.set('v.errorMessage','Application has already been submitted.');
                   } else {
                      if(rtnValue.Business_Unit__c == 'BAAS') {
                        if(rtnValue.Status == 'Application Fee Paid') {
                            if(rtnValue.Subscription_fee_payment_status__c != 'Paid') {
                                component.set('v.errorMessage','Your Application Fee payment process is not completed.');
                            }else if(rtnValue.BAAS_All_Doc_list_check__c == false){
                                component.set('v.errorMessage', 'Please Upload All Request documents and tick the Document List checkbox.')
                            }else if(submissionStartDate == undefined || submissionEndDate == undefined) {
                                 component.set('v.errorMessage','Please contact Admin to confirm Application Submission Period.');
                            }else{
                                 var submissionStartDateString = submissionStartDate+'T00:00:00';
                                 var submissionStartDateTime = new Date(submissionStartDateString);

                                 var submissionEndDateString = submissionEndDate+'T23:59:59';
                                 var submissionEndDateTime = new Date(submissionEndDateString);

                                 var today = new Date();

                                 if(submissionStartDateTime <= today) {
                                     if(today<= submissionEndDateTime) {
                                         component.set('v.successMessage','Success');
                                     }else {
                                         component.set('v.errorMessage','You have already passed the deadline, Please submit your application during the next designated window.');

                                     }
                                 }else {
                                     component.set('v.errorMessage','You can prepare you application at any time, however you can only submit during this designated window ' +
                                             submissionStartDate + ' to ' + submissionEndDate + '. and after your fee has been paid');
                                 }
                             }
                           } else if(rtnValue.Status == 'New'){
                              component.set('v.errorMessage','Please pay the Application Fee firstly');
                           } else {
                              component.set('v.errorMessage','Application has already been submitted.');
                           }
                       }else {
                           component.set('v.successMessage','Success');
                       }
                   }
               }else {
                   component.set('v.errorMessage','This case is not exist.');
               }
           });
           $A.enqueueAction(action);
       });
       $A.enqueueAction(getSubmissionPeriodaction);
    },

   backToCase: function (component, event, helpler) {
        helpler.backToCase(component, event, helpler);
   },

   submitCase: function (component, event, helpler) {
        helpler.submitCase(component, event, helpler);
        helpler.backToCase(component, event, helpler);
        helpler.showMyToast(component, event, helpler);
       
   }
})
/**
 * Created by junzhou on 31/5/17.
 */
({
    onScriptLoad: function(component, event, helper) {
        /*if (typeof jQuery !== "undefined" && typeof $j === "undefined") {
         window.$j = window.jQuery.noConflict(true);
         console.log('Loaded JQuery: %s', window.$j.fn.jquery);
         }*/
        //var startDate = component.get('v.minDate');
        //var endDate = window.moment().add(component.get('v.maxDateYearDiff'), 'y');
        //var endDateString = window.moment(endDate).format('DD/MM/YYYYY');
        var id = component.get('v.id');
        var date_input = jQuery('#' + id);
        var container = 'body'; // '#' + id;
        var options = {
            format: 'dd/mm/yyyy',
            container: container,
            autoclose: true
        };
        date_input.datepicker(options).on('changeDate', function(e) {
            component.set('v.value', e.target.value);
            if (component.get('v.value'))
                component.set('v.errorMessage', '');
        }).blur(function(e){
            component.set('v.value', e.target.value);
            if (component.get('v.value'))
                component.set('v.errorMessage', '');
        });
    },

    onBlurFunction: function(component, event, helper) {
        component.set('v.value', component.get('v.value'));
        var js = component.get("v.onBlurFunction");
        if (js) {
            $A.enqueueAction(js);
        }
        if (component.get('v.value'))
            component.set('v.errorMessage', '');
    },
})
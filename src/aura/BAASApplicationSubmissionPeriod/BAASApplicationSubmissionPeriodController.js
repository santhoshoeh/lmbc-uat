({
	doInit : function (component, event, helpler) {
		var caseId = component.get("v.caseId");
		var action = component.get("c.getBAASCaseDetails");
		var getSubmissionPeriodaction = component.get("c.getSubmissionPeriod");

		var submissionStartDate;
       	var submissionEndDate;

       	getSubmissionPeriodaction.setCallback(this, function(response) {
           	var rtnValue = response.getReturnValue();
           	submissionStartDate = rtnValue['submissionStartDate'] == undefined ? undefined: rtnValue['submissionStartDate'];
           	submissionEndDate = rtnValue['submissionEndDate'] == undefined? undefined : rtnValue['submissionEndDate'];
			
			action.setParams({"caseId":caseId});
           	action.setCallback(this, function(a){
           		var rtnValue = a.getReturnValue();

           		if (rtnValue !== null && rtnValue.Business_Unit__c == 'BAAS') {
           			component.set("v.caseStatus",rtnValue.Status);
           			component.set("v.BAASCaseMessage","By putting contact details in the fields labelled \"Contact Name\", \"Contact Phone\", \"Contact Email\" you are consenting to these being published on the OEH Public Register of Accredited Assessors. If you do not wish to have contact details on the public register, leave these fields blank.");
           			if(rtnValue.Status == 'New') {
           				if(submissionStartDate == undefined || submissionEndDate == undefined) {
							component.set('v.errorMessage','Please contact Admin to confirm Application Submission Period.');
						}else {
							component.set('v.errorMessage','You can prepare you application at any time, however you can only submit during this designated window ' +
			                                       submissionStartDate + ' to ' + submissionEndDate + '. and after your fee has been paid.');
			            }
           			}else if(rtnValue.Status == 'Application Fee Paid') {
           				if(submissionStartDate == undefined || submissionEndDate == undefined) {
							component.set('v.errorMessage','Please contact Admin to confirm Application Submission Period.');
						}else {
							component.set('v.errorMessage','You can submit your application during the designated window ' +
			                                       submissionStartDate + ' to ' + submissionEndDate + '. ');
			            }
           			}else if(rtnValue.Status != 'New' && rtnValue.Status != 'Application Fee Paid' && rtnValue.Status != 'Submitted'){
           				component.set('v.displayClockComponent',true);
           			}
           			
           		}
			});
			$A.enqueueAction(action);
		});
       	$A.enqueueAction(getSubmissionPeriodaction);
	}
})
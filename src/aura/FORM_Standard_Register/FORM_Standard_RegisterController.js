({
    initStandardRegister: function(component, event, helper) {
        var action = component.get('c.getOrganisationType');
        action.setCallback(this, function(response) {
            var rtnValue = response.getReturnValue();
            component.set('v.organisationType', rtnValue);
        });
        $A.enqueueAction(action);

        var workLocalityaction = component.get('c.getWorklocality');
        workLocalityaction.setCallback(this, function(response) {
            var rtnValue = response.getReturnValue();
            component.set('v.workLocality', rtnValue);
        });
        $A.enqueueAction(workLocalityaction);
    },

    submit:function(component, event, helper) {
        var valid = true;
        var fieldsToCheck = {
            'firstName': 'Please enter First Name',
            'lastName': 'Please enter Last Name',
            
            'mobile': 'Please enter a Mobile Number',
            'emailAddress' : 'Please enter an Email Address',
            'reConfirmEmailAddress': 'Please confirm Email Address'
        };//'phone': 'Please enter a Phone Number',

        Object.keys(fieldsToCheck).forEach(function(field) {
            var cmp = component.find(field);

            var value = cmp.get('v.value');
            var errorMessage = cmp.get('v.errorMessage');
            if (errorMessage) {
                valid = false;
            } else {
                if (value){
                    cmp.set('v.errorMessage', '');
                } else {
                    cmp.set('v.errorMessage', fieldsToCheck[field]);
                    valid = false;
                }
            }
        });

        // var organisationCmp = component.find('organisation');
        // var organisationValue = organisationCmp.get('v.value');
        // if (organisationValue) {
        //     var ABNACNCmp = component.find('ABNACN');
        //     var ABNACNValue = ABNACNCmp.get('v.value');
        //     var ABNACNError = ABNACNCmp.get('v.errorMessage');
        //     if (ABNACNError) {
        //         valid = false;
        //     } else {
        //         if (ABNACNValue) {
        //             ABNACNCmp.set('v.errorMessage', '');
        //         } else {
        //             ABNACNCmp.set('v.errorMessage','Please enter ABN or ACN');
        //             valid = false;
        //         }
        //     }
        // }

        component.set('v.isValid', valid);
    },

    validMobileNumber: function(component, event, helper) {
        var cmp = component.find("mobile");
        var mobileNumber = cmp.get('v.value');
        cmp.set('v.errorMessage', '');
        if (mobileNumber.length != 10) {
            cmp.set('v.errorMessage', 'Mobile number must contain 10 numbers');
            return;
        }
        if (/^[0-9]+$/.test(mobileNumber) === false) {
            cmp.set('v.errorMessage', 'Mobile number can only contain numbers');
            return;
        }
        if (mobileNumber.substring(0, 2) != '04') {
            cmp.set('v.errorMessage', 'Mobile number must start with 04');
        }
    },

    validPhoneNumber: function(component, event, helper) {
        var cmp = component.find("phone");
        var mobileNumber = cmp.get('v.value');
        cmp.set('v.errorMessage', '');
        if (mobileNumber.length != 10) {
            cmp.set('v.errorMessage', 'Phone number must contain 10 numbers');
            return;
        }
        if (/^[0-9]+$/.test(mobileNumber) === false) {
            cmp.set('v.errorMessage', 'Phone number can only contain numbers');
            return;
        }
        var validAreaCode = ['02','03','04','07','08'],
            areaCode = mobileNumber.substring(0, 2);
        if (validAreaCode.indexOf(areaCode) < 0) {
            cmp.set('v.errorMessage', 'Phone number must start with 02, 03, 04, 07 or 08');
        }
    },

    checkEmailAddress: function(component, event, helper) {
        var cmp = component.find("emailAddress");
        var emailAddress = cmp.get('v.value');
        var reConfirmCmp = component.find("reConfirmEmailAddress");
        var confirmEmailAddress = reConfirmCmp.get('v.value');

        reConfirmCmp.set('v.errorMessage', '');
        if(emailAddress == confirmEmailAddress) {
            reConfirmCmp.set('v.errorMessage', '');
        } else {
            reConfirmCmp.set('v.errorMessage', 'Please Confirm your Email address');
        }
    },

    validEmailAddress: function(component,event, helper) {
        var cmp = component.find("emailAddress");
        var emailAddress = cmp.get('v.value');
        var emailRegEx = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}/i;

        if (emailRegEx.test(emailAddress)==false) {
            cmp.set('v.errorMessage', 'Invalid Email Address Format');
        }
    },

    // validABNACN: function(component,event, helper) {
    //     var cmp = component.find("ABNACN");
    //     var ABNACN = cmp.get('v.value');
    //     var sum = 0;
    //     var weights = [];
    //     ABNACN = ABNACN.replace(/\s/g, '');
    //     if (!ABNACN) {
    //         return;
    //     }
    //     if (ABNACN.length != 9 && ABNACN.length != 11) {
    //         cmp.set('v.errorMessage', 'ABN or ACN must be either 9 or 11 digits');
    //         return;
    //     }
    //     if (/^[0-9]+$/.test(ABNACN) === false) {
    //         cmp.set('v.errorMessage', 'ABN or ACN can only contain numbers');
    //         return;
    //     }
    //     if (ABNACN.length == 11) {
    //         var splitABN = ABNACN.split("");
    //             weights = [10,1,3,5,7,9,11,13,15,17,19];
    //         for (var i = 0; i < weights.length; i++) {
    //             splitABN[i] = +splitABN[i];
    //             if (i === 0) {
    //                 splitABN[i]--;
    //             }
    //             sum = sum + (weights[i] * splitABN[i]);
    //         }
    //         if ((sum % 89) !== 0) {
    //             cmp.set('v.errorMessage', 'ABN is invalid');
    //         }
    //     }
    //     if (ABNACN.length == 9) {
    //         var splitACN = ABNACN.split("");
    //             weights = [8,7,6,5,4,3,2,1];
    //         for (var ii = 0; ii < weights.length; ii++) {
    //             splitACN[ii] = +splitACN[ii];
    //             sum = sum + (weights[ii] * splitACN[ii]);
    //         }
    //         var checkValue = (10 - (sum % 10));
    //         splitACN[8] = +splitACN[8];
    //         if (splitACN[8] !== checkValue) {
    //             cmp.set('v.errorMessage', 'ACN is invalid');
    //         }
    //     }
    // },

    limitToNumbers: function(component,event, helper) {
        $(".limitToNumbers").keypress( function(event) {
            var chr = String.fromCharCode(event.which);

            // Firefox triggers a keypress event with keyboard navigation,
            // so these navigation events need to be let through.
            var allowKeyCodes = [
                8,  // Tab
                9,  // Backspace
                13, // Enter
                46, // Delete
                33, // Page up
                34, // Page down
                35, // End
                36, // Home
                37, // Left
                38, // Up
                39, // Right
                40 // Down
            ];
            
            if ("1234567890".indexOf(chr) < 0)
                return false;
        });
    }

})
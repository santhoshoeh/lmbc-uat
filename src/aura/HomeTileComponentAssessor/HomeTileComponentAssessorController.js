({
    doInit: function(component, event, helper) {
        component.set('v.faqUrl', $A.get("$Label.c.FAQ_Url"));
        component.set('v.newsUrl', $A.get("$Label.c.News_Url"));
        
        var action = component.get("c.getLoggedInUserUrl");
        action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
            component.set('v.counts', response.getReturnValue());
            component.set('v.profileURL', response.getReturnValue());
        }
        else if (state === "ERROR") {
            alert('Error : ' + JSON.stringify(errors));
        }
        });
        $A.enqueueAction(action);
        
    },
    
    gotoDevAssessList : function (component, event, helper) {
    	var action = component.get("c.getListMyDevApplications");
    	action.setCallback(this, function(response){
        var state = response.getState();
        if (state === "SUCCESS") {
            var listviews = response.getReturnValue();
            var navEvent = $A.get("e.force:navigateToList");
            navEvent.setParams({
                "listViewId": listviews.Id,
                "listViewName": 'Assessment',
                "scope": "Case"
            });
            navEvent.fire();
        }
    });
    $A.enqueueAction(action);
	},
     
    gotoStewAssessList  : function (component, event, helper) {
    	var action = component.get("c.getListViewStewAssess");
    	action.setCallback(this, function(response){
        var state = response.getState();
        if (state === "SUCCESS") {
            var listviews = response.getReturnValue();
            var navEvent = $A.get("e.force:navigateToList");
            navEvent.setParams({
                "listViewId": listviews.Id,
                "scope": "Case"
            });
            navEvent.fire();
        }
    });
    $A.enqueueAction(action);
	}
    
})
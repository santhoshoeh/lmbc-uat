/**
 * Created by junzhou on 19/6/17.
 */
({
    onSelectChange : function(component, event, helper) {
        var inputSelectCmp = component.find("inputSelect");
        var selectedValue = inputSelectCmp.get('v.value');

        var inputSelectTextCmp = component.find("inputSelectText");
        inputSelectTextCmp.set('v.value', selectedValue);
    }
})
({
     
    createRecord : function(component) {
        var parentCaseId = component.get("v.parentCaseId"); 
        var createRecordEvent = $A.get("e.force:createRecord");		 
        var rectypeName = component.find("InputSelectDynamic");
        var rectypeId = component.get("v.myMap")[rectypeName.get("v.value")];
        //alert('test parent case id ' + component.get("v.caseRec").Related_Cases__c);
        //alert('test parentCaseId case id ' + parentCaseId);
		$A.get("e.force:closeQuickAction").fire();

        createRecordEvent.setParams({
            "entityApiName": "Case",
            "recordTypeId": rectypeId,
            "defaultFieldValues": {
             	"Related_Cases__c" : component.get("v.caseRec").Related_Cases__c,
                "Related_Assessment__c": parentCaseId  
        	}
        });
        createRecordEvent.fire();
    },
    
    showMyToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "message": 'Case was submitted successfully !!',
            "mode" : "dismissible",
            "type": "warning"
        });
        toastEvent.fire();
        
	}
})
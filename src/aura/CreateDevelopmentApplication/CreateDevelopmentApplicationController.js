({
   doInit : function (component, event, helpler) {
       //alert("doinit entry point");
       //populate application types
       var opts = [
            { class: "optionClass", label: "Biocertification Application", value: "Biocertification Application"},
            { class: "optionClass", label: "Stewardship Application", value: "Stewardship Application" },
            { class: "optionClass", label: "Approval Details", value: "Development Details" }
           
        ];
        component.find("InputSelectDynamic").set("v.options", opts);
       
       
       //getRecTypeIdM
       //alert("getting the recordtype map");
       var action = component.get('c.getRecTypeIdMap');
       action.setCallback(this,function(response){
            //alert(JSON.stringify(response.getReturnValue()));
            component.set('v.myMap',response.getReturnValue());
        });
        $A.enqueueAction(action);
       
       //alert('map application type ' + component.get("v.myMap));
            //
        //alert('   getting case details   ');
        var caseId = component.get("v.parentCaseId"); 
        var action = component.get("c.getCaseDetails");
               
       //alert('getting case details  caseId ' + caseId);

        action.setParams({"caseId":caseId});
        action.setCallback(this, function(a){
          //$A.get("e.force:closeQuickAction").fire();
          var rtnValue = a.getReturnValue();
          //alert('case returned  ' + rtnValue);
          if (rtnValue !== null) {
             component.set("v.caseRec",rtnValue);
              //alert('case returned  ' + component.get("v.caseRec"));
             component.set("v.showError",true);
          }
        });
    $A.enqueueAction(action);
    }, 
    createCaseButton : function(component, event, helper) {
        //helper.createRecord(component);
         var parentCaseId = component.get("v.parentCaseId"); 
        var createRecordEvent = $A.get("e.force:createRecord");		 
        var rectypeName = component.find("InputSelectDynamic");
        var rectypeId = component.get("v.myMap")[rectypeName.get("v.value")];
        //alert('test parent case id ' + component.get("v.caseRec").Related_Cases__c);
        //alert('test rectypeId case id ' + rectypeId);
		$A.get("e.force:closeQuickAction").fire();

        createRecordEvent.setParams({
            "entityApiName": "Case",
            "recordTypeId": rectypeId,
            "defaultFieldValues": {
             	"Related_Cases__c" : component.get("v.caseRec").Related_Cases__c,
                "Related_Assessment__c": parentCaseId,
				"Parent" : parentCaseId

        	}
        });
        createRecordEvent.fire();
        
        /*var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "message": 'Case was submitted successfully !!',
            "mode" : "dismissible",
            "type": "warning"
        });
        toastEvent.fire();*/
        
        //helpler.showMyToast(component, event, helpler);

    }
    
})
/**
 * Created by junzhou on 5/7/17.
 */
({
    doInit : function(component, event, helper) {
        var action =  component.get("c.getPaymentOption");
        var caseId = component.get("v.recordId");

        action.setParams({ "caseId" : caseId });
        action.setCallback(this,function(response){
            var state = response.getState();
            var rtnValue = response.getReturnValue();
            if(rtnValue.message == 'Success') {
                component.set('v.successMessage',rtnValue.message);
                component.set('v.paymentDetails',rtnValue);
                component.set('v.paymentMethod',rtnValue.paymentMethod[0]);
            }else {
                component.set('v.errorMessage',rtnValue.message);
            }
        });
        $A.enqueueAction(action);
    },

    onSelectChange : function(component, event, helper) {
        var inputSelectCmp = component.find("paymentOption");
        var selectedValue = inputSelectCmp.get('v.value');
        component.set('v.paymentMethod', selectedValue);
    },

    backToCase: function (component, event, helpler) {
        helpler.backToCase(component, event, helpler);
    },

    PayToCase: function(component, event, helper) {
        var action  = component.get('c.processPayment');
        var params = component.get('v.paymentDetails');
        var paymentMethod = component.get('v.paymentMethod');
        var caseId = component.get("v.recordId");

        var sPayment = {
            caseId: caseId,
            approvalFee : params.applicationFee,
            businessUnit: params.businessUnit,
            paymentMethod : paymentMethod,
            paymentItem: params.paymentItem
        };

        var paramJSON = JSON.stringify(sPayment);
        var param = {
            sPaymentJSONString : paramJSON
        };
        action.setParams(param);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set("v.url", response.getReturnValue());
                console.log('After close event');
                //document.getElementsByClassName('modal-container')[0].style.display = 'none';
                console.log('After styling modal');
                window.location.href = response.getReturnValue();

            }
        });
        $A.enqueueAction(action);
    }
})
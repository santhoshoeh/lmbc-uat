/**
 * Created by junzhou on 6/7/17.
 */
({
    backToCase : function (component, event, helper) {
        //alert('back to case ' + component.get("v.caseId"));
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.recordId"),
            "slideDevName": "related"
        });
        navEvt.fire();
    },
})
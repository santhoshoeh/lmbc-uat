({
    backToCase : function (component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        var caseID = component.get("v.recordId");
        console.log(caseID);
        navEvt.setParams({
            "recordId": component.get("v.recordId"),
            "slideDevName": "related"
        });
        navEvt.fire();
    },
    cloneCase : function (component, event, helpler) {
        var caseObj = component.get("v.CaseRec");
        $A.get("e.force:closeQuickAction").fire();
        console.log('caseObj.Id' + caseObj.Id);
        var action = component.get("c.getCaseRecordType");
        action.setParams({ "devName"  : "LIMR" });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var recTypeID = response.getReturnValue();
                console.log('record ID' + recTypeID );  
                var createRecordEvent = $A.get("e.force:createRecord");
                createRecordEvent.setParams({
                    "entityApiName": "Case",
                    "defaultFieldValues": {
                        'Business_Unit__c' : 'NVRM',
                        'Applicant_Type__c':caseObj.Applicant_Type__c,
                        'ContactId':caseObj.ContactId,
                        'Origin':caseObj.Origin,
                        'Landholder__c': caseObj.Landholder__c,
                        'ParentId': caseObj.Id,
                    },
                    "recordTypeId" : recTypeID 
                });
                createRecordEvent.fire(); 
            }
            else{
                console.log('Record Type not found');
            }
        });
        $A.enqueueAction(action);
        
    },
})
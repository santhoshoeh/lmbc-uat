({
    doInit: function(component, event, helper) {
        var action = component.get("c.getLoggedInUser");
        action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
            component.set('v.counts', response.getReturnValue());
        }
        else if (state === "ERROR") {
            alert('Error : ' + JSON.stringify(errors));
        }
        });
        $A.enqueueAction(action);
        
    }
})
({  
   doInit : function (component, event, helpler) {
       var caseId = component.get("v.caseId");
       
       var getAppCaseDet = component.get("c.getApplicationCaseDet");
 		getAppCaseDet.setParams({"caseId":caseId});
        getAppCaseDet.setCallback(this, function(a){
        var rtnValue = a.getReturnValue();
        if (rtnValue !== null) {
			if(rtnValue.Status == 'Submitted') {
				component.set('v.errorMessage','This case has already been submitted.');
            }       	
        }
        });
              
        $A.enqueueAction(getAppCaseDet);

       
       var action = component.get("c.getCaseDetails");
       var getSubmissionPeriodaction = component.get("c.getSubmissionPeriod");

       var submissionStartDate;
       var submissionEndDate;

       component.set('v.errorMessage','');
       getSubmissionPeriodaction.setCallback(this, function(response) {
           var rtnValue = response.getReturnValue();
           submissionStartDate = rtnValue['submissionStartDate'] == undefined ? undefined: rtnValue['submissionStartDate'];
           submissionEndDate = rtnValue['submissionEndDate'] == undefined? undefined : rtnValue['submissionEndDate'];

           action.setParams({"caseId":caseId});
           action.setCallback(this, function(a){
               //$A.get("e.force:closeQuickAction").fire();
               var rtnValue = a.getReturnValue();
               //alert('getting case details  rtnValue ' + rtnValue);
               if (rtnValue !== null) {
                   component.set("v.caseStatus",rtnValue.Status);
                   if(rtnValue.Status == 'Submitted') {
                       component.set('v.errorMessage','This case has already been submitted.');
                   } else {
                       console.log(rtnValue);
                       console.log(rtnValue.Child_Cases__r);
                       console.log(rtnValue.RecordType.Name);
                       if((rtnValue.Child_Cases__r == null || rtnValue.Child_Cases__r.length ==0 )
                          &&(rtnValue.RecordType.Name != 'Transfer Credit' && rtnValue.RecordType.Name != 'Retire Credits')){
                           component.set('v.errorMessage','Please attached a finalised assessment in related assessments before submitting the case.');
                       }
                       if(rtnValue.Business_Unit__c == 'BAAS') {
                           if(submissionStartDate == undefined || submissionEndDate == undefined) {
                               component.set('v.errorMessage','Please contact Admin to confirm Submission Open Period.');
                           }else{
                               var submissionStartDateString = submissionStartDate+'T00:00:00';
                               var submissionStartDateTime = new Date(submissionStartDateString);

                               var submissionEndDateString = submissionEndDate+'T23:59:59';
                               var submissionEndDateTime = new Date(submissionEndDateString);

                               var today = new Date();

                               if(submissionStartDateTime <= today) {
                                   if(today<= submissionEndDateTime) {
                                       component.set('v.successMessage','Success');
                                   }else {
                                       component.set('v.errorMessage','You have already passed the deadline, Please Contact your Admin.');

                                   }
                               }else {
                                   component.set('v.errorMessage','You only can submit Case from '+ submissionStartDate
                                       + ' to ' + submissionEndDate);
                               }
                           }
                       }else {
                           component.set('v.successMessage','Success');
                       }
                   }
               }else {
                   component.set('v.errorMessage','This case does not exist.');
               }
           });
           $A.enqueueAction(action);
       });
       $A.enqueueAction(getSubmissionPeriodaction);
       
       
        var validateAssessment = component.get("c.validateAssessment");
               
       //alert('getting case details caseId ' + caseId);
        validateAssessment.setParams({"caseId":caseId});
        validateAssessment.setCallback(this, function(a){
          //$A.get("e.force:closeQuickAction").fire();
          var rtnValue = a.getReturnValue();
          //alert('case returned  ' + rtnValue);
          if (rtnValue !== null) {
             component.set("v.errorMessage",rtnValue);
             //alert('case returned  ' + component.get("v.caseRec"));
             component.set("v.showError",true);
          }
        });
    	//alert('rectype id ' + component.set("v.rectypeID"));
        $A.enqueueAction(validateAssessment);
        
    },

   backToCase: function (component, event, helpler) {
        helpler.backToCase(component, event, helpler);
   },

   submitCase: function (component, event, helpler) {
        helpler.submitCase(component, event, helpler);
        helpler.backToCase(component, event, helpler);
        helpler.showMyToast(component, event, helpler);
   }
})
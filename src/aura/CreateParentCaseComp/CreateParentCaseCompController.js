({
    doInit : function (component, event, helpler) {
        //alert('   getting case details   ');
        var caseId = component.get("v.parentCaseId"); 
        var action = component.get("c.getAssessmentCaseDetails");
               
       //alert('getting case details  caseId ' + caseId);

        action.setCallback(this, function(a){
          //$A.get("e.force:closeQuickAction").fire();
          var rtnValue = a.getReturnValue();
          //alert('case returned  ' + rtnValue);
          if (rtnValue !== null) {
             component.set("v.rectypeID",rtnValue);
             //alert('case returned  ' + component.get("v.caseRec"));
             component.set("v.showError",true);
          }
        });
    	//alert('rectype id ' + component.set("v.rectypeID"));
        $A.enqueueAction(action);
    },
    createCaseButton : function(component, event, helper) {
        //alert('enter do click');
        //helper.createRecord(component);
        //
       var parentCaseId = component.get("v.parentCaseId"); 
        //alert('entring create case' + parentCaseId);
        //alert('case rectypeid  ' + component.get("v.rectypeId"));
        var createRecordEvent = $A.get("e.force:createRecord");
        //alert('rectype  ' + component.get("v.rectypeID");
        $A.get("e.force:closeQuickAction").fire();
        createRecordEvent.setParams({
            "entityApiName": "Case",
            "recordTypeId": "0120l00000008yl",
            "defaultFieldValues": {
             	"Related_Cases__c" : parentCaseId,
             	"Parent" : parentCaseId
        	}
        });
        createRecordEvent.fire();
        
        //helpler.showMyToast(component, event, helpler);

    }
    
})
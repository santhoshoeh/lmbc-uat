({
    createRecord : function(component) {
        var parentCaseId = component.get("v.parentCaseId"); 
        //alert('entring create case' + parentCaseId);
        //alert('case rectypeid  ' + component.get("v.rectypeId"));
        var createRecordEvent = $A.get("e.force:createRecord");
        //alert('rectype  ' + component.get("v.rectypeID");
        $A.get("e.force:closeQuickAction").fire();
        createRecordEvent.setParams({
            "entityApiName": "Case",
            "recordTypeId": "0120l00000008yl",
            "defaultFieldValues": {
             	"Related_Cases__c" : parentCaseId,
             	"Parent" : parentCaseId
        	}
        });
        createRecordEvent.fire();
    },
    showMyToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "message": 'Case was submitted successfully !!',
            "mode" : "dismissible",
            "type": "warning"
        });
        toastEvent.fire();
        
	}
})
({
	init : function(component, event, helper) {
        
		var daysPassed = 0,
			daysPercent = 0,
			daysTotal = 100,
			currentStatus = '',
			waitingForInfo = [],
			statusChangePercent = 0,
			clockStopped = false,
			widthGreen = 0,
			widthYellow = 0,
			widthOrange = 0,
			widthRed = 0;

        var caseID = component.get("v.caseId");
        var action = component.get('c.getCaseClockDetails');
        action.setParams({ "caseId"  : caseID });     
		action.setCallback(this, function(response) {
            setAxisItems();

            var rtnValue = response.getReturnValue();
            rtnValue = JSON.parse(rtnValue);
            console.log('BAAS rtnValue: ',rtnValue);
            daysPassed = rtnValue.clockDay > daysTotal ? daysTotal : rtnValue.clockDay;
            currentStatus = rtnValue.currentStatus;
            clockStopped = rtnValue.clockStopped;
            waitingForInfo = rtnValue.waitingForInfo;
			for (var i = 0; i < waitingForInfo.length; i++) {
				statusChangePercent = (waitingForInfo[i].day / daysTotal) * 100;
				waitingForInfo[i].percent = statusChangePercent;
			}  
			console.log('BAAS clockStopped ' + clockStopped);
            daysPercent = (daysPassed / daysTotal) * 100;
			if (daysPercent <= 55) {
				widthGreen = daysPercent;
			} else if (daysPercent >= 55 && daysPercent <= 81) {
				widthGreen = 55;
				widthYellow = daysPercent - 55;
			} else if (daysPercent >= 81) {
				widthGreen = 55;
				widthYellow = 26;
				widthRed = daysPercent - 81;
			}

			component.set('v.daysPassed', rtnValue.clockDay);
			component.set('v.daysPercent', daysPercent);
			component.set('v.currentStatus', currentStatus);
			component.set('v.waitingForInfo', waitingForInfo);
			component.set('v.clockStopped', clockStopped);
			component.set('v.widthGreen', widthGreen);
			component.set('v.widthYellow', widthYellow);
			component.set('v.widthOrange', widthOrange);
			component.set('v.widthRed', widthRed);
        });
        $A.enqueueAction(action);    
        
        function setAxisItems() {
            var axisMarkers = [],
                axisNumbers = [],
            	days = daysTotal,
            	majorMarkerIncrement = 5,
                numberIncrement = days > 50 ? 10 : 5,
            	isMajorMarker,
                isNumberVisible,
                day,
                left;

            for (day=0; day <= days; day++) {
                left = (100 / days) * day;
                isMajorMarker = day/majorMarkerIncrement % 1 === 0; // True if marker is divisible by majorMarkerIncrement (uses modulus)
                isNumberVisible = day/numberIncrement % 1 === 0;
                axisMarkers.push({
                    isMajorMarker: isMajorMarker,
                    left: left
                });                    
				if (isNumberVisible) {
                    axisNumbers.push({
                        value: day,
                        left: left
                    });                
                }
            }
            component.set('v.axisMarkers', axisMarkers);     
            component.set('v.axisNumbers', axisNumbers); 
        }
        
	}
})
/**
 * Created by junzhou on 1/6/17.
 */
({
    onBlurFunction: function(component, event, helper) {
        component.set('v.value', component.get('v.value'));
        var js = component.get("v.onBlurFunction");
        if (js) {
            $A.enqueueAction(js);
        }
        if (component.get('v.value'))
            component.set('v.errorMessage', '');
    },

    keypressFunction: function(component, event, helper) {
        component.set('v.value', component.get('v.value'));
        var js = component.get("v.keypress");
        if (js) {
            $A.enqueueAction(js);
        }
        if (component.get('v.value'))
            component.set('v.errorMessage', '');
    }
})
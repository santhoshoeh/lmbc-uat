({

    createAssessCaseButton : function(component, event, helper) {
        var parentCaseId = component.get("v.parentCaseId"); 
        var createRecordEvent = $A.get("e.force:createRecord");	
        var parentType = component.get("v.caseRec").Type;
        var rectypeName;
        if(parentType == 'Stewardship'){
            rectypeName = 'Assessment';
        }else if(parentType == 'Development assessment case'){
            rectypeName = 'Development Assessment';
        }else if(parentType == 'Clearing'){
            rectypeName = 'Assessment';
        }else if(parentType == 'Biocertification'){
            rectypeName = 'Assessment';
        }else if(parentType == 'Activity'){
            rectypeName = 'Assessment';
        } 
        var rectypeId = component.get("v.myMap")[rectypeName];
        var parentCaseId = component.get("v.parentCaseId"); 
        var createRecordEvent = $A.get("e.force:createRecord");
        var caseType =  component.get("v.caseRec").Type;

        var createAssessment = component.get("c.createAssessment");
        createAssessment.setParams({
                parentID: parentCaseId,
                recTypeID: rectypeId,
                caseType: caseType
            });
        createAssessment.setCallback(this, function(a){
            
          console.log('rtnValue assess id a' + a);
          var rtnValue = a.getReturnValue();
          console.log('rtnValue assess id ' + rtnValue);
          if (rtnValue !== null) {
             component.set("v.assessmentCaseId",rtnValue);
                             
              console.log('in before loop assess id ' + component.get("v.assessmentCaseId"));

            while (component.get("v.assessmentCaseId") == 'undefined' 
                  	|| component.get("v.assessmentCaseId") == null) {
                          
                console.log('in loop assess id ' + component.get("v.assessmentCaseId"));

            }          
          }
            
            var sObectEvent = $A.get("e.force:navigateToSObject");
        	console.log('in loop assess id navigating ' + component.get("v.assessmentCaseId"));
            sObectEvent.setParams({
                "recordId": component.get("v.assessmentCaseId")  ,
                "slideDevName": "Detail"
              });
            sObectEvent.fire();
    		//$A.get("e.force:closeQuickAction").fire();
        });
        
    $A.enqueueAction(createAssessment);
    
},
    directToCase : function(component, event, helper){
        var sObectEvent = $A.get("e.force:navigateToSObject");
        sObectEvent.setParams({
            "recordId": component.get("v.assessmentCaseId")  ,
            "slideDevName": "Detail"
          });
        sObectEvent.fire();
        //$A.get("e.force:closeQuickAction").fire();

    },
    showMyToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "message": 'Assessment was created successfully !!',
            "mode" : "dismissible",
            "type": "warning"
        });
        toastEvent.fire();
        
	}
})
({
    doInit : function (component, event, helpler) {
       //alert('   getting case details   ');
        var caseId = component.get("v.parentCaseId"); 
        var action = component.get("c.getAssessmentCaseDetails");
               
       //alert('getting case details  caseId ' + caseId);

        action.setCallback(this, function(a){
          //$A.get("e.force:closeQuickAction").fire();
          var rtnValue = a.getReturnValue();
          //alert('case returned  ' + rtnValue);
          if (rtnValue !== null) {
             component.set("v.rectypeID",rtnValue);
             //alert('case returned  ' + component.get("v.caseRec"));
             component.set("v.showError",true);
          }
        });
    	//alert('rectype id ' + component.set("v.rectypeID"));
        $A.enqueueAction(action);
        
        
        /*var validateAssessment = component.get("c.validateAssessment");
               
       //alert('getting case details caseId ' + caseId);
        validateAssessment.setParams({"caseId":caseId});
        validateAssessment.setCallback(this, function(a){
          //$A.get("e.force:closeQuickAction").fire();
          var rtnValue = a.getReturnValue();
          //alert('case returned  ' + rtnValue);
          if (rtnValue !== null) {
             component.set("v.errorMessage",rtnValue);
             //alert('case returned  ' + component.get("v.caseRec"));
             component.set("v.showError",true);
          }
        });
    	//alert('rectype id ' + component.set("v.rectypeID"));
        $A.enqueueAction(validateAssessment);*/
        
        //alert('getting case details   ');
        var caseId = component.get("v.parentCaseId"); 
        var action = component.get("c.isAssessor");
        action.setCallback(this, function(a){
          //$A.get("e.force:closeQuickAction").fire();
          var rtnValue = a.getReturnValue();
          //alert('case returned  ' + rtnValue);
          if (rtnValue !== null) {
             component.set("v.isAssessor",rtnValue);
              if(rtnValue == false){
             	component.set("v.errorMessage",'Only Assessor is Authorised to create an Application, please get in touch with your assessor for creating an Assessment. Meanwhile you can start the application process.');
              } 
             //alert(' returned isAssessor ' + component.get("v.isAssessor"));
             component.set("v.showError",true);
          }
        });
    	//alert('rectype id ' + component.set("v.rectypeID"));
        $A.enqueueAction(action);
        
        //get recordtype map
       var action = component.get('c.getRecTypeIdMap');
       action.setCallback(this,function(response){
            //alert(JSON.stringify(response.getReturnValue()));
            component.set('v.myMap',response.getReturnValue());
        });
        $A.enqueueAction(action);
        
        //alert('   getting case details   ');
        var caseId = component.get("v.parentCaseId"); 
        var action = component.get("c.getCaseDetails");
               
       //alert('getting case details  caseId ' + caseId);

        action.setParams({"caseId":caseId});
        action.setCallback(this, function(a){
          //$A.get("e.force:closeQuickAction").fire();
          var rtnValue = a.getReturnValue();
          //alert('case returned  ' + rtnValue);
          //alert('case returned  ' + component.get("v.caseRec").Type);
          if (rtnValue !== null) {
             component.set("v.caseRec",rtnValue);
             //alert('case returned  ' + component.get("v.caseRec").Type);
             component.set("v.showError",true);
          }
             component.set("v.showDiv",true);
        });
        
    $A.enqueueAction(action);
        
    },
    createCaseButton : function(component, event, helper) {
 	var parentCaseId = component.get("v.parentCaseId"); 
        var parentType = component.get("v.caseRec").Type;
        var rectypeName;
        if(parentType == 'Stewardship'){
            rectypeName = 'Assessment';
        }else if(parentType == 'Development assessment case'){
            rectypeName = 'Development Assessment';
        }else if(parentType == 'Clearing'){
            rectypeName = 'Assessment';
        }else if(parentType == 'Biocertification'){
            rectypeName = 'Assessment';
        }else if(parentType == 'Activity'){
            rectypeName = 'Assessment';
        } 
        var rectypeId = component.get("v.myMap")[rectypeName];
        var parentCaseId = component.get("v.parentCaseId"); 
        var caseType =  component.get("v.caseRec").Type;

        var createAssessment = component.get("c.createAssessment");
        createAssessment.setParams({
                parentID: parentCaseId,
                recTypeID: rectypeId,
                caseType: caseType
            });
        createAssessment.setCallback(this, function(a){
            
          console.log('rtnValue assess id a' + a);
          var rtnValue = a.getReturnValue();
          console.log('rtnValue assess id ' + rtnValue);
          if (rtnValue !== null) {
             component.set("v.assessmentCaseId",rtnValue);
                             
              console.log('in before loop assess id ' + component.get("v.assessmentCaseId"));

            /*while (component.get("v.assessmentCaseId") == 'undefined' 
                  	|| component.get("v.assessmentCaseId") == null) {
                          
                console.log('in loop assess id ' + component.get("v.assessmentCaseId"));

            }  */        
          }
            
            var sObectEvent = $A.get("e.force:navigateToSObject");
        	console.log('in loop assess id navigating ' + component.get("v.assessmentCaseId"));
            sObectEvent.setParams({
                "recordId": component.get("v.assessmentCaseId")  ,
                "slideDevName": "Detail"
              });
            sObectEvent.fire();
    		//$A.get("e.force:closeQuickAction").fire();
        });
        
    $A.enqueueAction(createAssessment);
        /*//alert('enter do click');
        //helper.createRecord(component);
        //helper.createRecord(component);
        var parentCaseId = component.get("v.parentCaseId"); 
        var createRecordEvent = $A.get("e.force:createRecord");	
        //alert('Parent case Type ' + component.get("v.caseRec").Type);
        var parentType = component.get("v.caseRec").Type;
        var rectypeName;
        if(parentType == 'Stewardship'){
            rectypeName = 'Assessment';
        }else if(parentType == 'Approval Details'){
            rectypeName = 'Development Assessment';
        }else if(parentType == 'Clearing'){
            rectypeName = 'Assessment';
        }else if(parentType == 'Biocertification'){
            rectypeName = 'Assessment';
        }else if(parentType == 'Activity'){
            rectypeName = 'Assessment';
        } 
        //alert('rectypeName    ' + rectypeName);
        //var rectypeName = component.find("InputSelectDynamic");
        var rectypeId = component.get("v.myMap")[rectypeName];
        var parentCaseId = component.get("v.parentCaseId"); 
        //alert('case type returned  ' + component.get("v.caseRec"));

        //alert('case rectypeid  ' + component.get("v.rectypeId"));
        var createRecordEvent = $A.get("e.force:createRecord");
        //alert('type  ' + component.get("v.caseRec").Type);
        $A.get("e.force:closeQuickAction").fire();
        createRecordEvent.setParams({
            "entityApiName": "Case",
            "recordTypeId": rectypeId,
            "defaultFieldValues": {
             	"Related_Cases__c" : parentCaseId,
             	"Business_Unit__c" : 'BOAM',
                "Type" : component.get("v.caseRec").Type
             
        	}
        });
        createRecordEvent.fire();
        
        //helpler.showMyToast(component, event, helpler);*/

    },
    backToCase : function (component, event, helper) {
        //alert('back to case ' + component.get("v.caseId"));
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
          "recordId": component.get("v.parentCaseId"),
          "slideDevName": "related"
        });
        navEvt.fire();
	}
    
})
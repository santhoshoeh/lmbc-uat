/**
 * Created by junzhou on 5/6/17.
 */
({
    handleLogin: function (component, event, helpler) {
        var params = component.get('v.form');
        var action = component.get("c.communityUserLogin");
        var startUrl = component.get("v.startUrl");
        startUrl = decodeURIComponent(startUrl);
        component.set("v.errorMessage",'');

        if(params == undefined || action == undefined 
            || params.username == undefined || params.pwd == undefined
            || params.username == '' || params.pwd == '') {
            component.set("v.errorMessage",'Please enter your Username and Password');
        }else {
            component.set('v.buttonDisable', true);
            action.setParams({
                userName: params.username,
                password: params.pwd,
                businessUnit: 'BAAS'
            });
            console.log(params.username + ' ' + params.pwd + ' ' + startUrl);
            action.setCallback(this, function(response){
                var rtnValue = response.getReturnValue();
                var rtnObject = JSON.parse(rtnValue);
                if (rtnValue !== null) {
                    component.set("v.errorMessage",rtnObject.msg);
                }
                else {
                    component.set('v.buttonDisable', false);
                    component.set("v.errorMessage", '');
                }
            });
            $A.enqueueAction(action);
        }

    }
})
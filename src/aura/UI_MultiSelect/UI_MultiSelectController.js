/**
 * Created by junzhou on 15/6/17.
 */
({
	init: function(component, event, helper) {
		var options = component.get('v.options');
        var checkboxInputCmp = component.find('multiselect');
        var trainingCourses = [];
		for (var i = 0; i < options.length; i++) {
			if (options[i].compulsory) {
				trainingCourses.push(options[i].value);
			}
		}
		checkboxInputCmp.set('v.value', trainingCourses);
	},

    checkAllCheckbox: function(component, event, helper) {
        var checkboxInputCmp = component.find('multiselect');
        var checkboxInputValue = checkboxInputCmp.get('v.value');

        var trainingCourses = [];
        $('input[class$=multiCheckBox]:checked').each(function(){
            trainingCourses.push($(this).attr('id'));
        });

        checkboxInputCmp.set('v.value', trainingCourses);
    }
})
({
    
    submitCase: function (component, event, helpler) {
        var caseId = component.get("v.caseId"); 
        console.log('caseId ' + caseId);
        var action = component.get("c.submitCaseMethod");
        action.setParams({"caseId":caseId});
          action.setCallback(this, function(a){
          var rtnValue = a.getReturnValue();
          if (rtnValue !== null) {
          	 $A.get("e.force:refreshView").fire();
             $A.get("e.force:closeQuickAction").fire();
       	  }
          else {
          	component.set("errorMess","There was error submitting case.Please contact Administrator");
          }
        });
              
        $A.enqueueAction(action);
        $A.get('e.force:refreshView').fire();

    },
    backToCase : function (component, event, helper) {
        //alert('back to case ' + component.get("v.caseId"));
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
          "recordId": component.get("v.caseId"),
          "slideDevName": "related"
        });
        navEvt.fire();
	}, 
    showMyToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "message": 'Case was submitted successfully !!',
            "mode" : "dismissible",
            "type": "success"
        });
        toastEvent.fire();
	}
    
})
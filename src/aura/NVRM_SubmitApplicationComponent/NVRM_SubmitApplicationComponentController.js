({  
	   doInit : function (component, event, helpler) {
	        var caseId = component.get("v.caseId");
            console.log('caseId ' + caseId);
	        var action = component.get("c.caseSubmitValidation");
	       
	        //component.set('v.errorMessage','');
	        action.setParams({"caseId":caseId});
	        action.setCallback(this, function(a){              
                var rtnValue = a.getReturnValue();
                console.log("rtnValue" + rtnValue);
                if(rtnValue != null ) {
                    component.set("v.caseSubmitInValid",true);
                    component.set("v.errorMess",rtnValue);
                } else {
                	component.set("v.caseSubmitInValid",false);
                    component.set("v.errorMess",'');
                }
                console.log(component.get("v.caseSubmitInValid"));
	               
	        });
	        $A.enqueueAction(action);
	       
	    },

	   backToCase: function (component, event, helpler) {
	        helpler.backToCase(component, event, helpler);
	   },

	   submitCase: function (component, event, helpler) {
	        helpler.submitCase(component, event, helpler);
	        helpler.backToCase(component, event, helpler);
	        helpler.showMyToast(component, event, helpler);
	       
	   }
	})
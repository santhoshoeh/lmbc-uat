({
	init : function(component, event, helper) {

		var daysPassed = 0,
			daysPercent = 0,
			daysTotal = 40,
			currentStatus = '',
			waitingForInfo = [],
			statusChangePercent = 0,
			clockStopped = true,
			widthGreen = 0,
			widthYellow = 0,
			widthOrange = 0,
			widthRed = 0;
		console.log('Inside init function');
        var caseID = component.get("v.recordId");
        console.log('caseID ' + caseID);
        var action = component.get('c.getCaseClockDetails');
        action.setParams({ "caseId"  : caseID });
        action.setCallback(this, function(response) {
            var rtnValue = response.getReturnValue();
            rtnValue = JSON.parse(rtnValue);
            console.log('rtnValue: ',rtnValue);
            daysPassed = rtnValue.clockDay;
            currentStatus = rtnValue.currentStatus;
            clockStopped = rtnValue.clockStopped;
            waitingForInfo = rtnValue.waitingForInfo;

			for (var i = 0; i < waitingForInfo.length; i++) {
				statusChangePercent = (waitingForInfo[i].day / daysTotal) * 100;
				waitingForInfo[i].percent = statusChangePercent;
			}
			console.log('clockStopped ' + clockStopped);
            daysPercent = (daysPassed / daysTotal) * 100;
			if (daysPercent <= 37.5) {
				widthGreen = daysPercent;
			} else if (daysPercent >= 37.5 && daysPercent <= 62.5) {
				widthGreen = 37.5;
				widthYellow = daysPercent - 37.5;
			} else if (daysPercent >= 62.5 && daysPercent <= 87.5) {
				widthGreen = 37.5;
				widthYellow = 25;
				widthOrange = daysPercent - 62.5;
			} else if (daysPercent > 87.5) {
				widthGreen = 37.5;
				widthYellow = 25;
				widthOrange = 25;
				widthRed = daysPercent - 87.5;
			}

			component.set('v.daysPassed', daysPassed);
			component.set('v.daysPercent', daysPercent);
			component.set('v.currentStatus', currentStatus);
			component.set('v.waitingForInfo', waitingForInfo);
			component.set('v.clockStopped', clockStopped);
			component.set('v.widthGreen', widthGreen);
			component.set('v.widthYellow', widthYellow);
			component.set('v.widthOrange', widthOrange);
			component.set('v.widthRed', widthRed);

        });
        $A.enqueueAction(action);

	}
})
({
    doInit: function(component, event, helper) {
        var getUrlAction = component.get("c.getLoggedInUserUrl");
        getUrlAction.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
            console.log('retval : ' + response.getReturnValue());
            component.set('v.profileURL', response.getReturnValue());
        }
        else if (state === "ERROR") {
            alert('Error : ' + JSON.stringify(errors));
        }
        });
        $A.enqueueAction(getUrlAction);
        
        var action = component.get("c.getLoggedInUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.counts', response.getReturnValue());
            }
            else if (state === "ERROR") {
                alert('Error : ' + JSON.stringify(errors));
            }
        });
        $A.enqueueAction(action);
        
    }
})
/**
 * Created by junzhou on 8/6/17.
 */
({
    ABNACNvalidationCheck:function(component, event, helper) {
        var result = false;
        var ABNweight = [10, 1, 3, 5, 7, 9, 11, 13, 15, 17, 19];
        var ACNweight = [8,7,6,5,4,3,2,1];

        var ABNACN = component.get('v.form.ABNACN');
        ABNACN = ABNACN.replace(/[^\d]/, '');

        if(ABNACN.length == 9) {
            var sum = 0, weight;

            for(var index = 0; index <= ACNweight.length - 1; index++) {
                weight = ACNweight[index];
                var digit = ABNACN[index];
                sum += weight * digit;
            }
            result = (10-(sum%10))%10;

            if(result == false) {
                component.set("v.errorMessage",'Invalid ABN Number');
            }
        }else if(ABNACN.length == 11) {
            var sum = 0,
                weight;

            for (var index = 0; index <= ABNweight.length - 1; index++) {
                weight = ABNweight[index];
                var digit = ABNACN[index] - (index ? 0 : 1);
                sum += weight * digit;
            }
            result = sum % 89 === 0;

            if(result == false) {
                component.set("v.errorMessage",'Invalid ABN Number');
            }
        }else {
            component.set("v.errorMessage",'Invalid ABN/ACN Number');
        }
    }
})
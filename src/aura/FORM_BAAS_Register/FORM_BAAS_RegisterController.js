/**
 * Created by junzhou on 31/5/17.
 */
({
    initBAASRegister: function(component, event, helper) {
    },

    checkedIsTrainingInterested: function(component, event, helper) {
        component.set('v.trainingInterested', component.find('trainingInterested').get('v.value'));
    },

    submit:function(component, event, helper) {
        var form = component.get('v.form');
        var valid = true;
        component.set('v.isValid', valid);
    },
})

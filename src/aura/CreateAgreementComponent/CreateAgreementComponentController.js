({
   doInit : function (component, event, helpler) {
       //populate application types
       var opts = [
            { class: "optionClass", label: "Biocertification Application", value: "Biocertification Application"},
            { class: "optionClass", label: "Stewardship Application", value: "Stewardship Application" },
            { class: "optionClass", label: "Development Application", value: "Development Application" }
           
        ];
        //component.find("InputSelectDynamic").set("v.options", opts);
       
       
       //getRecTypeIdM
       //alert("getting the recordtype map");
       var action = component.get('c.getRecTypeIdMap');
       action.setCallback(this,function(response){
            //alert(JSON.stringify(response.getReturnValue()));
            component.set('v.myMap',response.getReturnValue());
        });
        $A.enqueueAction(action);
       
       //alert('map application type ' + component.get("v.myMap"));
            //
        //alert('   getting case details   ');
        var caseId = component.get("v.parentCaseId"); 
        var action = component.get("c.getCaseDetails");
               
       //alert('getting case details  caseId ' + caseId);

        action.setParams({"caseId":caseId});
        action.setCallback(this, function(a){
          //$A.get("e.force:closeQuickAction").fire();
          var rtnValue = a.getReturnValue();
          //alert('case returned  ' + rtnValue);
          if (rtnValue !== null) {
                                  
          	component.set("v.caseRec",rtnValue);

            if(rtnValue.Status != 'Approved') {
            	component.set('v.errorMessage','Application has to be Approved before generating Agreement.');
         	}
            
              //alert('rtnValue.Application_Received_Date__c   ' + rtnValue.Application_Received_Date__c );
              if(rtnValue.Agreement__r != null){
            	component.set('v.errorMessage','Agreement was already created for this Application, an application can have only one agreement ');
              } 
              
             //alert('case returned  ' + component.get("v.caseRec"));
             component.set("v.showError",true);
          }
        });
    $A.enqueueAction(action);
    }, 
    createCaseButton : function(component, event, helper) {
        //helper.createRecord(component);
        //alert('create vvv case');

        var parentCaseId = component.get("v.parentCaseId"); 
        var createRecordEvent = $A.get("e.force:createRecord");		 
        var rectypeName = "Agreement";
                                
        //alert('create rectypeName ' + rectypeName);

        var rectypeId = component.get("v.myMap")[rectypeName];
                        
        //alert('test parent case id ' + component.get("v.caseRec").Related_Cases__c);
        //alert('test rectypeId case id ::::' + rectypeId);
		$A.get("e.force:closeQuickAction").fire();

        createRecordEvent.setParams({
            "entityApiName": "Case",
            "recordTypeId": rectypeId,
            "defaultFieldValues": {
             	"Type" : component.get("v.caseRec").Type,
             	"Related_Cases__c" : component.get("v.caseRec").Related_Cases__c,
				"Application_Type__c" : component.get("v.caseRec").Application_Type__c,
				"Application_Received_Date__c" : component.get("v.caseRec").Submitted_Date__c,
				"BAM_calculator_proposal_ID__c" : component.get("v.caseRec").Assessment_Number__c,
                "Related_Application__c" :  component.get("v.parentCaseId")
        	}
        });
        createRecordEvent.fire();

    },
     backToCase : function (component, event, helper) {
        //alert('back to case ' + component.get("v.caseId"));
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
          "recordId": component.get("v.parentCaseId"),
          "slideDevName": "related"
        });
        navEvt.fire();
	}
    
})
({
   doInit : function (component, event, helpler) {
       var opts = [
            { class: "optionClass", label: "Biocertification Application", value: "Biocertification Application"},
            { class: "optionClass", label: "Stewardship Application", value: "Stewardship Application" },
            { class: "optionClass", label: "Approval Details", value: "Development Details" }
        ];
              
       var action = component.get('c.getRecTypeIdMap');
       action.setCallback(this,function(response){
            //alert(JSON.stringify(response.getReturnValue()));
            component.set('v.myMap',response.getReturnValue());
        });
        $A.enqueueAction(action);
       
        //alert('   getting case details   ');
        var caseId = component.get("v.parentCaseId"); 
        var action = component.get("c.getCaseDetails");
               

        action.setParams({"caseId":caseId});
        action.setCallback(this, function(a){
          //$A.get("e.force:closeQuickAction").fire();
          var rtnValue = a.getReturnValue();
          if (rtnValue !== null) {
             component.set("v.caseRec",rtnValue);
             component.set("v.showError",true);
        	  console.log('   getting case details   rtnValue ' + rtnValue);
              console.log('   getting case details   rtnValue:::' + rtnValue.Child_Cases__r);
              if(rtnValue.Child_Cases__r != null){
                  component.set('v.errorMessage','Application was already created for this Case, you can have only one Application for a case');
              }
          }
        });
    $A.enqueueAction(action);

    }, 
    backToCase : function (component, event, helper) {
        //alert('back to case ' + component.get("v.caseId"));
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
          "recordId": component.get("v.parentCaseId"),
          "slideDevName": "related"
        });
        navEvt.fire();
	},
    createCaseButton : function(component, event, helper) {
        
        //helper.createRecord(component);
        var parentCaseId = component.get("v.parentCaseId"); 
        var createRecordEvent = $A.get("e.force:createRecord");	
        //alert('Parent case Type ' + component.get("v.caseRec").Type);
        var parentType = component.get("v.caseRec").Type;
        var rectypeName;
        if(parentType == 'Stewardship'){
            rectypeName = 'Stewardship Application';
        }else if(parentType == 'Development assessment case'){
            rectypeName = 'Approval Details';
        }else if(parentType == 'Clearing'){
            rectypeName = 'Clearing Application';
        }else if(parentType == 'Biocertification'){
            rectypeName = 'Biocertification Application';
        }else if(parentType == 'Activity'){
            rectypeName = 'Activity Application';
        } 
        var rectypeId = component.get("v.myMap")[rectypeName];
        var relatedCaseId;
            
        relatedCaseId = component.get("v.caseRec").Id;
        
		$A.get("e.force:closeQuickAction").fire();
              
        createRecordEvent.setParams({
            "entityApiName": "Case",
            "recordTypeId": rectypeId ,
            "defaultFieldValues": {
             	"Related_Cases__c" : relatedCaseId,
                "Type" : component.get("v.caseRec").Type,
                "Business_Unit__c": 'BOAM'
        	}
        });
        createRecordEvent.fire();
        
       
    }
    
})